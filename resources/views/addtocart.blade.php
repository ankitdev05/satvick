@extends('layouts.website')
@section('content')
<?php
$currencyCode = Session::get('currency_code');
if($currencyCode == "INR") {
    $currencyCodeLabel = "Rs";
} else {
    $currencyCodeLabel = $currencyCode;
}
?>
<section>
   <div class="BagArea">
      <div class="container">
         <div class="Baghead">
            <h1>My Bag</h1>
            <h2>@if(count($cartData) > 0) {{ count($cartData) }} items @else Cart empty @endif</h2>
         </div>
         @if(count($cartData) <= 0)
         <div class="EmptyBag">
            <img src="{{url('/')}}/public/images/Empty-bag.png">
            <h3>Your bag is empty</h3>
            <h4><i class="fa fa-plus"></i> Add something to make me happy :)</h4>
         </div>
         
         @else

         <div class="BagBody">
            <div class="row">
               <div class="col-sm-8">
                  <div class="BagsLeft">
                     <div class="Delivery">
                        <img src="{{url('/')}}/public/images/mybagitm.png">
                        <p><span>Yeh! </span> ADD {{ $currencyCodeLabel }}  500 item more, to get your DELIVERY FREE</p>
                     </div>
                     <div class="BagOrder">
                        <article>
                        @foreach($cartData as $cart) 
                           <aside>
                              <!--<h1>Order no |  <span>112044-334455-10</span></h1>-->
                              <figure>
                                 <img src="{{ $cart['productimage'] }}">
                              </figure>
                              <figcaption>
                                 <h2>{{ $cart['name'] }}</h2>
                                 <h3>{{ $cart['brand'] }}</h3>
                                 <h4> @if(strlen($cart['size'])>0) {{ $cart['size_label'] }}: {{ $cart['size'] }} | @endif @if(strlen($cart['color'])>0) color: {{ $cart['color'] }} | @endif  Qty: {{ $cart['qty'] }} </h4>
                                 <h5>{{ $currencyCodeLabel }} {{ $cart['sp'] }} @if($cart['percentage'] != 0) <del>{{ $currencyCodeLabel }} {{ $cart['mrp'] }}</del> <span>Saved {{ $cart['percentage'] }}%</span> @endif </h5>
                                 <h6><span>Sold By:</span> {{ $cart['seller'] }}</h6>
                              </figcaption>
                              <div class="Spinner">
                                 <div class="input-group number-spinner spin">
                                    <span class="input-group-btn data-dwn">
                                        <button data-dir="dwn" class="{{$cart['cartid']}}"><span class="glyphicon glyphicon-minus"></span></button>
                                    </span>
                                    
                                    <input type="text" id="changequantity" class="form-control text-center" value="{{ $cart['qty'] }}" min="0">

                                    <span class="input-group-btn data-up">
                                        <button data-dir="up" class="{{$cart['cartid']}}"><span class="glyphicon glyphicon-plus"></span></button>
                                    </span>
                                 </div>
                                  <p>
                                    @if(Auth::check())
                                        <a href="{{ route('add-wishlist', $cart['proid']) }}">Move to wishlist</a>
                                    @else
                                        <a href="javascript:void(0)" data-toggle="modal" data-target="#Login">Move to wishlist</a>
                                    @endif
                                    <a href="{{ route('itemremove', $cart['cartid']) }}">remove</a>
                                  </p>
                              </div>
                           </aside>

                        @endforeach

                        </article>

                        <div class="Links addtocart_webview">
                            <a href="{{ route('home') }}">Continue shopping</a>
                            @if(Auth::check())
                                <a href="{{ route('place-order') }}">Place order</a>
                            @else
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#Login">Place order</a>
                            @endif
                        </div>
                     </div>
                  </div>
               </div>
               <div class="col-sm-4">
                  <div class="BagsRight">
                     <aside>
                        <h4>price Details</h4>
                     </aside>
                     <article>
                        <h4>Price ({{ count($cartData) }} item) <span>Rs {{ $paying['amount'] }} </span></h4>
                        <!--<h4>delivery Charges <span>Rs 00</span></h4>-->
                        <h5>Amount payable <span>Rs {{ $paying['payable'] }}</span></h5>
                     </article>
                     <div class="Saved">
                        <p>Your total savings on this order Rs {{ $paying['discount'] }}</p>
                     </div>
                  </div>
               </div>
            </div>

            <div class="row addtocart_mobileview">
               <div class="col-sm-12">
                  <div class="BagsLeft">
                     <div class="BagOrder">
                        <div class="Links">
                            <a href="{{ route('home') }}">Continue shopping</a>
                            @if(Auth::check())
                                <a href="{{ route('place-order') }}">Place order</a>
                            @else
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#Login">Place order</a>
                            @endif
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         @endif
      </div>
   </div>
</section>
@endsection

