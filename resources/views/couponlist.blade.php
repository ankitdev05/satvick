@extends('layouts.website')

@section('content')

    <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                <li ><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li> 
                                <li class="active"><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li> 
                                 <li ><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="UserDashboard">
                        @if(count($return_array) > 0)
                        @foreach($return_array as $coupon)

                            <div class="CouponBox">
                                <aside>
                                    <p>{{ $coupon['discount'] }} <span>@if($coupon['discount_type'] == "price") Off @else % @endif</span></p>
                                </aside>
                                <figcaption>
                                    <p>On minimum purchase of Rs.{{ $coupon['min_price'] }}</p>
                                    <h6><span>Expiry :</span> {{ $coupon['expiry_date'] }} </h6>
                                </figcaption>
                                <article>
                                    <p> {{ $coupon['cupon_code'] }} <span></span></p>
                                </article>
                            </div>

                        @endforeach
                        @endif            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection