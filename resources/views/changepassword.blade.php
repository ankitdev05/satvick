@extends('layouts.website')

@section('content')
<style type="text/css">
    span.formerror {
    float: left;
    width: 100%;
    font-size: 10px;
    color: red;
    margin: 3px auto;
}
</style>
  <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                        @if(Session::has('message_forgot'))
                            <p class="alert alert-info">{{ Session::get('message_forgot') }}</p>
                        @endif
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                <li ><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li>
                                <li><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li>
                                 <li ><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li class="active"><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="UserDashboard">

                            <div class="PasswordArea">
                                <div class="row">
                                    <div class="col-sm-7">
                                        
                                        <div>
                        @if (Session::has('success'))

                        <div class="alert alert-success" role="alert">
                           <strong>Success:</strong> {{ Session::get('success') }}
                        </div>

                        @endif
                        @if (Session::has('error'))

                        <div class="alert alert-danger" role="alert">
                           <strong>Error:</strong> {{ Session::get('error') }}
                        </div>

                        @endif
                     </div>
                     
                                        <form action="{{ route('update-password') }}" method="post">
                                            @csrf
                                            <h1>Changes password</h1>
                                            <div class="form-group">
                                                <label>Enter old password</label>
                                                <input type="password" name="oldpassword" class="form-control" placeholder="xxxxxxxxxx">
                                                <span class="formerror">
                                    
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label>New password</label>
                                                <input type="password" name="password" class="form-control" placeholder="xxxxxxxxxx">
                                                <span class="formerror">
                                    
                                                </span>
                                            </div>
                                            <div class="form-group">
                                                <label>Re-enter New password</label>
                                                <input type="password" name="confirm_password" class="form-control" placeholder="xxxxxxxxxx">
                                                 <span class="formerror">
                                                 </span>
                                            </div>
                                            <button>Submit</button>
                                        </form>
                                    </div>

                                    <div class="col-sm-5">
                                        <div class="Description">
                                            <h1>Description</h1>
                                            <p>Changes your password. we highly recommend you create a strong one.</p>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection