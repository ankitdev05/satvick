@extends('layouts.website')

@section('content')


	<section>
        <div class="DiscountArea">
            <div class="container">
                <div class="DiscountBox" style="background-image: url(images/Women.png);">
                    <h1>GET 10 % EXTRA DISCOUNT</h1>
                    <h2>USE CODE: <span>WELCOME 10</span></h2>

                    <div class="row">
                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">
                          <img src="{{url('/')}}/public/images/sports/sport-tshirt.png">
                          <p>SPORT TSHIRTS</p>
                          </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">
                             <img src="{{url('/')}}/public/images/sports/sports.png">
                          <p>SPORT</p>
                          </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">
                             <img src="{{url('/')}}/public/images/sports/Cricket-Jersey.png">
                          <p>CRICKET JERSEY</p>
                          </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">
                            <img src="{{url('/')}}/public/images/sports/sports-shirts.png">
                          <p>SPORTS</p>
                          </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">
                           <img src="{{url('/')}}/public/images/sports/football-shose.png">
                          <p>FOOTBALL SHOES</p>
                          </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">
                            <img src="{{url('/')}}/public/images/sports/footbal.png">
                          <p>FOOTBALL</p>
                          </a>
                            </div>
                        </div>
                    </div>

                    <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category" class="view">VIEW ALL</a> 

                </div>
            </div>
        </div>
    </section>

     

  <div class="flas">
     <h1><img src="{{url('/')}}/public/images/flash.png"> FLASH SALE</h1>
     <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW All</a>
</div>
<div class="sales sports">
 <div class="container">
  <div class="row">
        <div class="col-md-3 col-xs-12">
                  <div class="hot">
            <img src="{{url('/')}}/public/images/sports/FLASH-SALE1.png">
            <span>48%<br>OFF</span>
              <div class="triangle-left">
              
              </div>
              <div class="triangle-right">
              
              </div>
        </div>  
           <div class="cross">
            <p>Rs.809</p>
             <strong>Rs.1799</strong>
                  </div>
      </div>    
         <div class="col-md-3 col-xs-12">
                  <div class="hot">
           <img src="{{url('/')}}/public/images/sports/FLASH-SALE2.png">
            <span>48%<br>OFF</span>
              <div class="triangle-left">
              
              </div>
              <div class="triangle-right">
              
              </div>
        </div>  
         <div class="cross">
            <p>Rs.809</p>
             <strong>Rs.1799</strong>
                  </div>
      </div>  
         <div class="col-md-3 col-xs-12">
                  <div class="hot">
           <img src="{{url('/')}}/public/images/sports/FLASH-SALE3.png">
            <span>48%<br>OFF</span>
              <div class="triangle-left">
              
              </div>
              <div class="triangle-right">
              
              </div>
        </div>  
         <div class="cross">
            <p>Rs.809</p>
             <strong>Rs.1799</strong>
                  </div>
      </div>  
         <div class="col-md-3 col-xs-12">
                  <div class="hot">
           <img src="{{url('/')}}/public/images/sports/FLASH-SALE4.png">
            <span>48%<br>OFF</span>
              <div class="triangle-left">
              
              </div>
              <div class="triangle-right">
              
              </div>
        </div>  
         <div class="cross">
            <p>Rs.809</p>
             <strong>Rs.1799</strong>
                  </div>
      </div>  
     </div>
      </div>  </div>
      <div class="shopby sports">
                  <div class="container">
  <div class="row">
                         <div class="col-md-12">
                         <h2 class="by">BADMINTON</h2>
                         </div> 
                         <div class="bor">
               <div class="col-md-3 col-xs-12">
                                  <div class="hots">
                           <img src="{{url('/')}}/public/images/sports/Badminton.png">
                              <div class="cross">
                                 <p>GYM COLLECTION</p>
                                 
                                      </div>
                                    </div>
                            </div>  
                        <div class="col-md-3 col-xs-12">
                         <div class="hots">
                    <img src="{{url('/')}}/public/images/sports/Badminton2.png">
              <div class="cross">
                 <p>NYLON SHUTTLE</p>
                
                       </div>
                        </div>
                            </div>  
                        <div class="col-md-3 col-xs-12">
                         <div class="hots">
                    <img src="{{url('/')}}/public/images/sports/Badminton3.png">
              <div class="cross">
                 <p>BADMINTON KIT</p>
                
                       </div>
                        </div>
                            </div>  
        <div class="col-md-3 col-xs-12">
                         <div class="hots">
                    <img src="{{url('/')}}/public/images/sports/Badminton4.png">
              <div class="cross">
                 <p>BADMINTON STRING</p>
              
                       </div>
                        </div>
                            </div> 
                            </div>
        </div>  

  
    </div>  
 </div>     
 <div class="rights">
         <div id="owl-demo3" class="owl-carousel owl-theme">
            <div class="item">
               
              <div class="watch">
              <img src="{{url('/')}}/public/images/new/img012.png">
              </div>          
         </div>

         <div class="owl-nav disabled"><button type="button" role="presentation" class="owl-prev"><span aria-label="Previous">‹</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">›</span></button></div>
                   
     </div>  
     <div class="absu">
  <h4>BUY 4 ITEMS </br>FOR    1599</h4>
  <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">LIMITED OFFER<span aria-label="Next">›</span></a>
</div>    
  </div>


 <div class="col-md-12 col-xs-12">
     <div class="seealls">
     <h4>FITNESS ACCESSORIES</h4>
       <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW ALL</a>
   </div>
 </div>
<div class="lefts">
     
         <div id="owl-demo4" class="owl-carousel owl-theme owl-loaded owl-drag">
    
  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1449px;"><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
               
              <div class="pro">
               <img src="{{url('/')}}/public/images/sports/fitness-accessories1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                                 <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>    
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
              
              <div class="pro">
               <img src="{{url('/')}}/public/images/sports/fitness-accessories2.png">
              </div>
                       <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
               <img src="{{url('/')}}/public/images/sports/fitness-accessories3.png">
              </div>
              <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                               
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
                   </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
               <img src="{{url('/')}}/public/images/sports/fitness-accessories4.png">
              </div>
            <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                    <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item" style="width: 279.75px; margin-right: 10px;"><div class="item">
                    
              <div class="pro">
               <img src="{{url('/')}}/public/images/sports/fitness-accessories1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                      </div>

                             <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>

         </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">&#8249;</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">&#8250;</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
 
</div>

<div class="col-md-12 col-xs-12">
     <div class="seealls">
     <h4>SPORT T-SHIRT</h4>
       <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW ALL</a>
   </div>
 </div>
<div class="loots">
     
         <div id="owl-demo5" class="owl-carousel owl-theme owl-loaded owl-drag">
    
  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1449px;"><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
               
              <div class="pro">
               <img src="{{url('/')}}/public/images/sports/SPORTS-T-shirt1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                                 <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>    
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
              
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/SPORTS-T-shirt2.png">
              </div>
                       <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
            <img src="{{url('/')}}/public/images/sports/SPORTS-T-shirt3.png">
              </div>
              <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                               
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
                   </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/SPORTS-T-shirt4.png">
              </div>
            <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                    <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item" style="width: 279.75px; margin-right: 10px;"><div class="item">
                    
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/SPORTS-T-shirt1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                      </div>

                             <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>

         </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">&#8249;</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">&#8250;</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
 
</div>
<div class="col-md-12 col-xs-12">
     <div class="seealls">
     <h4>TRACK PANTS</h4>
       <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW ALL</a>
   </div>
 </div>
<div class="loot">
     
         <div id="owl-demo6" class="owl-carousel owl-theme owl-loaded owl-drag">
    
  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1449px;"><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
               
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/TRACK-PANTS1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                                 <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>    
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
              
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/TRACK-PANTS2.png">
              </div>
                       <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/TRACK-PANTS3.png">
              </div>
              <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                               
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
                   </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/TRACK-PANTS4.png">
              </div>
            <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                    <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item" style="width: 279.75px; margin-right: 10px;"><div class="item">
                    
              <div class="pro">
            <img src="{{url('/')}}/public/images/sports/TRACK-PANTS1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                      </div>

                             <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>

         </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">&#8249;</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">&#8250;</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
 
</div>
<div class="col-md-12 col-xs-12">
     <div class="seealls">
     <h4>SPORTS SHOES</h4>
       <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW ALL</a>
   </div>
 </div>
<div class="loop">
     
         <div id="owl-demo7" class="owl-carousel owl-theme owl-loaded owl-drag">
    
  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1449px;"><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
               
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Shoes1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                                 <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>    
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
              
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Shoes2.png">
              </div>
                       <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Shoes3.png">
              </div>
              <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                               
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
                   </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Shoes4.png">
              </div>
            <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                    <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item" style="width: 279.75px; margin-right: 10px;"><div class="item">
                    
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Shoes1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                      </div>

                             <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>

         </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">&#8249;</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">&#8250;</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
 
</div>
<div class="col-md-12 col-xs-12">
     <div class="seealls">
     <h4>SPORTS JACKETS</h4>
       <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW ALL</a>
   </div>
 </div>
<div class="les">
     
         <div id="owl-demo8" class="owl-carousel owl-theme owl-loaded owl-drag">
    
  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1449px;"><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
               
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Jackets1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                                 <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>    
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
              
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Jackets2.png">
              </div>
                       <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Jackets3.png">
              </div>
              <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                               
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
                   </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Jackets4.png">
              </div>
            <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                    <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item" style="width: 279.75px; margin-right: 10px;"><div class="item">
                    
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/Sports-Jackets1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                      </div>

                             <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>

         </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">&#8249;</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">&#8250;</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
 
</div>
<div class="col-md-12 col-xs-12">
     <div class="seealls">
     <h4>KIDS ACCESSORIES</h4>
       <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">VIEW ALL</a>
   </div>
 </div>
<div class="lef">
     
         <div id="owl-demo9" class="owl-carousel owl-theme owl-loaded owl-drag">
    
  <div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(0px, 0px, 0px); transition: all 0s ease 0s; width: 1449px;"><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
               
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/kids-accessories1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                                 <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>    
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
              
              <div class="pro">
            <img src="{{url('/')}}/public/images/sports/kids-accessories2.png">
              </div>
                       <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/kids-accessories3.png">
              </div>
              <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                               
                                      </div>
                                       <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
                   </div></div><div class="owl-item active" style="width: 279.75px; margin-right: 10px;"><div class="item">
                
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/kids-accessories4.png">
              </div>
            <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                 
                                      </div>
                    <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>
         </div></div><div class="owl-item" style="width: 279.75px; margin-right: 10px;"><div class="item">
                    
              <div class="pro">
              <img src="{{url('/')}}/public/images/sports/kids-accessories1.png">
              </div>
                        <div class="cr">
                                 <p>levis</p>
                                 <span>Men slim fit Jeans</span>
                                      </div>

                             <div class="crop">
                                    <p>Rs.809</p>
                                  <span>Rs.1799 </span>
                                  <strong>  (55%OFF)</strong>
                                          </div>

         </div></div></div></div><div class="owl-nav"><button type="button" role="presentation" class="owl-prev disabled"><span aria-label="Previous">&#8249;</span></button><button type="button" role="presentation" class="owl-next"><span aria-label="Next">&#8250;</span></button></div><div class="owl-dots"><button role="button" class="owl-dot active"><span></span></button><button role="button" class="owl-dot"><span></span></button></div></div>
 
</div>


<div class="left">
      <div class="space-60">
      <h2>FASHION EDIT</h2>
      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
    </div>
       <div class="ext">
          <div class="col-md-4 col-xs-12">
              <h4>BUY 1 GET 1</h4>
              <h1>EXTRA 50% OFF</h1>
              <a href="{{url('/')}}/productlisting?filter_data=SPORTS&filter_type=Category">SHOP NOW</a>
          </div>
          <div class="col-md-8 col-xs-12">
              <div class="col-md-3 col-xs-12">
                <img src="{{url('/')}}/public/images/sports/casual-wear.png">
                 <p>casual wear</p>
              </div>
              <div class="col-md-3 col-xs-12">
              <div class="gei"></div>
                <img src="{{url('/')}}/public/images/sports/CLOTHS.png">
                 <p>casual wear</p>
              </div>
              <div class="col-md-3 col-xs-12">
                <img src="{{url('/')}}/public/images/sports/denim-shirts.png">
                 <p>casual wear</p>
              </div>
              <div class="col-md-3 col-xs-12">
               <div class="gei"></div>
                 <img src="{{url('/')}}/public/images/sports/tops.png">
                 <p>casual wear</p>
              </div>
          </div>
       </div>
</div>

<div class="shopby">
           <div class="container">
              <div class="row">
                        
                         <div class="bors">
               <div class="col-md-6 col-xs-12">
                                                         
                                  <div class="col-md-4 col-xs-6">
                                  <div class="case">
                                       <img src="{{url('/')}}/public/images/sports/Footer1.png">
                                  </div>     
                                  </div> 

                                  <div class="col-md-8 col-xs-6">
                                          <div class="col-md-12 col-xs-12">
                                            <h5>SPORTS CLOTHING</h5>
                                  </div> 
                                         <div class="col-md-6 col-xs-12">
                                          <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>
                                 
                                         </div>
                                         <div class="col-md-6 col-xs-12">
                                              <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>

                                         </div>
                                  </div>

                              </div> 
                        <div class="col-md-6 col-xs-12">
                                  <div class="col-md-4 col-xs-6">
                                  <div class="case">
                                       <img src="{{url('/')}}/public/images/sports/Footer2.png">
                                   </div>    
                                  </div> 

                                  <div class="col-md-8 col-xs-6">
                                          <div class="col-md-12 col-xs-12">
                                            <h5>SPORTS SPORTS</h5>
                                  </div> 
                                         <div class="col-md-6 col-xs-12">
                                            <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>
                                         </div>
                                         <div class="col-md-6 col-xs-12">
                                                <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>
                                         </div>
                                  </div>
                            </div>  
               
         </div> 
    </div>  
 </div>

<div class="shopbysys">
           <div class="container">
              <div class="row">
                        
                         <div class="bors">
               <div class="col-md-6 col-xs-12">
                                                         
                                  <div class="col-md-4 col-xs-6">
                                    <div class="case">
                                       <img src="{{url('/')}}/public/images/sports/Footer3.png">
                                    </div>   
                                  </div> 

                                  <div class="col-md-8 col-xs-6">
                                          <div class="col-md-12 col-xs-12">
                                            <h5>SPORTS SHOES</h5>
                                  </div> 
                                         <div class="col-md-6 col-xs-12">
                                               <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>
                                         </div>
                                         <div class="col-md-6 col-xs-12">
                                               <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>

                                         </div>
                                  </div>

                              </div> 
                        <div class="col-md-6 col-xs-12">
                                  <div class="col-md-4 col-xs-6">
                                  <div class="case">
                                     <img src="{{url('/')}}/public/images/sports/Footer4.png">
                                      </div>
                                  </div> 

                                  <div class="col-md-8 col-xs-6">
                                          <div class="col-md-12 col-xs-12">
                                            <h5>EHHNIC WEAR</h5>
                                  </div> 
                                         <div class="col-md-6 col-xs-12">
                                              <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>
                                         </div>
                                         <div class="col-md-6 col-xs-12">
                                             <a>KURTA PAJAMA</a>
                                             <a>CASUAL SHIRTS</a>
                                             <a>DENIM JEANS</a>
                                             <a>NEHRU JACKET</a>

                                         </div>
                                  </div>
                            </div>  
               
         </div> </div></div>
    </div>  
 </div>

@endsection