@if(count($return_array) <= 0)
    <p style='color:black;'>No Record Found!</p>
@else

    @foreach ($return_array as  $value)
        <div class="col-md-3 col-xs-12">
            <a href="{{ route('product/description/', base64_encode($value['product_id'])) }}">
                <div class="lis">
                    <figure><img src="{{$value['image']}}"></figure>
                    <p>{{$value['brand']}}</p>
                    <strong>{{$value['name']}}</strong>
                    <div class="cros">
                        <p>Rs.{{$value['sp']}}</p>
                        @if($value['percentage'] != 0)
                        <span>Rs.{{$value['mrp']}} </span>
                        <strong>  ({{$value['percentage']}}%OFF)</strong>
                        @endif
                    </div>
                </div>
            </a>
        </div>
    @endforeach
@endif