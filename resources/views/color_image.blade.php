  <ol class="carousel-indicators">
                        @foreach ($return_array as $key1=>$value)
                            <li data-target="#ProductSlide" data-slide-to="{{$key1}}" @if(trim($key1) == 0) class="active" @endif></li>
                        @endforeach
                     </ol>
 <div class="carousel-inner">
                        @foreach ($return_array as $key1=>$value)
                             
                            <div class="item @if(trim($key1) == 0) active @endif">
                            	<?php $url = url('public/').$value ?>
                               <img src="{{$url}}">
                            </div>
                        @endforeach
                     </div>