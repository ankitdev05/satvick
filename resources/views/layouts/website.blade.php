<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <title>Satvick Life</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
   @include('includes.head')
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
    

<script>
    $(document).ready(function() { 

        if(localStorage.getItem('lastactivity')) {

            var date1 = localStorage.getItem('lastactivity');
            var date2 = Date.now();
            if (date1 !== undefined) {
                  let difference = date2 - date1;
                  let diff = ((difference) / 1000).toFixed(0);
                
                if (diff > 500) {
                    localStorage.clear();

                    if ("geolocation" in navigator) {

                        navigator.geolocation.getCurrentPosition(function(position) {

                            var currentLatitude = position.coords.latitude;
                            var currentLongitude = position.coords.longitude;

                            var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                            var geocoder = geocoder = new google.maps.Geocoder();
                            geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                                if (status == google.maps.GeocoderStatus.OK) {
                                    
                                    if (results[9]) {

                                        var date = Date.now();
                                        localStorage.setItem("lastactivity", date);

                                        var getLength = results.length - 1;
                                        var getLength1 = results[getLength].address_components.length - 1;
                                        var country_code = results[getLength].address_components[getLength1].short_name;

                                        var flagurl = "https://www.countryflags.io/"+country_code+"/flat/64.png";
                                        localStorage.setItem("getflag", flagurl);

                                        var getCurrency = "https://restcountries.eu/rest/v1/alpha/"+country_code;
                                        jQuery.getJSON(getCurrency, function (data) {
                                                
                                                var currency = data.currencies[0];
                                                $.ajax({
                                                    type: "POST",
                                                    url: "{{route('change-rate')}}",
                                                    data: {currency:currency,_token:'{{csrf_token()}}'},
                                                    success: function(response) {
                                                        //console.log(response);
                                                        location.reload();
                                                    },
                                                    error: function(xhr, status, error) {
                                                      var err = eval("(" + xhr.responseText + ")");
                                                      console.log(err.Message);
                                                      location.reload();
                                                    }
                                                });
                                            }
                                        );
                                    }
                                }
                            });
                        },showError);
                    }

                }
            }

        } else {
        
            if ("geolocation" in navigator) {

                navigator.geolocation.getCurrentPosition(function(position) {

                    var currentLatitude = position.coords.latitude;
                    var currentLongitude = position.coords.longitude;

                    var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
                    var geocoder = geocoder = new google.maps.Geocoder();
                    geocoder.geocode({ 'latLng': latlng }, function (results, status) {

                        if (status == google.maps.GeocoderStatus.OK) {
                            
                            if (results[9]) {

                                var date = Date.now();
                                localStorage.setItem("lastactivity", date);

                                var getLength = results.length - 1;
                                var getLength1 = results[getLength].address_components.length - 1;
                                var country_code = results[getLength].address_components[getLength1].short_name;
                                var getCurrency = "https://restcountries.eu/rest/v1/alpha/"+country_code;

                                var flagurl = "https://www.countryflags.io/"+country_code+"/flat/64.png";
                                localStorage.setItem("getflag", flagurl);

                                jQuery.getJSON(getCurrency, function (data) {
                                        
                                        var currency = data.currencies[0];
                                        $.ajax({
                                            type: "POST",
                                            url: "{{route('change-rate')}}",
                                            data: {currency:currency,_token:'{{csrf_token()}}'},
                                            success: function(response) {
                                                //console.log(response);
                                                location.reload();
                                            },
                                            error: function(xhr, status, error) {
                                              var err = eval("(" + xhr.responseText + ")");
                                              //console.log(err.Message);
                                              location.reload();
                                            }
                                        });
                                    }
                                );
                            }
                        }
                    });
                },showError);
            }
        }

        function showError(error) {
            switch(error.code) {
                case error.PERMISSION_DENIED:
                    $('#gpsModal').modal('show');
                    break;
                case error.POSITION_UNAVAILABLE:
                    alert("Location information is unavailable.")
                    break;
                case error.TIMEOUT:
                    alert("The request to get user location timed out.")
                    break;
                case error.UNKNOWN_ERROR:
                    alert("An unknown error occurred.")
                    break;
            }
        }
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD4DT_GCr9NRwa59751r3XgB6TDPiLxq5s&libraries=places"
></script>

</head>

<body>

    <header>
        @include('includes.header')
       
    </header>

     @yield('content')


    <footer>
    	 @include('includes.footer')

    </footer>

    <script src="{{URL('/')}}/public/js/owl.carousel.min.js"></script>
    <script src="{{URL('/')}}/public/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        const countdown = document.getElementById('my-countdown');
        const endDate = new Date(countdown.dataset.endDate);
        const endTimestamp = endDate.getTime();

        window.setInterval(() => {
            let nowTimestamp = Date.now();
            let deltaT = Math.abs(endTimestamp - nowTimestamp) / 1000;

            let days = 0;
            let hours = 0;
            let minutes = 0;
            let seconds = 0;

            if (nowTimestamp < endTimestamp) {
                days = Math.floor(deltaT / 86400);
                deltaT -= days * 86400;
                hours = Math.floor(deltaT / 3600) % 24;
                deltaT -= hours * 3600;
                minutes = Math.floor(deltaT / 60) % 60;
                deltaT -= minutes * 60;
                seconds = Math.floor(deltaT % 60);
            }

            countdown.children[0].innerHTML = days;
            countdown.children[1].innerHTML = hours;
            countdown.children[2].innerHTML = minutes;
            countdown.children[3].innerHTML = seconds;
        }, 1000);
    </script>

    <script>
        jQuery("#Watch").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoplay: true,
            // autoPlayTime: 3000,
            items: 1,

        });
    </script>

    

    <script>
        jQuery("#owl-demo").owlCarousel({
            navigation: true,
            loop: false,
            margin: 10,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
     <script>
        jQuery("#HotDeal").owlCarousel({
            navigation: true,
            loop: false,
            margin: 10,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
    
    <script>
        jQuery("#owl-demo1").owlCarousel({
            navigation: true,
            loop: false,
            margin: 10,
            autoPlay: 1000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
    <script>
        jQuery("#owl-demo2").owlCarousel({
            navigation: true,
            loop: false,
            margin: 10,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
    <script>
        jQuery("#owl-demo3").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 3000,
            items: 1,

        });
    </script>
    <script>
        jQuery("#owl-demo4").owlCarousel({
            navigation: true,
            loop: true,
            margin: 15,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>

      <script>
        jQuery("#Ethnic, #jumpsuits, #lastest_top, #Footwear, #Watches, #Stylish_Tops").owlCarousel({
            nav: true,  
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"], 
            loop: false,
            margin: 10,
            autoPlay: 3000,
            dots:false,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        }); 
    </script>

    <script>
        $('.nav__trigger', ).on('click', function(e) {
            e.preventDefault();
            $(this).parent().toggleClass('nav--active').show();
            $('body').toggleClass('no-scroll');
        });
        $('.nav li, .nav').on('click', function() {
            $(".nav__trigger").parent().removeClass("nav--active");
            $(".nav__icon").removeClass("nav--active");
            $('body').removeClass('no-scroll');
        });
    </script>

    <script type="text/javascript">
        // var path = "{{-- route('autocomplete') --}}";
        // $('input.typeahead').typeahead({
        //     source:  function (query, process) {

        //     return $.get(path, { key: query }, function (data) {
        //             return process(data);
        //         });
        //     }
        // });

        $(document).ready(function(){
            $("#search-box").keyup(function() {
                var key = $(this).val();
                if(key.length >= 2) {
                    $.ajax({
                        type: "GET",
                        url: "{{ route('autocomplete') }}",
                        data:'key='+$(this).val(),
                        beforeSend: function(){
                            $("#search-box").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data) {
                            //console.log(data);
                            $("#suggesstion-box").show();
                                var content = '';
                                
                                $.each(data, function(index, value) {
                                    
                                    content += '<span> <a href="{{ url("product/search/") }}/'+value.catid +'/'+ value.subsubcatid +'/'+ value.brand +'">' + value.name + '</a></span>';
                                });

                            $("#suggesstion-box").html(content);
                            $("#search-box").css("background","#FFF");
                        }
                    });
                } else {
                    $("#suggesstion-box").hide();
                }
            });
        });

        $(document).ready(function(){
            $("#search-box1").keyup(function() {
                var key = $(this).val();
                if(key.length >= 2) {
                    $.ajax({
                        type: "GET",
                        url: "{{ route('autocomplete') }}",
                        data:'key='+$(this).val(),
                        beforeSend: function(){
                            $("#search-box1").css("background","#FFF url(LoaderIcon.gif) no-repeat 165px");
                        },
                        success: function(data) {
                            //console.log(data);
                            $("#suggesstion-box1").show();
                                var content = '';
                                
                                $.each(data, function(index, value) {
                                    
                                    content += '<span> <a href="{{ url("product/search/") }}/'+value.catid +'/'+ value.subsubcatid +'/'+ value.brand +'">' + value.name + '</a></span>';
                                });

                            $("#suggesstion-box1").html(content);
                            $("#search-box1").css("background","#FFF");
                        }
                    });
                } else {
                    $("#suggesstion-box1").hide();
                }
            });
        });
        $(document).ready(function () {
            $('#Closenav').on('click', function() {
                $("#suggesstion-box1").hide();
            });
        });
        //To select country name
        function selectCountry(val) {
            $("#search-box").val(val);
            $("#suggesstion-box").hide();
        }
        function selectCountry(val) {
            $("#search-box1").val(val);
            $("#suggesstion-box1").hide();
        }
    </script> 
    @stack('script')   

</body>
</html>