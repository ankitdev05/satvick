<!doctype html>
<html class="no-js" lang="zxx">

<head>
    <meta charset="utf-8">
    <title>@yield('title') | Yod</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
   @include('includes.head')
   <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
   <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

</head>

<body>

   

     @yield('content')






  




<!-- 

    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-xs-12">
                    <div class="link">
                        <p>DOWNLOAD THE APP</p>
                        <div class="col-md-12 col-xs-6">
                            <div class="ANDROID.png">
                                <img src="images/ANDRIOD.png">
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-6">
                            <div class="apple">
                                <img src="images/APPLE.png">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="link">
                        <p>YARD OF DEALS</p>
                        <span><a href="#">Help / Support</a></span>
                        <span><a href="#"></a></span>
                        <span><a href="#">About Us </a></span>
                        <span><a href="#">Careers</a></span>
                        <span><a href="#">Gift Cards</a></span>
                        <span><a href="#">FAQs</a></span>
                        <span><a href="#">Refer $ Earn</a></span>
                        <span><a href="#">Terms and Condition Offers</a></span>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="link">
                        <p>OUR POLICIES</p>
                        <span><a href="#">Terms and Condition </a></span>
                        <span><a href="#">Privacy Policy </a></span>
                        <span><a href="#">Vendor Code of Conduct </a></span>
                        <span><a href="#">Whitehat </a></span>
                        <span><a href="#">Deal of The Day </a></span>
                    </div>
                </div>
                <div class="col-md-3 col-xs-12">
                    <div class="link">
                        <p>SUBSCRIBE TO NEWSLETTERS</p>
                        <input type="search" placeholder="YOUR MAIL ID">
                        <div class="arrow">
                            <span aria-label="Next">›</span>
                        </div>
                    </div>
                    <div class="sub">
                        <a>SUBSCRIBE</a>
                    </div>
                    <div class="share">
                        <ul>
                            <li>
                                <a href=""><img src="images/facebook-logo.png"></a>
                            </li>
                            <li>
                                <a href=""><img src="images/twitter.png"></a>
                            </li>
                            <li>
                                <a href=""><img src="images/google-plus-logo.png"></a>
                            </li>
                            <li>
                                <a href=""><img src="images/youtube.png"></a>
                            </li>
                            <li>
                                <a href=""><img src="images/instagram.png"></a>
                            </li>
                            <li>
                                <a href=""><img src="images/pinterest.png"></a>
                            </li>
                        </ul>

                    </div>

                </div>
            </div>

            <div class="on">
                <div class="col-md-4 col-xs-12">
                    <div class="recyle">
                        <img src="images/rupee1.png">
                        <p>CASE ON DELIVERY</p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="recyle">
                        <img src="images/return.png">
                        <p>15 DAYS RETURNS </p>
                    </div>
                </div>
                <div class="col-md-4 col-xs-12">
                    <div class="recyle">
                        <img src="images/free.png">
                        <p>FREE SGIPPING</p>
                    </div>
                </div>
            </div>
            <div class="turms">
                <div class="container">
                    <div class="row">
                        <div class="one">
                            <h3>ONLINE FASHION SHOPPING: OCNVENIENT, FAST AND AFFORDABLE!</h3>
                            <p>Shopping is fun and exhilarating and more so when you can shop 24x7 without leaving the comfort of home. This in simpler words is what we call YARDOFDEALS.com! Online shopping at YARDOFDEALS.com is convenient and affordable. You can find your desired products more quickly and easily using our user-friendly online shopping platform. Fill your cart up to the brim in just a few seconds and get swift home delivery for all orders. All of this topped with our exclusive offers makes for an exciting, irresistible combo!</p>
                        </div>
                        <div class="two">
                            <h3>YARDOFDEALS.com: the hottest online fashion destination of all times!</h3>
                            <p>‘Fashion’ is definitely more accessible with yARDOFDEALS.com. We showcase the chicest of products be it clothing, shoes, jewellery, accessories and cosmetics. brands that were only heard of but never found online before are now showcased at yARDOFDEALS.com. yARDOFDEALS.com is here to make dreams come true for all River Island and Mango lovers. Dorothy Perkins and Miss Selfridge are some other international labels that you will find here. If your wardrobe had been craving for a designer ethnic collection, then you can feast your eyes on Rohit Bal for yARDOFDEALS.com. Also, take a tour of our fashion blog to stay abreast of the latest runway trends and be a trendsetter among your immediate circles.</p>
                        </div>
                        <div class="three">
                            <h3>Avail added online shopping benefits</h3>
                            <p>You choose your product, order it online, and we deliver it right at your doorstep anywhere in India. You just need to pay for the product, while we ensure free shipping* all the time on almost everything with no strings attached. For any second thoughts after purchase, we have in place a no questions asked return policy as well. To offer you a safe and risk-free online shopping experience, we offer COD facility. Could you have asked for more?</p>
                        </div>
                        <div class="four">
                            <h3>YARDOFDEALS.com: the 24 x7 Online Fashion &amp; Lifestyle Store for everyone</h3>
                            <p>Forget the fashion streets of the world. We, at yARDOFDEALS.com, have all that you need to glam up your fashion quotient. From an extensive range of men’s shirts, western dresses for women, funky clothes for kids and matching footwear, sports shoes and accessories for everyone, we purvey diversity of choices in online shopping in India under one umbrella. Your yARDOFDEALS Online Shop has arrived! Do not miss the attractive best buy prices and super discount coupons. Get more, pay less!</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="policy">
                <div class="container">
                    <div class="row">
                        <p>YARDOFDEALS.COM ©2019</p>
                        <img src="images/dot.png">
                        <span>ALL RIGHTS RESERVED</span>
                    </div>
                </div>
            </div>
        </div>
    </div>

 -->

   
    <script src="public/js/owl.carousel.min.js"></script>
    <script src="public/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        const countdown = document.getElementById('my-countdown');
        const endDate = new Date(countdown.dataset.endDate);
        const endTimestamp = endDate.getTime();

        window.setInterval(() => {
            let nowTimestamp = Date.now();
            let deltaT = Math.abs(endTimestamp - nowTimestamp) / 1000;

            let days = 0;
            let hours = 0;
            let minutes = 0;
            let seconds = 0;

            if (nowTimestamp < endTimestamp) {
                days = Math.floor(deltaT / 86400);
                deltaT -= days * 86400;
                hours = Math.floor(deltaT / 3600) % 24;
                deltaT -= hours * 3600;
                minutes = Math.floor(deltaT / 60) % 60;
                deltaT -= minutes * 60;
                seconds = Math.floor(deltaT % 60);
            }

            countdown.children[0].innerHTML = days;
            countdown.children[1].innerHTML = hours;
            countdown.children[2].innerHTML = minutes;
            countdown.children[3].innerHTML = seconds;
        }, 1000);
    </script>

    <script>
        jQuery("#Watch").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 3000,
            items: 1,

        });
    </script>

    

    <script>
        jQuery("#owl-demo").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
    <script>
        jQuery("#owl-demo1").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 1000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
    <script>
        jQuery("#owl-demo2").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>
    <script>
        jQuery("#owl-demo3").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 3000,
            items: 1,

        });
    </script>
    <script>
        jQuery("#owl-demo4").owlCarousel({
            navigation: true,
            loop: true,
            margin: 10,
            autoPlay: 3000,
            items: 4,
            responsive: {
                0: {
                    items: 1,
                    nav: true
                },
                600: {
                    items: 1,
                    nav: false
                },
                1000: {
                    items: 4,
                    nav: true,
                    loop: false
                }
            }
        });
    </script>

      <script>
        jQuery("#Ethnic, #jumpsuits, #lastest_top, #Footwear, #Watches, #Stylish_Tops").owlCarousel({
            nav: true,  
            navText: ["<i class='fa fa-angle-left'></i>", "<i class='fa fa-angle-right'></i>"], 
            loop: true,
            margin: 10,
            autoPlay: 3000,
            dots:false,
            responsive: {
                0: {
                    items: 2
                },
                600: {
                    items: 3
                },
                1000: {
                    items: 4
                }
            }
        }); 
    </script>

    <script>
        $('.nav__trigger', ).on('click', function(e) {
            e.preventDefault();
            $(this).parent().toggleClass('nav--active').show();
            $('body').toggleClass('no-scroll');
        });
        $('.nav li, .nav').on('click', function() {
            $(".nav__trigger").parent().removeClass("nav--active");
            $(".nav__icon").removeClass("nav--active");
            $('body').removeClass('no-scroll');
        });
    </script>

    <script type="text/javascript">
    var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        source:  function (query, process) {
        return $.get(path, { key: query }, function (data) {
                return process(data);
            });
        }
    });
</script>    

</body>
</html>