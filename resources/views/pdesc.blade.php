@extends('layouts.website')
@section('content')
@php error_reporting(0) ;@endphp
<?php
$currencyCode = Session::get('currency_code');
if($currencyCode == "INR") {
    $currencyCodeLabel = "Rs";
} else {
    $currencyCodeLabel = $currencyCode;
}
?>
<style>
  .SimilarArea.SimilarArea2 .SimilarBox{
    min-height: 300px;
}
.alreadyselectsize{
  background-color: #71298c;
    border-color: #71298c;
    color: #fff;
}
.selectedcolormark {
  border-color: #df048b;
}
</style>
<section>
   <div class="DetailsArea">
      <div class="container">
         <div class="DetailsBread">
            <ul>
               <li><a href="{{ route('home') }}">Home</a></li>
               <li><a href="{{ route('shop', $urlname['first']) }}"> {{ $urlname['first'] }} </a></li>
               <li><a href="{{ route('products', [$urlname['first'], base64_encode('subsubcat'), base64_encode($urlname['subsubid'])]) }}"> {{ $urlname['first'] }} {{ $urlname['subsubname'] }} </a></li>
               <li><a href="#"> {{$return_array['name']}} </a></li>
            </ul>
         </div>

         <div class="DetailsBody">
            <div class="row">
               <div class="col-sm-4 col-md-4">
                  <div id="ProductSlide" class="carousel slide" data-ride="carousel">
                    <span id="show_img">
                     <ol class="carousel-indicators">
                        @foreach ($return_array['image_list'] as $key=>$value111)
                            <li data-target="#ProductSlide" data-slide-to="{{$key}}" @if(trim($key) == 0) class="active" @endif></li>
                        @endforeach
                     </ol>
                     
                     <div class="carousel-inner" >
                        @foreach ($return_array['image_list'] as $key1=>$value111)
                             
                            <div class="item @if(trim($key1) == 0) active @endif">
                               <img src="{{$value111}}">
                            </div>
                        @endforeach
                     </div>
                     <span>
                  </div>
               </div>

               <div class="col-sm-8 col-md-6">
                  <div class="Describe">
                     <form method="post" action="{{ route('addtocart') }}">
                      @csrf
                     <aside>
                        <h1>{{$return_array['brand']}}  <span>{{$return_array['name']}}</span></h1>

                        <h2>{{ $currencyCodeLabel }} <span id="sp_change">{{$return_array['sp']}}</span> @if($return_array['percentage'] > 0) <del>{{ $currencyCodeLabel }} <span id="mrp_change">{{$return_array['mrp']}}</span> </del><span>( <span id="dis_change">{{$return_array['percentage']}}</span>% off)</span>@endif</h2>
                        
                        <h3>Additional tax may apply; charged at checkout </h3>
                        <input type="hidden" name="" value="{{$return_array['product_id']}}" id="pro_id">
                        <input type="hidden" name="is_variant" value="{{$return_array['is_variant']}}">

                        @if($return_array['is_variant'] == "2")

                          @if(!empty($return_array['size']))
                            <div class="Size">
                               <h4>Select {{$return_array['size_label']}}</h4>
                                @php $s=1; @endphp
                                @foreach($return_array['size'] as $prosize)
                                   <div class="chart">
                                      <input id="size{{ $s }}" name="cartsize" value="{{ $prosize }}" type="radio" @if($prosize == $return_array['seletionsize']) checked="checked" @endif>
                                      <label for="size{{ $s }}" @if($prosize == $seletionsize) class="alreadyselectsize" @endif style="font-size: 12px;"> {{ $prosize }} </label>
                                   </div>
                                   @php $s++; @endphp
                                @endforeach
                                    @if(count($getdata)>0)
                                    <a href="#" class="size-chart" data-toggle="modal" data-target="#chartsize">{{$return_array['size_label']}} chart <i class="fa fa-angle-right"></i></a>
                                    @endif
                            </div>
                          @else 
                            <input type="hidden" name="cartsize" value=" ">
                          @endif
                          
                          @if(count($return_array['color']) > 0)
                            <div class="Color">
                                <p>Color</p>

                                @foreach($return_array['color'] as $colorr)
                                  <?php //print_r($colorr);die;?>
                                    <label class="color_shade"> 
                                       <input type="radio" value="{{ $colorr }}" name="cartcolor"  @if($colorr == $return_array['seletioncolor']) checked="checked"  @endif>
                                       <span class="checkmark @if($colorr == $seletioncolor) selectedcolormark  @endif" style="background-color: {{ $colorr }}"></span>
                                    </label>
                                @endforeach
                            </div>
                          @else
                            <input type="hidden" name="cartcolor" value="">
                          @endif

                        @else
                            <input type="hidden" name="cartsize" value="">
                            <input type="hidden" name="cartcolor" value="">

                        @endif
                        <BR>

                        @if($return_array['haveCart'])

                        @else

                            @if($return_array['qty'] > 0)
                              
                              <h4 id="qtyaddedonchangeheading"><b>Select Quantity</b></h4>
                            
                              @php $fqty = $return_array['qty']; @endphp
                              @if($fqty > 5)
                                  @php $fqty = 5; @endphp
                              @else
                                @php $fqty = $fqty; @endphp
                              @endif
                                <select name="cartqty" class="proqtychange" id="qtyaddedonchange" style="width: 50px;height: 28px;font-size: 17px;">
                                <?php
                                  for($xx = 1; $xx <=$fqty ;$xx++) {
                                ?>
                                    <option value="{{ $xx }}"> {{ $xx }} </option>
                                <?php
                                  }
                                ?>
                                </select>

                            @else
                              <h4 id="qtyaddedonchangeheading" style="display:none"><b>Select Quantity</b></h4>
                              <select name="cartqty" class="proqtychange" id="qtyaddedonchange" style="width: 50px;height: 28px;font-size: 17px;display:none;">

                              </select>

                            @endif

                        @endif

                        <p id="provarianterror" class="text-danger"></p>
                        <div class="Links">
                          @if($return_array['haveCart'])
                            <a href="{{ route('mybag') }}"><img src="{{url('/')}}/public/images/bag-icon.png">Go to bag</a>
                          @else
                            @if($return_array['qty'] <= 0)
                                <button type="button" class="outofstockbutton"> Out of Stock </button>
                                <button type="submit" class="provariantbuttonagain" style="display: none;"><img src="{{url('/')}}/public/images/bag-icon.png"> Add to bag</button>
                            @else
                                <button type="button" class="outofstockbutton" style="display: none;"> Out of Stock </button>
                                <button type="submit" id="provariantbutton" class="provariantbuttonagain"><img src="{{url('/')}}/public/images/bag-icon.png"> Add to bag</button>
                            @endif

                          @endif
                           <a href="{{ route('add-wishlist', $return_array['product_id']) }}"><img src="{{url('/')}}/public/images/Wishlist.png">wishlist</a>
                           <input type="hidden" name="productcart" value="{{ $return_array['product_id'] }}">
                        </div>
                     </aside>
                     <form>

                     <figcaption>
                        <h2>Share this Product</h2>
                        <div class="ShareProduct">
                           <a href="#" class="Share"><i class="fa fa-share-alt"></i> Share </a>
                           <ol>
                              <li><a href="http://www.facebook.com/sharer.php?u={{ url()->current() }}"><i class="fa fa-facebook"></i></a></li>
                              <li><a href="http://twitter.com/share?text={{$return_array['name']}}&url={{ url()->current() }}"><i class="fa fa-twitter"></i></a></li>
                              <li><a href="https://plus.google.com/share?url={{ url()->current() }}"><i class="fa fa-google-plus"></i></a></li>
                           </ol>
                           <div class="clear"></div>
                        </div>
                        <h3>Expected Delivery In {{ $return_array['expected'] }} Days</h3>
                        
                        @if(count($return_array['bestoffer']) > 0)
                            <h4>OFFERS
                              @foreach($return_array['bestoffer'] as $bestoffer)
                               <span> {{ $bestoffer }} </span>
                              @endforeach
                            </h4>
                        @endif

                        @if(count($return_array['bankoffer']) > 0)
                        <h4> BANK OFFERS
                            
                          @foreach($return_array['bankoffer'] as $bankoffer)
                                 <span> {{ $bankoffer }} </span>
                          @endforeach
                        </h4>
                        @endif

                     </figcaption>
                  </div>
               </div>
            </div>
         </div>
         <div class="Product_Delivery">
            <div class="row">
               <div class="col-sm-4 col-md-4">
                  <div class="Details">
                     <h3>Product Details</h3>
                     <h4>{{$return_array['description']}}</h4>
                     <p>Material & Care Specifications<span>100% cotton Machine-wash</span></p>
                     <ol>
                        <li>Fit <span> {{ $return_array['fit'] }} </span></li>
                        <li>Brand <span> {{ $return_array['brand'] }} </span></li>
                        <li>Length <span> {{ $return_array['length'] }} </span></li>
                     </ol>
                     <!--<a href="#">See more</a>-->
                  </div>
               </div>
               <div class="col-sm-4 col-md-4">
                  <div class="Delivery">
                      <!--<h3>Delivery options <img src="{{url('/')}}/public/images/delivery_512x512.png"></h3>
                        <input type="" name="" placeholder="Enter Pincode">
                        <button>check</button>-->
                      
                     <ul>
                        <li>Tax: Applicable tax on the basis of exact location & discount will be charged at the time of checkout</li>
                        <li>100% Original Products</li>
                        <li>Free Delivery Available</li>
                        <li>Cash on delivery might be available</li>
                        <li>Easy 7 days returns and exchanges</li>
                        <li>Product Code: {{ $return_array['productcode'] }}</li>
                        @if(strlen($return_array['sold_by']) > 0)
                            <li>Sold by: @if($return_array['sold_by'] == "admin") Seller @else {{ $return_array['sold_by'] }} @endif</li>
                        @endif
                        <li>View Supplier Information</li>
                     </ul>
                  </div>
               </div>

               <div class="col-sm-4 col-md-4">
                  <div class="Details">
                    <p>More Specifications</p>
                    @if(count($return_array['attributes']) > 0)
                    <ol>
                      @foreach($return_array['attributes'] as $moreAttri)
                        <li>{{ $moreAttri['labal'] }} <span> {{ $moreAttri['text'] }} </span> </li>
                      @endforeach
                    </ol>
                    @endif
                  </div>
               </div>

            </div>
         </div>
      </div>
   </div>
</section>
<section>
   <div class="SimilarArea SimilarArea2">
      <div class="container">
        @if(count($return_array['similars']) > 0)
         <h2>Similar product</h2>
         <div class="row">
            
            @foreach($return_array['similars'] as $similar)
            <div class="col-sm-2">
               <div class="SimilarBox">
                <a href="{{ route('product/description/', base64_encode($similar['product_id'])) }}">
                  <figure><img src="{{ $similar['image'] }}"></figure>
                  <h4> {{ $similar['brand'] }} </h4>
                  <h5> {{ $similar['name'] }} </h5>
                  <h6>{{ $currencyCodeLabel }} {{ $similar['sp'] }} @if($similar['percentage'] != 0) <del>{{ $currencyCodeLabel }} {{ $similar['mrp'] }} </del> <span>  ({{ $similar['percentage'] }}%OFF)</span> @endif</h6>
                </a>
               </div>
            </div>
            @endforeach
         </div>
        @endif
      </div>
      <div class="MoreLinks">
         <a href="{{ route('products', [$urlname['first'], base64_encode('subsubcat'), base64_encode($urlname['subsubid']), base64_encode('0'), base64_encode($return_array['brand']) ]) }}">More {{ $urlname['subsubname'] }} by {{$return_array['brand']}}  <i class="fa fa-angle-right"></i></a>
         <span id="show_url">
         <a href="{{ route('products', [$urlname['first'], base64_encode('subsubcat'), base64_encode($urlname['subsubid']), base64_encode($return_array['defaultcolorcode']), base64_encode('0') ]) }}">More 
          <span id="img_color"> {{ $return_array['defaultcolorname'] }} </span> {{ $urlname['subsubname'] }}  <i class="fa fa-angle-right"></i></a>
      </span>

         <a href="{{ route('products', [$urlname['first'], base64_encode('subsubcat'), base64_encode($urlname['subsubid']) ]) }}">More {{ $urlname['subsubname'] }}  <i class="fa fa-angle-right"></i></a>
      </div>
   </div>
</section>
<section>
   <div class="SimilarArea SimilarArea2">
      <div class="container">
        @if(count($return_array['likedpros']) > 0)
         <h2>Customers Also liked</h2>
         <div class="row">
            @foreach($return_array['likedpros'] as $liked)

                <div class="col-sm-2">
                   <div class="SimilarBox">
                    <a href="{{ route('product/description/', base64_encode($liked['product_id'])) }}">
                      <figure><img src="{{ $liked['image'] }}"></figure>
                      <h4>{{ $liked['brand'] }}</h4>
                      <h5>{{ $liked['name'] }}</h5>
                      <h6>{{ $currencyCodeLabel }} {{ $liked['sp'] }} @if($liked['percentage'] != 0) <del>{{ $currencyCodeLabel }} {{ $liked['mrp'] }} </del> <span>  ({{ $liked['percentage'] }}%OFF)</span> @endif</h6>
                    </a>
                   </div>
                </div>

            @endforeach
         </div>
        @endif
      </div>
   </div>
</section>
<div id="divResult"></div>
<!-- Modal -->
<div id="chartsize" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Size Chart</h4>
      </div>
      <div class="modal-body">
    
  <table class="table table-striped">
  <tbody>
  <?php 
   $name= array();
   $xx=array();
  foreach($getdata as $value) {
    $name[] = $value['heading'];
    $xx[] = $value['heading_values'];
    
  }
  $b=implode(",",$name);
  $c=explode(",",$b);
  
  $bb=implode(",",$xx);
  $ccc=explode(",",$bb);
  
  $newdata=array();
  foreach($c as $k=>$v)
  {
    $newdata[$v][]=$ccc[$k];
  }
   
    ?>
    <tr>
    <?php foreach($newdata as $kkk=>$vvv){?>
      
        <th><?php echo ucfirst($kkk);?></th>
         
     
    <?php }
    
    ?>
     </tr>
     
     <?php foreach($getdata as $kk1=>$vn){?>
          <tr>
         <?php $rr = explode(",",$vn['heading_values']);
          foreach($rr as $vvvvvv){
             if($vvvvvv) {
                 $val1 = $vvvvvv;
             } else {
                 $val1 = 'N/A';
             }
     
     ?>
     <td><?php echo $val1;?></td>
      <?php }?>
          
         
          </tr>
    <?php }?>
    </tbody>
  </table>
      </div>
    </div>

  </div>
</div>


@endsection

@push('script')
<script>

$(function() {

  $('input[type=radio]').on('change',function() {
    
    var color = $('input[name="cartcolor"]:checked').val();
    var size = $('input[name="cartsize"]:checked').val();
    var pro_id = $("#pro_id").val();

    $.ajax({

        url:"{{route('getColorImage')}}",
        method:"POST",
        data:{
          "_token": "{{ csrf_token() }}",
          pro_id:pro_id,
          color:color,
        },
        success:function(data) {
          if(data) {       
              var res =   JSON.parse(data);
              $("#img_color").text(res.color_name);
              $('#show_img').html(res.contents);
              $('#show_url').html(res.url);

              $.ajax({
                  url:"{{route('getVariantPrice')}}",
                  method:"POST",
                  data:{
                    "_token": "{{ csrf_token() }}",
                    pro_id:pro_id,
                    color:color,
                    size:size,
                  },
                  success:function(data) {
                      var res =   JSON.parse(data);
                      //console.log(res);
                      if(res.status == "success") {
                        
                        $("#provariantbutton").attr('disabled', false);
                        $('#provarianterror').html(' ');

                        $("#sp_change").html(res.sp);
                        $('#mrp_change').html(res.mrp);
                        $('#dis_change').html(res.discount);
                        $('.proqtychange').html(res.qtyHTML);

                        if(res.qtyHTML.length <= 0) {
                            $("#provariantbutton").attr('disabled', true);

                            $('#qtyaddedonchangeheading').css("display","none");
                            $('#qtyaddedonchange').css("display","none");
                            $('#qtyaddedonchange').html("");
                            $('.outofstockbutton').css("display","inline-block");
                            $('.provariantbuttonagain').css("display","none");

                            $('#provarianterror').html('Product not available with this variant selection');

                        } else {
                            $("#provariantbutton").attr('disabled', false);
                            $('#provarianterror').html(' ');
                            $('#qtyaddedonchangeheading').css("display","block");
                            $('#qtyaddedonchange').css("display","inline-block");
                            $('#qtyaddedonchange').html(res.qtyHTML);
                            $('.outofstockbutton').css("display","none");
                            $('.provariantbuttonagain').css("display","inline-block");
                        }
                      
                      } else {
                          $('.proqtychange').html(res.qtyHTML);
                          $("#provariantbutton").attr('disabled', true);

                          $('.outofstockbutton').css("display","inline-block");
                          $('.provariantbuttonagain').css("display","none");
                          $('#qtyaddedonchange').css("display","none");
                          $('#qtyaddedonchangeheading').css("display","none");
                          $('#provarianterror').html('Product not available with this variant selection');
                      }
                  }
              });
          }
        }
      });
  });
});

</script>


@endpush
