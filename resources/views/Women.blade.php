@extends('layouts.website')

@section('content')
<section>
        <div class="DiscountArea">
            <div class="container">
                <div class="DiscountBox" style="background-image: url('{{url('/')}}/public/images/Women.png');">
                    <h1>GET 10 % EXTRA DISCOUNT</h1>
                    <h2>USE CODE: <span>WELCOME 10</span></h2>

                    <div class="row">
                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/woman.png"></figure>
                                    <p>Kurtis</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/woman2.png"></figure>
                                    <p>Tops</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/woman3.png"></figure>
                                    <p>Tshirts</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/woman4.png"></figure>
                                    <p>Women Jeans</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/woman5.png"></figure>
                                    <p>Trousers</p>
                                </a>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/woman6.png"></figure>
                                    <p>Skirts Palazzos</p>
                                </a>
                            </div>
                        </div>
                    </div>

                    <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category" class="view">View all</a>

                </div>
            </div>
        </div>
    </section>
  
    <section>
        <div class="FlashArea">
            <div class="container">
                <h1><img src="{{url('/')}}/public/images/flash.png"> Flash Sale <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">View all</a></h1> 

                <div class="FlashBody">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    	<img src="{{url('/')}}/public/images/new/img5.png">
                                	</a>
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    	<img src="{{url('/')}}/public/images/new/img4.png">
                                	</a>
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    	<img src="{{url('/')}}/public/images/new/img3.png">
                                	</a>
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    	<img src="{{url('/')}}/public/images/new/img2.png">
                                	</a>
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                    </div>
                </div>
                    

            </div>
        </div>
    </section>
  

    <section>         
        <div class="container">
            <div class="OccasionArea">
                <h2>Shop by Occasion</h2>
                <div class="row">
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure>
                            	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                            		<img src="{{url('/')}}/public/images/new/img5.png">
                            	</a>
                            </figure>
                            <p>Gym Collection </p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure>
                            	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                            		<img src="{{url('/')}}/public/images/new/img6.png">
                            	</a>
                            </figure>
                            <p>Jeans</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure>
                            	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                            		<img src="{{url('/')}}/public/images/new/img7.png">
                            	</a>
                            </figure>
                            <p>Formal Dress</p>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="Occasionbox">
                            <figure>
                            	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                            		<img src="{{url('/')}}/public/images/new/img8.png">
                            	</a>
                            </figure>
                            <p>party Dress</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="OccasionArea"> 
                <div id="BuySlider" class="carousel slide" data-ride="carousel" style="background-image: url('{{url('/')}}/public/images/Buy.png')"> 
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="BuySliderBox">
                                <h3>Buy 4 Items</h3>
                                <h3>For <i class="fa fa-inr"></i> 1599</h3>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Limited offer</a>
                            </div>
                        </div> 
                        <div class="item">
                            <div class="BuySliderBox">
                                <h3>Buy 2 Items</h3>
                                <h3>For <i class="fa fa-inr"></i> 1599</h3>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Limited offer</a>
                            </div>
                        </div> 
                        <div class="item">
                            <div class="BuySliderBox">
                                <h3>Buy 5 Items</h3>
                                <h3>For <i class="fa fa-inr"></i> 1599</h3>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Limited offer</a>
                            </div>
                        </div> 
                    </div>
                 
                    <a class="left carousel-control" href="#BuySlider" data-slide="prev">
                        <span class="glyphicon glyphicon-menu-left"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#BuySlider" data-slide="next">
                        <span class="glyphicon glyphicon-menu-right"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>

        </div>         
    </section>




    <section>
        <div class="CollectionArea">
            <div class="container">

                <div class="CollectionList">
                    <h2>Ethnic Collection</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Ethnic">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image1.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/imag 2.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image3.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-4.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Dress and jumpsuits</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="jumpsuits">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-6.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-7.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-8.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-4.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>lastest tops</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="lastest_top">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-23.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-9.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-11.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-12.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Footwear</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Footwear">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-16.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-14.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-13.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-15.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>Fossil smart watches</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Watches">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-17.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-19.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-18.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-20.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="CollectionList">
                    <h2>New Stylish Tops</h2>
                    <div id="demos">
                        <div class="owl-carousel owl-theme" id="Stylish_Tops">
                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/imge-21.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-22.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-23.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                            <div class="item">
                                <div class="CollectionBox">
                                    <figure>
                                    	<a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    		<img src="{{url('/')}}/public/images/new/image-24.png">
                                    	</a>
                                    </figure>
                                    <figcaption>
                                        <h3>levis</h3>
                                        <h4>Men slim fit Jeans</h4>
                                        <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                                    </figcaption>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
 
            </div>
        </div>
    </section>

    <section>
        <div class="FashionBuyArea">
            <div class="container">

                <h3>Fashion edit</h3>
                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</h4>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="FashionBuyLeft">
                            <h1>BUY 1 GET 1
                                <span>EXTRA 50% OFF</span> </h1>
                            <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Shop now</a>
                        </div>
                    </div>

                    <div class="col-sm-7">
                        <ul>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/new/image-25.png"></figure>
                                    <p>Casual wear</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/new/image-26.png"></figure>
                                    <p>tops</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/new/image-27.png"></figure>
                                    <p>tops</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/new/image-28.png"></figure>
                                    <p>Cloths</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="ClothingArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-31.png"></figure>
                            <h3>clothing</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-32.png"></figure>
                            <h3>clothing</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-33.png"></figure>
                            <h3>clothing</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="Clothing">
                            <figure><img src="{{url('/')}}/public/images/new/image-34.png"></figure>
                            <h3>clothing</h3>
                            <ul>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">KURTA PAJAMA</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">Jackets</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">nehru jacket</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">TRACKSUITS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">DENIM JEANS</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">casual shirts</a></li>
                                <li><a href="{{url('/')}}/productlisting?filter_data=WOMEN&filter_type=Category">sweat shirts</a></li> 
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
@endsection