@extends('layouts.website')

@section('content')
<style type="text/css">
	span.formerror {
    float: left;
    width: 100%;
    font-size: 10px;
    color: red;
    margin: 3px auto;
}
</style>
<section>
    <div class="LoginArea">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1">
                    @if(Session::has('message_forgot'))
						<p class="alert alert-info">{{ Session::get('message_forgot') }}</p>
					@endif

                    <div class="LoginForm">
                        <form method="post" action="{{ route('reset') }}">
                        @csrf
                            <h3>Reset your password</h3>

                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Your Email Address">
                            </div>

                            <div class="form-group">
                                <input type="text" name="otp" class="form-control" placeholder="OTP">
                            </div>

                            <div class="form-group">
                                <input type="password" name="password" class="form-control" placeholder="*******">
                            </div>

                            <button type="submit">Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection