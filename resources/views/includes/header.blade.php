<?php
$currencyCode = Session::get('currency_code');
if($currencyCode == "INR") {
    $currencyCodeLabel = "Rs";
} else {
    $currencyCodeLabel = $currencyCode;
}
?>
<div class="DesktopHeader">
    <div class="LogoArea">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="Logo">
                        <a href="{{url('/')}}"><img src="{{url('/')}}/public/images/Logo.png"></a>
                    </div>
                </div>
                <div class="col-md-4 col-sx-8">
                    <div class="Case">
                        <span>CASH ON DELIVERY</span>/<span>FREE SHIPPING*</span>
                        <p>FREE $ EASY RETURNS</p>
                    </div>
                </div>
                <div class="col-md-6 col-xs-12">
                    <div class="Navi">
                        <ul>
                            <li>
                                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalcontact">
                                    <img src="{{url('/')}}/public/images/customer3.png">
                                    <span>CONTACT US</span>
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <img src="{{url('/')}}/public/images/Phone-1.png">
                                    <span>DOWNLOAD APP</span>
                                </a>
                            </li>

                           
                            @if(Auth::check())
                                <li class="Dropdown">
                                <a href="JavaScript:Void(0);">
                                    <img src="{{url('/')}}/public/images/man.png">
                                    <span><?php echo ucfirst(Auth::user()->name); ?></span>
                                </a>
                                <ol> 
                                    <li><a href="{{url('/')}}/profile"><i class="fa fa-user" aria-hidden="true"></i> My Profile</a></li> 
                                    <li><a href="{{url('/')}}/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> Sign out</a></li>
                                </ol>
                            </li>
                            @else 
                                <li>
                                    <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Login">
                                        <img src="{{url('/')}}/public/images/man.png">
                                        <span>login / Register</span>
                                    </a>
                                </li>
                            @endif
                           

                            <li>
                                <a href="{{ route('mybag') }}">
                                    <img src="{{url('/')}}/public/images/Bag-icon.png">
                                    <span>MY BAG</span>
                                    <sup> {{ getCount() }}</sup>
                                </a>
                            </li>

                            <li>
                                <a href="JavaScript:Void(0);">
                                    <span><?php //echo $currencyCode; ?> <img src="" id="flagclass"> </span>
                                    <span>Currency</span>
                                </a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <nav class="navbar">
        <div class="container">
            <div class="Navigation">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#Menu" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse" id="Menu">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="{{ route('shop','Women') }}">Women </a></li>
                        <li><a href="{{ route('shop','Men') }}">Men</a></li>
                        <li><a href="{{ route('shop','Kids') }}">Kids</a></li>
                        <li><a href="{{ route('shop','Accessories') }}">Accessories </a></li>
                        <li><a href="{{ route('shop','Essentials') }}">Essentials </a></li>
                    </ul>
                    <form action="#">
                        <input type="text" name="filter_data" id="search-box" class="typeahead" placeholder="What are you looking for today?">
                        <div id="suggesstion-box"></div>

                        <input type="hidden" name="filter_type" value="search">
                        <button type="button"><img src="{{url('/')}}/public/images/magnifying-glass.png"></button>
                    </form>
                </div>
            </div>
        </div>
    </nav>
</div>

<div class="MobileHeader">             
    <div class="Navigation">
        <ol>
            <li> 
                <a href="{{url('/')}}"><img src="{{url('/')}}/public/images/Logo.png"></a>
            </li> 
        </ol>
        <ul>                
            <li>
                <a href="javascript:void(0)" data-toggle="modal" data-target="#myModalcontact">
                    <img src="{{url('/')}}/public/images/customer3.png"> 
                </a>
            </li>

            <li>
                <a href="#">
                    <img src="{{url('/')}}/public/images/Phone-1.png"> 
                </a>
            </li>

           
            @if(Auth::check())
                <li class="Dropdown">
                <a href="{{url('/')}}/profile">
                    <img src="{{url('/')}}/public/images/man.png">
                    <span><?php //echo ucfirst(Auth::user()->name); ?></span>
                </a>
                </li>   
            @else 
                <li>
                    <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Login">
                        <img src="{{url('/')}}/public/images/man.png"> 
                    </a>
                </li>
            @endif
           

            <li>
                <a href="{{ route('mybag') }}">
                    <img src="{{url('/')}}/public/images/Bag-icon.png"> 
                </a>
            </li>

            <li>
                <a href="javascript:void(0);" id="Openav">
                    <span></span>
                    <span></span>
                    <span></span>
                </a>
            </li>

        </ul>
    </div>  

    <div class="SideBar">
        <a href="javascript:void(0);" id="Closenav">x</a>
        <ul>
            <li>
                <form class="myNewSearch" action="#">
                        <input type="text" name="filter_data" id="search-box1" class="typeahead" placeholder="What are you looking for today?">
                        <div id="suggesstion-box1" style="display:none;"></div>

                        <input type="hidden" name="filter_type" value="search">
                    </form>
            </li>
            @if(Auth::check())
                <li><a href="{{url('/')}}/profile"> My Profile</a></li> 
                <li><a href="{{url('/')}}/logout"> Sign out</a></li>
            @endif
            <li><a href="{{ route('shop','Women') }}">Women </a></li>
            <li><a href="{{ route('shop','Men') }}">Men</a></li>
            <li><a href="{{ route('shop','Kids') }}">Kids</a></li>
            <li><a href="{{ route('shop','Accessories') }}">Accessories </a></li>
            <li><a href="{{ route('shop','sports') }}">sports </a></li>

        </ul>
    </div>

    <div class="Case">
        <span>CASH ON DELIVERY</span>/<span>FREE SHIPPING*</span>
        <p>FREE $ EASY RETURNS</p>
    </div>

    <div class="clear"></div>
</div>

<script type="text/javascript">
    if(localStorage.getItem('getflag')) {
        var flagget = localStorage.getItem('getflag');
        // $("#flagclass").prop('src', flagget);
        $(document).ready(function() {
            $("#flagclass").prop('src', flagget);
        });
    }    
</script>