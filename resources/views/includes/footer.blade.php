

 
<div class="container">
   <div class="Footer">
      <div class="row">
         <div class="col-sm-3">
            <div class="Foots">
               <h3>Download the app</h3>
               <div class="App">
                  <a href="#"><img src="/yodapi/public/images/ANDRIOD.png"></a>
               </div>
               <div class="App">
                  <a href="#"><img src="/yodapi/public/images/APPLE.png"></a>
               </div>
            </div>
         </div>
         <div class="col-sm-3">
            <div class="Foots">
               <h3>yard of deals</h3>
               <ul>
                  <li><a href="javascript:void(0)" data-toggle="modal" data-target="#myModalcontact1">Help / Support</a></li>
                  <li><a href="{{ route('about-us') }}">About Us </a></li>
                  <li><a href="{{ route('faq') }}">FAQs</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-3">
            <div class="Foots">
               <h3>our policies</h3>
               <ul>
                  <li><a href="{{ route('terms-condition') }}">Terms and Condition</a></li>
                  <li><a href="{{ route('privacy-policy') }}">Privacy Policy</a></li>
               </ul>
            </div>
         </div>
         <div class="col-sm-3">
            <div class="Foots">
               <h3>Subscribe to newsletter</h3>
               <form>
                  <input type="text" name="">
                  <button>Subscribe</button>
               </form>
               <ol>
                  <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                  <li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
               </ol>
            </div>
         </div>
      </div>
      <div class="clear"></div>
   </div>
</div>
<div class="FooterBottom">
   <div class="container">
      <ul>
         <li>
            <img src="{{URL('/')}}/public/images/rupee1.png">
            <h6>CASH ON DELIVERY</h6>
         </li>
         <li>
            <img src="{{URL('/')}}/public/images/return.png">
            <h6>15 DAYS RETURNS </h6>
         </li>
         <li>
            <img src="{{URL('/')}}/public/images/free.png">
            <h6>FREE SHIPPING</h6>
         </li>
         <div class="clear"></div>
      </ul>
      @php $footerContent =  getFooterContent(); @endphp
      <?php echo ucfirst($footerContent); ?>
      <aside>
         <p>YARDOFDEALS.COM ©2019 | ALL RIGHTS RESERVED</p>
      </aside>
   </div>
</div>
</footer>
<div class="ModalBox">
   <div id="Login" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-body">
               <div class="LoginForm">
                  <a href="JavaScript:Void(0);" data-dismiss="modal" class="CloseButton">&times;</a>
                  <form method="post" action="{{action('LoginController@userlogin')}}">
                     <h3>Login to Yard Of Deals</h3>
                     <h4>or</h4>
                     <ul>
                        <li><a href="redirect/facebook"><img src="{{url('/')}}/public/images/Icon-1.png"> facebook</a></li>
                        <li><a href="#"><img src="{{url('/')}}/public/images/Icon-2.png"> Google</a></li>
                     </ul>
                     <h5>- or using email -</h5>
                     <div class="form-group">
                        <p class="successmsg text-success text-center" style="display:none;color: #3c763d;font-size: 20px;">
                        </p>
                        <div>
                          <input id="loginemail" type="email" name="email" class="form-control" placeholder="Your Email Address / Phone Number">
                          <span class="formerror" id="loginemailerror">
                             @if(isset($messages['email']['0']) && !empty($messages['email']['0']))
                                 {{ $messages['email']['0'] }}
                             @endif
                          </span>
                        </div>
                        <div>
                          <input id="loginpass" type="password" name="password" class="form-control" placeholder="Enter Password">
                          <input type="hidden" name="typelogin" value="email" id="typelogin">
                          <span class="formerror" id="loginpassworderror">
                             @if(isset($messages['password']['0']) && !empty($messages['password']['0']))
                                 {{ $messages['password']['0'] }}
                             @endif
                          </span>
                        </div>
                     </div>
                     
                     <button id="loginbtn" type="button">Login</button>
                     <div class="Links">
                        <a href="{{url('/')}}/forgot">Forgot password ?</a>
                        <p>New To YOD? <a href="JavaScript:Void(0);" id="RegisterModal" data-toggle="modal" data-target="#Register" data-dismiss="modal">create Account</a></p>
                     </div>
                     <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="Register" class="modal fade" role="dialog">
      <div class="modal-dialog">
         <div class="modal-content">
            <div class="modal-body">
               <div class="LoginForm">
                  <a href="JavaScript:Void(0);" data-dismiss="modal" class="CloseButton">&times;</a>
                  <form method="post" action="{{action('LoginController@usersignup')}}">
                     <h3>Login to Yard Of Deals</h3>
                     <h4>or</h4>
                     <ul>
                        <li><a href="{{url('/redirect')}}"><img src="{{url('/')}}/public/images/Icon-1.png"> facebook</a></li>
                        <li><a href="{{url('/redirect')}}"><img src="{{url('/')}}/public/images/Icon-2.png"> Google</a></li>
                     </ul>
                     <h5>- or using email -</h5>
                     <div class="form-group">
                        <div>
                          <input type="text" id="signupname" name="name" class="form-control" placeholder="Your Full Name">
                          <span class="formerror" id="signupnameerror"><?php
                           if(isset($messages['name']['0']) && !empty($messages['name']['0'])){
                               echo $messages['name']['0'];
                           }
                           ?></span>
                        </div>
                        
                        <div>
                          <input type="email" name="email" id="signupemail" class="form-control" placeholder="Your Email Address">
                          <span class="formerror" id="signupemailerror"><?php
                           if(isset($messages['email']['0']) && !empty($messages['email']['0'])){
                               echo $messages['email']['0'];
                           }
                           ?></span>
                        </div>
                        
                        <div>
                          <input type="password" name="password" class="form-control" placeholder="Password" id="signuppass">
                          <span class="formerror" id="signuppasserror"><?php
                           if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                               echo $messages['password']['0'];
                           }
                           ?></span>
                        </div>
                        
                        <div>
                          <input type="password" name="password" class="form-control" placeholder="Confirm Password" id="signuppass">
                          <span class="formerror" id="signuppasserror"><?php
                           if(isset($messages['password']['0']) && !empty($messages['password']['0'])){
                               echo $messages['password']['0'];
                           }
                           ?></span>
                        </div>

                        <div>
                          <input type="text" name="phone" class="form-control" placeholder="Mobile Number (For order status updates)" id="signupphone">
                          <span class="formerror" id="signupphoneerror"><?php
                           if(isset($messages['phone']['0']) && !empty($messages['phone']['0'])){
                               echo $messages['phone']['0'];
                           }
                           ?></span>
                        </div>

                        <div class="Select">
                           <p>I'm a</p>
                           <label class="radio-inline"><input type="radio" name="gender" value="Male" checked="">Male</label>
                           <label class="radio-inline"><input type="radio" value="Female" name="gender">female</label>
                        </div>
                     </div>
                     <button type="button" id="signupbtn">Register</button>
                     <div class="Links">
                        <h6>Alredy have a account? <a href="JavaScript:Void(0);" id="Loginmodal" data-toggle="modal" data-target="#Login" data-dismiss="modal">Login!</a></h6>
                     </div>
                     <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>



<!-- Modal -->
<div id="myModalcontact" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="JavaScript:Void(0);" data-dismiss="modal" class="CloseButton">×</a>
        <h4 class="modal-title">Contact Us</h4>
      </div>
      <div class="modal-body">
        <ul class="ContactInfo">
          <li>
            <a href="#">
              <i class="fa fa-mobile"></i>
               +91 9876543210
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="fa fa-envelope-o"></i>
              info@gmail.com
            </a>
          </li>
       </ul>
      </div>
      
    </div>

  </div>
</div>

<div id="myModalcontact1" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <a href="JavaScript:Void(0);" data-dismiss="modal" class="CloseButton">×</a>
        <h4 class="modal-title">Help / Support</h4>
      </div>
      <div class="modal-body">
        <ul class="ContactInfo">
          <li>
            <a href="#">
              <i class="fa fa-mobile"></i>
               +91 9876543210
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="fa fa-envelope-o"></i>
              info@gmail.com
            </a>
          </li>
       </ul>
      </div>
    </div>

  </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

<script type="text/javascript">

  $(document).ready(function() {
      $('#loginemail').keyup(function() {
         var email =  $('#loginemail').val();
         var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
         
         if(!regex.test(email)) {
            //alert('asa');
             $("#typelogin").val("phone");
         } else {
            $("#typelogin").val("email");
         }
      });
   });

   $("#loginbtn").click(function(){
      var username = $("#loginemail").val();
      var password = $("#loginpass").val();
      var typelogin = $("#typelogin").val();

      if(username.length <= 0 && password.length >= 0) {
          $("#loginemailerror").text('Email filed is required');
          $("#loginpassworderror").text('Password filed is required');

      } else if(username.length <= 0 && password.length >= 0) {
          $("#loginemailerror").text('Email filed is required');
          $("#loginpassworderror").text('');
                
      } else if(password.length <= 0 && username.length >= 0) {
          $("#loginpassworderror").text('Password filed is required');

            if(IsEmail(username)==false) {
                $("#loginemailerror").text('Please enter valid email id');
            } else {
              $("#loginemailerror").text('');
            }

      } else {

        $.post("{{action('LoginController@userlogin')}}",{email:username,password:password,typelogin:typelogin,_token:"{{ Session::token() }}"},function(result) {
               if(result.login.status == 0) {
                   if(result.login.messages.email) {
                       $("#loginemailerror").text(result.login.messages.email[0]);
                   }

                   if(result.login.messages.password) {
                       $("#loginpassworderror").text(result.login.messages.password[0]);
                   }
               } else {
                   location.reload();
               }
        });
      }
   });

  function IsEmail(email) {
      var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      
      if(!regex.test(email)) {
        return false;
      }else{
        return true;
      }
  }
   
      $("#signupbtn").click(function(){
         var signupname = $("#signupname").val();
         var signupphone = $("#signupphone").val();
         var signupemail = $("#signupemail").val();
         var signuppass = $("#signuppass").val();
         var gender = $("[name=gender]").val();
   
         $.post("{{action('LoginController@usersignup')}}",{name:signupname,phone:signupphone,gender:gender,email:signupemail,password:signuppass,_token:"{{ Session::token() }}"},function(result) {
   
             if(result.signup.status == 0){
                 if(result.signup.messages.email){
                     $("#signupemailerror").text(result.signup.messages.email[0]);
                 }

                 if(result.signup.messages.password){
                     $("#signuppasserror").text(result.signup.messages.password[0]);
                 }

                 if(result.signup.messages.name){
                     $("#signupnameerror").text(result.signup.messages.name[0]);
                 }

                 if(result.signup.messages.phone){
                     $("#signupphoneerror").text(result.signup.messages.phone[0]);
                 }

             } else {
                  $('.successmsg').css('display','block');
                  $('.successmsg').html("Account Created Successfully");
                  $("#Register").modal('hide');
                  $("#Login").modal('show');
                  $("body").removeClass("RegisteBackdrop"); 
                  $("body").addClass("modal-open");
                  //location.reload();
             }
         });
   });
</script>
<script>
   $(document).ready(function(){
       $("#RegisterModal").click(function(){
           $("body").addClass("RegisteBackdrop"); 
       });
       $("#Loginmodal").click(function(){
           $("body").removeClass("RegisteBackdrop"); 
           $("body").addClass("modal-open"); 
       });
       $(".CloseButton").click(function(){
           $("body").removeClass("RegisteBackdrop");  
       });
   });
</script>



<script>
   $('#RegisterModal').modal({
       backdrop: 'static',
       keyboard: false
   })
</script>

<script type="text/javascript">
    $(function() {
      var action;
      $(".number-spinner.spin button").mousedown(function () {
          btn = $(this);
          input = btn.closest('.number-spinner.spin').find('input#changequantity');
          btn.closest('.number-spinner.spin').find('button').prop("disabled", false);

          if (btn.attr('data-dir') == 'up') {
                var countermax = parseInt(input.val());

                if(countermax <19) {
                  action = setInterval(function() {
                      
                      if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                          //input.val(parseInt(input.val())+1);
                      
                      } else {
                          btn.prop("disabled", true);
                          clearInterval(action);
                      }
                  }, 50);
                }

                var qty = $('#changequantity').val();
                qty = parseInt(qty) + 1;
                var cid = $('.data-up button').attr('class');
                $.ajax({
                    type: "POST",
                    url: "{{ route('updatecart') }}",
                    data: {qty:qty, cid:cid, _token: '{{csrf_token()}}'},
                    success: function(response) {
                       location.reload();
                    }
                });

          } else {
                action = setInterval(function(){
                    if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                        //input.val(parseInt(input.val())-1);
                    }else{
                        btn.prop("disabled", true);
                        clearInterval(action);
                    }
                }, 50);

                var qty = $('#changequantity').val();
                qty = parseInt(qty) - 1;
                var cid = $('.data-dwn button').attr('class');
                $.ajax({
                    type: "POST",
                    url: "{{ route('updatecart') }}",
                    data: {qty:qty, cid:cid, _token: '{{csrf_token()}}'},
                    success: function(response) {
                       location.reload();
                    }
                });
          }
      }).mouseup(function(){
          clearInterval(action);
      });
  });
  </script>
  <script type="text/javascript">
    $(document).ready(function(){
        $('#colaptwo').on('click', function() {
            $('#secondpanel').addClass('Active');
            $('#collapse2').show();
            $('#collapse1').hide();
        });
    });
    $(document).ready(function(){
        $('#colapthree').on('click', function() {
            $('#thirdpanel').addClass('Active');
            $('#collapse3').show();
            $('#collapse2').hide();
            $('#BagsRightshow').css("display","block");
            $('#WrapRightshow').css("display","block");
        });
    });
  </script>

<script>
  $(document).ready(function(){
    $("#Openav").click(function(){
      $(".SideBar").css({"width": "235px", "right": "0px"});
    });
    $("#Closenav").click(function(){
      $(".SideBar").css({"width": "0px", "right": "-90px"});
    });
  });
</script>


@stack('script')

