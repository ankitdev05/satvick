@extends('layouts.website')

@section('content')
<style type="text/css">
	span.formerror {
    float: left;
    width: 100%;
    font-size: 10px;
    color: red;
    margin: 3px auto;
}
</style>
<section>
    <div class="LoginArea">
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1">
                    @if(Session::has('message_forgot'))
						<p class="alert alert-info">{{ Session::get('message_forgot') }}</p>
					@endif

                    <div class="LoginForm">
                        <form method="post" action="{{ route('forgot-password') }}">
                        @csrf
                            <h3>Forgot your password</h3>

                            <div class="form-group">
                                <input type="email" name="email" class="form-control" placeholder="Your Email Address">
                                <span class="formerror">
                            	@if(isset($messages['email']['0']) && !empty($messages['email']['0']))
                            		{{ $messages['email']['0'] }}
                            	@endif
                                </span>
                            </div>

                            <button type="submit">Submit</button>

                            <div class="Links">
                                <a href="JavaScript:Void(0);" data-toggle="modal" data-target="#Login">Login ?</a>
                                <p>New To YOD? <a href="JavaScript:Void(0);" id="RegisterModal" data-toggle="modal" data-target="#Register" data-dismiss="modal">create Account</a></p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

@endsection