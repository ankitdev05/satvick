@extends('layouts.website')

@section('content')


  <section>
        <div class="DiscountArea">
            <div class="container">
                <div class="DiscountBox" style="background-image: url('{{url('/')}}/public/images/Women.png');">
                    <h1>GET 10 % EXTRA DISCOUNT</h1>
                    <h2>USE CODE: <span>WELCOME 10</span></h2>

                    <div class="row">
                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/Headphoness.png"></figure>
                                    <p>HEADPHONES</p>
                                </a>
                            </div>
                        </div> 

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/watch1.png"></figure>
                                    <p>WATCH</p>
                                </a>
                            </div>
                        </div> 

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/wire1.png"></figure>
                                    <p>WIRELESS</p>
                                </a>
                            </div>
                        </div> 

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/backpack1.png"></figure>
                                    <p>BACKPACK</p>
                                </a>
                            </div>
                        </div> 

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/case1.png"></figure>
                                    <p>MOBILE CASE</p>
                                </a>
                            </div>
                        </div> 

                        <div class="col-sm-2">
                            <div class="Discountlist">
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/oval.png"></figure>
                                    <p>OVAL FRAMES</p>
                                </a>
                            </div>
                        </div> 

                    <<a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category" class="view">View all</a> 

                </div>
            </div>
        </div>
    </section>
 


   <section>
        <div class="FlashArea">
            <div class="container">
                <h1><img src="{{url('/')}}/public/images/flash.png"> Flash Sale <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">View all</a></h1> 

                <div class="FlashBody">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches1.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches2.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches3.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="FlashBox">
                                <figure>
                                    <span>48%</span>
                                    <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches4.png">
                                </figure>
                                <aside>
                                    <p>Rs.809 <del>Rs.1799</del></p>
                                </aside>
                            </div>
                        </div>

                    </div>
                </div>
                    

            </div>
        </div>
    </section>

 

<section>         
  <div class="container">
      <div class="OccasionArea">
          <h2>SHOP BY OCCASION</h2>
          <div class="row">
              <div class="col-sm-3">
                  <div class="Occasionbox">
                      <figure><img src="{{url('/')}}/public/images/accesssories/SHOP-BY-OCCASION1.png"></figure>
                      <p>Gym Collection </p>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="Occasionbox">
                      <figure><img src="{{url('/')}}/public/images/accesssories/SHOP-BY-OCCASION2.png"></figure>
                      <p>Jeans</p>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="Occasionbox">
                      <figure><img src="{{url('/')}}/public/images/accesssories/SHOP-BY-OCCASION3.png"></figure>
                      <p>GYM COLLECTION</p>
                  </div>
              </div>
              <div class="col-sm-3">
                  <div class="Occasionbox">
                      <figure><img src="{{url('/')}}/public/images/accesssories/SHOP-BY-OCCASION4.png"></figure>
                      <p>GYM COLLECTION</p>
                  </div>
              </div>
          </div>
      </div>

      <div class="OccasionArea"> 
        <div id="BuySlider" class="carousel slide" data-ride="carousel" style="background-image: url('{{url('/')}}/public/images/MenSlider.png');"> 
            <div class="carousel-inner">
                <div class="item active">
                    <div class="BuySliderBox">
                        <h3>Buy 4 Items</h3>
                        <h3>For <i class="fa fa-inr"></i> 1599</h3>
                        <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Limited offer</a>
                    </div>
                </div> 
                <div class="item">
                    <div class="BuySliderBox">
                        <h3>Buy 2 Items</h3>
                        <h3>For <i class="fa fa-inr"></i> 1599</h3>
                        <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Limited offer</a>
                    </div>
                </div> 
                <div class="item">
                    <div class="BuySliderBox">
                        <h3>Buy 5 Items</h3>
                        <h3>For <i class="fa fa-inr"></i> 1599</h3>
                        <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Limited offer</a>
                    </div>
                </div> 
            </div>
         
            <a class="left carousel-control" href="#BuySlider" data-slide="prev">
                <span class="glyphicon glyphicon-menu-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#BuySlider" data-slide="next">
                <span class="glyphicon glyphicon-menu-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
      </div>
    </div>
</section>


  <section>
    <div class="CollectionArea">
      <div class="container">

        <div class="CollectionList">
            <h2>WATCHES</h2>
            <div id="demos">
                <div class="owl-carousel owl-theme" id="Ethnic">
                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                              <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Watches1.png">
                              </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div> 

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                              <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Watches2.png">
                              </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                              <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Watches3.png">
                              </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                              <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Watches2.png">
                              </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="CollectionList">
            <h2>LATEST WATCHES</h2>
            <div id="demos">
                <div class="owl-carousel owl-theme" id="jumpsuits">
                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches1.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches2.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches3.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/LATEST-Watches4.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <div class="CollectionList">
          <h2>MOBILE CASE COVER</h2>
          <div id="demos">
              <div class="owl-carousel owl-theme" id="lastest_top">
                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Rucksack1.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Rucksack2.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Rucksack3.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Rucksack4.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

              </div>
          </div>
        </div>


        <div class="CollectionList">
          <h2>MOBILE CASE COVER</h2>
          <div id="demos">
              <div class="owl-carousel owl-theme" id="Footwear">
                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Mobile-Case-Cover1.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Mobile-Case-Cover2.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Mobile-Case-Cover3.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                              <img src="{{url('/')}}/public/images/accesssories/Mobile-Case-Cover4.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

              </div>
          </div>
        </div>


        <div class="CollectionList">
          <h2>FOSSIL SMART WATCHES</h2>
          <div id="demos">
              <div class="owl-carousel owl-theme" id="Watches">
                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Fossil-Smart-Watches1.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Fossil-Smart-Watches2.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Fossil-Smart-Watches3.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

                  <div class="item">
                      <div class="CollectionBox">
                          <figure>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                <img src="{{url('/')}}/public/images/accesssories/Fossil-Smart-Watches4.png">
                            </a>
                          </figure>
                          <figcaption>
                              <h3>levis</h3>
                              <h4>Men slim fit Jeans</h4>
                              <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                          </figcaption>
                      </div>
                  </div>

              </div>
          </div>
        </div>


        <div class="CollectionList">
            <h2>Latest Iphone case</h2>
            <div id="demos">
                <div class="owl-carousel owl-theme" id="Stylish_Tops">
                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/Accessories-accessories1.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/Accessories-accessories2.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/Accessories-accessories3.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                    <div class="item">
                        <div class="CollectionBox">
                            <figure>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                  <img src="{{url('/')}}/public/images/accesssories/Accessories-accessories4.png">
                                </a>
                            </figure>
                            <figcaption>
                                <h3>levis</h3>
                                <h4>Men slim fit Jeans</h4>
                                <p>Rs.809 <del>Rs.1799</del> <span>(55%OFF)</span></p>
                            </figcaption>
                        </div>
                    </div>

                </div>
            </div>
        </div>


      </div>
    </div>
  </section>

  <section>
        <div class="FashionBuyArea">
            <div class="container">

                <h3>Fashion edit</h3>
                <h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</h4>

                <div class="row">
                    <div class="col-sm-5">
                        <div class="FashionBuyLeft">
                            <h1>BUY 1 GET 1
                                <span>EXTRA 50% OFF</span> </h1>
                            <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Shop now</a>
                        </div>
                    </div>

                    <div class="col-sm-7">
                        <ul>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/accessories.png"></figure>
                                    <p>casual wear</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/wallet2.png"></figure>
                                    <p>casual wear</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/WATCHS.png"></figure>
                                    <p>casual wear</p>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">
                                    <figure><img src="{{url('/')}}/public/images/accesssories/BAGPACKS.png"></figure>
                                    <p>casual wear</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
  </section>

  <section>
      <div class="ClothingArea">
          <div class="container">
              <div class="row">
                  <div class="col-sm-6">
                      <div class="Clothing">
                          <figure><img src="{{url('/')}}/public/images/accesssories/footer1.png"></figure>
                          <h3>ACCESSORIES</h3>
                          <ul>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">KURTA PAJAMA</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Jackets</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">nehru jacket</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">TRACKSUITS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">casual shirts</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">sweat shirts</a></li> 
                          </ul>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="Clothing">
                          <figure><img src="{{url('/')}}/public/images/accesssories/footer2.png"></figure>
                          <h3>WATCHES</h3>
                          <ul>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">KURTA PAJAMA</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Jackets</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">nehru jacket</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">TRACKSUITS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">casual shirts</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">sweat shirts</a></li> 
                          </ul>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="Clothing">
                          <figure><img src="{{url('/')}}/public/images/accesssories/Footer3.png"></figure>
                          <h3>SUNCLASSES</h3>
                          <ul>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">KURTA PAJAMA</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Jackets</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">nehru jacket</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">TRACKSUITS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">casual shirts</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">sweat shirts</a></li> 
                          </ul>
                      </div>
                  </div>

                  <div class="col-sm-6">
                      <div class="Clothing">
                          <figure><img src="{{url('/')}}/public/images/accesssories/Footer4.png"></figure>
                          <h3>WIRELESS EARPHONES</h3>
                          <ul>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">KURTA PAJAMA</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">Jackets</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">nehru jacket</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">TRACKSUITS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">DENIM JEANS</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">casual shirts</a></li>
                              <li><a href="{{url('/')}}/productlisting?filter_data=Accessories&filter_type=Category">sweat shirts</a></li> 
                          </ul>
                      </div>
                  </div>

              </div>
          </div>
      </div>
  </section>

 
@endsection