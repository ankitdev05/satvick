@extends('layouts.website')

@section('content')
<style>
  
  figure{
    border: 1px solid #e0dbdb;
    width: 100%;
    height: 146px;
    margin: 0px!important;
  }
  h3{margin-top: 0px!important;}
</style>
 <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                <li ><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li>
                                <li><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li>  
                                <li ><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li class="active"><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="UserDashboard">
                            <div class="wishpro">
                                @foreach($return_array as $pros)
                                  <div class="row">
                                    <div class="col-md-4">
                                      <figure>
                                        <a href="{{ route('product/description/', base64_encode($pros['product_id'])) }}"> <img src="{{ $pros['image'] }}"> </a>
                                      </figure>
                                    </div>
                                    <div class="col-md-6">
                                        <figcaption>
                                           <h2>{{ $pros['brand'] }}</h2>
                                           <h3><a href="{{ route('product/description/', base64_encode($pros['product_id'])) }}">{{ $pros['name'] }}</a></h3>
                                           <h5>Rs.{{ $pros['sp'] }} @if($pros['percentage'] != 0) <del>Rs.{{ $pros['mrp'] }}</del> <span>Saved {{ $pros['percentage'] }}%</span> @endif </h5>
                                        </figcaption>
                                        <div class="Spinner">
                                           <p>
                                              <a class="btn btn-danger" href="{{ route('wishremove', $pros['wishid']) }}">remove</a>
                                           </p>
                                        </div>
                                    </div>
                                  </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection