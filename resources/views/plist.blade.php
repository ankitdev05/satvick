@extends('layouts.website')

@section('content')
<div class="listing">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-12">
                <div class="acct">
                    <span><a href="{{ route('home') }}"> Home </a></span>
                    <div class="sep">/</div>
                    @if($urlname['urltype'] == "common")
                        <span><a href="{{ route('shop', $urlname['first']) }}"> Shop </a></span>
                        <div class="sep">/</div>
                        <strong><a href="{{ route('products', [$urlname['first'], base64_encode($urlname['third']), base64_encode($urlname['id'])]) }}">{{$urlname['first']}}  {{$urlname['second']}} </a></strong>

                    @elseif($urlname['urltype'] == "homebrand")
                        <strong><a href="{{ route('product/brand', $urlname['id']) }}"> {{$urlname['second']}} </a></strong>

                    @elseif($urlname['urltype'] == "search")
                        <strong><a href="{{ route('product/search', [$urlname['first'], base64_encode($urlname['id']), $urlname['four']]) }}"> @if($urlname['four'] !=0) {{$urlname['four']}} @endif {{$urlname['first']}}  {{$urlname['second']}} </a></strong>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
<div class="lists">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="orderleft">
                    <div class="acct">
                        <strong> {{$urlname['first']}}  {{$urlname['second']}} </strong>
                        <div class="sep">-</div>
                        <span>{{ count($return_array) }} items</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clear"></div>
</div>
    <div class="clear"></div>

<div class="list">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="Filters">
                    @if($urlname['urltype'] == "common")
                        <h3>filter <a href="{{ route('products', [$urlname['first'], base64_encode($urlname['third']), base64_encode($urlname['id'])]) }}">clear all</a></h3>
                    @elseif($urlname['urltype'] == "homebrand")
                        <h3>filter <a href="{{ route('product/brand', $urlname['id']) }}">clear all</a></h3>
                    @elseif($urlname['urltype'] == "search")
                        <h3>filter <a href="{{ route('product/search', [$urlname['first'], base64_encode($urlname['id']), $urlname['four']]) }}">clear all</a></h3>
                    @endif
                    </div>
            </div>

            @php
                $fsort = '';
            @endphp

            <div class="col-md-9">
                <div class="FilterSort">
                  <div class="Sort">
                    <label for="Name">Sort by:</label>

                    @if($urlname['urltype'] == "common")
                    <form method="post" class="formaction" action="{{ route('filters', [$urlname['first'], base64_encode($urlname['third']), base64_encode($urlname['id'])]) }}">
                    @elseif($urlname['urltype'] == "homebrand")
                    <form method="post" class="formaction" action="{{ route('filters', [$urlname['first'], base64_encode('brand'), base64_encode(0)]) }}">
                    @elseif($urlname['urltype'] == "search")
                    <form method="post" class="formaction" action="{{ route('filters', [$urlname['first'], base64_encode($urlname['third']), base64_encode($urlname['id'])]) }}">
                    @endif

                        @csrf
                        <select name="sortby" id="filtersort">
                          <option value="new" @if(count($fsort) > 1) @if($fsort == "new") selected @endif @endif>WHAT'S NEW</option>
                          
                          <option value="popularity" @if(count($fsort) > 1) @if($fsort == "popularity") selected @endif @endif> POPULARITY</option>

                          <option value="discount" @if(count($fsort) > 1) @if($fsort == "discount") selected @endif @endif>DISCOUNT</option>

                          <option value="low_to_high" @if(count($fsort) > 1) @if($fsort == "low_to_high") selected @endif @endif>PRICE - LOW TO HIGH</option>

                          <option value="high_to_low" @if(count($fsort) > 1) @if($fsort == "high_to_low") selected @endif @endif>PRICE - HIGH TO LOW</option>

                          <option value="delivery_time" @if(count($fsort) > 1) @if($fsort == "delivery_time") selected @endif @endif>DELIVERY TIME</option>
                        </select>
                  </div>
                </div>
            </div>
        </div>
    <div class="clear"></div>
    </div>
    <div class="clear"></div>
</div>
    <div class="clear"></div>

<div class="catmain">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-xs-12">
                <div class="mainsidebar">
                    @if($urlname['second'] == "Flash Sale")
                    @else
                    <div class="cheaks">
                        <div class="leftsidebar">
                            <p>SIZE</p>
                        </div>
                        <form>
                            @foreach($filters['size'] as $fsize)
                                <label class="container">
                                    {{ $fsize }}
                                    <input type="checkbox" name="sizeby" class="filtersize" value="{{ $fsize }}">
                                    <span class="checkmark"></span>
                                </label>
                            @endforeach
                        </form>
                    </div>
                    @endif
                    <div class="cheaks">
                        <div class="leftsidebar">
                            <p>COLOR</p>
                        </div>

                        @php $p=1; @endphp
                        @foreach($filters['color'] as $fcolor)
                            @if($p <= 4)
                                <label class="container">
                                    <div class="blue" style="background-color: {{ $fcolor['hex'] }}"></div>{{ $fcolor['name'] }}
                                    <input type="checkbox" name="colorby" class="filtercolor" value="{{ $fcolor['hex'] }}">
                                    <span class="checkmark"></span>
                                </label>
                            @endif
                            @php $p++; @endphp
                        @endforeach
                        <div class="morecolors" style="display:none;">
                            @php $p=1; @endphp
                            @foreach($filters['color'] as $fcolor)
                                @if($p > 4)
                                    <label class="container">
                                        <div class="blue" style="background-color: {{ $fcolor['hex'] }}"></div>{{ $fcolor['name'] }}
                                        <input type="checkbox" name="colorby" class="filtercolor" value="{{ $fcolor['hex'] }}">
                                        <span class="checkmark"></span>
                                    </label>
                                @endif
                                @php $p++; @endphp
                            @endforeach
                        </div>
                        <div class="ref">
                            <a id="refcolors" href="javascript:void(0)">+ {{ count($filters['color']) - 4 }} more</a>
                        </div>
                        <div class="ref">
                            <a id="refcolorsminus" href="javascript:void(0)" style="display:none;">- {{ count($filters['color'])}} less</a>
                        </div>
                    </div>
                    @if(count($filters['brand']) > 0)
                    <div class="cheaks">
                        <div class="leftsidebar">
                            <p>BRAND</p>
                        </div>
                        @foreach($filters['brand'] as $brand)
                        <label class="container">
                            {{ $brand }}
                            <input type="checkbox" name="brandby" class="filterbrand" value="{{$brand}}">
                            <span class="checkmark"></span>
                        </label>
                        @endforeach
                    </div>
                    @else
                    <div class="cheaks">
                        <div class="leftsidebar">
                            <p>BRAND</p>
                        </div>
                        @if(isset($urlname['four']))
                        <label class="container">
                            {{ $urlname['four'] ?? '' }}
                            <input type="checkbox" name="brandby" class="filterbrand" value="{{ $urlname['four'] ?? '' }}" checked="">
                            <span class="checkmark"></span>
                        </label>
                        @endif
                    </div>
                    @endif
                    
                    <div class="RangeSlider">
                        <h4>Price</h4>
                        <div class="selector">              
                            <span id="min-price" class="slider-price">100</span> 
                            <span id="max-price" data-max="10000" class="slider-price">10000</span>
                            <div id="slider-range">
                                <span tabindex="100" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                                <span tabindex="10000" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                            </div>
                        </div>
                    </div>
                </div>

                
                </form>


            </div>
            <div class="col-md-9 col-xs-12">
                <div class="ordercenter" id="orderfilterchange">
                
                @if(count($return_array) <= 0)
                    <p style='color:black;'>No Record Found!</p>
                @else

                    @foreach ($return_array as  $value)
                        <div class="col-md-3 col-xs-12">
                            <a href="{{ route('product/description/', base64_encode($value['product_id'])) }}">
                                <div class="lis ProductListing">
                                    <figure><img src="{{$value['image']}}"></figure>
                                    <p>{{$value['brand']}}</p>
                                    <strong>{{$value['name']}}</strong>
                                    <div class="cros">
                                        <p>Rs.{{$value['sp']}}</p>
                                        @if($value['percentage'] != 0)
                                        <span>Rs.{{$value['mrp']}} </span>
                                        <strong>  ({{$value['percentage']}}%OFF)</strong>
                                        @endif
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                @endif
                </div>
            </div>

        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $("#refcolors").on('click', function(){
            $(".morecolors").css('display','block');
            $("#refcolors").css('display','none');
            $("#refcolorsminus").css('display','block');
        });
        $("#refcolorsminus").on('click', function(){
            $(".morecolors").css('display','none');
            $("#refcolors").css('display','block');
            $("#refcolorsminus").css('display','none');
        });
    });

    $(document).ready(function() {
        $("#filtersort").on('change', function() {
            var sort = $(this).val();
            var sizearr = [];
            $("input[name='sizeby']:checked").each(function() {
                console.log($(this).val());
                sizearr.push($(this).val()); 
            });
            var colorarr = [];
            $("input[name='colorby']:checked").each(function() {
                console.log($(this).val());
                colorarr.push($(this).val()); 
            });
            var brandarr = [];
            $("input[name='brandby']:checked").each(function() {
                console.log($(this).val());
                brandarr.push($(this).val()); 
            });

            var minprice = $("#min-price").html();
            var maxprice = $("#max-price").html();
            var urlform = $('.formaction').attr('action');

            $.ajax({
                type: "POST",
                url: urlform,
                data: {sort:sort,sizearr:sizearr,colorarr:colorarr,brandarr:brandarr,minprice:minprice,maxprice:maxprice,_token:'{{csrf_token()}}'},
                success: function(response) {
                    console.log(response);
                    $('#orderfilterchange').html(response);
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });
    $(document).ready(function() {
        $(".filtersize").on('click', function() {
            $(this).attr("checked", "checked");
            var sizearr = [];
            $("input[name='sizeby']:checked").each(function() {
                console.log($(this).val());
                sizearr.push($(this).val()); 
            });
            var colorarr = [];
            $("input[name='colorby']:checked").each(function() {
                console.log($(this).val());
                colorarr.push($(this).val()); 
            });
            var brandarr = [];
            $("input[name='brandby']:checked").each(function() {
                console.log($(this).val());
                brandarr.push($(this).val()); 
            });

            var minprice = $("#min-price").html();
            var maxprice = $("#max-price").html();
            var sort = $("#filtersort").val();
            var urlform = $('.formaction').attr('action');

            $.ajax({
                type: "POST",
                url: urlform,
                data: {sort:sort,sizearr:sizearr,colorarr:colorarr,brandarr:brandarr,minprice:minprice,maxprice:maxprice,_token:'{{csrf_token()}}'},
                success: function(response) {
                    console.log(response);
                    $('#orderfilterchange').html(response);
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });
    $(document).ready(function() {
        $(".filtercolor").on('click', function() {
            $(this).attr("checked", "checked");
            var colorarr = [];
            $("input[name='colorby']:checked").each(function() {
                console.log($(this).val());
                colorarr.push($(this).val()); 
            });
            console.log(colorarr);
            var sizearr = [];
            $("input[name='sizeby']:checked").each(function() {
                console.log($(this).val());
                sizearr.push($(this).val()); 
            });
            var brandarr = [];
            $("input[name='brandby']:checked").each(function() {
                console.log($(this).val());
                brandarr.push($(this).val()); 
            });

            var minprice = $("#min-price").html();
            var maxprice = $("#max-price").html();
            var sort = $("#filtersort").val();
            var urlform = $('.formaction').attr('action');

            $.ajax({
                type: "POST",
                url: urlform,
                data: {sort:sort,sizearr:sizearr,colorarr:colorarr,brandarr:brandarr,minprice:minprice,maxprice:maxprice,_token:'{{csrf_token()}}'},
                success: function(response) {
                    $('#orderfilterchange').html(response);
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });
    $(document).ready(function() {
        $(".filterbrand").on('click', function() {
            $(this).attr("checked", "checked");
            var brandarr = [];
            $("input[name='brandby']:checked").each(function() {
                console.log($(this).val());
                brandarr.push($(this).val()); 
            });
            var colorarr = [];
            $("input[name='colorby']:checked").each(function() {
                console.log($(this).val());
                colorarr.push($(this).val()); 
            });
            var sizearr = [];
            $("input[name='sizeby']:checked").each(function() {
                console.log($(this).val());
                sizearr.push($(this).val()); 
            });

            var minprice = $("#min-price").html();
            var maxprice = $("#max-price").html();
            var sort = $("#filtersort").val();
            var urlform = $('.formaction').attr('action');

            $.ajax({
                type: "POST",
                url: urlform,
                data: {sort:sort,sizearr:sizearr,colorarr:colorarr,brandarr:brandarr,minprice:minprice,maxprice:maxprice,_token:'{{csrf_token()}}'},
                success: function(response) {
                    console.log(response);
                    $('#orderfilterchange').html(response);
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });

    $(document).ready(function() {
        $(".filterbrand").on('click', function() {
            $(this).attr("checked", "checked");
            var brandarr = [];
            $("input[name='brandby']:checked").each(function() {
                console.log($(this).val());
                brandarr.push($(this).val()); 
            });
            var colorarr = [];
            $("input[name='colorby']:checked").each(function() {
                console.log($(this).val());
                colorarr.push($(this).val()); 
            });
            var sizearr = [];
            $("input[name='sizeby']:checked").each(function() {
                console.log($(this).val());
                sizearr.push($(this).val()); 
            });

            var minprice = $("#min-price").html();
            var maxprice = $("#max-price").html();

            var sort = $("#filtersort").val();
            var urlform = $('.formaction').attr('action');

            $.ajax({
                type: "POST",
                url: urlform,
                data: {sort:sort,sizearr:sizearr,colorarr:colorarr,brandarr:brandarr,minprice:minprice,maxprice:maxprice,_token:'{{csrf_token()}}'},
                success: function(response) {
                    console.log(response);
                    $('#orderfilterchange').html(response);
                },
                error: function() {
                    console.log('error');
                }
            });
        });
    });
</script>

<script src='https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js'></script> 
<script type="text/javascript">
    $("#slider-range").slider({
        range: true, 
        min: 100,
        max: 10000,
        step: 50,
        slide: function( event, ui ) {
            $( "#min-price").html(ui.values[ 0 ]);
            suffix = '';
            if (ui.values[ 1 ] == $( "#max-price").data('max') ){
               //suffix = ' +';
            }
            $( "#max-price").html(ui.values[ 1 ] + suffix);

            var brandarr = [];
            $("input[name='brandby']:checked").each(function() {
                console.log($(this).val());
                brandarr.push($(this).val()); 
            });
            var colorarr = [];
            $("input[name='colorby']:checked").each(function() {
                console.log($(this).val());
                colorarr.push($(this).val()); 
            });
            var sizearr = [];
            $("input[name='sizeby']:checked").each(function() {
                console.log($(this).val());
                sizearr.push($(this).val()); 
            });

            var minprice = $("#min-price").html();
            var maxprice = $("#max-price").html();

            var sort = $("#filtersort").val();
            var urlform = $('.formaction').attr('action');
            console.log(sizearr);
            console.log(colorarr);
            console.log(brandarr);
            console.log(minprice);
            console.log(maxprice);
            $.ajax({
                type: "POST",
                url: urlform,
                data: {sort:sort,sizearr:sizearr,colorarr:colorarr,brandarr:brandarr,minprice:minprice,maxprice:maxprice,_token:'{{csrf_token()}}'},
                success: function(response) {
                    console.log(response);
                    $('#orderfilterchange').html(response);
                },
                error: function() {
                    console.log('error');
                }
            });
        }
    })
</script>
@endsection

