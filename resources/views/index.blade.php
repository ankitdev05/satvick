@extends('layouts.website')
@section('content')


<section>
   <div class="container">
      @if(count($announceArr) > 0)
      <div class="Exclusive_offer" style="color:#fff;">
        @if(isset($announceArr[0]))
         {{ $announceArr[0]['content'] }} <a href="{{ route('products', [$announceArr[0]['cat'], base64_encode('subsubcat'), base64_encode($announceArr[0]['subcat'])]) }}"> Known more </a>
        @endif
      </div>

      @if(isset($announceArr[1]))
      <div class="Exclusive_offer" style="background-color:#723e8b;color:#fff;">
         {{ $announceArr[1]['content'] }}
      </div>
      @endif
      
      @endif
   </div>
</section>

 

@foreach ($priority as $pri)

  @switch($pri->name)
    @case ('section1')
      @if($return_array['banners'])

        <section>
          <div class="CodeArea">
            <div class="container">
              <div class="CodeImages">
                <div id="Watch" class="watchMy owl-carousel owl-theme">
                  @if($return_array['banners'])
                  @foreach ($return_array['banners'] as $value)
                  <div class="item">
                    <div class="watch ">
                      <a href="{{ route('products', [$value['cat'], base64_encode('subsubcat'), base64_encode($value['filter_data'])]) }}"><img src="{{ $value['image'] }}" width="100%;"></a>
                    </div>
                  </div>
                  @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </section>
      @endif
        @break

    @case ('section2')
        <section>
          <div class="DressArea">
            <div class="container">
              <div class="row">
               
                <div class="col-sm-4">
                  <div class="Dressbox">
                    <figure>
                      <a href="#"><img src="public/images/female.png"></a>
                    </figure>
                    <figcaption>
                      <h3>Women</h3>
                      <ul>
                        @if($womansubcategory)
                        @foreach ($womansubcategory as $key => $value)
                          <li><a href="{{ route('products', [$value->category_id, base64_encode('subcat'), base64_encode($value->id)]) }}">{{ $value->name }}</a></li>
                        @endforeach
                        @endif
                      </ul>
                      <a href="{{ route('shop','Women') }}" class="shop">shop all <i class="fa fa-caret-right"></i></a>
                    </figcaption>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="Dressbox">
                    <figure>
                      <a href="#"><img src="public/images/manq.png"></a>
                    </figure>
                    <figcaption>
                      <h3>Men</h3>
                      <ul>
                        @if($mansubcategory)
                        @foreach ($mansubcategory as $key => $value)
                          <li><a href="{{ route('products', [$value->category_id, base64_encode('subcat'), base64_encode($value->id)]) }}">{{ $value->name }}</a></li>
                        @endforeach
                        @endif
                      </ul>
                      <a href="{{ route('shop','Men') }}" class="shop">shop all <i class="fa fa-caret-right"></i></a>
                    </figcaption>
                  </div>
                </div>

                <div class="col-sm-4">
                  <div class="Dressbox">
                    <figure>
                      <a href="#"><img src="public/images/girl.png"></a>
                    </figure>
                    <figcaption>
                      <h3>Kids</h3>
                      <ul>
                        @if($kidsubcategory)
                        @foreach ($kidsubcategory as $key => $value)
                          <li><a href="{{ route('products', [$value->category_id, base64_encode('subcat'), base64_encode($value->id)]) }}">{{ $value->name }}</a></li>
                        @endforeach
                        @endif
                      </ul>
                      <a href="{{ route('shop','Kids') }}" class="shop">shop all <i class="fa fa-caret-right"></i></a>
                    </figcaption>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        @break

    @case ('section3')

        <section>
          <div class="discount">
            <div class="container">
              <div class="row">
              

              <div id="HotDeal" class="owl-carousel owl-theme">
                  @if($return_array['hotdeal'])
                  @foreach($return_array['hotdeal'] as $deal)
                    <div class="item">
                      <div class="pics">
                    <div class="hot">
                      <a href="{{ route('products', [$deal['cat'], base64_encode('subsubcat'), base64_encode($deal['filter_data'])]) }}">
                        <img src="{{ $deal['image'] }}">
                      </a>
                    </div>
                    <div class="line1"></div>
                    <div class="extra"></div>
                  </div>
                    </div>
                  @endforeach
                  @endif
                </div>

                 
              
              </div>
            </div>
          </div>
          <div class="clear"></div>
        </section>
        @break

    @case ('section4')

      @if(!empty($return_array['flash_sale_status']))
        <section>
          <input type="hidden" name="ok" id="flash_sale_time" value="{{ $return_array['flash_sale_time'] }}">
          <div class="FlashArea">
            <div class="container">
              <h1><img src="public/images/flash.png"> Flash Sale</h1>
              <div class="FlashHead">
                <p>End in</p>
                <p id="demo"></p>
              </div>
              <div class="FlashBody">
                <div id="owl-demo" class="owl-carousel owl-theme">
                  @foreach ($return_array['flash_sale'] as $value)
                    <div class="item">
                      <div class="FlashBox">
                          <figure>
                              <span>{{ $value['percentage'] }}%</span>
                              <a href="{{ route('product/description/', base64_encode($value['product_id'])) }}"><img src="{{ $value['image'] }}"></a>
                          </figure>
                          <figcaption>
                              <p><i class="fa fa-inr"></i>{{ $value['sp'] }}  @if($value['percentage'] != 0)<sup><i class="fa fa-inr"></i>{{ $value['mrp'] }}</sup> @endif </p>
                          </figcaption>
                      </div>
                    </div>
                  @endforeach
                </div>
              </div>
            </div>
          </div>
        </section>
      @endif
        @break

    @case ('section5')

        <section>
          <div class="container">
            <div class="StyleArea">
              <div class="StyleHead">
                <h2>STYLE FEED</h2>
                <p>Outfit ideas, editor picks, styling inspiration and Face + Body tips</p>
              </div>
              <div class="StyleBody">
                <div id="owl-demo2" class="owl-carousel owl-theme">
                  @if($return_array['stylefeed'])
                  @foreach ($return_array['stylefeed'] as $value)
                    <div class="item">
                      <div class="StyleBox">
                        <figure>
                            <a href="{{ route('products', [$value['cat'], base64_encode('subsubcat'), base64_encode($value['filter_data'])]) }}">
                                <img src="{{ $value['image'] }}">
                            </a>
                        </figure>
                      </div>
                    </div>
                  @endforeach
                  @endif
                </div>
              </div>
              <div class="seeall">
                  
              </div>
            </div>
          </div>
        </section>
        @break;

    @case ('section7')

        <section>
          <div class="WatchArea">
            <div class="container">
                <div class="brand">
                    <P>BRANDS IN FOCUS</P>
                </div>

                <div class="BRANDS">
                    <div id="owl-demo4" class="owl-carousel owl-theme">
                      @if($return_array['brandinfocus'])
                      @foreach($return_array['brandinfocus'] as $brandinfocus)
                        <div class="item">
                            <div class="wat">
                                <a href="{{ route('product/brand', $brandinfocus['filter_data']) }}"><img src="{{ $brandinfocus['image'] }}" style="width: 100%;height: 200px;"></a>
                            </div>
                            <div class="watex">
                                <P> {{ $brandinfocus['filter_data'] }} </P>
                            </div>
                        </div>
                      @endforeach
                      @endif
                    </div>

                    <div class="owl-controls clickable">
                        <div class="owl-pagination">
                            <div class="owl-page active"><span class=""></span></div>
                            <div class="owl-page"><span class=""></span></div>
                        </div>
                        <div class="owl-buttons">
                            <div class="owl-prev">prev</div>
                            <div class="owl-next">next</div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </section>
        @break

    @case ('section8')

        <section>
          <div class="GymArea">
            <div class="container">
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  @php $xx=1; @endphp
                  @if(count($return_array['banner2'])>0)
                  @foreach ($return_array['banner2'] as $value)
                    @if($xx <4 )
                      <div class="gym">
                          <a href="{{ route('products', [$value['cat'], base64_encode('subsubcat'), base64_encode($value['filter_data'])]) }}"> <img src="{{ $value['image'] }}"></a>
                      </div>
                    @endif
                    @php $xx++; @endphp
                  @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </section>
        @break

    @case ('section9')

        <section>
          <div class="GymArea">
            <div class="container"> 
              <div class="gymimg">
                <div class="row">';
                  @php $xx=1; @endphp
                  
                  @if($return_array['slider2'])
                  
                  @foreach ($return_array['slider2'] as $value)
                  @if($xx <=4 )
                    <div class="col-md-3 col-xs-12">
                      <div class="GymBox">
                        <a href="{{ route('products', [$value['cat'], base64_encode('subsubcat'), base64_encode($value['filter_data'])]) }}"> 
                          <img src="{{ $value['image'] }}">
                        </a>
                      </div>
                    </div>
                    @endif
                     @php $xx++; @endphp
                  @endforeach

                  @endif
                </div>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </section>
        @break

    @case ('section10')

        <section>
          <div class="GymArea">
            <div class="container">
              <div class="row">
                <div class="col-md-12 col-xs-12">
                  @php $xx=1; @endphp
                  @if(count($return_array['banner1']) > 0)
                  @foreach ($return_array['banner1'] as $value)
                    @if($xx <4 )
                      <div class="gym">
                          <a href="{{ route('products', [$value['cat'], base64_encode('subsubcat'), base64_encode($value['filter_data'])]) }}"> <img src="{{ $value['image'] }}"></a>
                      </div>
                    @endif
                    @php $xx++; @endphp
                  @endforeach
                  @endif
                </div>
              </div>
            </div>
          </div>
        </section>
        @break

    @case ('section11')

        <section>
          <div class="GymArea">
            <div class="container"> 
              <div class="gymimg">
                <div class="row">';
                  @php $xx=1; @endphp

                  @if($return_array['slider1'])

                  @foreach ($return_array['slider1'] as $value)
                  @if($xx <=4 )
                    <div class="col-md-3 col-xs-12">
                      <div class="GymBox">
                        <a href="{{ route('products', [$value['cat'], base64_encode('subsubcat'), base64_encode($value['filter_data'])]) }}"> 
                          <img src="{{ $value['image'] }}">
                        </a>
                      </div>
                    </div>
                    @endif
                     @php $xx++; @endphp
                  @endforeach

                  @endif
                </div>
                <div class="clear"></div>
              </div>
            </div>
          </div>
        </section>
        @break

    @default

        <section>
          <div class="PickedArea">
            <h1><span>handpicked</span></h1>

            @if($return_array['handpicked'])

            @php $handp = $return_array['handpicked']; @endphp
            <div class="container">
                <div class="row">
                  <div class="col-sm-4"> 
                    @if(isset($handp[0]))
                    <div class="PickBox">
                      <figure><img src="{{ $handp[0]['image'] }}"></figure>
                      <figcaption><a href="{{ route('products', [$handp[0]['cat'], base64_encode('subsubcat'), base64_encode($handp[0]['filter_data'])]) }}">Shop</a></figcaption>
                    </div>
                    @endif
                    @if(isset($handp[1]))
                    <div class="PickBox">
                      <figure><img src="{{ $handp[1]['image'] }}"></figure>
                      <figcaption><a href="{{ route('products', [$handp[1]['cat'], base64_encode('subsubcat'), base64_encode($handp[1]['filter_data'])]) }}">Shop</a></figcaption>
                    </div>
                    @endif
                  </div>
                  <div class="col-sm-4">
                    <div class="PickBox">
                        <figure class="popcake"><img src="public/images/hand-3.png"></figure>
                        <figcaption></figcaption>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    @if(isset($handp[2]))
                    <div class="PickBox">
                        <figure><img src="{{ $handp[2]['image'] }}"></figure>
                        <figcaption><a href="{{ route('products', [$handp[2]['cat'], base64_encode('subsubcat'), base64_encode($handp[2]['filter_data'])]) }}">Shop</a></figcaption>
                    </div>
                    @endif

                    @if(isset($handp[3]))
                    <div class="PickBox">
                        <figure><img src="{{ $handp[3]['image'] }}"></figure>
                        <figcaption><a href="{{ route('products', [$handp[3]['cat'], base64_encode('subsubcat'), base64_encode($handp[3]['filter_data'])]) }}">Shop</a></figcaption>
                    </div>
                    @endif
                  </div>
                </div>
            </div>

            @endif
          </div>
        </section>

    @endswitch

@endforeach


<script>
   // Set the date we're counting down to]
   //var flash_sale_time = document.getElementById("flash_sale_time").value;
   
   //var countDownDate = new Date(flash_sale_time).getTime();

    var countDownDate = <?php echo strtotime($return_array['flash_sale_time']) ?> * 1000;
   
   var now = <?php echo time() ?> * 1000;

  // Update the count down every 1 second
  var x = setInterval(function() {
    now = now + 1000;
    // Find the distance between now an the count down date
    var distance = countDownDate - now;
       
     // Time calculations for days, hours, minutes and seconds
     var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);
       
     // Output the result in an element with id="demo"
     document.getElementById("demo").innerHTML = '<div class="countdown" id="my-countdown" data-end-date="2019-08-20T19:00:00+0200"><span class="days" id="days1" title="Days">'+days+'</span><span class="hours" id="hours1" title="Hours">'+hours+'</span><span class="minutes" id="minutes1" title="Minutes">'+minutes+'</span><span class="seconds" id="seconds1" title="Seconds">'+seconds+'</span></div>';
   
      
       
     // If the count down is over, write some text 
     if (distance < 0) {
       clearInterval(x);
       document.getElementById("demo").innerHTML = "EXPIRED";
     }
   }, 1000);
</script>

@endsection

