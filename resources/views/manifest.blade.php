<!DOCTYPE html>
<html>
<head>
	
	<title></title>
<!-- 
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap" rel="stylesheet">
   -->
	<style type="text/css">
		@media print {
		   body {
		      -webkit-print-color-adjust: exact;
		   }
		}
	</style>

</head>
<body style="font-family: 'D-DIN', sans-serif; margin: 0; background-color: #edebeb; padding: 10px; color: #3a3a3a; box-sizing: border-box;"> 
 
	<div style=" margin: auto; background-color: #fff; padding: 10px; box-sizing: border-box;">

	    <div style="margin: 0 0 25px 0; border: 1px solid #ddd; padding: 25px; box-sizing: border-box;">
 	        <aside style="float: left; width: 47%">
	        	<p style="margin: 0 0 10px; font-size: 15px; color: #3a3a3a; font-family: 'D-DIN'; display: block; line-height:24px;">
	        		<span style="width: 180px; font-weight: 500; display: inline-block; ">ORDERD THROUGH</span>
	        		<span style="width: 10px; text-align: center; display:inline-block;">:</span>
	        		<span style=" font-weight: 500; color: #000; "> YOD</span>
	        	</p>
	        	<p style="margin: 0 0 10px; font-size: 15px; color: #3a3a3a; font-family: 'D-DIN'; display: block; line-height:24px;">
	        		<span style="width: 110px; font-weight: 500; display: inline-block; ">
	        			<img src="http://mobuloustech.com/yodapi/public/images/Logo.png" width="90px">
	        		</span> 
	        	</p> 
	        </aside>

	        <aside style="float: right; width: 47%">
	        	<p style="margin: 0 0 10px; font-size: 15px; color: #3a3a3a; font-family: 'D-DIN'; display: block; line-height:24px;">
	        		<span style="width: 180px; font-weight: 500; display: inline-block; ">Document Number</span>
	        		<span style="width: 10px; text-align: center; display:inline-block;">:</span>
	        		<span style=" font-weight: 500; color: #000; "> {{ rand(10000, 9999999) }} </span>
	        	</p>
	        	<p style="margin: 0 0 10px; font-size: 15px; color: #3a3a3a; font-family: 'D-DIN'; display: block; line-height:24px;">
	        		<span style="width: 180px; font-weight: 500; display: inline-block; ">Total Shipment to Dispatch</span>
	        		<span style="width: 10px; text-align: center; display:inline-block;">:</span>
	        		<span> {{ $items['ordercount'] }} </span>
	        	</p>
	        	<p style="margin: 0 0 10px; font-size: 15px; color: #3a3a3a; font-family: 'D-DIN'; display: block; line-height:24px;">
	        		<span style="width: 180px; font-weight: 500; display: inline-block; ">Total Shipment to Check</span>
	        		<span style="width: 10px; text-align: center; display:inline-block;">:</span>
	        		<span>0</span>
	        	</p> 
	        </aside>

	        <div style="clear: both"></div>
	    </div>
     
	    <div style="margin: 0 0 30px 0; box-sizing: border-box;">
	    	<table style=" box-sizing: border-box; width: 100%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd; margin-bottom: 25px">
	    		<tr style=" box-sizing: border-box;">
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				S.No
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Tracking ID
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Forms Required
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Order ID
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				RTS Done On
	    			</th>
	    			<th style=" background-color: #dcdcdc; font-weight: 600; text-transform: capitalize; color: #000; font-size: 13px; box-sizing: border-box; padding: 8px 10px; text-align: left;">
	    				Notes
	    			</th> 
	    		</tr>

	    		@foreach($items['lists'] as $list)
	    		<tr>
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 12px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				1.
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 12px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $list['wbn'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 12px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				 
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 12px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $list['order_number'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 12px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				{{ $list['date'] }}
	    			</td>

	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 12px 15px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;">
	    				 
	    			</td> 
	    		</tr> 
	    		@endforeach
	    	</table>

	    	<table style="border: 1px dotted #787878; width: 100%; box-sizing: border-box; margin: 0 0 30px 0; border-left: none;  border-right: none; padding: 10px 0;">
	    		<tr>
	    			<td style=" color: #404040; ">
	    				<p style="margin: 0; text-align: center; text-transform: uppercase; font-weight: 500; font-size: 13px; color: #000; letter-spacing: 0.5px;">TO be filled by ekart logistics executive</p>
	    			</td>
	    		</tr>
	    	</table>

	    	<table style="box-sizing: border-box; width: 100%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd; margin-bottom: 25px">
	    		<tr>
	    			<td style="width:100%; border-collapse: collapse; box-sizing: border-box; border-spacing: 0 ">
	    				<table style=" box-sizing: border-box; width:100%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd;"> 
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 150px;background-color: #f3f3f3;">
				    				Pickup In Time
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    		
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 150px;background-color: #f3f3f3;">
				    				Pickup Out Time
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    		
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 150px;background-color: #f3f3f3;">
				    				FE Name
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    		
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 150px;background-color: #f3f3f3;">
				    				FE Signature
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    	
				    	</table>
	    			</td>
	    		</tr>
	    		<tr>
	    			<td style="width:100%; border-collapse: collapse; box-sizing: border-box; border-spacing: 0">
	    				<table style=" box-sizing: border-box; width:100%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd;"> 
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width:150px;background-color: #f3f3f3;">
				    				Total Item Picked
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width:150px;background-color: #f3f3f3;">
				    				All Shipment have yod packaging
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 9px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				
				    				<label style="font-weight: 400; margin: 0 25px 0px 0;  display: inline-block; font-size: 15px;">
				    					<input style="margin: 0px 5px 0 0" type="radio" name="optradio" checked>Yes
				    				</label>
				    				  
				    				<label style="font-weight: 400; margin: 0 25px 0px 0;  display: inline-block; font-size: 15px;">
				    					<input style="margin: 0px 5px 0 0" type="radio" name="optradio" checked>No
				    				</label>

				    			</td> 
				    		</tr>
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width:150px;background-color: #f3f3f3;">
				    				Seller Name
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    		<tr>	    			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width:150px;background-color: #f3f3f3;">
				    				Seller Signature
				    			</td>   			
				    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
				    				: 
				    			</td>   			
				    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
				    				 
				    			</td> 
				    		</tr>
				    	</table>
	    			</td>
	    		</tr>
	    	</table>

	    </div>

	    <!-- <div style="border: 1px dotted #787878; box-sizing: border-box; margin: 0 0 30px 0; border-left: none;  border-right: none; padding: 10px 0;">
	    	<p style="margin: 0; text-align: center; text-transform: uppercase; font-weight: 500; color: #000; letter-spacing: 0.5px;">TO be filled by ekart logistics executive</p>
	    </div> -->

<!-- 
	    <div style="margin: 0 0 0px 0; box-sizing: border-box;">
	    	<table style=" box-sizing: border-box; width:45%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd; float: left;"> 
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				Pickup In Time
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				Pickup Out Time
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				FE Name
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				FE Signature
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    	</table>
	    	<table style=" box-sizing: border-box; width:45%; border-collapse: collapse; border-spacing: 0;border: 1px solid #ddd; float: right;"> 
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				Total Item Picked
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				All Shipment have yod packaging
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 19px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				
	    				<label style="font-weight: 400; margin: 0 25px 0px 0;  display: inline-block; font-size: 15px;">
	    					<input style="margin: 0px 5px 0 0" type="radio" name="optradio" checked>Yes
	    				</label>
	    				  
	    				<label style="font-weight: 400; margin: 0 25px 0px 0;  display: inline-block; font-size: 15px;">
	    					<input style="margin: 0px 5px 0 0" type="radio" name="optradio" checked>No
	    				</label>

	    			</td> 
	    		</tr>
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				Seller Name
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    		<tr>	    			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px;  border-bottom: 1px solid #ddd; width: 350px;background-color: #f3f3f3;">
	    				Seller Signature
	    			</td>   			
	    			<td style="font-weight: 500; font-size: 13px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd; text-align: center; width: 50px;background-color: #f3f3f3;">
	    				: 
	    			</td>   			
	    			<td style="font-size: 14px; vertical-align: top; color: #404040; padding: 8px 10px; border-right: 1px solid #ddd; border-bottom: 1px solid #ddd;width: 400px; font-weight: 600; color:#000; font-family:'D-DIN';">
	    				 
	    			</td> 
	    		</tr>
	    	</table>
	    	<div style="clear: both;"></div>
	    </div> -->

    </div>

</body>
</html>