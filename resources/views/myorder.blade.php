@extends('layouts.website')

@section('content')
<?php
$currencyCode = Session::get('currency_code');
if($currencyCode == "INR") {
    $currencyCodeLabel = "Rs";
} else {
    $currencyCodeLabel = $currencyCode;
}
?>
 <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                    @if(Session::has('message_add'))
                        <p class="alert alert-info">{{ Session::get('message_add') }}</p>
                    @endif
                    
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                 <li ><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li>
                                <li><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li>  
                                 <li ><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li class="active"><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="col-sm-9">
                        <div class="UserDashboard">

                            <div class="MyOrderArea">
                                <div class="BagOrder">
                                    <article>                                       
                                    @if(count($return_array) > 0 )
                                    @php $k=1; @endphp
                                    @foreach($return_array as $orderArr)
                                        <aside>
                                            <h1>Order no |  <span> {{ $orderArr['order_number'] }} </span></h1>
                                            <figure>
                                                <a href="{{ route('product/description/', base64_encode($orderArr['product_id'])) }}"><img src="{{ $orderArr['image'] }}"></a>
                                            </figure>
                                            <figcaption>
                                                <h2><a href="{{ route('product/description/', base64_encode($orderArr['product_id'])) }}">{{ $orderArr['product_name'] }}</a> <a href="Javascript:void(0);" data-toggle="modal" data-target="#Track{{ $k }}"><i class="fa fa-map"></i> Track Order</a> </h2>
                                                <h3>{{ $orderArr['brand'] }}</h3>
                                                <h4><strong>{{ $currencyCodeLabel }}  {{ $orderArr['amount'] }} @if($orderArr['size']) | {{$orderArr['size_label']}} : {{ $orderArr['size'] }} @endif @if($orderArr['color']) | color: {{ $orderArr['color'] }} @endif </strong></h4>
                                                <h5><strong>Order Satus : </strong> {{ $orderArr['status'] }} </h5>
                                                <h5><strong>Order Date : </strong> {{ $orderArr['order_date'] }} </h5>
                                                <h5><strong>Delivery Date : </strong> {{ $orderArr['dispatch_by'] }} </h5>
                                                <h6><span>Shipping Addrees</span> {{ $orderArr['location'] }} </h6>
                                            </figcaption> 
                                        </aside>

                                        <div id="Track{{ $k }}" class="modal fade track" role="dialog">
                                            <div class="modal-dialog"> 
                                                <div class="modal-content">
                                                    <div class="TrackBox">
                                                        <a href="Javascript:void(0);" data-dismiss="modal">&times;</a>
                                                        <h4>Track Your Product</h4>
                                                        <aside>
                                                        </aside>
                                                        <article>
                                                            <ul>
                                                            @if(count($orderArr['notifyStatus']) > 0)
                                                            @foreach($orderArr['notifyStatus'] as $notify)
                                                                <li> <span> {{ $notify['date'] }} </span> {{ $notify['content'] }} </li>
                                                            @endforeach
                                                            @endif
                                                            </ul>
                                                        </article>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        @php $k++; @endphp
                                    @endforeach
                                    @endif
                                    </article>

 

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
@endsection