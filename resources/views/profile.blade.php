@extends('layouts.website')

@section('content')
<style type="text/css">
    span.formerror {
    float: left;
    width: 100%;
    font-size: 10px;
    color: red;
    margin: 3px auto;
}
</style>
 <section>
        <div class="DashboardArea">
            <div class="container">
                <div class="row">
                
                    <div class="col-sm-3">
                        <div class="SideNavbar">
                            <ul>
                                <li class="active"><a href="{{ route('profile') }}"><i class="fa fa-user"></i>My Profile</a></li> 
                                <li><a href="{{ route('notifications') }}"><i class="fa fa-bell"></i>Notifications</a></li>  
                                <li><a href="{{ route('refer') }}"><i class="fa fa-money"></i>Refer $ Earn</a></li>
                                <li><a href="{{ route('coupons') }}"><i class="fa fa-money"></i>Coupons</a></li>
                                 <li ><a href="{{ route('address') }}"><i class="fa fa-home"></i>Address</a></li> 
                                <li><a href="{{ route('change-password') }}"><i class="fa fa-refresh"></i> Change password</a></li>
                                <li><a href="{{ route('orders') }}"><i class="fa fa-refresh"></i> My Orders </a></li>
                                <li><a href="{{ route('wishlist') }}"><i class="fa fa-refresh"></i> Whishlist </a></li>
                                <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> logout</a></li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="col-sm-9">
                        <div class="UserDashboard">

                            <div class="ProfileArea">
                            <div class="row">
                                <div>
                        @if (Session::has('success'))

                        <div class="alert alert-success" role="alert">
                           <strong>Success:</strong> {{ Session::get('success') }}
                        </div>

                        @endif
                        @if (Session::has('error'))

                        <div class="alert alert-danger" role="alert">
                           <strong>Error:</strong> {{ Session::get('error') }}
                        </div>

                        @endif
                     </div>
                                <div class="col-sm-4">                            
                                    <div class="Profileinfo">
                                        <figure>
                                          
                                            {{-- <a href="#"><i id="editbox" class="fa fa-pencil-square-o" aria-hidden="true"></i></a> --}}
                                            
                                            @if(strlen(Auth::user()->image) > 0)
                                                <img src="{{ url('/public/') . Auth::user()->image }}">
                                            @else
                                                <img src="{{ url('/public/') . '/img/imagenotfound.jpg' }}">
                                            @endif
                                        </figure>
                                        <figcaption>
                                            <h4><span>Name :</span> {{ Auth::user()->name }}</h4>
                                            <h4><span>Email :</span> {{ Auth::user()->email }}</h4>
                                            <h4><span>Contact number :</span> {{ Auth::user()->phone }}</h4>
                                        </figcaption>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                            
                                    <form method="post" action="{{ route('profile-update') }}" enctype="multipart/form-data">
                                        @csrf
                                        <h3>personal infomation</h3>
                                        <p>Euismod Ac penatibus magna vel tempor, porttitor ullamcorper urna, massa natoque venenatis mollis libero neque velit risus, magnis vehicula nam.</p>

                                        <div class="form-group">
                                            <label>Name</label>
                                            <input type="text"  name="name" class="form-control name" value="{{ Auth::user()->name }}">
                                        </div>

                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" name="email" class="form-control name" value="{{ Auth::user()->email }}" disabled="">
                                        </div>

                                        <div class="form-group">
                                            <label>Upload Photo</label>
                                            <input type="file" name="image" class="form-control name name">
                                        </div>

                                        <div class="form-group">
                                            <label>Contact number</label>
                                            <input type="text" name="phone" class="form-control name" value="{{ Auth::user()->phone }}">
                                        </div>
                                       
                                        <button id="submitbutton">Save changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
<script type="text/javascript">

    $("#editbox").click(function(){
           //alert("hello"); 
           $("#submitbutton").css('display','block');
           $(".name").prop("disabled", false);
           
           $("#editbox").css('display','none');
    });

</script>
@endsection