<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    //

    protected $fillable = [
         'name', 'email', 'gstnum', 'phone', 'pan_num', 'password', 'brand', 'catg', 'status','pan_numfile','gstnumfile','address','image','latitude','longitude','state','city','pincode','country'
    ];
}
