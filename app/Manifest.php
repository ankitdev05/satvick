<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manifest extends Model
{
    //

    protected $fillable = [
        'order_id', 'order_count', 'handover_date'
    ];
}
