<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuponcode extends Model
{
    
    protected $fillable = [ 'name','discount','type','min_price','startdate','expdate'];
}
