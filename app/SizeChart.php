<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SizeChart extends Model
{
    //

    protected $fillable = [
         'heading', 'heading_values', 'user_id', 'subsub_cat_id','status'];
}
