<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerMistake extends Model
{
    //

    protected $fillable = [
        'order_id', 'seller_id','type'
    ];
}
