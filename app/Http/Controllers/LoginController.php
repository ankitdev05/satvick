<?php

namespace App\Http\Controllers;

use Illuminate\Pagination\LengthAwarePaginator;
use Propaganistas\LaravelPhone\LaravelPhoneServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\Subcategory;
use App\Cart;
use App\Subsubcategory;
use App\Notification;
use App\Wishlist;
use App\Cuponcode;
use App\PasswordReset;
use App\Color;
use Image;
use Session;
use Validator;
use Illuminate\Support\Facades\Mail;
use App\Mail\ResetPassword;
use Socialite;
use Auth;
use Intervention\Image\ImageServiceProvider;
use Illuminate\Support\Facades\Hash;

class LoginController extends APIBaseController {

	private $baseurlslash;
	private $baseurl;

	public function __construct() {
	  $this->baseurlslash = "http://mobuloustech.com/yodapi/public/";
	  $this->baseurl = "http://mobuloustech.com/yodapi/public";
	}

	public function userlogin(Request $request, MessageBag $message_bag) {
        if ($request->isMethod('post')) {

          if($request->typelogin == "email") {
            
    	        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password,'status'=>1],$request->remember)) {

                  if ($request->session()->has('cartsession')) {
                      $session_id = session::get('cartsession');
                  } else {
                      $session_id = Session::getId();
                      $request->session()->put('cartsession', $session_id);
                  }
                  $getCarts = Cart::where(["user_id"=>$session_id])->get();
                  if($getCarts) {

                      foreach ($getCarts as $getCart) {
                          $checkCart = Cart::where(["user_id"=>Auth::user()->id, "product_id"=>$getCart->product_id])->first();
                          if($checkCart) {

                              $checkCart->quantity = $checkCart->quantity + $getCart->quantity;
                              $checkCart->save();
                              
                          } else {
                              DB::table('carts')->where(["user_id"=>$session_id, "product_id"=>$getCart->product_id])->update(['user_id'=>Auth::user()->id]);
                          }
                      }
                  }

                  return $this->sendResponse(["status"=>1,"messages"=>""], 'Login Successfully',$request->path());

              } else {
                  $check = User::where(['email'=>$request->email])->first();
                
                  if($check) {

                      if($check->status == 0) {
                          Session::flash('_old_input.email',$request->email);
                          $message_bag->add('email','User is inactive.');
                      } else {
                          Session::flash('_old_input.email',$request->email);
                          $message_bag->add('email','These credentials do not match.');  
                      }
                  } else{
                      Session::flash('_old_input.email',$request->email);
                      $message_bag->add('email','These credentials do not match.');
                  }

                  return $this->sendResponse(["status"=>0,"messages"=>$message_bag], 'Error',$request->path());
              }
          } else {
            
              if (Auth::guard('web')->attempt(['phone' => $request->email, 'password' => $request->password,'status'=>1],$request->remember)) {

                  if ($request->session()->has('cartsession')) {
                      $session_id = session::get('cartsession');
                  } else {
                      $session_id = Session::getId();
                      $request->session()->put('cartsession', $session_id);
                  }
                  $getCarts = Cart::where(["user_id"=>$session_id])->get();
                  if($getCarts) {
                       foreach ($getCarts as $getCart) {
                          $checkCart = Cart::where(["user_id"=>Auth::user()->id, "product_id"=>$getCart->product_id])->first();
                          if($checkCart) {

                              $checkCart->quantity = $checkCart->quantity + $getCart->quantity;
                              $checkCart->save();
                              
                          } else {
                              DB::table('carts')->where(["user_id"=>$session_id, "product_id"=>$getCart->product_id])->update(['user_id'=>Auth::user()->id]);
                          }
                      }
                  }

                  return $this->sendResponse(["status"=>1,"messages"=>""], 'Login Successfully',$request->path());

              } else {
                  $check = User::where(['phone'=>$request->email])->first();
                
                  if($check) {

                      if($check->status == 0) {
                          Session::flash('_old_input.email',$request->email);
                          $message_bag->add('email','User is inactive.');
                      } else {
                          Session::flash('_old_input.email',$request->email);
                          $message_bag->add('email','These credentials do not match.');  
                      }
                  } else{
                      Session::flash('_old_input.email',$request->email);
                      $message_bag->add('email','These credentials do not match.');
                  }

                  return $this->sendResponse(["status"=>0,"messages"=>$message_bag], 'Error',$request->path());
              }
          }
        } else {
            return redirect('/');
        }
    }

    public function redirectsocial(Request $request) {
       return Socialite::driver('facebook')->redirect();
    }
   
    public function callback(Request $request, $service) {
        $user = Socialite::with($service)->user();
        $user = $service->createOrGetUser(Socialite::driver($service)->user());
        auth()->login($user);
    }

    public function createOrGetUser(ProviderUser $providerUser) {
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialFacebookAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);

            $user = User::whereEmail($providerUser->getEmail())->first();

            if (!$user) {

                $user = User::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                ]);
            }

            $account->user()->associate($user);
            $account->save();
            return $user;
        }
    }


    public function usersignup(Request $request) {
      if ($request->isMethod('post')) {
          $input = $request->all();

          $val_arr = [
              'password' => 'required',
              'name'=>'required',
              'email'=>'required',
              'phone'=>'required',
              'gender'=>'required',
          ];

          $messages = [
              'name.required'=>'Please enter your Full Name',
              'email.required'=>'Please enter Email ID',
              'phone.required'=>'Please enter Phone Number',
              'password.required'=>'Please enter Password',
          ];

          $validator = Validator::make($input, $val_arr,$messages);
          if($validator->fails()) {     
              $messages =$validator->errors()->toArray();
              return $this->sendResponse(["status"=>"0","messages"=>$messages], 'You login successfully.',$request->path());
          }

          $check_username = User::where('email',$input['email'])->first();
          if (!empty($check_username)) {
              $messages['email'][0] = "Email Already exist";
              return $this->sendResponse(["status"=>"0","messages"=>$messages], 'You login successfully.',$request->path());  
          }
          
          if (!empty($check_phone)) {
              $messages['phone'][0] = "Phone number Already exist";
              return $this->sendResponse(["status"=>"0","messages"=>$messages], 'You login successfully.',$request->path());
          }
          
          $details = User::create([
              "name"=>$input['name'],
              "email"=>$input['email'],
              "password"=> Hash::make($input['password']),
              "phone"=>$input['phone'],
              "gender"=>$input['gender'],
              "status"=>1
          ]);

          return $this->sendResponse(["status"=>"1","messages"=>""], 'Account created successfully.',$request->path());
      } else {
           return redirect('/');
      }
    }
    
    public function forgot(Request $request) {
        return view('forgot');
    }    

    public function forgotpassweb(Request $request) {
       	if ($request->isMethod('post')) {
            $input = $request->all();
  	        $val_arr = [
  	            'email'=>'required',
  	        ];

  	        $message = [
  	            'email.required'=>'Please enter Email ID to continue',
  	        ];

        	  $validator = Validator::make($input, $val_arr,$message);

  	        if($validator->fails()) {
  	          	Session::flash('message_forgot', $validator->errors()->first()); 
  	            unset($_POST);
  	            return back();  
  	        }
        	  $check_mail = DB::select("select * from `users` where email = '".$input['email']."'");

  	        if(empty($check_mail)) {
  	            Session::flash('message_forgot', "This Email ID is not registered with us.");
  	            unset($_POST);
  	            return back();
  	        }

  	        $email = $input['email'];
            $subject = "Your Password";     
            $postData ="";
          
            try {
  				        Mail::to($request->email)->send(new ResetPassword($request->email));
                  Session::flash('message_forgot', "Password reset Link send to your email id.");
                  return redirect()->back();
            } catch(Exception $e) {
                Session::flash('message_forgot',"Mail not send"); 
                unset($_POST);
               	return back();
  		      }
       	} else {
            return redirect('/');
       	}
   	}

    public function reset(Request $request) {
        return view('reset');
    }    

    public function resetpass(Request $request) {
        $reset = PasswordReset::where(["email"=>$request->email,"token"=>$request->otp])->first();
        if($reset) {
            $check = User::where(["email"=>$request->email])->first();
        
            if($check) {
                $check->password = Hash::make($request->password);
                $check->save();
                Session::flash('success', 'Password changed successfully');

                PasswordReset::where(["email"=>$request->email,"token"=>$request->otp])->delete();
                return redirect()->route('home');
            } else{
                Session::flash('success', 'Password changed failed!');
                return redirect()->back();
            }
        } else {
            Session::flash('message_forgot', "Email or OTP is invalid");
            return redirect()->back();
        }
    }

   	public function logout(Request $request) {
    	  Auth::logout();
      	return redirect('/');
   	}


    public function profile(Request $request) {
        return view('profile');
    }

    public function profileupdate(Request $request) {

        $userdetails = User::where('id',Auth::user()->id)->first();

        if($request->image) {
            $filename = substr( md5( Auth::user()->id . '-' . time() ), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
            $path = public_path('users-photos/' . $filename);
            Image::make($request->image)->save($path);
            $userdetails->image = '/users-photos/'.$filename;
        }

        $userdetails->name = $request->name;
        $userdetails->phone = $request->phone;
        $userdetails->save();
        
        Session::flash('success', 'Profile updated successfully');
        return redirect()->route('profile');
    }

    public function address(Request $request) {
        $detailsaddress = Address::where(["user_id"=>Auth::user()->id])->get();
        return view('address',compact("detailsaddress"));
    }

    public function changepassword(Request $request) {
        return view('changepassword');
    }

    public function passwordupdate(Request $request) {
        $this->validate($request, [
            'password' => ['required', 'string', 'min:5','required_with:confirm_password'],
            'confirm_password' => ['required']
        ]);
        $check = User::where(["id"=>Auth::user()->id])->first();
        
        if($check) {
            $check->password = Hash::make($request->password);
            $check->save();
            Session::flash('success', 'Password changed successfully');
            return redirect()->back();
        } else{
            Session::flash('error', 'Password changed failed!');
            return redirect()->back();
        }
        return view('changepassword');
    }

    public function add_address(Request $request) {

        $input = $request->all();

        if(isset($input['remark'])) {
            $input['remark'] = 1;
            DB::table('addresses')->where(['user_id'=>Auth::user()->id,'remark'=>'1'])->update(['remark'=>'0']);
        }
        $input["user_id"] = Auth::user()->id;

        Address::create($input);

        Session::flash('message_add', "Address added successfully."); 
        return redirect()->back();
    }

    public function update_address(Request $request) {
      
      $input = $request->all();
      $input["user_id"] = Auth::user()->id;

      unset($input['_token']);

      if(isset($input['remark'])) {
          $input['remark'] = 1;
          DB::table('addresses')->where(['user_id'=>Auth::user()->id,'remark'=>'1'])->update(['remark'=>'0']);
      }

      Address::where("id",$input['id'])->update($input);
      Session::flash('message_add', "Address updated successfully."); 
      return redirect()->back();
    }

    public function removeaddress(Request $request, $id) {
        DB::table('addresses')->where(['id'=>$id])->delete();
        Session::flash('message_add', "Address removed successfully."); 
        return redirect()->back();
    }

    public function refer(Request $request) {
       return view('refer');   
    }

    public function notification(Request $request) {
      $notifyArr = array();
      $getNotify = Notification::where('user_id',Auth::user()->id)->orderBy('id','DESC')->limit(50)->get();
      if($getNotify) {
        foreach($getNotify as $notify) {
          $notifyArr[] = ["title"=>$notify->title, "content"=>$notify->content];
        }
      }
      return view('notification', compact('notifyArr'));
    }

    public function wishlistproduct(Request $request) {
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }
        $findproductids = Wishlist::where('user_id', Auth::user()->id)->get();
        $return_array = array();
        
        foreach ($findproductids as $value) {
            $productdetails = Product::where('id', $value->product_id)->first();
            if($productdetails) {
              $imagearray = explode(",", $productdetails->images);
          
              if (!empty($imagearray)) {
          
                  if (!empty($imagearray[0])) {
                      if (strpos($imagearray[0], $this->baseurlslash) == false) {
                          $imagearray[0] = $this->baseurl . $imagearray[0];
                      }
                  } else {
                      $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                  }
              } else {
                  $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
              }
              $percentagetemp = 100 - ($productdetails->sp * $currencyValue * 100) / $productdetails->mrp * $currencyValue;
              $return_array[] = array("wishid"=>$value->id, "image" => $imagearray[0], "brand" => $productdetails->brand, "name" => $productdetails->name, "sp" => $productdetails->sp * $currencyValue, "mrp" => $productdetails->mrp * $currencyValue, "percentage" => (string)round($percentagetemp), "product_id" => (string)$productdetails->id);
            }
        }

        return view('wishlist', compact('return_array'));
    }

    public function removefromwish($cid) {
        $getCarts = Wishlist::where(["id"=>$cid])->first();
        if($getCarts) {
            Wishlist::where(["id"=>$cid])->delete();
        }

        return redirect()->back();
    }

    public function myorder() {
      $currencyValue = Session::get('currency_value');
      if(empty($currencyValue)) {
          $currencyValue = 1;
      }
        $return_array = array();

        $details = Order::select('orders.id as orderId','orders.status as orderstatus','orders.created_at as created', 'orders.*','users.*')->join('users', 'users.id', '=', 'orders.user_id')->where(['orders.user_id' => Auth::user()->id])->orderBy('orders.id','DESC')->get();
    
        foreach ($details as $met) {

            $cartfindall = OrderDetail::where(['order_id'=>$met->orderId])->first();
            $detailsproduct = Product::where('id', $met->product_id)->first();
                
            $coupen = Cuponcode::select('discount')->where('name', $met->coupen_code)->first();
            if($coupen) {
                $coupendiscount = (int)$coupen->discount * $currencyValue;
            } else {
                $coupendiscount = 0;
            }
            $percentagetemp = 100 - ($detailsproduct->sp * $currencyValue * 100) / $detailsproduct->mrp * $currencyValue;
                
            $getD = strtotime($met->created_at);
            $addedDate = strtotime("+7 day", $getD);
            $currentDate = date('d/m/y');
            $currentConvert = strtotime($currentDate);

            if($currentConvert > $addedDate) {
                $exchangeStatus = 0;
            } else {
                $exchangeStatus = 1;
            }
            $notifyArr = array();
            $getNotify = Notification::where(['order_id'=>$met->orderId, 'type'=>'order'])->get();

            if($getNotify) {
                foreach($getNotify as $notify) {
                    $notifyArr[] = ["date"=> date("d M", strtotime($notify->updated_at)), "content"=>$notify->content];
                }
            }

            if(!empty($cartfindall->color)) {
                        $colorname = Color::where(["hexcode"=>$cartfindall->color])->first();
                        if($colorname) {
                          $colorname = $colorname->name;
                        } else {
                          $colorname = "";  
                        }
                    } else {
                        $colorname = "";
                    }

            $return_array[] = array(
                "image" => $this->baseurl . explode(",", $detailsproduct->images) [0], 
                "sku" => $detailsproduct->sku, 
                "brand" => $detailsproduct->brand, 
                "product_id" => $detailsproduct->id, 
                "id" => $met->orderId,
                "order_number" => $met->order_number,
                "Buyer" => $met->buyer, 
                "location" => $met->location, 
                "order_date" => date("d/m/Y", strtotime($met->created)), 
                "dispatch_by" => date("d/m/Y", strtotime($met->dispatch_at)), 
                "payment_type" => $met->payment_type, 
                "coupan_code" => !empty($met->coupen_code) ? $met->coupen_code : '', 
                "status" => $met->orderstatus,
                "product_name" => $detailsproduct->name, 
                "amount" => $cartfindall->final_price,
                "percentage" => $coupendiscount,
                "mrp" => $cartfindall->sale_price, 
                "quantity" => $cartfindall->quantity,
                "color" => $colorname, 
                "size" => $cartfindall->size, 
                "exchangeStatus"=>$exchangeStatus,
                "notifyStatus"=>$notifyArr,
                "size_label"=>$detailsproduct->size_label,
            );
        }

        return view('myorder', compact('return_array'));
    }

    public function coupon(Request $request) {
      $details = Cuponcode::get();
      $return_array = array();

      foreach ($details as $met) {
          $flashdatenew = date('Y-m-d H:i:s', strtotime($met->startdate));
          $flashStr = strtotime($flashdatenew);
          $currentDate = date('Y-m-d H:i:s');
          $currentStr = strtotime($currentDate);

          if($currentStr > $flashStr) {
              $flashdateend = date('Y-m-d H:i:s', strtotime($met->expdate));
              $flashStrEnd = strtotime($flashdateend);
              $currentDateEnd = date('Y-m-d H:i:s');
              $currentStrEnd = strtotime($currentDateEnd);

              if($currentStrEnd < $flashStrEnd) {

                  $return_array[] = array("cupon_code" => $met->name, "discount" => $met->discount,"discount_type"=>$met->type, "expiry_date" => date("M d Y", strtotime($met->expdate)), "cupon_id" => $met->id, "min_price" => $met->min_price);
              }
          }
      }
      return view('couponlist', compact('return_array'));
    }
}