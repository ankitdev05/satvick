<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\ProductPrice;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Subsubcategory;
use App\Cuponcode;
use App\Content;
use App\Faq;
use App\Priority;
use App\Notification;
use App\Size;
use App\Color;
use App\Offer;
use App\Announcement;
use App\Refer;
use App\Promotion;
use App\PromotionList;
use App\Attribute;
use App\ReferEarn;
use App\Support;
use App\SizeChart;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;

class ServiceController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "https://mobuloustech.com/satvick/public/";
        $this->baseurl = "https://mobuloustech.com/satvick/public";
    }

    public function notficationlist(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
    
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }

        Notification::where(['user_id'=> $input['user_id']])->update(['status' => 1]);

        $alldetails = Notification::select('id','order_id','order_number', 'title','content', 'created_at')->where(['user_id'=> $input['user_id']])->orderBy('id','DESC')->get();

        return $this->sendResponse($alldetails, 'Notification list retrieved successfully', $request->path());
    }

    public function innerpageapi(Request $request, $id) {
        $input = $request->all();

        if($id == "essential" || $id == "Essential" || $id == "ESSENTIAL") {
            $id = "ESSENTIALS";
        }

        $subcategories_list = Subcategory::where('category_id', $id)->get();
        $subcategorylist_array = array();
        $othercatlist = array();
        
        $px=1;

        foreach ($subcategories_list as $value1) {
        
            if (empty($value1->image)) {
                $value1->image = url('/public/') . "/img/imagenotfound.jpg";
            } else {
                $value1->image = url('/public/') . $value1->image;
            }
            
            $subcategorylist_array[] = array("filter_type" => "", "filter_data" => $value1->id, "image" => $value1->image, "name" => $value1->name);

            if($px <= 4) {
                $productdetails = Product::where(['subcategory_id' => $value1->id])->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->limit(20)->get();
                $temp_datalist = array();
                
                if(count($productdetails) >0) {
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                
                        if (!empty($imagearray)) {
                
                            if (!empty($imagearray[0])) {
                
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                        if (!empty($findflagofwishlist)) {
                            $like_Status = "1";
                        } else {
                            $like_Status = "0";
                        }
                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        
                        if (!empty($value->status)) {
                            $temp_datalist[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                        }
                    }
                    $othercatlist[] = array("section_name" => $value1->name, "subcat_id" => $value1->id, "data" => $temp_datalist);
                }
            }

            $px++;
        }

        $flash_sale = array();
        $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
        
        if (!empty($findflashdetails)) {

            $flashdatenew = date('Y-m-d H:i:s', strtotime($findflashdetails->time));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr >= $flashStr) {

                $flashdateend = date('Y-m-d H:i:s', strtotime($findflashdetails->endtime));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                $timeDiff = abs($flashStrEnd - $currentStrEnd);
                $findflashdetailstime = $timeDiff;

                if($currentStrEnd <= $flashStrEnd) {

                    $findflashdetails->products = trim($findflashdetails->products);

                    if(strlen($findflashdetails->products)>=1) {
                      $productdetails = Product::whereIn('id', explode(",", $findflashdetails->products))->where(["category_id"=>$id, "deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    } else {
                      $productdetails = Product::where(['subsubcategory_id'=>$findflashdetails->subsubcategory])->where(["category_id"=>$id,"deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    }
                
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                
                        if (!empty($imagearray)) {
                            if (!empty($imagearray[0])) {
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                        if (!empty($findflagofwishlist)) {
                            $like_Status = "1";
                        } else {
                            $like_Status = "0";
                        }
                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        $flash_sale[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                    }
                } else {
                    $flash_sale = [];  
                    $findflashdetailstime ="";  
                }
            } else {
                $flash_sale = [];
                $findflashdetailstime ="";
            }
        }
        
        $subcategories_list1 = Subsubcategory::where(['category_id' => $id, 'occasion_status' => '1'])->orderBy('updated_at','DESC')->get();
        $subcategorylist_array1 = array();
        foreach ($subcategories_list1 as $value) {
            if (empty($value->image)) {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $subcategorylist_array1[] = array("filter_type" => "", "filter_data" => $value->id, "image" => $value->image, "name" => $value->name);
        }
        
        $allinnerbanner = Banner::where('type', $id . "_inner")->orderBy('id', 'ASC')->get();
        $subcategorylist_array2 = array();
        foreach ($allinnerbanner as $value) {
            if (empty($value->image)) {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            } else {
                $value->image = url('/public/') . $value->image;
            }

            $getCat = Subcategory::select("name")->where(['id'=>$value->subcategory_id])->first();
            
            $subcategorylist_array2[] = array("filter_type" => "", "filter_data" => $value->subcategory_id, "image" => $value->image, "name" => $getCat->name);
        }

        return $this->sendResponse1(['subcategory_slider' => $subcategorylist_array, 'flash_sale' => $flash_sale, "occasion_list" => $subcategorylist_array1, "bannerlist" => $subcategorylist_array2, "other_section" => $othercatlist,"flash_sale_time" => $findflashdetailstime], 'Inner page details retrieved successfully', $request->path());
    }

    public function cancel_cupon(Request $request) {
        $input = $request->all();

        $getCarts = Cart::where(["user_id"=>$input['user_id']])->get();
        foreach ($getCarts as $getCart) {
            $updateCart = Cart::where(["id"=>$getCart->id])->first();
            $updateCart->discount = 0;
            $updateCart->coupon_code = "";
            $updateCart->save();
        }

        return $this->sendResponse([], 'Coupon canceled successfully', $request->path());
    }

    public function apply_cupon(Request $request) {
        $input = $request->all();

        $detailsofcupon = Cuponcode::where('name', $input['code'])->first();
        $data = array();

        if (empty($detailsofcupon)) {

            return $this->sendError($request->path(), "Coupon Code Not Found");
        
        } else {

            $check_code_reused = Order::where(['user_id' => $input['user_id'], 'coupen_code' => $input['code']])->first();
            if ($check_code_reused) {
                $data = [];
                return $this->sendError($request->path(), "You have already used this coupon!");
            }

            $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
            $totalAmount = 0;
            $getCarts = Cart::where(["user_id"=>$input['user_id']])->get();

            if($checkoffer) {

                foreach ($getCarts as $getCart) {

                    $pro = Product::select('user_id', 'sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();
                    $checkoffer = Offer::where(['catid'=>$pro->subsubcategory_id, 'coupon_id'=> $detailsofcupon->id])->first();
                    if($checkoffer) {

                        $promotion = Promotion::where(['seller_id'=>$pro->user_id, 'offer_id'=> $checkoffer->id])->first();
                        if($promotion) {

                            $productPromotion = PromotionList::where(['promotion_id'=>$promotion->id, 'product_id'=> $getCart->product_id,'status'=>1])->first();
                            if($productPromotion) {

                                if($pro->is_variant == 2) {

                                    if(!empty($getCart->color) && !empty($getCart->size)) {
                                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                                    } else if(!empty($getCart->color) && empty($getCart->size)) {
                                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                                    } else if(empty($getCart->color) && !empty($getCart->size)) {
                                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                                    }

                                    $get_price_sp = $proPriceDetail->sp; 
                                    $get_price_mrp = $proPriceDetail->mrp;
                                } else {
                                    $get_price_sp = $pro->sp; 
                                    $get_price_mrp = $pro->mrp;
                                }

                                $cartAmount = $get_price_sp * $getCart->quantity;
                                $totalAmount = $totalAmount + $cartAmount;
                            }
                        }
                    }
                }
            
            } else {

                foreach ($getCarts as $getCart) {
                    $pro = Product::select('sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();
                    if($pro->is_variant == 2) {

                        if(!empty($getCart->color) && !empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                        } else if(!empty($getCart->color) && empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                        } else if(empty($getCart->color) && !empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                        }

                        $get_price_sp = $proPriceDetail->sp; 
                        $get_price_mrp = $proPriceDetail->mrp;
                    } else {
                        $get_price_sp = $pro->sp; 
                        $get_price_mrp = $pro->mrp;
                    }

                    $cartAmount = $get_price_sp * $getCart->quantity;
                    $totalAmount = $totalAmount + $cartAmount;
                }
            }

            if($totalAmount > 0) {
                if ($totalAmount < $detailsofcupon->min_price) {
                    $data = ["total"=>$totalAmount, "proid"=>$getCart->product_id];
                    return $this->sendError($request->path(), "Shop more to unlock coupon code!");
                    die;
                }
            } else {
                return $this->sendError($request->path(), "Coupon code not valid for this products!");
                die;
            }

            $totalAmount = 0;
            $totalSave = 0;
            $total_price = 0;
            $final_discounted_price = 0;

            $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
            $getCarts = Cart::where(["user_id"=>$input['user_id']])->get();
            
            if($checkoffer) {
                
                if(count($getCarts) > 0) {

                    foreach ($getCarts as $getCart) {
                    
                        $total_discounted_price = 0;
                        $pro = Product::select('user_id', 'sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();

                        $checkoffer = Offer::where(['catid'=>$pro->subsubcategory_id, 'coupon_id'=> $detailsofcupon->id])->first();
                        if($checkoffer) {

                            $promotion = Promotion::where(['seller_id'=>$pro->user_id, 'offer_id'=> $checkoffer->id])->first();
                            if($promotion) {
                        
                                $productPromotion = PromotionList::where(['promotion_id'=>$promotion->id, 'product_id'=> $getCart->product_id])->first();
                                if($productPromotion) {
                            
                                    if($pro->is_variant == 2) {

                                        if(!empty($getCart->color) && !empty($getCart->size)) {
                                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                                        } else if(!empty($getCart->color) && empty($getCart->size)) {
                                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                                        } else if(empty($getCart->color) && !empty($getCart->size)) {
                                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                                        }

                                        $get_price_sp = $proPriceDetail->sp; 
                                        $get_price_mrp = $proPriceDetail->mrp;
                                    } else {
                                        $get_price_sp = $pro->sp; 
                                        $get_price_mrp = $pro->mrp;
                                    }

                                    $cartAmount = $get_price_sp * $getCart->quantity;
                                    $totalAmount = $totalAmount + $cartAmount;

                                    $totalMrp = $get_price_mrp * $getCart->quantity;
                                    $cal = $totalMrp - $cartAmount;
                                    $totalsavediff = $totalSave + $cal;

                                    if($detailsofcupon->type == "percent") {
                                        $dicounted_price = ($totalAmount * $detailsofcupon->discount) / 100;
                                        $total_price = $total_price + round($totalAmount - $dicounted_price);
                                        $totalSave = $totalSave +  $totalsavediff + $dicounted_price;

                                        $final_discounted_price = $final_discounted_price + $total_discounted_price;
                                    } else {
                                        $dicounted_price = $detailsofcupon->discount;
                                        $total_price = $total_price + round($totalAmount - $dicounted_price);
                                        $totalSave = $totalSave + $totalsavediff + $dicounted_price;
                                        
                                        $final_discounted_price = $final_discounted_price + $dicounted_price;
                                    }

                                    $updateCart = Cart::where(["id"=>$getCart->id])->first();
                                    $updateCart->discount = $dicounted_price;
                                    $updateCart->coupon_code = $detailsofcupon->name;
                                    $updateCart->save();
                                }
                            }
                        }
                    }

                    $data = ["total_price"=>$totalAmount, "discounted_amount"=>$final_discounted_price, "final_price"=>$total_price, "total_save"=>$totalSave];
                    return $this->sendResponse($data, 'Coupon applied successfully', $request->path());
                
                } else {

                    return $this->sendError($request->path(), "Coupon Code Not able to apply");
                }

            } else {

                if(count($getCarts) > 0) {

                    foreach ($getCarts as $getCart) {
                    
                        $total_discounted_price = 0;
                        $pro = Product::select('sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();
                
                        if($pro->is_variant == 2) {

                            if(!empty($getCart->color) && !empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                            } else if(!empty($getCart->color) && empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                            } else if(empty($getCart->color) && !empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                            }

                            $get_price_sp = $proPriceDetail->sp; 
                            $get_price_mrp = $proPriceDetail->mrp;
                        } else {
                            $get_price_sp = $pro->sp; 
                            $get_price_mrp = $pro->mrp;
                        }

                        $cartAmount = $get_price_sp * $getCart->quantity;
                        $totalAmount = $totalAmount + $cartAmount;

                        $totalMrp = $get_price_mrp * $getCart->quantity;
                        $cal = $totalMrp - $cartAmount;
                        $totalsavediff = $totalSave + $cal;

                        if($detailsofcupon->type == "percent") {
                            $dicounted_price = ($totalAmount * $detailsofcupon->discount) / 100;
                            $total_price = $total_price + round($totalAmount - $dicounted_price);
                            $totalSave = $totalSave +  $totalsavediff + $dicounted_price;

                            $final_discounted_price = $final_discounted_price + $total_discounted_price;
                        } else {
                            $dicounted_price = $detailsofcupon->discount;
                            $total_price = $total_price + round($totalAmount - $dicounted_price);
                            $totalSave = $totalSave + $totalsavediff + $dicounted_price;
                            
                            $final_discounted_price = $final_discounted_price + $dicounted_price;
                        }

                        $updateCart = Cart::where(["id"=>$getCart->id])->first();
                        $updateCart->discount = $dicounted_price;
                        $updateCart->coupon_code = $detailsofcupon->name;
                        $updateCart->save();
                    }

                    $data = ["total_price"=>$totalAmount, "discounted_amount"=>$final_discounted_price, "final_price"=>$total_price, "total_save"=>$totalSave];
                    return $this->sendResponse($data, 'Coupon applied successfully', $request->path());


                } else {
                    return $this->sendError($request->path(), "Coupon Code Not able to apply");
                }
            }
        }
    }


    public function referal_list(Request $request, $id) {
        $referArr = array();
        $user = User::where(['id' => $id])->select('referal_code')->first();
        $referArr = ['referal_code'=>$user->referal_code];

        $refers = Refer::where(['refercode' => $user->referal_code])->get();
        
        if(count($refers) > 0) {
            foreach($refers as $refer) {
                $referUser = User::where(['id' => $refer->referto])->select('name','email')->first();
                $referArr['list'][] = ["name"=>$referUser->name, "email"=>$referUser->email];
            }
        } else {
            $referArr['list'] = [];
        }

        $referArr['referal_price'] = 100;

        return $this->sendResponse($referArr, 'Referal list retrieved successfully', $request->path());
    }

    public function checkemail($email) {
        $email_inmembers = DB::table('sellers')->where('email', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function themeorder(Request $request) {
        $details = Priority::orderby('section_number', 'asc')->pluck('image');
        return $this->sendResponse1($details, 'Successfully updated data', $request->path());
    }

    public function setthemeorder(Request $request) {
        $input = $request->all();
        // $return_array = explode(",", $input['simpleList']);
        // dd($return_array);
        $ikmj = 1;
        foreach ($input['simpleList'] as $value) {
            $finddetails = Priority::where('image', $value)->first();
            $finddetails->section_number = $ikmj;
            $finddetails->save();
            $ikmj++;
        }
        // dd(var_dump($input['simpleList']));
        $details = Priority::orderby('section_number', 'asc')->pluck('image');
        return $this->sendResponse1($details, 'Successfully updated data', $request->path());
    }

    public function checkphone1($email) {
        $email_inmembers = DB::table('users')->where('phone', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function checkemail1($email) {
        $email_inmembers = DB::table('users')->where('email', $email)->first();
        if (!empty($email_inmembers)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function myorderdetails(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }


        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id","orders.created_at as created", "orders.*", "users.name as buyer")->where(['orders.user_id' => $input['user_id']])->orderBy('orders.id','DESC')->get();
        
        $return_array = array();
    
        foreach ($details as $met) {
            
            $cartfindall = OrderDetail::where(['order_id' => $met->id])->first();
            if($cartfindall) {

                $detailsproduct = Product::where('id', $met->product_id)->first();
                if($detailsproduct) {
                    if($detailsproduct->is_variant == 2) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$detailsproduct->id, "size"=>$cartfindall->size, "color"=>$cartfindall->color])->first();

                        if(!empty($cartfindall->color) && !empty($cartfindall->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$detailsproduct->id, "size"=>$cartfindall->size, "color"=>$cartfindall->color])->first();
                        } else if(!empty($cartfindall->color) && empty($cartfindall->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$detailsproduct->id, "color"=>$cartfindall->color])->first();
                        } else if(empty($cartfindall->color) && !empty($cartfindall->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$detailsproduct->id, "size"=>$cartfindall->size])->first();
                        }
                        if($proPriceDetail) {
                            $get_price_sp = $proPriceDetail->sp; 
                            $get_price_mrp = $proPriceDetail->mrp;
                        } else {
                            $get_price_sp = $detailsproduct->sp; 
                            $get_price_mrp = $detailsproduct->mrp;    
                        }
                    } else {
                        $get_price_sp = $detailsproduct->sp; 
                        $get_price_mrp = $detailsproduct->mrp;
                    }
                } else {
                    $get_price_sp = 1; 
                    $get_price_mrp = 1;
                }

                if(strlen($met->coupen_code) > 0) {
                    $coupen = Cuponcode::select('discount')->where('name', $met->coupen_code)->first();
                    if($coupen) {
                        $coupendiscount = (int)$coupen->discount;
                    } else {
                        $coupendiscount = 0;
                    }
                } else {
                    $coupendiscount = 0;
                }
                $percentagetemp = 100 - ($get_price_sp * 100) / $get_price_mrp;

                
                $getD = strtotime($met->created_at);
                $addedDate = strtotime("+7 day", $getD);
                $currentDate = date('d/m/y');
                $currentConvert = strtotime($currentDate);

                if($currentConvert > $addedDate) {
                    $exchangeStatus = 0;
                } else {
                    $exchangeStatus = 1;
                }
                $notifyArr = array();
                $getNotify = Notification::where(['order_id'=>$met->id, 'type'=>'order'])->get();
                if($getNotify) {
                    foreach($getNotify as $notify) {
                        $notifyArr[] = ["date"=> date("d M", strtotime($notify->updated_at)), "content"=>$notify->content];
                    }
                }

                $return_array[] = array(
                    "image" => $this->baseurl . explode(",", $detailsproduct->images) [0], 
                    "sku" => $detailsproduct->sku, 
                    "brand" => $detailsproduct->brand, 
                    "product_id" => $detailsproduct->id, 
                    "id" => $met->id,
                    "order_number" => $met->order_number,
                    "Buyer" => $met->buyer, 
                    "location" => $met->location, 
                    "order_date" => date("d/m/Y", strtotime($met->created)), 
                    "dispatch_by" => date("d/m/Y", strtotime($met->dispatch_at)), 
                    "payment_type" => $met->payment_type, 
                    "coupan_code" => !empty($met->coupen_code) ? $met->coupen_code : '', 
                    "status" => $met->status,
                    "product_name" => $detailsproduct->name, 
                    "amount" => $cartfindall->final_price,
                    "percentage" => (int)$percentagetemp,
                    "mrp" => $get_price_mrp, 
                    "quantity" => $cartfindall->quantity,
                    "color" => $cartfindall->color, 
                    "size" => $cartfindall->size, "exchangeStatus"=>$exchangeStatus,
                    "notifyStatus"=>$notifyArr,
                    "size_label"=>$detailsproduct->size_label
                );
            }

        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function myorderhelp(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', ]);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
    
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
    
        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->where(['orders.user_id'=>$input['user_id']])->select("orders.id as id", "orders.*", "users.name as buyer")->orderby('orders.id', 'DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
                $cartfindall = OrderDetail::where(['order_id'=> $met->id])->first();
                
                    $detailsproduct = Product::where('id', $met->product_id)->first();
                    $coupen = Cuponcode::select('discount')->where('name', $met->coupen_code)->first();
                    if($coupen) {
                        $coupendiscount = (int)$coupen->discount;
                    } else {
                        $coupendiscount = 0;
                    }
                    $percentagetemp = 100 - ($detailsproduct->sp * 100) / $detailsproduct->mrp;
                        
                    $getD = strtotime($met->created_at);
                    $addedDate = strtotime("+7 day", $getD);
                    $currentDate = date('d/m/y');
                    $currentConvert = strtotime($currentDate);

                    if($currentConvert > $addedDate) {
                        $exchangeStatus = 0;
                    } else {
                        $exchangeStatus = 1;
                    }

                        $return_array[] = array(
                            "image" => $this->baseurl . explode(",", $detailsproduct->images) [0], 
                            "sku" => $detailsproduct->sku, 
                            "brand" => $detailsproduct->brand, 
                            "product_id" => $detailsproduct->id, 
                            "id" => $met->id,
                            "order_number" => $met->order_number,
                            "Buyer" => $met->buyer, 
                            "location" => $met->location, 
                            "order_date" => date("d/m/y", strtotime($met->created_at)), 
                            "dispatch_by" => date("d/m/y", strtotime($met->dispatch_at)), 
                            "payment_type" => $met->payment_type, 
                            "coupan_code" => !empty($met->coupen_code) ? $met->coupen_code : '', 
                            "status" => $met->status,
                            "product_name" => $detailsproduct->name, 
                            "amount" => $cartfindall->final_price,
                            "percentage" => $coupendiscount,
                            "mrp" => $cartfindall->sale_price, 
                            "quantity" => $cartfindall->quantity,
                            "color" => $cartfindall->color, 
                            "size" => $cartfindall->size, 
                            "exchangeStatus"=>$exchangeStatus,
                            "size_label"=>$detailsproduct->size_label,
                        );
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }
    
    public function placeorder(Request $request) {
        $input = $request->all();
        $placeorder = array();

        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'gift_wrapup_status' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        
        if($input['payment_type'] == "Cash On Delivery") {
            $findallcartlist = Cart::where(["user_id" => $input['user_id']])->get();
            $userGet = User::where(['id' => $input['user_id']])->first();
            
            foreach ($findallcartlist as $value) {
                
                $totalPrice = 0;
                $finalPrice = 0;
                $discountAmt = 0;
                $refer_discount = 0;

                $quantity = $value->quantity;
                $findp = Product::find($value->product_id);

                if($findp) {

                    if($findp->is_variant == 2) {
                        if(!empty($value->color) && !empty($value->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size, "color"=>$value->color])->first();
                        } else if(!empty($value->color) && empty($value->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "color"=>$value->color])->first();
                        } else if(empty($value->color) && !empty($value->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size])->first();
                        }

                        $pQty = $proPriceDetail->quantity;
                        $updatepquantity = (int)$pQty - (int)$value->quantity;
                        DB::table('product_prices')->where(['id' => $proPriceDetail->id])->update(['quantity' => $updatepquantity]);

                        $get_price_sp = $proPriceDetail->sp; 
                        $get_price_mrp = $proPriceDetail->mrp;
                    } else {
                        $get_price_sp = $findp->sp; 
                        $get_price_mrp = $findp->mrp;

                        $pQty = $findp->quantity;
                        $updatepquantity = (int)$pQty - (int)$value->quantity;
                        DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);
                    }

                    $sellerGet = Seller::where(['id' => $findp->user_id])->first();

                    $totalPrice = $value->quantity * $get_price_sp;

                    if($findp->hsn_percent > 0) {
                        $calulate_valuable_tax = $totalPrice / 100 * $findp->hsn_percent;
                        $calulate_valuable_tax = $totalPrice - $calulate_valuable_tax;
                    } else {
                        $calulate_valuable_tax = $totalPrice;
                    }

                    $adminCommmisionPrice = $totalPrice / 100 * $sellerGet->commission;
                    $getShippingPrice = $findp->national;
                    $priceTCS = $totalPrice / 100 * 1;
                    $calculateTax = $adminCommmisionPrice + $getShippingPrice;
                    $calculateTax = $calculateTax / 100 * 18;
                    $sellerEarningAmount = $totalPrice - $adminCommmisionPrice - $getShippingPrice - $priceTCS - $calculateTax;

                    $finalPrice = $totalPrice + $getShippingPrice;

                    // if(strlen($input['coupan_code']) > 1) {
                    //     $findCoupen = Cuponcode::where(['name' => $input['coupan_code']])->first();
                    //     $discount = (int)$findCoupen->discount;

                    //     if($findCoupen->type == "percent") {
                    //         $getDiscount = ($finalPrice * $discount) / 100;
                    //         $discountAmt = $getDiscount;
                    //         $finalPrice = $finalPrice - $getDiscount;
                    //     } else {
                    //         $getDiscount = $finalPrice - $discount;
                    //         $discountAmt = $discount;
                    //         $finalPrice = $getDiscount;
                    //     }   
                    // }

                    if(strlen($value->coupon_code) > 0) {
                        $getDiscount = $value->discount;
                        $discountAmt = $getDiscount;
                        $finalPrice = $finalPrice - $getDiscount;

                    } else {
                        $discountAmt = 0;
                    }

                    if($discountAmt > 0 ) {
                        $detailsofcupon = Cuponcode::where('name', $value->coupon_code)->first();
                        $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
                        if($checkoffer) {

                            $breakDiscount = $discountAmt / 2;
                            $adminCommmisionPrice = $adminCommmisionPrice - $breakDiscount;
                            $sellerEarningAmount = $sellerEarningAmount - $breakDiscount;

                        } else {
                            $adminCommmisionPrice = $adminCommmisionPrice - $discountAmt;
                            $sellerEarningAmount = $sellerEarningAmount + $discountAmt;
                        }
                    }

                    $verifyRefer = Refer::where(["referto"=>$input['user_id'], "status"=>1])->orderBy("id","ASC")->first();
                    if($verifyRefer) {
                        $getRefer = ReferEarn::where(["id"=>1])->first();
                        $disAmt = $getRefer->price;
                        $refer_discount =  $disAmt;
                        $finalPrice = $finalPrice - $disAmt;
                        Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                        $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                    } else {
                        $verifyRefer = Refer::where(["referby"=>$input['user_id'], "status"=>1])->orderBy("id","ASC")->first();
                        if($verifyRefer) {
                            $getRefer = ReferEarn::where(["id"=>1])->first();
                            $disAmt = $getRefer->price;
                            $refer_discount = $disAmt;
                            $finalPrice = $finalPrice - $disAmt;
                            Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                            $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                        }
                    }

                    if($input['gift_wrapup_status'] == 'yes') {
                        $finalPrice = $finalPrice + 25;
                        $gift_wrapup_status = 1;
                        $gift_wrapup_price = 25;
                        $sellerEarningAmount = $sellerEarningAmount + 25;
                    } else {
                        $gift_wrapup_status = 0;
                        $gift_wrapup_price = 0;
                    }
                    
                    if(strlen($input['tracking_id']) > 1) {
                        $payStatus = 'completed';
                    } else {
                        $payStatus = 'pending';
                    }

                    //$finalPrice = $finalPrice + $getShippingPrice;

                    $shippingDate = $findp->ships_in;
                    $shipDate = "+$shippingDate days";

                    $http = new Client;
                    $generateWBN = $http->get(url('https://staging-express.delhivery.com/waybill/api/bulk/json/?cl='.$userGet->name.'&token=dcafa28eb05642c39f31b14724e28cf45f91256a&count=1'));
                    $data_wbn = json_decode($generateWBN->getBody());
                    $orderNum = 'satvick'. $this->random_strings(5);
                    
                    if(!empty($value->color)) {
                        $colorname = Color::where(["hexcode"=>$value->color])->first();
                        $colorname = $colorname->name;
                    } else {
                        $colorname = "";
                    }

                    $get_currentdate_time = strtotime(date('H:i:sa'));
                    $allocateTime = "11:59:59am";
                    $compare_time = strtotime($allocateTime);
                    if($get_currentdate_time <= $compare_time){
                        $slatime = 1;
                    } else {
                        $slatime = 0;
                    }

                    $orderData = array("order_number"=> $orderNum,
                                        "product_id" => $findp->id,
                                        "seller_id" => $findp->user_id,
                                        "user_id" => $input['user_id'],
                                        "location" => $input['location'],
                                        "state" => $input['state'],
                                        "city" => $input['city'],
                                        "country" => $input['country'],
                                        "pincode" => $input['pincode'],
                                        "phone" => $input['phone'],
                                        "name" => $input['name'],
                                        "address_type" => $input['address_type'],
                                        "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                        "payment_type" => $input['payment_type'],
                                        "transcation_id" => '',
                                        "coupen_code" => $input['coupan_code'],
                                        "gift_wrap_status" => $gift_wrapup_status,
                                        "sla_time"=>$slatime,
                                        "payment_status" => $payStatus,
                                        "status" => 'new order',
                                    );
                    $oid = Order::create($orderData);

                    $orderProducts = array("order_id" => $oid->id,
                                        "size" => $value->size,
                                        "color" => $colorname,
                                        "quantity" => $value->quantity,
                                        "price" => $get_price_mrp,
                                        "sale_price" => $get_price_sp,
                                        "discount_price" => $discountAmt,
                                        "total_price" => $totalPrice,
                                        "refer_discount"=> $refer_discount,
                                        "giftwrap_amount"=> $gift_wrapup_price,
                                        "seller_shipping"=> $getShippingPrice,
                                        "valuable_tax" => $calulate_valuable_tax,
                                        "gst_tax" => $calculateTax,
                                        "tcs_tax" => $priceTCS,
                                        "final_price" => $finalPrice,
                                        "seller_amount" => $sellerEarningAmount,
                                        "admin_commision" => $adminCommmisionPrice,
                                        "total_shipping"=> 0,
                                        "wbn_num" => $data_wbn
                                    );
                    OrderDetail::create($orderProducts);
                    Cart::where(['id'=>$value->id])->delete();

                    $notifyData = [
                        'user_id'=>$input['user_id'],
                        'order_id'=>$oid->id,
                        'order_number'=>$orderNum,
                        'type'=>'Order',
                        'title'=>'New Order - '.$orderNum,
                        'content'=>'Your new order has been placed'
                    ];
                    Notification::create($notifyData);
                    $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$input['user_id']])->first();
                    if($userDevice) {
                        $badge = Notification::where(["user_id"=>$input['user_id'],'status'=>0])->count();
                        $this->pushNotification($notifyData, $userDevice, $badge);
                    }
                }
            }

            return $this->sendResponse(["status" => "success",'msg'=>$notifyData], "Your order completed successfully", $request->path());

        } else {

            $findallcartlist = Cart::where(["user_id" => $input['user_id']])->get();
            $userGet = User::where(['id' => $input['user_id']])->first();
            
            foreach ($findallcartlist as $value) {
                
                $totalPrice = 0;
                $finalPrice = 0;
                $discountAmt = 0;
                $refer_discount = 0;

                $quantity = $value->quantity;
                $findp = Product::find($value->product_id);

                if($findp) {

                    if($findp->is_variant == 2) {
                        if(!empty($value->color) && !empty($value->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size, "color"=>$value->color])->first();
                        } else if(!empty($value->color) && empty($value->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "color"=>$value->color])->first();
                        } else if(empty($value->color) && !empty($value->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size])->first();
                        }

                        $pQty = $proPriceDetail->quantity;
                        $updatepquantity = (int)$pQty - (int)$value->quantity;
                        DB::table('product_prices')->where(['id' => $proPriceDetail->id])->update(['quantity' => $updatepquantity]);

                        $get_price_sp = $proPriceDetail->sp; 
                        $get_price_mrp = $proPriceDetail->mrp;
                    } else {
                        $get_price_sp = $findp->sp; 
                        $get_price_mrp = $findp->mrp;

                        $pQty = $findp->quantity;
                        $updatepquantity = (int)$pQty - (int)$value->quantity;
                        DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);
                    }
                    
                    $sellerGet = Seller::where(['id' => $findp->user_id])->first();

                    $totalPrice = $value->quantity * $get_price_sp;
                    
                    if($findp->hsn_percent > 0) {
                        $calulate_valuable_tax = $totalPrice / 100 * $findp->hsn_percent;
                        $calulate_valuable_tax = $totalPrice - $calulate_valuable_tax;
                    } else {
                        $calulate_valuable_tax = $totalPrice;
                    }

                    $adminCommmisionPrice = $totalPrice / 100 * $sellerGet->commission;
                    $getShippingPrice = 0;
                    $priceTCS = $totalPrice / 100 * 1;
                    $calculateTax = $adminCommmisionPrice + $getShippingPrice;
                    $calculateTax = $calculateTax / 100 * 18;
                    $sellerEarningAmount = $totalPrice - $adminCommmisionPrice - $getShippingPrice - $priceTCS - $calculateTax;
                    $finalPrice = $totalPrice + $findp->national;

                    // if(strlen($input['coupan_code']) > 1) {
                    //     $findCoupen = Cuponcode::where(['name' => $input['coupan_code']])->first();
                    //     $discount = (int)$findCoupen->discount;

                    //     if($findCoupen->type == "percent") {
                    //         $getDiscount = ($finalPrice * $discount) / 100;
                    //         $discountAmt = $getDiscount;
                    //         $finalPrice = $finalPrice - $getDiscount;
                    //     } else {
                    //         $getDiscount = $finalPrice - $discount;
                    //         $discountAmt = $discount;
                    //         $finalPrice = $getDiscount;
                    //     }
                    //     $adminCommmisionPrice = $adminCommmisionPrice - $discountAmt;
                    // }

                    if(strlen($value->coupon_code) > 0) {
                        $getDiscount = $value->discount;
                        $discountAmt = $getDiscount;
                        $finalPrice = $finalPrice - $getDiscount;

                    } else {
                        $discountAmt = 0;
                    }

                    if($discountAmt > 0 ) {
                        $detailsofcupon = Cuponcode::where('name', $value->coupon_code)->first();
                        $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
                        if($checkoffer) {

                            $breakDiscount = $discountAmt / 2;
                            $adminCommmisionPrice = $adminCommmisionPrice - $breakDiscount;
                            $sellerEarningAmount = $sellerEarningAmount - $breakDiscount;

                        } else {
                            $adminCommmisionPrice = $adminCommmisionPrice - $discountAmt;
                            $sellerEarningAmount = $sellerEarningAmount + $discountAmt;
                        }
                    }

                    $verifyRefer = Refer::where(["referto"=>$input['user_id'], "status"=>1])->orderBy("id","ASC")->first();
                    if($verifyRefer) {
                        $getRefer = ReferEarn::where(["id"=>1])->first();
                        $disAmt = $getRefer->price;
                        $refer_discount =  $disAmt;
                        $finalPrice = $finalPrice - $disAmt;
                        Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                        $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                    } else {
                        $verifyRefer = Refer::where(["referby"=>$input['user_id'], "status"=>1])->orderBy("id","ASC")->first();
                        if($verifyRefer) {
                            $getRefer = ReferEarn::where(["id"=>1])->first();
                            $disAmt = $getRefer->price;
                            $refer_discount = $disAmt;
                            $finalPrice = $finalPrice - $disAmt;
                            Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                            $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                        }
                    }

                    if($input['gift_wrapup_status'] == 'yes') {
                        $finalPrice = $finalPrice + 25;
                        $gift_wrapup_status = 1;
                        $gift_wrapup_price = 25;
                        $sellerEarningAmount = $sellerEarningAmount + 25;
                    } else {
                        $gift_wrapup_status = 0;
                        $gift_wrapup_price = 0;
                    }
                    
                    if(strlen($input['tracking_id']) > 1) {
                        $payStatus = 'completed';
                    } else {
                        $payStatus = 'pending';
                    }
                    $shippingDate = $findp->ships_in;
                    $shipDate = "+$shippingDate days";

                    $http = new Client;
                    $generateWBN = $http->get(url('https://staging-express.delhivery.com/waybill/api/bulk/json/?cl='.$userGet->name.'&token=dcafa28eb05642c39f31b14724e28cf45f91256a&count=1'));
                    $data_wbn = json_decode($generateWBN->getBody());
                    $orderNum = 'satvick'. $this->random_strings(5);

                    $get_currentdate_time = strtotime(date('H:i:sa'));
                    $allocateTime = "11:59:59am";
                    $compare_time = strtotime($allocateTime);
                    if($get_currentdate_time <= $compare_time){
                        $slatime = 1;
                    } else {
                        $slatime = 0;
                    }

                    $orderData = array("order_number"=> $orderNum,
                                        "product_id" => $findp->id,
                                        "seller_id" => $findp->user_id,
                                        "user_id" => $input['user_id'],
                                        "location" => $input['location'],
                                        "state" => $input['state'],
                                        "city" => $input['city'],
                                        "country" => $input['country'],
                                        "pincode" => $input['pincode'],
                                        "phone" => $input['phone'],
                                        "name" => $input['name'],
                                        "address_type" => $input['address_type'],
                                        "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                        "payment_type" => $input['payment_type'],
                                        "transcation_id" => '',
                                        "coupen_code" => $input['coupan_code'],
                                        "gift_wrap_status" => $gift_wrapup_status,
                                        "payment_status" => $payStatus,
                                        "sla_time"=>$slatime,
                                        "status" => 'new order',
                                    );
                    $oid = Order::create($orderData);

                    $orderProducts = array("order_id" => $oid->id,
                                        "size" => $value->size,
                                        "color" => $value->color,
                                        "quantity" => $value->quantity,
                                        "price" => $get_price_mrp,
                                        "sale_price" => $get_price_sp,
                                        "discount_price" => $discountAmt,
                                        "total_price" => $totalPrice,
                                        "refer_discount"=> $refer_discount,
                                        "giftwrap_amount"=> $gift_wrapup_price,
                                        "seller_shipping"=> $getShippingPrice,
                                        "valuable_tax" => $calulate_valuable_tax,
                                        "gst_tax" => $calculateTax,
                                        "tcs_tax" => $priceTCS,
                                        "final_price" => $finalPrice,
                                        "seller_amount" => $sellerEarningAmount,
                                        "admin_commision" => $adminCommmisionPrice,
                                        "total_shipping"=> 0,
                                        "wbn_num" => $data_wbn
                                    );
                    OrderDetail::create($orderProducts);
                    Cart::where(['id'=>$value->id])->delete();

                    $pQty = $findp->quantity;
                    $updatepquantity = (int)$pQty - (int)$value->quantity;
                    DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);

                    $notifyData = [
                        'user_id'=>$input['user_id'],
                        'order_id'=>$oid->id,
                        'order_number'=>$orderNum,
                        'type'=>'Order',
                        'title'=>'New Order - '.$orderNum,
                        'content'=>'Your new order has been placed'
                    ];
                    Notification::create($notifyData);
                    $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$input['user_id']])->first();
                    $badge = Notification::where(["user_id"=>$input['user_id'],'status'=>0])->count();
                    $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
                }
            }
            return $this->sendResponse(["status" => "success",'msg'=>$notifyResponse], "Your order completed successfully", $request->path());

        }
    }

    public function exchangeorder($id, $status) {

        if($status == "reject") {

            $details = Orderrequest::where(['order_id' => $id])->first();
            if ($details) {
                return $this->sendError($request->path(), 'Order not found');
            }

            $orderData = Order::where(["id"=>$id])->first();
            $notifyData = [
                'user_id'=>$orderData->user_id,
                'order_id'=>$orderData->id,
                'order_number'=>$orderData->order_number,
                'type'=>'Order',
                'title'=>'Request cancel',
                'content'=>'Your order request for exchange has been cancelled'
            ];
            Notification::create($notifyData);
            $userDevice = Token::select('deviceType', 'deviceToken')->where(['user_id'=>$orderData->user_id])->first();
            $badge = Notification::where(["user_id"=>$orderData->user_id, 'status'=>0])->count();
            $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);

            Orderrequest::where(['order_id' => $id])->update(["status"=>1]);

            return $this->sendResponse(["status" => "success",'msg'=>$notifyResponse], "Order for exchange completed", $request->path());

        } else {
            
            $requestDetail = Orderrequest::where(['order_id' => $id])->first();

            if($requestDetail->request_type == "exchange") {

                $orderData = Order::where(["id"=>$id])->first();

                $http = new Client;
                $generateWBN = $http->get(url('https://staging-express.delhivery.com/waybill/api/bulk/json/?cl='.$userGet->name.'&token=dcafa28eb05642c39f31b14724e28cf45f91256a&count=1'));
                $data_wbn = json_decode($generateWBN->getBody());

                $findp = Product::find($orderData->product_id);
                $shippingDate = $findp->ships_in;
                $shipDate = "+$shippingDate days";

                $orderData = array("order_number"=> $orderData->order_number.'-Re',
                                    "product_id" => $orderData->product_id,
                                    "seller_id" => $orderData->seller_id,
                                    "user_id" => $orderData->user_id,
                                    "location" => $orderData->location,
                                    "state" => $orderData->state,
                                    "city" => $orderData->city,
                                    "country" => $orderData->country,
                                    "pincode" => $orderData->pincode,
                                    "phone" => $orderData->phone,
                                    "name" => $orderData->name,
                                    "address_type" => $orderData->address_type,
                                    "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                    "payment_type" => $orderData->payment_type,
                                    "transcation_id" => '',
                                    "coupen_code" => $orderData->coupen_code,
                                    "gift_wrap_status" => $orderData->gift_wrap_status,
                                    "payment_status" => 'success',
                                    "status" => 'reorder',
                                );
                    $oid = Order::create($orderData);

                $orderDetailData = OrderDetail::where(["order_id"=>$id])->first();

                $orderProducts = array("order_id" => $oid->id,
                                    "size" => $requestDetail->size,
                                    "color" => $requestDetail->color,
                                    "quantity" => $orderDetailData->quantity,
                                    "price" => $orderDetailData->price,
                                    "sale_price" => $orderDetailData->sale_price,
                                    "discount_price" => $orderDetailData->discount_price,
                                    "total_price" => $orderDetailData->total_price,
                                    "refer_discount"=> $orderDetailData->refer_discount,
                                    "giftwrap_amount"=> $orderDetailData->giftwrap_amount,
                                    "seller_shipping"=> $orderDetailData->seller_shipping,
                                    "valuable_tax" => $orderDetailData->valuable_tax,
                                    "gst_tax" => $orderDetailData->gst_tax,
                                    "tcs_tax" => $orderDetailData->tcs_tax,
                                    "final_price" => $orderDetailData->final_price,
                                    "seller_amount" => $orderDetailData->seller_amount,
                                    "admin_commision" => $orderDetailData->admin_commision,
                                    "total_shipping"=> $orderDetailData->total_shipping,
                                    "wbn_num" => $data_wbn
                                );
                    OrderDetail::create($orderProducts);
                
                $notifyData = [
                    'user_id'=>$orderData->user_id,
                    'order_id'=>$oid->id,
                    'order_number'=>$orderData->order_number.'-Re',
                    'type'=>'Order',
                    'title'=>'Re-Order - '.$orderNum,
                    'content'=>'Your order request for exchange has been confirmed'
                ];
                Notification::create($notifyData);
                $userDevice = Token::select('deviceType', 'deviceToken')->where(['user_id'=>$orderData->user_id])->first();
                $badge = Notification::where(["user_id"=>$orderData->user_id, 'status'=>0])->count();
                $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);

                Orderrequest::where(['order_id' => $id])->update(["status"=>2]);
                DB::table('orders')->where(['id' => $input['id']])->update(['status' => "cancel"]);
            
            } else {

                $order = Order::where(["id"=>$id])->first();
                $details = OrderDetail::where(["order_id"=>$id])->first();
                $sellerDetail = Seller::where(['id' => $orderData->seller_id])->first();
                $prod = Product::where(['id'=>$orderData->product_id])->first();
                
                if($order->payment_type == "Cash On Delivery") {
                    $paymentType = "COD";
                    $codamt = $details->final_price;
                } else {
                    $paymentType = "Prepaid";
                    $codamt = 0;
                }

                $jsonData['shipments'][] = [
                            "add" => $order->location,
                            "address_type" => $order->address_type,
                            "phone" => $order->phone,
                            "payment_mode"=> "Pickup",
                            "name" => $order->name,
                            "city" => $order->city,
                            "state" => $order->state,
                            "pin" => $order->pincode,
                            "country" => $order->country,
                            "order" => $order->order_number,
                            "hsn_code" => $prod->sku,
                            "client" => "THEBRANDSTANDSURFACE-B2C",
                            "shipment_width" =>$prod->width,
                            "seller_gst_tin" => $sellerDetail->gstnum,
                            "seller_name" =>$sellerDetail->name,
                            "seller_add" =>$sellerDetail->address,
                            "return_state"=> $order->state,
                            "return_city"=> $order->city,
                            "return_country"=> $order->country,
                            "return_add"=> $order->location,
                            "return_pin"=> $order->pincode,
                            "extra_parameters"=> [
                              "return_reason"=> $requestDetail->reason
                            ],
                            "return_name"=>$order->name,

                            "weight" => $prod->weight,
                            "cod_amount" => $codamt,
                            "dangerous_good" => "False",
                            "waybill" => $details->wbn_num,
                            "order_date" =>$order->created_at,
                            "total_amount" => $details->final_price,
                            "quantity" =>$details->quantity,
                ];

                $jsonData['pickup_location'] = [
                        "name" => $sellerDetail->name,
                        "city" => $sellerDetail->city,
                        "pin" => $sellerDetail->pincode,
                        "country" => $sellerDetail->country,
                        "phone" => $sellerDetail->phone,
                        "add" => $sellerDetail->address
                ];

                $jsonarray = 'format=json&data='. json_encode($jsonData);
                $this->shippingapiordercreate($jsonarray);

                $jsonarray = [
                    "pickup_time"=>date("H:i:s", strtotime('+12 hours')),
                    "pickup_date"=>date("H:i:s", strtotime('+12 hours')),
                    "pickup_location"=>$sellerDetail->name,
                    "expected_package_count"=>1,
                ];
                $this->shippingapiorderpickup($jsonarray);
                DB::table('orders')->where(['id' => $id])->update(['status' => "return"]);
            }

            return $this->sendResponse(["status" => "success",'msg'=>$notifyResponse], "Order request updated", $request->path());
        }
    }

    public function generateOrderid(Request $request) {
        $orderNum = 'satvick'. $this->random_strings(5);
        $order = ["order_number"=>$orderNum];
        return $this->sendResponse($order, 'Notification list retrieved successfully', $request->path());
    }

    public function test(Request $request) {
        $input = $request->all();
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('sellergstnumfile/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $input['gstnumfile'] = '/sellergstnumfile/' . $thumbnail_img_name;
        }
        if ($request->pan_numfile) {
            $thumbnail_img = $request->pan_numfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('pancardnumfileseller/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $input['pan_numfile'] = '/pancardnumfileseller/' . $thumbnail_img_name;
        }
        if ($request->brandfile) {
            $thumbnail_img = $request->brandfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('pancardnumfileseller/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $input['brandfile'] = '/pancardnumfileseller/' . $thumbnail_img_name;
        }

        // $jsonarray = [
        //     "phone" => $input['phone'],
        //     "city" => $input['city'],
        //     "name" => $input['name'],
        //     "pin" => $input['pincode'],
        //     "address" => $input['address'],
        //     "country" => "India",
        //     "email" => $input['email'],
        //     "registered_name" => $input['name'],
        //     "return_address" => $input['address'],
        //     "return_pin" => $input['pincode'],
        //     "return_city" => $input['city'],
        //     "return_state" => $input['state'],
        //     "return_country" => "India",
        // ];

        // $headers['headers']  = [
        //     "Content-Type" => "application/json",
        //     "Authorization" => "Token dcafa28eb05642c39f31b14724e28cf45f91256a",
        //     "Accept" => "application/json",
        // ];
        
        // $url = "https://staging-express.delhivery.com/api/backend/clientwarehouse/create/";
        // $body = $jsonarray;

        // $client = new Client($headers);
        // $response = $client->post($url, [
        //     'http_errors' => false,
        //     'json' => $body,
        // ]);
        // $status = $response->getStatusCode();
        // $data = json_decode($response->getBody()->getContents());

        $obj = Seller::create($input);
        // $brandarray = array("image" => $input['brandfile'], "description" => $input['brand'], "name" => $input['brand'], "seller_id" => $obj->id, "admin_status" => "");
        // Brand::create($brandarray);

        return $this->sendResponse($obj, "Your account created successfully", $request->path());
    }

    public function test1(Request $request) {
        $input = $request->all();
        $obj = Seller::create($input);

        return $this->sendResponse($obj, "Your account created successfully", $request->path());
    }

    public function sellerlogin(Request $request) {
        $input = $request->all();
        $detailsother = Seller::where(['email' => $input['username'], 'password' => $input['password']])->first();
        
        if (!empty($detailsother)) {

            $detailsother->image = $this->baseurlslash.$detailsother->image;
            if(empty($detailsother->status)) {
               return $this->sendError($request->path(),"Your account is inactive");
            }
            return $this->sendResponse($detailsother, 'Login successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Your username or Password is Incorrect");
        }
    }

    public function sellerloginbyadmin(Request $request, $id) {
        $detailsother = Seller::where(['id' => $id])->first();
        
        if (!empty($detailsother)) {
            return $this->sendResponse($detailsother, 'Login successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Seller not found");
        }
    }
    
    public function sellerlogin1(Request $request) {
        $input = $request->all();
        $detailsother = Seller::where(['email' => $input['username'], 'password' => $input['password']])->first();
        if (!empty($detailsother)) {
            return $this->sendResponse($detailsother, 'You login successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Your username or Password is Incorrect");
        }
    }

    public function adminlogin(Request $request) {
        $input = $request->all();
        $detailsother = Admin::where(['email' => $input['username'], 'password' => $input['password']])->first();
        if (!empty($detailsother)) {
            $detailsother->image = $this->baseurlslash.$detailsother->image;
            return $this->sendResponse($detailsother, 'Logged in successfully.', $request->path());
        } else {
            return $this->sendError($request->path(), "Your Username or password is incorrect");
        }
    }
    public function forgotpasswordseller(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['email' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        if ($this->checkemail($input['email']) == 0) {
            return $this->sendError($request->path(), "Email Id does not Exist");
        }
        $userdetails = Seller::where('email', $input['email'])->first();
        $otp = $userdetails->password;
        $email = $input['email'];
        $subject = "Request for Forgot Password";
        $postData = "";
        try {
            Mail::send('emails.otpverify', ['otp' => $otp], function ($message) use ($postData, $email) {
                $message->from('asecclass@asecenglish.awsapps.com', 'satvick');
                $message->to($email, 'satvick')->subject('Request for Forgot Password');
            });
        }
        catch(Exception $e) {
        }
        return $this->sendResponse(['status' => 'success'], 'Password sent to your email successfully', $request->path());
    }

    public function signup(Request $request) {
        $input = $request->all();
        $detailsofrecord = array();
            
        $validator = Validator::make($input, [
            'password' => 'required|min:6',
            'name' => 'required',
            'email' => 'required|unique:users,email',
            'phone' => 'required|unique:users,phone',
            'country' => 'required',
            'deviceType' => 'required',
            'deviceToken' => 'required'
        ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $input['referal_code'] = 'satvick'. $this->random_strings(4);
        $input['password'] = Hash::make($input['password']);
        $input['status'] = 1;
        $insertmember = User::create($input);
        
        $input['user_id'] = (string)$insertmember->id;
        $input['token'] = str_random(25);
        $newtokenarray = Token::create($input);

        if(isset($input['refered_by'])) {
            if(strlen($input['refered_by']) > 0) {
                
                $getUser = User::where(["referal_code"=>$input['refered_by']])->first();
                if($getUser) {
                    Refer::create([
                        "referto"=>$insertmember->id,
                        "referby"=>$getUser->id,
                        "refercode"=>$input['refered_by'],
                    ]);
                }
            }
        }

        if (!empty($input['cartlist'])) {
            $this->addtocart1($details['user_id'], explode(",", $input['cartlist']), explode(",", $input['size']), explode(",", $input['color']), explode(",", $input['quantity']));
        }
        return $this->sendResponse($input, 'User created successfully.', $request->path());
    }

    public function login(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'email' => 'required',
            'password' => 'required',
            'deviceToken' => 'required',
            'deviceType' => 'required|in:android,ios', 
        ]);

        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $checkUser = User::where(['email' => $input['email']])->first();

        if (empty($checkUser)) {
            $checkUser = User::where(['phone' => $input['email']])->first();

            if (empty($checkUser)) {
                return $this->sendError($request->path(), "You are not a registered user.");
            }
        }

        if (empty($checkUser->status)) {
            return $this->sendError($request->path(), "Your account is inactive");
        }

        if(!Hash::check($input['password'], $checkUser->password)) {
            return $this->sendError($request->path(), "Your Id or Password is Incorrect");
        }
        
        $token_s = str_random(25);
        $findtokencolm = Token::where('user_id', $checkUser->id)->first();
    
        if (!empty($findtokencolm)) {
            $token_saver = Token::where('user_id', $checkUser->id)->update(['token' => $token_s, 'deviceToken' => $input['deviceToken'], 'deviceType' => $input['deviceType']]);
        } else {
            $token_saver = Token::create(array("user_id" => $checkUser->id, "token" => $token_s, "deviceToken" => $input['deviceToken'], "deviceType" => $input['deviceType']));
        }

        $userid = (string)$checkUser->id;
    
        if (!empty($input['cartlist'])) {
            $this->addtocart1($userid, explode(",", $input['cartlist']), explode(",", $input['size']), explode(",", $input['color']), explode(",", $input['quantity']));
        }


        if (!empty($checkUser->image)) {
            $checkUser->image = url('/public/') . "/" . $checkUser->image;
        } else {
            $checkUser->image = url('/public/') . "/img/imagenotfound.jpg";
        }

        $checkUser->token = $token_s;

        return $this->sendResponse($checkUser, 'User login successfully.', $request->path());
    }

    public function sociallogin(Request $request) {
        $input = $request->all();

        $validator = Validator::make($input, [
            'social_id' => 'required',
            'social_type' => 'required',
            'deviceToken' => 'required',
            'deviceType' => 'required|in:android,ios', 
        ]);

        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $checkUser = User::where(['social_id'=>$input['social_id'], 'social_type'=>$input['social_type']])->first();

        if ($checkUser) {
            
            if (empty($checkUser->status)) {
                return $this->sendError($request->path(), "Your account is inactive");
            }

            $token_s = str_random(25);
            $findtokencolm = Token::where('user_id', $checkUser->id)->first();
        
            if (!empty($findtokencolm)) {
                $token_saver = Token::where('user_id', $checkUser->id)->update(['token' => $token_s, 'deviceToken' => $input['deviceToken'], 'deviceType' => $input['deviceType']]);
            } else {
                $token_saver = Token::create(array("user_id" => $checkUser->id, "token" => $token_s, "deviceToken" => $input['deviceToken'], "deviceType" => $input['deviceType']));
            }

            $userid = (string)$checkUser->id;
    
            if (!empty($input['cartlist'])) {
                $this->addtocart1($userid, explode(",", $input['cartlist']), explode(",", $input['size']), explode(",", $input['color']), explode(",", $input['quantity']));
            }

            $checkUser->token = $token_s;
            if (!empty($checkUser->image)) {
                $checkUser->image = url('/public/') . "/" . $checkUser->image;
            } else {
                $checkUser->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            return $this->sendResponse($checkUser, 'User login successfully', $request->path());
        
        } else {

            $input['referal_code'] = 'satvick'. $this->random_strings(4);
            $input['status'] = 1;
            $insertmember = User::create($input);
            
            $input['user_id'] = (string)$insertmember->id;
            $input['token'] = str_random(25);
            $newtokenarray = Token::create($input);

            if (!empty($input['cartlist'])) {
                $this->addtocart1($insertmember->id, explode(",", $input['cartlist']), explode(",", $input['size']), explode(",", $input['color']), explode(",", $input['quantity']));
            }

            $checkUser = User::where(['id'=>$insertmember->id])->first();

            $checkUser->token = $input['token'];
            if (!empty($checkUser->image)) {
                $checkUser->image = url('/public/') . "/" . $checkUser->image;
            } else {
                $checkUser->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            return $this->sendResponse($checkUser, 'User login successfully', $request->path());

        }
    }
    
    public function changepassword(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'password' => 'required|min:6', 'oldpassword' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        $post = User::find($input['user_id']);
        if ($post->password != $input['oldpassword']) {
            return $this->sendError($request->path(), "Old password is incorrect.");
        }
        $post->password = Hash::make($input['password']);
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password Changed Successfully.', $request->path());
    }

    public function changepasswordbyphone(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', 'password' => 'required|min:6', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $post = User::where('phone', $input['phone'])->first();
        $post->password = Hash::make($input['password']);
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password updated successfully.', $request->path());
    }

    public function notifystatus(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        $userdetails = User::find($input['user_id']);
        if ($userdetails->notify_status == "0") {
            $userdetails->notify_status = "1";
            $userdetails->save();
            return $this->sendResponse(['status' => "1"], 'Notification status change successfully', $request->path());
        } else {
            $userdetails->notify_status = "0";
            $userdetails->save();
            return $this->sendResponse(['status' => "0"], 'Notification status change successfully', $request->path());
        }
    }

    public function viewprofile(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        $userdetails = User::find($input['user_id']);

        if (!empty($userdetails->image)) {
            $userdetailsimage = url('/public/') . $userdetails->image;
        } else {
            $userdetailsimage = url('/public/') . "/img/noImagefound.png";
        }
        if (empty($userdetails->dob)) {
            $userdetailsdob = "";
        } else {
            $userdetailsdob = $userdetails->dob;
        }
        if (empty($userdetails->gender)) {
            $userdetailsgender = "";
        } else {
            $userdetailsgender = $userdetails->gender;
        }
        
        $return_array = array("user_id" => (string)$userdetails->id,
                              "image" => $userdetailsimage,
                              "name" => $userdetails->name,
                              "email" => $userdetails->email,
                              "phone" => $userdetails->phone,
                              "dob" => $userdetailsdob,
                              "gender" => $userdetailsgender);
        return $this->sendResponse($return_array, 'User details retrieved successfully', $request->path());
    }

    public function viewprofileuseradmin(Request $request, $id) {

        if(empty($id)) {
            return $this->sendError($request->path(), 'User not found');
        }

        $check_token = Token::where(['user_id' => $id])->first();
        
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'User not found');
        }

        $userdetails = User::find($id);
        
        if (!empty($userdetails->image)) {
            $userdetails->image = $userdetails->image;
        } else {
            $userdetails->image = "/img/imagenotfound.jpg";
        }
        
        if (empty($userdetails->dob)) {
            $userdetails->dob = "";
        }
        
        if (empty($userdetails->gender)) {
            $userdetails->gender = "";
        }
        
        $return_array = array("user_id" => (string)$userdetails->id, "image" => $userdetails->image, "name" => $userdetails->name, "email" => $userdetails->email, "phone" => $userdetails->phone, "dob" => $userdetails->dob, "gender" => $userdetails->gender);
        return $this->sendResponse($return_array, 'User details retrieved successfully', $request->path());
    }

    public function set_as_default(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['user_id' => 'required', 'token' => 'required', 'address_id' => 'required', 'remark' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        if (isset($input['remark']) && !empty($input['remark']) && $input['remark'] == 1) {
            DB::table('addresses')->where(['user_id' => $input['user_id'], 'remark' => '1'])->update(['remark' => '0']);
        }
        $detailsadd = Address::find($input['address_id']);
        $detailsadd->remark = $input['remark'];
        $detailsadd->save();
        return $this->sendResponse(['status' => "success"], 'Address set as default successfully', $request->path());
    }
    public function addaddress(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['user_id' => 'required', 'token' => 'required', 'name' => 'required', 'phone' => 'required', 'pincode' => 'required', 'address' => 'required', 'town' => 'required', 'city' => 'required', 'state' => 'required', 'latitude' => 'required', 'longitude' => 'required' ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        $checkAddress = Address::where(["user_id" => $input['user_id']])->count();
        if ($checkAddress == 2) {
            return $this->sendError($request->path(), "Address not able to save more than two");
        } else {
            if ($input['popup'] == 0) {
                $checkRemark = Address::where(["user_id" => $input['user_id'], 'remark' => $input['remark']])->first();
                if ($checkRemark) {
                    return $this->sendError($request->path(), "already saved");
                } else {
                    Address::create(['user_id' => $input['user_id'], 'name' => $input['name'], 'phone' => $input['phone'], 'pincode' => $input['pincode'], 'address' => $input['address'], 'town' => $input['town'], 'city' => $input['city'], 'state' => $input['state'], 'type' => $input['type'], 'remark' => $input['remark'],'latitude' => $input['latitude'],'longitude' => $input['longitude'],'country' => $input['country'] ]);
                    return $this->sendResponse(['status' => "success"], 'Address added successfully', $request->path());
                }
            } else {
                $checkRemark = Address::where(["user_id" => $input['user_id'], 'remark' => $input['remark']])->first();
                $checkRemark->name = $input['name'];
                $checkRemark->phone = $input['phone'];
                $checkRemark->pincode = $input['pincode'];
                $checkRemark->address = $input['address'];
                $checkRemark->town = $input['town'];
                $checkRemark->city = $input['city'];
                $checkRemark->state = $input['state'];
                $checkRemark->latitude = $input['latitude'];
                $checkRemark->longitude = $input['longitude'];
                $checkRemark->country = $input['country'];
                $checkRemark->save();
                return $this->sendResponse(['status' => "success"], 'Address updated successfully', $request->path());
            }
        }
    }
    public function editaddress(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['user_id' => 'required', 'token' => 'required', 'name' => 'required', 'phone' => 'required', 'pincode' => 'required', 'address' => 'required', 'town' => 'required', 'city' => 'required', 'state' => 'required', 'address_id' => 'required', 'latitude' => 'required', 'longitude' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        $detailsadd = Address::find($input['address_id']);
        $detailsadd->name = $input['name'];
        $detailsadd->phone = $input['phone'];
        $detailsadd->pincode = $input['pincode'];
        $detailsadd->address = $input['address'];
        $detailsadd->town = $input['town'];
        $detailsadd->city = $input['city'];
        $detailsadd->state = $input['state'];
        $detailsadd->latitude = $input['latitude'];
        $detailsadd->longitude = $input['longitude'];
        $detailsadd->country = $input['country'];
        $detailsadd->save();
        return $this->sendResponse(['status' => "success"], 'Address updated successfully', $request->path());
    }
    public function removeaddress(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['user_id' => 'required', 'token' => 'required', 'address_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        DB::table('addresses')->where(['id' => $input['address_id']])->delete();
        return $this->sendResponse(['status' => "success"], 'Address removed successfully', $request->path());
    }

    public function homeapi(Request $request) {
        $input = $request->all();
        $return_array = array();
        $banners_array = array();
        $banner1 = array();
        $banner2 = array();
        $men = array();
        $women = array();
        $kids = array();
        $slider1 = array();
        $slider2 = array();
        $handpicked = array();
        $stylefeed = array();
        $brandinfocus = array();
        $hotdeal = array();
        $themedetailsarray = array();
        $announceArr = array();
        $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();
        
        $bannerdetails = Banner::get();
        foreach ($bannerdetails as $value) {

            $checkSubSubcat = Subsubcategory::where('id', $value->subsubcategory_id)->first();

            if($checkSubSubcat && $checkSubSubcat->status == 1) {

                if (!empty($value->image)) {
                    if (strpos($value->image, $this->baseurlslash) == false) {
                        $value->image = $this->baseurl . $value->image;
                    }
                } else {
                    $value->image = url('/public/') . "/img/imagenotfound.jpg";
                }

                if ($value->type == 'homeslider') {
                    $banners_array[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'banner1') {
                    $banner1[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'banner2') {
                    $banner2[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'slider1') {
                    $slider1[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'slider2') {
                    $slider2[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'handpicked') {
                    $handpicked[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'stylefeed') {
                    $stylefeed[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
                if ($value->type == 'hotdeal') {
                    $hotdeal[] = array("filter_data" => $value->subsubcategory_id, "type" => "subsubcategory_id", "image" => $value->image);
                }
            }
        }
        
        $deatilsbrand = Brand::where('focus_status', '1')->get();
        foreach ($deatilsbrand as $value) {
            if (!empty($value->image)) {
                if (strpos($value->image, $this->baseurlslash) == false) {
                    $value->image = $this->baseurl . $value->image;
                }
            } else {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $brandinfocus[] = array("filter_data" => $value->name, "type" => "brand", "image" => $value->image);
        }
        
        $themedetails = Theme::get();
        foreach ($themedetails as $value) {
            if (!empty($value->imagemain)) {
                if (strpos($value->imagemain, $this->baseurlslash) == false) {
                    $value->imagemain = $this->baseurl . $value->imagemain;
                }
            } else {
                $value->imagemain = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($value->herimage)) {
                if (strpos($value->herimage, $this->baseurlslash) == false) {
                    $value->herimage = $this->baseurl . $value->herimage;
                }
            } else {
                $value->herimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($value->himimage)) {
                if (strpos($value->himimage, $this->baseurlslash) == false) {
                    $value->himimage = $this->baseurl . $value->himimage;
                }
            } else {
                $value->himimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            $themedetailsarray[] = array("image" => $value->imagemain, "himimage" => $value->himimage, "herimage" => $value->herimage, "title" => $value->title, "description" => $value->description, "filter_data" => (string)$value->id, "type" => "theme_id");
        }
        
        $flash_sale = array();
        $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
        
        if (!empty($findflashdetails)) {
            $flashdatenew = date('Y-m-d H:i:s', strtotime($findflashdetails->time));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr >= $flashStr) {

                $flashdateend = date('Y-m-d H:i:s', strtotime($findflashdetails->endtime));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                if($currentStrEnd <= $flashStrEnd) {

                    $findflashdetailsstatus = $findflashdetails->status;
                    $flashtime = date('d-m-Y, h:i:s', strtotime($findflashdetails->endtime));
                    $flashtime = strtotime($findflashdetails->endtime);
                    $currentDate = date('d-m-Y, h:i:s');
                    $currentDateStr = strtotime($currentDate);

                    $timeDiff = abs($flashtime - $currentDateStr);
                    $findflashdetailstime = $timeDiff;

                    $findflashdetails->products = trim($findflashdetails->products);

                    if(strlen($findflashdetails->products)>=1) {
                      $productdetails = Product::whereIn('id', explode(",", $findflashdetails->products))->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    } else {
                      $productdetails = Product::where(['subsubcategory_id'=>$findflashdetails->subsubcategory])->where(["deleteStatus"=>1,"status"=>1])->orderBy('id','DESC')->get();
                    }
                    
                    foreach ($productdetails as $value) {
                        $imagearray = explode(",", $value->images);
                        if (!empty($imagearray)) {
                            if (!empty($imagearray[0])) {
                                if (strpos($imagearray[0], $this->baseurlslash) == false) {
                                    $imagearray[0] = $this->baseurl . $imagearray[0];
                                }
                            } else {
                                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                            }
                        } else {
                            $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                        }
                        $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                        
                        if (!empty($findflagofwishlist)) {
                            $like_Status = "1";
                        } else {
                            $like_Status = "0";
                        }
                        $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                        $flash_sale[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                    }
                }  else {
                    $flash_sale = [];
                    $findflashdetailsstatus = '';
                    $findflashdetailstime = '';    
                } 
            } else {
                $flash_sale = [];
                $findflashdetailsstatus = '';
                $findflashdetailstime = '';    
            }
        } else {
            $flash_sale = [];
            $findflashdetailsstatus = '';
            $findflashdetailstime = '';
        }

        $announceGets = Offer::where(['offer_type' => 3])->orderBy('id','ASC')->get();
        if($announceGets) {
            foreach($announceGets as $announceGet) {
                $announceArr[] = array("filter_data" => $announceGet->catid, "type" => "subsubcategory_id", "content" => $announceGet->description);
            }
        }


        $men = Subcategory::where(['category_id' => 'MEN'])->skip(0)->take(5)->get();
        $women = Subcategory::where(['category_id' => 'WOMEN'])->skip(0)->take(5)->get();
        $kids = Subcategory::where(['category_id' => 'KIDS'])->skip(0)->take(5)->get();
        
        $return_array = array("banners" => $banners_array,
                              "handpicked" => $handpicked, 
                              "banner1" => $banner1, 
                              "banner2" => $banner2, 
                              "hotdeal" => $hotdeal, 
                              "slider2" => $slider2, 
                              "slider1" => $slider1, 
                              "stylefeed" => $stylefeed, 
                              "brandinfocus" => $brandinfocus, 
                              "themedetails" => $themedetailsarray, 
                              "flash_sale" => $flash_sale, 
                              "flash_sale_status" => $findflashdetailsstatus, 
                              "flash_sale_time" => $findflashdetailstime,
                              "num_of_addtocart" => $num_of_addtocart, 
                              'announcement_bar' => $announceArr, 
                              'men' => $men, 
                              'women' => $women, 
                              'kids' => $kids);
        
        return $this->sendResponse($return_array, 'Home details retrieved successfully', $request->path());
    }
    
    public function viewaddress(Request $request, $userid) {
        $details = Address::where(['user_id' => $userid])->get();

        return $this->sendResponse($details, 'Address list retrieved successfully', $request->path());
    }
    
    public function productlistforseller(Request $request, $userid) {

        $details = Product::select("sku", "name", "category_id", "brand", "sp", "mrp", "quantity", "status", "is_variant", "id")->where(['user_id' => $userid, 'deleteStatus'=>1])->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {

            $getSeller = Seller::where(["id"=>$userid])->first();
            $comm = $getSeller->commission;

            if($met->is_variant == 2) {
                $getProPrices = ProductPrice::where('product_id', $met->id)->get();
                if($getProPrices) {
                    
                    foreach($getProPrices as $getProPrice) {

                        $return_array[] = [$met->sku, $met->name, $met->category_id, $getProPrice->size, $getProPrice->sp, $getProPrice->mrp, $getProPrice->quantity, [$met->id, $getProPrice->id, $getProPrice->quantity], [$met->id, $getProPrice->id, $getProPrice->sp, $comm, $getProPrice->quantity],[$met->status, $met->id], $met->id, $met->id];
                    }
                }
            } else {

                $getProPriceid = 0;

                $return_array[] = [$met->sku, $met->name, $met->category_id, '-', $met->sp, $met->mrp, $met->quantity, [$met->id,$getProPriceid, $met->quantity], [$met->id,$getProPriceid,$met->sp,$comm, $met->quantity], [$met->status, $met->id], $met->id,$met->id];
            }
        }
        return $this->sendResponse1($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function faqlist(Request $request) {
        $details = Faq::get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->question, $met->ans, $met->id);
        }
        return $this->sendResponse1($return_array, 'Faq list retrieved successfully', $request->path());
    }
    public function addfaq(Request $request) {
        $input = $request->all();
        Faq::create($input);
        return $this->sendResponse1(['success' => '1'], 'Faq added successfully', $request->path());
    }
    public function deletefaq(Request $request, $id) {
        $details = Faq::find($id);
        Faq::where('id', $id)->delete();
        return $this->sendResponse1([], 'Faq deleted successfully', $request->path());
    }

    public function productlistforadmin(Request $request, $userid) {
        $details = Product::select("sku","user_id", "name", "category_id", "brand", "sp","mrp", "quantity", "status","payment_mode", "id")->where(["deleteStatus"=>1])->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {
            $getSeller = Seller::where(["id"=>$met->user_id])->first();
            $comm = $getSeller->commission;
            if($met->payment_mode == 1) {
                $paymode = "Active";
            } else {
                $paymode = "Inactive";
            }
            $return_array[] = array($met->sku, $met->name, $met->category_id, $met->brand, $paymode, $met->sp, $met->mrp, $met->quantity, [$met->id, $met->quantity], [$met->id,$met->sp,$comm, $met->quantity],[$met->status, $met->id], $met->id,$met->id);
        }
        return $this->sendResponse1($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function bannerlistforadmin(Request $request) {
        $details = Banner::all();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {

                $subcat = Subcategory::where(['id'=>$met->subcategory_id])->first();
                if($subcat) {
                    $subcatname = $subcat->name;
                } else {
                    $subcatname = '';
                }
                $subsubcat = Subsubcategory::where(['id'=>$met->subsubcategory_id])->first();
                if($subsubcat) {
                    $subsubcatname = $subsubcat->name;
                } else {
                    $subsubcatname = '';
                }
            
                $return_array[] = array($met->image, $met->type, $met->category_id, $subcatname, $subsubcatname, $met->id);
            }
        }
        return $this->sendResponse1($return_array, 'Banner list retrieved successfully', $request->path());
    }

    public function themelistforadmin(Request $request) {
        $details = Theme::select("id", "imagemain", "himimage", "herimage", "title", "description")->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->imagemain)) {
                if (strpos($met->imagemain, $this->baseurlslash) == false) {
                    $met->imagemain = $this->baseurl . $met->imagemain;
                }
            } else {
                $met->imagemain = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($met->himimage)) {
                if (strpos($met->himimage, $this->baseurlslash) == false) {
                    $met->himimage = $this->baseurl . $met->himimage;
                }
            } else {
                $met->himimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            if (!empty($met->herimage)) {
                if (strpos($met->herimage, $this->baseurlslash) == false) {
                    $met->herimage = $this->baseurl . $met->herimage;
                }
            } else {
                $met->herimage = url('/public/') . "/img/imagenotfound.jpg";
            }
            $return_array[] = array($met->title, $met->description, $met->imagemain, $met->himimage, $met->herimage, $met->id);
        }
        return $this->sendResponse1($return_array, 'Theme list retrieved successfully', $request->path());
    }

    public function brandlistforadmin(Request $request) {
        $details = Brand::select("id", "name", "image", "description", "focus_status", "admin_status")->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->image)) {
                if (strpos($met->image, $this->baseurlslash) == false) {
                    $met->image = $met->image;
                }
            } else {
                $met->image = url('/public/') . "/img/imagenotfound.jpg";
            }
            $return_array[] = array($met->name, $met->description, $met->image, [$met->admin_status, $met->id], [$met->focus_status, $met->id], $met->id);
        }
        return $this->sendResponse1($return_array, 'Brand list retrieved successfully', $request->path());
    }

    public function brandviewforadmin(Request $request, $id) {
        $details = Brand::select("id", "name", "image", "description", "focus_status", "admin_status")->where(["id"=>$id])->first();
        $return_array = array();

        if (!empty($details->image)) {
            
                $details->image = $details->image;
            
        } else {
            $details->image = url('/public/') . "/img/imagenotfound.jpg";
        }
        $return_array = ["name"=>$details->name, "description"=>$details->description, "image"=> $details->image,"id"=> $details->id];
        
        return $this->sendResponse1($return_array, 'Brand retrieved successfully', $request->path());
    }

    public function brandeditforadmin(Request $request, $id) {
        $details = Brand::where(["id"=>$id])->first();

        if($details) {
            
            $details->name = $request->name;
            $details->image = $request->image;
            $details->description = $request->description;
            $details->save();

            return $this->sendResponse(['status' => 'success'], 'Brand updated successfully', $request->path());
        } else {
            return $this->sendError($request->path(), 'Brand not found');
        }
    }

    public function productstatuschange(Request $request, $userid) {
        $details = Product::find($userid);
        if ($details->status) {
            $details->status = "";
        } else {
            $details->status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Product status updated successfully', $request->path());
    }

    public function brandapprovalstatuschange(Request $request, $userid) {
        $details = Brand::find($userid);
        if ($details->admin_status) {
            $details->admin_status = "";
        } else {
            $details->admin_status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->admin_status, $userid], 'Brand Approval status updated successfully', $request->path());
    }
    public function brandfocusstatuschange(Request $request, $userid) {
        $details = Brand::find($userid);
        if ($details->focus_status) {
            $details->focus_status = "";
        } else {
            $details->focus_status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->focus_status, $userid], 'Brand Infocus status updated successfully', $request->path());
    }

    public function productstatuschange1(Request $request, $userid) {
        $details = User::find($userid);
        
        if ($details->status) {
            $details->status = "";
        } else {
            $details->status = "1";
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Buyer status updated successfully', $request->path());
    }

    public function productstatuschange11(Request $request, $userid) {
        $details = Seller::find($userid);
        if ($details->status) {
            $details->status = "";
            Product::where(['user_id'=>$userid])->update(['deleteStatus' => 0]);
        } else {
            $details->status = "1";
            Product::where(['user_id'=>$userid])->update(['deleteStatus' => 1]);
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Seller status updated successfully', $request->path());
    }

    public function cupons(Request $request) {
        $details = Cuponcode::get();
        $return_array = array();

        foreach ($details as $met) {
            $flashdatenew = date('Y-m-d H:i:s', strtotime($met->startdate));
            $flashStr = strtotime($flashdatenew);
            $currentDate = date('Y-m-d H:i:s');
            $currentStr = strtotime($currentDate);

            if($currentStr > $flashStr) {
                $flashdateend = date('Y-m-d H:i:s', strtotime($met->expdate));
                $flashStrEnd = strtotime($flashdateend);
                $currentDateEnd = date('Y-m-d H:i:s');
                $currentStrEnd = strtotime($currentDateEnd);

                if($currentStrEnd < $flashStrEnd) {

                    $return_array[] = array("cupon_code" => $met->name, "discount" => $met->discount,"discount_type"=>$met->type, "expiry_date" => date("M d Y", strtotime($met->expdate)), "cupon_id" => $met->id, "min_price" => $met->min_price);
                }
            }
        }
        return $this->sendResponse1($return_array, 'Cupon list retrieved successfully', $request->path());
    }

    public function cuponlist(Request $request) {
        $details = Cuponcode::orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {

            $return_array[] = array($met->name, $met->min_price, $met->discount, date("M d Y", strtotime($met->startdate)), date("M d Y", strtotime($met->expdate)), $met->id);
        }
        return $this->sendResponse1($return_array, 'Coupon list retrieved successfully', $request->path());
    }

    public function createcode(Request $request) {
        $input = $request->all();
        $details = Cuponcode::create($input);
        return $this->sendResponse1($input, 'Coupon added successfully', $request->path());
    }

    public function productstatuschange2(Request $request, $userid) {
        $details = Subsubcategory::find($userid);
        if ($details->status) {
            $details->status = "";
            Product::where(["subcategory_id"=>$userid])->update(["status"=>'']);
        } else {
            $details->status = "1";
            Product::where(["subcategory_id"=>$userid])->update(["status"=>1]);
        }
        $details->save();
        return $this->sendResponse1([$details->status, $userid], 'Subcategory status updated successfully', $request->path());
    }

    public function request_for_order(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'order_id' => 'required', 'product_id' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }

        $details = Orderrequest::where(['order_id' => $input['order_id'], 'product_id' => $input['product_id']])->first();
        if ($details) {
            return $this->sendError($request->path(), 'You have already request for cancellation!');
        }

        $orderGet = Order::where(['id'=>$input['order_id']])->first();
        $details = OrderDetail::where(['order_id'=>$input['order_id']])->first();

        $input['seller_id'] = $orderGet->seller_id;
        $input['refund_by'] = "user";
        $input['price'] = $details->final_price;

        Orderrequest::create($input);

        if($input['request_type'] == "cancel") {

            $msg = "Your order has been cancelled";
            $jsonarray = [
                "waybill"=>$details->wbn_num,
                "cancellation"=>"true"
            ];

            $headers['headers'] = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
                'Accept' => 'application/json',
            ];

            $url = "https://staging-express.delhivery.com/api/p/edit";
            $myBody = $jsonarray;

            $client = new Client($headers);
            $response = $client->post($url, [
                'http_errors' => false,
                'json' => $myBody,
            ]);
            $status = $response->getStatusCode();
            $data = json_decode($response->getBody()->getContents());

            Order::where(['id'=>$input['order_id']])->update(['status' => $input['request_type']]);
        } else {
            Order::where(['id'=>$input['order_id']])->update(['status' => $input['request_type']]);
        }

        return $this->sendResponse1([], 'Your request submited, We notify soon.', $request->path());
    }

    public function productdelete(Request $request, $userid) {
        $details = Product::find($userid);
        $product_name = $details->name;
        Product::where(['id'=>$details->id])->update(['deleteStatus' => 0]);
        return $this->sendResponse1([], $product_name . 'Deleted successfully', $request->path());
    }

    public function userdelete(Request $request, $userid) {
        $details = User::find($userid);
        $product_name = $details->name;
        User::where('id', $userid)->delete();
        Cart::where('user_id', $userid)->delete();
        Notification::where('user_id', $userid)->delete();
        Order::where('user_id', $userid)->delete();
        Token::where('user_id', $userid)->delete();
        Wishlist::where('user_id', $userid)->delete();
        return $this->sendResponse1([], 'Buyer deleted successfully', $request->path());
    }

    public function sellerdelete(Request $request, $userid) {
        $details = Seller::find($userid);
        $product_name = $details->name;
        Seller::where('id', $userid)->delete();
        Brand::where('seller_id', $userid)->update(['seller_id' => 1]);
        Product::where('user_id', $userid)->update(['deleteStatus' => 0]);
        return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
    }

    public function catdelete(Request $request, $userid) {
        $verifyProduct = Product::where(["subcategory_id"=>$userid])->first();
        if($verifyProduct) {
            return $this->sendError($request->path(), 'SubCategory not able to delete because related product available');
        } else {

            $verifyBanner = Banner::where(["subcategory_id"=>$userid])->first();
            if($verifyBanner) {
                return $this->sendError($request->path(), 'SubCategory not able to delete because related banner available');
            } else {
                $details = DB::table("subcategories")->where('id', $userid)->first();
                $product_name = $details->name;
                DB::table("subcategories")->where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
            }
        }
    }

    public function subsubcatdelete(Request $request, $userid) {
        $verifyProduct = Product::where(["subsubcategory_id"=>$userid])->first();
        if($verifyProduct) {
            return $this->sendError($request->path(), 'Sub-SubCategory not able to delete because related product available');
        } else {

            $verifyBanner = Subsubcategory::where(["id"=>$userid])->first();
            if($verifyBanner) {
                return $this->sendError($request->path(), 'Sub-SubCategory not available');
            } else {
                DB::table("subsubcategories")->where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' Deleted successfully', $request->path());
            }
        }
    }
    
    public function catdeletebrand(Request $request, $userid) {
        $details = DB::table("brands")->where('id', $userid)->first();

        if($details) {
            $findbrand = Product::where(["brand"=>$details->name])->first();
            
            if($findbrand) {
                return $this->sendError($request->path(), 'Brand not able to delete because related product available');
            } else {
                $product_name = $details->name;
                DB::table("brands")->where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
            }
        } else {
            return $this->sendError($request->path(), 'Brand Not found');
        }
    }

    public function catdeleteb(Request $request, $userid) {
        DB::table("banners")->where('id', $userid)->delete();
        return $this->sendResponse1([], 'A banner deleted successfully', $request->path());
    }
    
    public function catdeletet(Request $request, $userid) {
        $verifyTheme = Product::where(["theme_id"=>$userid])->first();
        
        if($verifyTheme) {
            return $this->sendError($request->path(), 'Theme not able to delete because related product available');
        }else {
            DB::table("themes")->where('id', $userid)->delete();
            return $this->sendResponse1([], 'A Theme deleted successfully', $request->path());
        }
    }

    public function catdelete11(Request $request, $userid) {
        
        $details = DB::table("subsubcategories")->where('id', $userid)->first();
        $product_name = $details->name;
        DB::table("subsubcategories")->where('id', $userid)->delete();
        return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
    }
    public function catdelete113(Request $request, $userid) {
        $details = Cuponcode::where('id', $userid)->first();
        if($details) {
            $product_name = $details->name;
            $checkinOffer = Offer::where(["coupon_id"=>$userid])->first();
            if($checkinOffer) {
                return $this->sendResponse1([], 'Coupon added in offers! Please remove first from offers', $request->path());
            } else {
                Cuponcode::where('id', $userid)->delete();
                return $this->sendResponse1([], $product_name . ' deleted successfully', $request->path());
            }
        } else {
            return $this->sendResponse1([], 'Coupon not found', $request->path());
        }
    }
    public function catdelete11pp(Request $request, $userid) {
        $details = Flashsale::find(1);
        if(strlen($details->products) > 0) {
            $newarray = explode(",", $details->products);
            $return_array = array();
            foreach ($newarray as $value) {
                if ($value != $userid) {
                    $return_array[] = $value;
                }
            }
            $details->products = implode(",", $return_array);
        } else {
            $details->subsubcategory = "";
        }
        $details->save();
        return $this->sendResponse1([], 'Product removed from flash sale successfully', $request->path());
    }

    public function addproductbyseller(Request $request, $userid) {
        $input = $request->all();
        $colorWithImageArr = $request->color_with_image;

        $return_array = array();
        $input['user_id'] = $userid;
        $percentage = 100 - ($input['sp'] * 100) / $input['mrp'];
        $input['discount'] = $percentage;
        if(!empty($colorWithImageArr)) {
            $input['images'] = implode(',',$colorWithImageArr[0]['image']);
        }else {
            $input['images'] = "";
        }

        if(isset($input['size'])) {
            if(!empty($input['size'])) {
                $return_array1 = array();
                foreach ($input['size'] as $value) {
                    $return_array1[] = $value['value'];
                }
                $input['size'] = implode(",", $return_array1);
            } else {
                $input['size'] = "";
            }
        }

        // if(isset($input['color'])) {
        //     if(!empty($input['color'])) {
        //         $return_array2 = array();
        //         foreach ($input['color'] as $value) {
        //             $return_array2[] = $value['value'];
        //         }
        //         $input['color'] = implode(",", $return_array2);
        //     } else {
        //         $input['color'] = "";
        //     }
        // }

        $scom = Seller::select("commission")->where(["id"=>$userid])->first();
        $calComm = ($input['sp']  * $scom->commission) / 100;
        $gstRate = ($calComm * 18) / 100;
        $input['commision'] = $calComm;
        $input['tax'] = $gstRate;

        if($input['is_variant'] == 1) {
            unset($input['size_label']);
        }

        $details = Product::create($input);

        if(isset($input['product_variant'])) {

            if(count($input['product_variant']) > 0) {
            
                foreach($input['product_variant'] as $variant) {
                    $scom = Seller::select("commission")->where(["id"=>$userid])->first();
                    $calComm = ($variant['sp']  * $scom->commission) / 100;
                    $gstRate = ($calComm * 18) / 100;
                    $input_commision = $calComm;
                    $input_tax = $gstRate;

                    $input_discount = 100 - ($variant['sp'] * 100) / $variant['mrp'];

                    ProductPrice::create([
                        "product_id"=>$details->id,
                        "sp"=>$variant['sp'],
                        "commision"=>$input_commision,
                        "tax"=>$input_tax,
                        "quantity"=>$variant['quantity'],
                        "mrp"=>$variant['mrp'],
                        "discount"=>$input_discount,
                        "size"=>$variant['size'],
                        //"color"=>$variant['color'],
                    ]);
                }
            }
        }

        // add product image with color seprate table
        if(!empty($colorWithImageArr)) {
            foreach ($colorWithImageArr as $value) {

                if(isset($value['size'])) {
                    $image = implode(',',$value['image']);
                    $pro_img_color = ProductImageColor::create([
                               'product_id' => $details->id,
                               'images'     => $image,
                               //'color'=>  $value['name'],
                               'size'=>  $value['size'],
                               'status' =>1,
                              ]);
                }
            }
        }

        if(!empty($input['attribute'])) {
            $attris = $input['attribute'];
            foreach($attris as $attri) {
                Attribute::create([
                    "product_id"=>$details->id,
                    "label"=>$attri['labal'],
                    "labeltext"=>$attri['text']
                ]);
            }
        }

        return $this->sendResponse1($colorWithImageArr, 'Product added successfully', $request->path());
    }

    public function getproductdetails(Request $request, $idk) {
      // echo "hi";die;
        $details = Product::find($idk);
        $product_name = $details->name;
        $sizearray = explode(",", $details->size);
        
        foreach ($sizearray as $value) {
            $return_array[] = ['value' => $value, 'label' => $value];
        }
        //$colorarray = explode(",", $details->color);
        
        //foreach ($colorarray as $value) {
            //$getColor = Color::where('hexcode',$value)->first();
            //$value->name - $value->hexcode
            //$return_array11[] = ['value' => $value, 'label' => $value];
        //}
       // $image_array = explode(",", $details->images);
        $return_new_array = array();
        $image_array = ProductImageColor::where('product_id',$idk)->get();
        $finalImgArr = [];
        foreach ($image_array as $value) {
            // $getColor = Color::where('hexcode',$value->color)->first();
            // if($getColor) {
            //     $colorName = $getColor->name;
            // } else {
            //     $colorName = "";
            // }
                
                //$nameWithHexa = "$colorName";
                $finalImgArr[]  = array(
                        //'color' => $nameWithHexa,
                        'value' =>$value->color,
                        'size' =>$value->size,
                        'image' => explode(',',$value->images),
                     );
        }

      
        $details->imagearray = $finalImgArr;
        $details->size1 = $return_array11;
        //$details->color1 = $return_array;

        $attriArr = array();
        $getAttri = Attribute::select('label','labeltext')->where(["product_id"=>$idk])->get();
        if($getAttri) {
            foreach($getAttri as $getAttr) {
                $attriArr[]= ["labal"=>$getAttr->label,"text"=>$getAttr->labeltext];
            }

            $details->attributes = $attriArr;
        } else {
            $details->attributes = $attriArr;
        }

        $productPrices = ProductPrice::where(["product_id"=>$idk])->get();

        $details->variant_prices = $productPrices;

        return $this->sendResponse1($details, $product_name . ' details retrieved successfully', $request->path());
    }

    function getImageByColor(Request $request) {

     $input = $request->all();
     $validator = Validator::make($input, ['product_id'=>'required']);
     if ($validator->fails()) {
         return $this->sendError($request->path(), $validator->errors()->first());
     }

     $colorCode = $request->color_code;
     $image  = ProductImageColor::where(['color'=>$colorCode,'product_id'=>$request->product_id])->first();
     $return_new_array = array();
     
        if(!empty($image)) {
            
            $image_array = explode(",", $image->images);

            foreach ($image_array as $key => $value) {
                $return_new_array[] = url('public/').$value;
            }
        }

        $priceDetail = array();
        $proPriceDetail = ProductPrice::where(["product_id"=>$request->product_id, "size"=>$request->size, "color"=>$request->color_code])->first();
        if($proPriceDetail) {
            $priceDetail = ["sp"=>$proPriceDetail->sp, "mrp"=>$proPriceDetail->mrp, "percentage"=>$proPriceDetail->discount, "quantity"=>$proPriceDetail->quantity];
        } else {
            $priceDetail = ["sp"=>"", "mrp"=>"", "percentage"=>"", "quantity"=>""];

            return $this->sendError($request->path(), 'Product not available with this variants');
        }

        return $this->sendResponse1(['image_list'=>$return_new_array, 'priceDetail'=>$priceDetail],'image show successfully', $request->path());
    }

    public function addsubcatg(Request $request) {
        $input = $request->all();
        $verifySubCat = Subcategory::where(["name"=>$input['name'],"category_id"=>$input['category_id']])->first();

        if($verifySubCat) {
            return $this->sendError($request->path(), 'Subcategory Already Added');
        } else {
            $details = Subcategory::create($input);
            return $this->sendResponse1($input, 'Subcategory added successfully', $request->path());
        }
    }

    public function editproductbyseller(Request $request, $idk) {
        $input = $request->all();
       // echo "<pre>";print_r($_POST);die;
        $colorWithImageArr = $input['color_with_image'];
        
        $finddetails = Product::find($idk);
        $finddetails->name = $input['name'];
        $finddetails->category_id = $input['category_id'];
        $finddetails->subcategory_id = $input['subcategory_id'];
        $finddetails->subsubcategory_id = $input['subsubcategory_id'];
        $finddetails->description = $input['description'];
        //$finddetails->fit = $input['fit'];
        $finddetails->mrp = $input['mrp'];
        $finddetails->sp = $input['sp'];
        $percentage = 100 - ($input['sp'] * 100) / $input['mrp'];
        $finddetails->discount = $percentage;
        $finddetails->quantity = $input['quantity'];
        $finddetails->brand = $input['brand'];
        $finddetails->hsn_code = $input['hsn_code'];
        $finddetails->hsn_percent = $input['hsn_percent'];
        $finddetails->weight = $input['weight'];
        //$finddetails->height = $input['height'];
        //$finddetails->length = $input['length'];
        //$finddetails->width = $input['width'];
        $finddetails->ships_in = $input['ships_in'];
        $finddetails->sku = $input['sku'];
        $finddetails->hsn_percent = $input['hsn_percent'];
        $finddetails->national = $input['national'];
        if(isset($input['payment_mode'])) {
            $finddetails->payment_mode = $input['payment_mode'];
        }
        $finddetails->size_label = $input['size_label'];
        $finddetails->is_variant = $input['is_variant'];
        //$finddetails->variant_type = $input['variant_type'];

        $return_array = array();
        if(!empty($colorWithImageArr)) {
            $images = implode(',',$colorWithImageArr[0]['image']);
            $finddetails->images = $images;
        }else {
            $finddetails->images = "";
        }
        
        if(isset($input['size'])) {
            if(!empty($input['size'])) {
                $return_array1 = array();
                foreach ($input['size'] as $value) {
                    $return_array1[] = $value['value'];
                }
                $sizes = implode(",", $return_array1);
                $finddetails->size = $sizes;
            } else {
                $finddetails->size = "";
            }
        }


        // if(isset($input['color'])) {
        //     if(!empty($input['color'])) {
        //         $return_array2 = array();
        //         foreach ($input['color'] as $value) {
        //             $return_array2[] = $value['value'];
        //         }
        //         $colors = implode(",", $return_array2);
        //         $finddetails->color = $colors;
        //     } else {
        //         $finddetails->color = "";
        //     }
        // }

        $finddetails->save();

        if(!empty($colorWithImageArr)) {
            $del = ProductImageColor::where('product_id',$idk)->delete();
            foreach ($colorWithImageArr as $value) { 
                if(!empty($value['size'])) {
                    $image = implode(',',$value['image']);
                    $pro_img_color = ProductImageColor::create([
                                   'product_id' => $idk,
                                   'images'     => $image,
                                   //'color'=>  $value['name'],  
                                   'size' => $value['size'],
                                   'status' =>1,
                                  ]);
                }
            }
        }

        if(isset($input['product_variant'])) {

            if(count($input['product_variant']) > 0) {

                ProductPrice::where(["product_id"=>$idk])->delete();
                
                foreach($input['product_variant'] as $variant) {
                    $scom = Seller::select("commission")->where(["id"=>$finddetails->user_id])->first();
                    $calComm = ($variant['sp']  * $scom->commission) / 100;
                    $gstRate = ($calComm * 18) / 100;
                    $input_commision = $calComm;
                    $input_tax = $gstRate;

                    $input_discount = 100 - ($variant['sp'] * 100) / $variant['mrp'];

                    ProductPrice::create([
                        "product_id"=>$idk,
                        "sp"=>$variant['sp'],
                        "commision"=>$input_commision,
                        "tax"=>$input_tax,
                        "quantity"=>$variant['quantity'],
                        "mrp"=>$variant['mrp'],
                        "discount"=>$input_discount,
                        "size"=>$variant['size'],
                        //"color"=>$variant['color'],
                    ]);
                }
            }
        }

        if(isset($input['attribute'])) {
            if(count($input['attribute']) > 0) {
                Attribute::where('product_id', $idk)->delete();

                $attris = $input['attribute'];
                foreach($attris as $attri) {
                    Attribute::create([
                        "product_id"=>$idk,
                        "label"=>$attri['labal'],
                        "labeltext"=>$attri['text']
                    ]);
                }
            }
        }

        return $this->sendResponse1($finddetails, $input['name'] . ' updated successfully', $request->path());
    }


    public function editproductbyadmin(Request $request, $idk) {
        $input = $request->all();
       
        $finddetails = Product::find($idk);
        $finddetails->name = $input['name'];
        $finddetails->category_id = $input['category_id'];
        $finddetails->subcategory_id = $input['subcategory_id'];
        $finddetails->subsubcategory_id = $input['subsubcategory_id'];
        $finddetails->description = $input['description'];
        //$finddetails->fit = $input['fit'];
        $finddetails->brand = $input['brand'];
        $finddetails->hsn_code = $input['hsn_code'];
        $finddetails->hsn_percent = $input['hsn_percent'];
        $finddetails->weight = $input['weight'];
        //$finddetails->height = $input['height'];
        //$finddetails->length = $input['length'];
        //$finddetails->width = $input['width'];
        $finddetails->ships_in = $input['ships_in'];
        $finddetails->sku = $input['sku'];
        $finddetails->hsn_percent = $input['hsn_percent'];
        $finddetails->national = $input['national'];
        
        if(isset($input['payment_mode'])) {
            $finddetails->payment_mode = $input['payment_mode'];
        }

        $finddetails->save();

        return $this->sendResponse1($finddetails, $input['name'] . ' updated successfully', $request->path());
    }

    public function fileuploade(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('productimages/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath = 'productimages/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploadecat(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('catimages/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath = '/catimages/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploade1(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('sellergstnumfile/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath = url('/') . '/public/sellergstnumfile/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploadebanner(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('banner/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath =  '/banner/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function fileuploadebrand(Request $request) {
        $input = $request->all();
        $fileuploadedpath = "";
        if ($request->gstnumfile) {
            $thumbnail_img = $request->gstnumfile;
            list($type, $thumbnail_img) = explode(';', $thumbnail_img);
            list(, $thumbnail_img) = explode(',', $thumbnail_img);
            $thumbnail_img = base64_decode($thumbnail_img);
            $thumbnail_img_name = rand(1000000, 9999999) . date('ymdhis') . '.png';
            $thumbnail_img_name_dir = public_path('brandimg/' . $thumbnail_img_name);
            file_put_contents($thumbnail_img_name_dir, $thumbnail_img);
            $fileuploadedpath =  '/brandimg/' . $thumbnail_img_name;
        }
        return $this->sendResponse1($fileuploadedpath, 'Image uploaded successfully', $request->path());
    }

    public function getsubcategorylist(Request $request, $id) {
        $details = Subcategory::where(['category_id' => $id])->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $value) {
            $subsubcategorieslist = Subsubcategory::where('subcategory_id', $value->id)->select("id as remark_id", "name as remark_name")->get()->toArray();
            $return_array[] = array("Subcategory_id" => (string)$value->id, "Subcategory_name" => $value->name, "remark_list" => $subsubcategorieslist);
        }
        return $this->sendResponse($return_array, 'Subcategory list retrieved successfully', $request->path());
    }
    
    public function sublistbycat(Request $request, $id) {
        $details = Subcategory::where(['category_id' => $id])->get();
        $return_array = array();
        foreach ($details as $value) {
            $return_array[] = [$value->id, $value->name];
        }
        return $this->sendResponse($return_array, 'Subcategory list retrieved successfully', $request->path());
    }

    public function subsubcatbyid(Request $request, $id) {
        $details = SubSubcategory::where(['id' => $id])->first();
        return $this->sendResponse($details, 'Subsubcategory details retrieved successfully', $request->path());
    }

    public function sublistbycatremark(Request $request, $id) {
        $details = Subsubcategory::where(['subcategory_id' => $id])->get();
        $return_array = array();
        foreach ($details as $value) {
            if($value->status == 1) {
                if($value->occasion_status == 1) {
                    $name = $value->name . ' - occasion';
                } else {
                    $name = $value->name;
                }
                $return_array[] = [$value->id, $name];
            }
        }
        return $this->sendResponse($return_array, 'Remark list retrieved successfully', $request->path());
    }

    public function brandlistforseller(Request $request) {
        $details = Brand::get();
        $return_array = array();
        foreach ($details as $value) {
            if (!empty($value->admin_status)) {
                $return_array[] = [$value->id, $value->name];
            }
        }
        return $this->sendResponse($return_array, 'Brand list retrieved successfully', $request->path());
    }

    public function searchlist(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['key' => 'required']);
        $brandArr = array();
        $catArr = array();

        $getBrands = Product::select('brand','subsubcategory_id', 'category_id')->where('brand', 'like', '%' . $input['key'] . '%')->distinct('subsubcategory_id')->where(["deleteStatus"=>1,"status"=>1])->get();
        
        foreach($getBrands as $getBrand) {
            $subsubname = Subsubcategory::select('name','status')->where('id',$getBrand->subsubcategory_id)->first();
            if($subsubname) {

                if($subsubname->status == 1) {
                    $text = $getBrand->brand.' '.$subsubname->name.' '.$getBrand->category_id;
                    $brandArr[] =  ["name"=>$text, "subsubcatid"=> "$getBrand->subsubcategory_id", "catid"=>$getBrand->category_id, 'brand'=>$getBrand->brand];
                }
            }
        }

        $getCats = Subsubcategory::select('id', 'name', 'category_id','status')->where('name', 'like', '%' . $input['key'] . '%')->get();

        foreach($getCats as $getCat) {

            if($getCat->status == 1) {
                $text = $getCat->name.' '.$getCat->category_id;
                $catArr[] = ["name"=>$text, "subsubcatid"=> "$getCat->id", "catid"=>$getCat->category_id, 'brand'=>'0'];
            }
        }
    
        $finalarray = array_merge($brandArr, $catArr);
        sort($finalarray);
        return $this->sendResponse($finalarray, 'List retrieved successfully', $request->path());
    }

    public function themelistforseller(Request $request) {
        $details = Theme::get();
        $return_array = array();
        foreach ($details as $value) {
            $return_array[] = [$value->id, $value->title];
        }
        return $this->sendResponse($return_array, 'Brand list retrieved successfully', $request->path());
    }

    public function addtowish(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'product_id' => 'required', ]);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        
        $finddetails = Wishlist::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->first();
        
        if (!empty($finddetails)) {
            Wishlist::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->delete();
            return $this->sendResponse(["status" => "0"], 'Product removed from wishlist successfully', $request->path());
        } else {
            Cart::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->delete();
            Wishlist::create($input);
            return $this->sendResponse(["status" => "1"], 'Product added to wishlist successfully', $request->path());
        }
    }

    public function addtocart(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'product_id' => 'required', ]);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
    
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }

        $finddetails = Cart::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->first();
        
        if($finddetails) {
            return $this->sendError($request->path(), 'Already added');
        } else {

            $haveCoupon = "false";
            $checkCoupons = Cart::where(['user_id' => $input['user_id']])->get();
            foreach($checkCoupons as $checkCoupon) {
                if(strlen($checkCoupon->coupon_code) > 0) {
                    $haveCoupon = "true";
                }
            }
            if($haveCoupon == "true") {
                foreach($checkCoupons as $checkCoupon) {
                    $update = Cart::where(['id' => $checkCoupon->id])->first();
                    $update->discount = 0.00;
                    $update->coupon_code = "";
                    $update->save();
                }   
            }

            Wishlist::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->delete();
            Cart::create($input);
            $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();

            return $this->sendResponse(["status" => "1", "total_items_in_cart" => $num_of_addtocart], 'Product added to Cart successfully', $request->path());
        }
    }

    public function removetocart(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'product_id' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }

        Cart::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->delete();
        return $this->sendResponse(['status' => "success"], 'Product removed', $request->path());
    }


    public function updatecart(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'product_id' => 'required', 'quantity' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        $finddetails = Cart::where(['product_id' => $input['product_id'], "user_id" => $input['user_id']])->first();
        if ($finddetails) {
            $finddetails->quantity = $input['quantity'];
            $finddetails->save();   
            return $this->sendResponse(['status' => "success"], 'Product updated', $request->path());
        } else {
            return $this->sendError($request->path(), 'product not found');
        }
    }

    public function addtocart1($user_id, $arrayofproduct, $arrayofsize, $arrayofcolor, $arrayofqty) {
        $q = 0;
        foreach ($arrayofproduct as $value) {
            $checkCart = Cart::where(["product_id"=>$value,"user_id"=>$user_id])->first();

            if($checkCart) {
                Cart::where(["product_id"=>$value, "user_id" => $user_id])->delete();
            }
            $input = array("product_id" => $value, "user_id" => $user_id, "size" => $arrayofsize[$q], "color" => $arrayofcolor[$q], "quantity" => $arrayofqty[$q]);
            Cart::create($input);
            $q++;
        }
    }

    public function productdetails(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['product_id' => 'required', ]);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();
        
        $productdetails_list = Product::find($input['product_id']);
        
        // Similar products
        $similar_list = Product::where('subsubcategory_id', $productdetails_list->subsubcategory_id)->get();
        //Size chart start
        $subSubCat_id = $productdetails_list->subsubcategory_id;
        $getdata = SizeChart::where('subsub_cat_id',$subSubCat_id)->get();
        $newData = array();
        foreach($getdata as $data) {
           $heading = explode(',', $data->heading); 
           $heading_values = explode(',', $data->heading_values); 
           //echo "<pre>";print_r($heading);die;
           foreach($heading as $k=>$v) {

           // echo "<pre>";print_r($k);
            @$newData[$v][] = $heading_values[$k]; 
           }
       }

        foreach($newData as $k1=>$v1) {
            $new = [];
            foreach($v1 as $c){
                if($c==''){
                   $t = 'N/A';
                }else{
                 $t = $c;
                }
                $new[] = $t;
            }

          $sizeChartRes[] = array(
                         'key' => $k1?$k1:'N/A',
                         'value' => $new
          );
       }

       if(!empty($sizeChartRes)) {
        $sizeChartRes1 = $sizeChartRes;
       } else {
         $sizeChartRes1 = [];
       }
       //end size chart
        $similar = array();
        foreach ($similar_list as $val) {
            if ($val->id != $input['product_id']) {
        
                if ($val->images) {
                    $img = explode(',', $val->images);
                    $res['image'] = $this->baseurl. $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $val->id;
                $res['name'] = $val->name;
                $res['mrp'] = $val->mrp;
                $res['discount'] = round($val->discount);
                $res['brand'] = $val->brand;
                $percentage = 100 - ($val->sp * 100) / $val->mrp;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp;
                $similar[] = $res;
            }
        }

        // customer also like products
        $oldOrders = Order::select("product_id")->distinct('product_id')->get();
        if(count($oldOrders) > 0) {
            $arrOldOrder = array();
            foreach ($oldOrders as $oldOrder) {
                $arrOldOrder[] = $oldOrder->product_id;
            }

            $liked_product = Product::whereIn('id', $arrOldOrder)->where(["deleteStatus"=>1,"status"=>1])->get();
            $curtomer_liked = array();
            
            foreach ($liked_product as $ke) {
            
                if ($ke->images) {
                    $img = explode(',', $ke->images);
                    $res['image'] = $this->baseurl . $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $ke->id;
                $res['name'] = $ke->name;
                $res['mrp'] = $ke->mrp;
                $res['discount'] = round($ke->discount);
                $res['brand'] = $ke->brand;
                $percentage = 100 - ($val->sp * 100) / $val->mrp;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp;
                $curtomer_liked[] = $res;
            }
        } else {
            $liked_product = Product::where('subsubcategory_id', $productdetails_list->subsubcategory_id)->where(["deleteStatus"=>1,"status"=>1])->get();
            $curtomer_liked = array();
            
            foreach ($liked_product as $ke) {
            
                if ($ke->images) {
                    $img = explode(',', $ke->images);
                    $res['image'] = $this->baseurl . $img[0];
                } else {
                    $res['image'] = url('/public/') . "/img/imagenotfound.jpg";
                }
                $res['product_id'] = $ke->id;
                $res['name'] = $ke->name;
                $res['mrp'] = $ke->mrp;
                $res['discount'] = round($ke->discount);
                $res['brand'] = $ke->brand;
                $percentage = 100 - ($val->sp * 100) / $val->mrp;
                $res['percentage'] = round($percentage);
                $res['sp'] = $val->sp;
                $curtomer_liked[] = $res;
            }
        }

        $solderdetails = Seller::find($productdetails_list->user_id);
        
        if (!empty($solderdetails)) {
            $solder_name = $solderdetails->name;
        } else {
            $solder_name = "";
        }
        $arrayimage = explode(",", $productdetails_list->images);
        $arrayimage1 = array();
        
        foreach ($arrayimage as $value) {
        
            if (!empty($value)) {
        
                if (strpos($value, $this->baseurlslash) == false) {
                    $arrayimage1[] = $this->baseurl . $value;
                }
            } else {
                $arrayimage1[] = url('/public/') . "/img/imagenotfound.jpg";
            }
        }

        if(strlen($productdetails_list->sizechart) > 1) {
            $sizeimg = $this->baseurlslash . $productdetails_list->sizechart;
        } else {
            $sizeimg = '';
        }
        $percentage = 100 - ($productdetails_list->sp * 100) / $productdetails_list->mrp;
        
        $bankOfferArr = array();
        $getBankOffers = Offer::where('offer_type',1)->get();
        if($getBankOffers) {
            foreach($getBankOffers as $getBankOffer) {
                $bankOfferArr[] = $getBankOffer->description;
            }
        }
        
        $otherOfferArr = array();
        $checkPromotions = Promotion::where(["seller_id"=>$solderdetails->id])->get();
        if($checkPromotions) {
            foreach($checkPromotions as $checkPromotion) {

                $checkProductPromotion = PromotionList::where(["product_id"=>$productdetails_list->id,'status'=>1])->first();
                if($checkProductPromotion) {
                    $getOtherOffers = Offer::where(['id'=>$checkPromotion->offer_id, 'offer_type'=>2, 'catid'=> $productdetails_list->subsubcategory_id])->first();
                    if($getOtherOffers) {

                        $getCoupon = Cuponcode::where('id', $getOtherOffers->coupon_id)->first();
                        if($getCoupon) {
                            $otherOfferArr[] = $getOtherOffers->description .'- CODE : '.$getCoupon->name;
                        }
                    }
                }
            }
        }


        if(empty($input['user_id'])) {
            $wishHave = "0";
        } else {
            $wishCheck = Wishlist::where(["user_id"=>$input['user_id'],"product_id"=>$input['product_id']])->first();
            if($wishCheck) {
                $wishHave = "1";
            } else {
                $wishHave = "0";
            }
        }

        $catName = Subsubcategory::select('name','id')->where(["id"=>$productdetails_list->subsubcategory_id])->first();
        
        if(!empty($productdetails_list->defaultcolor)) {
            $colorName = Color::select('name')->where(["hexcode"=>$productdetails_list->defaultcolor])->first();
            if($colorName) {
                $defaultcolorname = $colorName->name;
                if($colorName) {
                    $colorname = $colorName->name;
                } else {
                    $colorname = "";  
                }
            } else {
                $defaultcolorname = "";
                $colorname = "";  
            }
        } else {
            $defaultcolorname = "";
            $colorname = "";  
        }


        $attributeArr = array();
        $getAttris = Attribute::where(["product_id"=>$input['product_id']])->get();
        foreach($getAttris as $getAttri) {
            $attributeArr[] = ["keyName"=>$getAttri->label, "keyValue"=>$getAttri->labeltext];
        }
        if($productdetails_list->brand) {
            $brand = $productdetails_list->brand;
        } else {
            $brand = "";  
        }
        if($catName->name) {
            $catname = $catName->name;
        } else {
            $catname = "";  
        }

        if(!empty($productdetails_list->size)) {
            $getSizearr = array_unique(explode(",", $productdetails_list->size));
        }else {
            $getSizearr = [];
        }
        if(!empty($productdetails_list->color)) {
            $getColorarr = array_unique(explode(",", $productdetails_list->color));
            $carr = array();

            foreach($getColorarr as $getColorar) {
                $carr[] = $getColorar;
            }

            $getColorarr = $carr;

        }else {
            $getColorarr = [];
        }

        if($productdetails_list->is_variant == 2) {
            $getProPrices = ProductPrice::where('product_id', $productdetails_list->id)->get();
            if($getProPrices) {
                
                $product_quantity = $getProPrices[0]['quantity'];
            }
        } else {
            $product_quantity = $productdetails_list->quantity;
        }

        $return_array = array("image_list" => $arrayimage1, 
                            'quantity' => (string)$product_quantity, 
                            "brand" => $productdetails_list->brand,
                            "morebrand"=>[
                                        "More $catName->name By $brand",
                                        "More $colorname $catname",
                                        "More $catname",
                                    ],
                            "subsubcatid"=>$catName->id,
                            "name" => $productdetails_list->name, 
                            "description" => $productdetails_list->description, 
                            "fit" => $productdetails_list->fit?$productdetails_list->fit:'',
                            "mrp" => $productdetails_list->mrp, 
                            "sp" => $productdetails_list->sp, 
                            "percentage" => round($percentage), 
                            "size" => $getSizearr, 
                            "color" => $getColorarr, 
                            "defaultcolor" => $productdetails_list->defaultcolor,
                            "defaultcolorname" => $defaultcolorname,
                            "num_of_addtocart" => $num_of_addtocart, 
                            "sold_by" => $solder_name, 
                            "hsn_code" => $productdetails_list->hsn_code, 
                            "delivery_time" => $productdetails_list->ships_in, 
                            'similar_product' => $similar, 
                            'customer_also_liked' => $curtomer_liked,
                            'offers' => $bankOfferArr,
                            "details_offer" => $otherOfferArr,
                            "sizechart"=>$sizeimg,
                            "wishHave"=>$wishHave,
                            "attributes"=>$attributeArr,
                            'sizechart' => $sizeChartRes1,
                            "is_variant"=>$productdetails_list->is_variant,
                            "variant_type"=>$productdetails_list->variant_type,
                            "size_label"=>$productdetails_list->size_label,

                        );
        return $this->sendResponse($return_array, 'Product details retrieved successfully', $request->path());
    }

    public function filter_section(Request $request) {
        $input = $request->all();
        $colorData = array();
        $hexData = array();
        $sizeData = array();
        $sizeDataa = array();
        $brandData = array();
        $brandDataa = array();

        $getColors = Color::select("name","hexcode")->get();
        foreach ($getColors as $getColor) {
            $colorData[] = $getColor->name;
            $hexData[] = $getColor->hexcode;
        }
        
        if($input['type'] == "flashsale") {
            
            $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
            $brandGets = Product::select("brand")->whereIn('id', explode(",", $findflashdetails->products))->where(["deleteStatus"=>1,"status"=>1])->get();

            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }

        } else if($input['type'] == "theme") {
            
            $proid = array();
            $brandGets = Product::select('brand')->where(["theme_id"=>$input['key'], "category_id"=>$input['type']])->where(["deleteStatus"=>1,"status"=>1])->get();

            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }

        } else if($input['type'] == "subcat" || $input['type'] == "subsubcat") {

            if($input['type'] == "subcat") {
                $whereType = ["subcategory_id"=>$input['key']];
            } else {
                $whereType = ["subsubcategory_id"=>$input['key']];
            }

            $getSizes = Product::select("size")->where($whereType)->where(["deleteStatus"=>1,"status"=>1])->get();
            foreach ($getSizes as $getSize) {
                $sizeStrings = explode(',',$getSize->size);
                
                foreach($sizeStrings as $sizeString) {
                    $sizeData[] = $sizeString;
                }
            }
            $sizeDatas = array_unique($sizeData);
            foreach($sizeDatas as $sizeData) {
                $sizeDataa[] = $sizeData;
            }

            $brandGets = Product::select("brand")->where($whereType)->where(["deleteStatus"=>1,"status"=>1])->get();
            foreach ($brandGets as $brandGet) {
                $brandData[] = $brandGet->brand;
            }
            $brandDatas = array_unique($brandData);
            foreach($brandDatas as $brandData) {
                $brandDataa[] = $brandData;
            }
        }
        return $this->sendResponse(["size" => $sizeDataa, "color" => $colorData, "hexcode"=>$hexData, "brand" => $brandDataa], 'Filter details retrieved successfully', $request->path());
    }

    public function productlist(Request $request) {
        $input = $request->all();
        
        if (!empty($input['sortby'])) {
        
            if ($input['sortby'] == 'new') {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'popularity') {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'discount') {
                $sortcolumn_name = "discount";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'low_to_high') {
                $sortcolumn_name = "sp";
                $incorder_name = "asc";
            } else if ($input['sortby'] == 'high_to_low') {
                $sortcolumn_name = "sp";
                $incorder_name = "desc";
            } else if ($input['sortby'] == 'delivery_time') {
                $sortcolumn_name = "ships_in";
                $incorder_name = "asc";
            }
        } else {
            $sortcolumn_name = "id";
            $incorder_name = "desc";
        }

        if (!empty($input['filter_data'])) {
            
            if (empty($input['filter_type'])) {
                $productdetails = Product::where(['subcategory_id' => $input['filter_data']])->where(["deleteStatus"=>1,"status"=>1])->get();
            
            } else if ($input['filter_type'] == "Category") {
                $productdetails = Product::where(["category_id" => $input['filter_data']])->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
            
            } else if ($input['filter_type'] == "search") {
                $productdetails = Product::where('name', 'like', '%' . $input['filter_data'] . '%')->orwhere('category_id', 'like', '%' . $input['filter_data'] . '%')->orwhere('brand', 'like', '%' . $input['filter_data'] . '%')->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
            
            } else if ($input['filter_type'] == "flash") {
                $findflashdetails = Flashsale::where(['id' => 1, 'status' => '1'])->first();
            
                if (!empty($findflashdetails)) {
                    $finsalearray = explode(",", $findflashdetails->products);
                } else {
                    $finsalearray = array("0");
                }
                $productdetails = Product::whereIn('id', $finsalearray)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            
            } else {
            
                if (!empty($input['option'])) {
                    $productdetails = Product::where([$input['filter_type'] => $input['filter_data'], "category_id" => $input['option']])->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
                } else {
                    $productdetails = Product::where([$input['filter_type'] => $input['filter_data']])->where(["deleteStatus"=>1,"status"=>1])->orderby($sortcolumn_name, $incorder_name)->get();
                }
            }
        } else {

            $productdetails = Product::orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
        }

        $return_array = array();
        $color_array = array();
        $size_array = array();
        $brand_array = array();
        
        if (!empty($input['extra_filter_type'])) {
            $color_array = explode(",", $input['color']);
            $size_array = explode(",", $input['size']);
            $brand_array = explode(",", $input['brand']);
        }
        
        foreach ($productdetails as $value) {
            $check_status_unique = 0;
            $imagearray = explode(",", $value->images);
            
            if (!empty($imagearray)) {
            
                if (!empty($imagearray[0])) {
            
                    if (strpos($imagearray[0], $this->baseurlslash) == false) {
                        $imagearray[0] = $this->baseurl . $imagearray[0];
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }
            
            } else {
                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
            }
            $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
            
            if (!empty($findflagofwishlist)) {
                $like_Status = "1";
            } else {
                $like_Status = "0";
            }
            $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
            $associated_color_array = explode(",", $value->color);
            $associated_size_array = explode(",", $value->size);
            
            if (!empty($color_array) && sizeof(array_intersect($associated_color_array, $color_array)) > 0) {
                $check_status_unique = 1;
            }
            
            if (!empty($size_array) && sizeof(array_intersect($associated_size_array, $size_array)) > 0) {
                $check_status_unique = 1;
            }
            
            if (!empty($brand_array) && in_array($value->brand, $brand_array)) {
                $check_status_unique = 1;
            }
            
            if (isset($input['extra_filter_type']) && !empty($input['extra_filter_type'])) {
                if (!empty($value->status) && $check_status_unique == 1) {
                    $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                }
            } else {
                if (!empty($value->status)) {
                    $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
                }
            }
        }
        return $this->sendResponse($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function moreproductlist(Request $request) {
        $input = $request->all();
        $return_array = array();

        if (!empty($input['sortby'])) {

            if ($input['sortby'] == 'new') {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "popularity") {
                $sortcolumn_name = "id";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "discount") {
                $sortcolumn_name = "discount";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "low_to_high") {
                $sortcolumn_name = "sp";
                $incorder_name = "ASC";
            } else if ($input['sortby'] == "high_to_low") {
                $sortcolumn_name = "sp";
                $incorder_name = "desc";
            } else if ($input['sortby'] == "delivery_time") {
                $sortcolumn_name = "ships_in";
                $incorder_name = "asc";
            }
        } else {
            $sortcolumn_name = "id";
            $incorder_name = "DESC";
        }

        $query = Product::query();

        if(!empty($input['subcatid']) && empty($input['subsubcatid']) && empty($input['catid']) && empty($input['flash_sale']) && empty($input['theme'])) {
            
            $query->where(["subcategory_id"=>$input['subcatid']]);

        } else if(empty($input['subcatid']) && !empty($input['subsubcatid']) && empty($input['catid']) && empty($input['flash_sale']) && empty($input['theme'])) {

            if(!empty($input['brand']) && !empty($input['defaultcolor'])) {
                $query->where(["subsubcategory_id"=>$input['subsubcatid'], "brand"=>$input['brand'], "defaultcolor"=>$input['defaultcolor']]);

            } elseif(!empty($input['defaultcolor']) && empty($input['brand'])) {
                $query->where(["subsubcategory_id"=>$input['subsubcatid'], "defaultcolor"=>$input['defaultcolor']]);
            
            } elseif(empty($input['defaultcolor']) && !empty($input['brand'])) {
                $query->where(["subsubcategory_id"=>$input['subsubcatid'], "brand"=>$input['brand']]);

            } else {
                $query->where(["subsubcategory_id"=>$input['subsubcatid']]);
            }

        } else if(empty($input['subcatid']) && !empty($input['catid']) && !empty($input['subsubcatid']) && empty($input['flash_sale']) && empty($input['theme'])) {
            $query->where(["category_id"=>$input['catid']])->where(["subsubcategory_id"=>$input['subsubcatid']]);

        } else if(empty($input['subcatid']) && empty($input['subsubcatid']) && empty($input['catid']) && !empty($input['flash_sale']) && empty($input['theme'])) {
            $flashData = Flashsale::where(['id'=>1, 'status'=>1])->first();
            if(strlen($findflashdetails->products)>=1) {
                $fData = explode(',', $flashData->products);
                $query->whereIn("id", $fData);
            } else {
                $query->where(['subsubcategory_id'=>$flashData->subsubcategory]);
            }
        
        } else if(empty($input['subcatid']) && empty($input['subsubcatid']) && empty($input['catid']) && empty($input['flash_sale']) && !empty($input['theme'])) {
            $proid = array();
            $productids = Product::select('id')->where(["theme_id"=>$input['theme_id'], "category_id"=>$input['theme']])->get();
            foreach ($productids as $productid) {
                $proid[] = $productid->id;
            }
            $query->whereIn("id", $proid);
        
        }

        if(!empty($input['filter_data'])) {
            if(!empty($input['filter_brand'])) {
                $filterBrand = explode(',',$input['filter_brand']);
            }

            if(!empty($input['filter_brand'])) {
                $query->whereIn('brand', $filterBrand);
            }

            if($input['minValue'] == 100 && $input['maxValue'] != 10000) {
                $query->where('sp', '<=', $input['maxValue']);

            } else if($input['minValue'] != 100 && $input['maxValue'] == 10000) {
                $query->where('sp', '>=', $input['minValue']);

            } else if($input['minValue'] != 100 && $input['maxValue'] != 10000) {
                $query->where('sp', '>=', $input['minValue'])->where('sp', '<=', $input['maxValue']);
            }
        }
        $productids = $query->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
        $arrayids = array();
        foreach($productids as $productid) {
            $arrayids[] = $productid->id;
        }

        if(!empty($input['filter_data'])) {
            $filtersizesarr = array();

            if(!empty($input['filter_size'])) {
                $filtersizes = explode(',',$input['filter_size']);
                foreach($filtersizes as $filtersize) {
                    $fsizes = Product::select('id')->whereIn('id',$arrayids)->where('size','like','%'.$filtersize.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    foreach($fsizes as $fsize) {
                        $filtersizesarr[] = $fsize->id;
                    }
                }
                $filtersizesarr = array_unique($filtersizesarr);
                $productdetails = Product::whereIn('id',$filtersizesarr)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            } else {
                $filtersizesarr = [];
            }
        }

        if(!empty($input['filter_data'])) {
            $filtercolorsarr = array();
            
            if(!empty($input['filter_color'])) {
                $filtercolors = explode(',',$input['filter_color']);

                foreach($filtercolors as $filtercolor) {
                    if(!empty($input['filter_size'])) {
                        $fcolors = Product::select('id')->whereIn('id',$filtersizesarr)->where('color','like','%'.$filtercolor.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    } else {
                        $fcolors = Product::select('id')->whereIn('id',$arrayids)->where('color','like','%'.$filtercolor.'%')->where(["deleteStatus"=>1,"status"=>1])->get();
                    }

                    foreach($fcolors as $fcolor) {
                        $filtercolorsarr[] = $fcolor->id;
                    }
                }
                $filtercolorsarr = array_unique($filtercolorsarr);
                $productdetails = Product::whereIn('id',$filtercolorsarr)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            } else {
                $filtercolorsarr = [];
            }
        }

        if(!empty($input['filter_data'])) {

            if(empty($input['filter_color']) && empty($input['filter_size'])) {
                $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
            }
        } else {
            $productdetails = Product::whereIn('id',$arrayids)->orderby($sortcolumn_name, $incorder_name)->where(["deleteStatus"=>1,"status"=>1])->get();
        }

        foreach ($productdetails as $value) {
            
            $verifySeller = Seller::where(["id"=>$value->user_id])->whereIn("status", [1])->first();

            if($verifySeller) {
                $check_status_unique = 0;
                $imagearray = explode(",", $value->images);
                
                if (!empty($imagearray)) {
                
                    if (!empty($imagearray[0])) {
                
                        if (strpos($imagearray[0], $this->baseurlslash) == false) {
                            $imagearray[0] = $this->baseurl . $imagearray[0];
                        }
                    } else {
                        $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                    }
                
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }

                if(!empty($input['user_id'])) {
                    $findflagofwishlist = Wishlist::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                    
                    if (!empty($findflagofwishlist)) {
                        $like_Status = "1";
                    } else {
                        $like_Status = "0";
                    }
                } else {
                    $like_Status = "0";
                }

                $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
                
                $return_array[] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "like_Status" => $like_Status);
            }

        }

        return $this->sendResponse($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function checkphone(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $userdetails = User::where('phone', $input['phone'])->first();

        if (!empty($userdetails)) {
            return $this->sendError($request->path(), "Phone Number Already Exist");

        } else {
            return $this->sendResponse(['status' => 'success'], 'Phone Number Does not exist', $request->path());
        }
    }

    public function verifysignup(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', 'email' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $userdetails = User::where('phone', $input['phone'])->first();

        if (!empty($userdetails)) {
            return $this->sendError($request->path(), "Phone Number Already Exist");

        } else {
            $userdetails = User::where('email', $input['email'])->first();
            
            if (!empty($userdetails)) {
                return $this->sendError($request->path(), "Email Already Exist");
            } else {
                return $this->sendResponse(['status' => 'success'], 'Phone Number Does not exist', $request->path());
            }
        }
    }

    public function checkphonedata(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', 'deviceToken' => '', 'deviceType' => '', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $details = User::where('phone', $input['phone'])->first();
        $token_s = str_random(25);
        $findtokencolm = Token::where('user_id', $details['id'])->first();
        if (!empty($findtokencolm)) {
            $token_saver = Token::where('user_id', $details['id'])->update(['token' => $token_s, 'deviceToken' => $input['deviceToken'], 'deviceType' => $input['deviceType']]);
        } else {
            $token_saver = Token::create(array("user_id" => $details['id'], "token" => $token_s, "deviceToken" => $input['deviceToken'], "deviceType" => $input['deviceType']));
        }
        $details['user_id'] = (string)$details['id'];
        unset($details['id']);
        if (!empty($details['image'])) {
            $details['image'] = url('/public/') . "/" . $details['image'];
        } else {
            $details['image'] = url('/public/') . "/img/imagenotfound.jpg";
        }
        $details['token'] = $token_s;
        return $this->sendResponse($details, 'User login successfully.', $request->path());
    }

    public function checkemailseller(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $userdetails = Seller::where('email', $input['phone'])->first();
        if (!empty($userdetails)) {
            return $this->sendResponse(['status' => '1'], 'Email Number Already Exist', $request->path());
        } else {
            return $this->sendResponse(['status' => '0'], 'Email Number Already Exist', $request->path());
        }
    }
    public function checkphoneseller(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['phone' => 'required', ]);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $userdetails = Seller::where('phone', $input['phone'])->first();
        if (!empty($userdetails)) {
            return $this->sendResponse(['status' => '1'], 'Email Number Already Exist', $request->path());
        } else {
            return $this->sendResponse(['status' => '0'], 'Email Number Already Exist', $request->path());
        }
    }

    public function wishlistproduct(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', ]);
        $totalP = array();

        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        
        $num_of_addtocart = Cart::where('user_id', $input['user_id'])->count();
        $findproductids = Wishlist::where('user_id', $input['user_id'])->pluck('product_id');
        $productdetails = Product::WhereIn('id', $findproductids)->where(["deleteStatus"=>1,"status"=>1])->get();
        $offers = array("offer1" => '10% offers Availabel paying through SBI Debit Card', "offer2" => '5% offers Availabel paying through ICICI Debit Card');
        
        $return_array = array();
        
        foreach ($productdetails as $value) {
            $imagearray = explode(",", $value->images);
        
            if (!empty($imagearray)) {
        
                if (!empty($imagearray[0])) {
                    if (strpos($imagearray[0], $this->baseurlslash) == false) {
                        $imagearray[0] = $this->baseurl . $imagearray[0];
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }
            } else {
                $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
            }
            $percentagetemp = 100 - ($value->sp * 100) / $value->mrp;
            $return_array['list'][] = array("image" => $imagearray[0], "brand" => $value->brand, "name" => $value->name, "sp" => $value->sp, "mrp" => $value->mrp, "percentage" => (string)round($percentagetemp), "product_id" => (string)$value->id, "num_of_addtocart" => $num_of_addtocart);
        }

        $return_array['num_of_addtocart'] = $num_of_addtocart;
        return $this->sendResponse($return_array, 'Product list retrieved successfully', $request->path());
    }

    public function codavailable(Request $request, $id) {

        $addtocarts = Cart::where(['user_id' => $id])->get();
        $flag = "true";
        $xx = 0;
        $yy = 0;
        foreach($addtocarts as $addtocart) {

            $value = Product::where('id', $addtocart->product_id)->first();
            
            if($value->payment_mode == 1) {
                $yy++;
            }

            if($value->payment_mode == 2) {
                $xx++;
            }
        }

        if($xx == $yy) {
            $flag = "true";
        
        } else if($xx >= $yy) {
            $flag = "false";
        
        } else if($xx <= $yy) {
            $flag = "true";
        }

        $return_array['cod_available'] = $flag;
        return $this->sendResponse($return_array, 'COD retrieved successfully', $request->path());
    }

    public function Cartlistproduct(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', ]);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();

        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        if (empty($check_token)) {

            if(strlen($input['cartlist']) <= 0) {
                return $this->sendResponse(['status' => 'error'], 'No Item in a cart', $request->path());
            }

            $findproductids = explode(",", $input['cartlist']);
            $findproductsize = explode(",", $input['size']);
            $findproductcolor = explode(",", $input['color']);
            $findproductqty = explode(",", $input['quantity']);
            if(isset($input['giftwrap'])) {
                $havegiftwrap = $input['giftwrap'];
            } else {
                $havegiftwrap = 0;
            }
        } else {
            $findproductids = Cart::where(['user_id' => $input['user_id']])->orderBy('id', 'asc')->pluck('product_id');

            if($findproductids) {
                if(isset($input['giftwrap'])) {
                    if($input['giftwrap'] == 1) {
                        $havegiftwrap = 1;
                    } else {
                        $checkWrap = Cart::where(['user_id' => $input['user_id']])->first();
                        if($checkWrap) {
                            $havegiftwrap = $checkWrap->giftwrap;
                        } else {
                            $havegiftwrap = $input['giftwrap'];
                        }
                    }

                } else {
                    $havegiftwrap = $input['giftwrap'];
                }
            } else {
                return $this->sendResponse(['status' => 'error'], 'No Item in a cart', $request->path());
            }
        }

        $return_array = array();
        $offers = array();
        $totalAmount = 0;
        $total_delivery_charges = 0;
        $q = 0;

        foreach($findproductids as $findproductid) {

            $value = Product::where('id', $findproductid)->first();
            
            //foreach ($productdetails as $value) {

                $solderdetails = Seller::find($value->user_id);
                if (!empty($solderdetails)) {
                    $solder_name = $solderdetails->name;
                } else {
                    $solder_name = "";
                }

                $imagearray = explode(",", $value->images);
                if (!empty($imagearray)) {
                    if (!empty($imagearray[0])) {
                        if (strpos($imagearray[0], $this->baseurlslash) == false) {
                            $imagearray[0] = $this->baseurl . $imagearray[0];
                        }
                    } else {
                        $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                    }
                } else {
                    $imagearray[0] = url('/public/') . "/img/imagenotfound.jpg";
                }

                if($input['token'] == 1) {
                    $carteachitem = "";    
                } else {
                    $carteachitem = Cart::where(['user_id' => $input['user_id'], "product_id" => $value->id])->first();
                }
                if (empty($carteachitem)) {
                    $size = $findproductsize[$q];
                    $color = $findproductcolor[$q];
                    $quantity = $findproductqty[$q];
                    $discountCoupon = 0;
                    $couponCode = "";
                } else {
                    $size = $carteachitem->size;
                    $color = $carteachitem->color;
                    $quantity = $carteachitem->quantity;
                    $discountCoupon = $carteachitem->discount;
                    $couponCode = $carteachitem->coupon_code??"";
                }

                // echo "<PRE>";
                // var_dump($findproductids);
                // var_dump($findproductsize);
                // var_dump($findproductcolor);die;

                if($value->is_variant == 2) {

                    if(!empty($color) && !empty($size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->id, "size"=>$size, "color"=>$color])->first();
                    } else if(!empty($color) && empty($size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->id, "color"=>$color])->first();
                    } else if(empty($color) && !empty($size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->id, "size"=>$size])->first();
                    }

                    $get_price_sp = $proPriceDetail->sp;
                    $get_price_mrp = $proPriceDetail->mrp;
                } else {
                    $get_price_sp = $value->sp; 
                    $get_price_mrp = $value->mrp;
                }

                $total_price = $get_price_sp * $quantity;
                $totalAmount = $totalAmount + $total_price;
                $percentagetemp = 100 - ($get_price_sp * 100) / $get_price_mrp;

                if($havegiftwrap == 1) {
                    $total_giftwrap = $quantity * 25;
                } else {
                    $total_giftwrap = 0;
                }

                $total_delivery_charges = $total_delivery_charges + $value->national;

                $num_of_addtocart = Cart::where(['user_id' => $input['user_id']])->count();

                if(!empty($color)) {
                    $colorname = Color::where(["hexcode"=>$color])->first();
                    $colorname = $colorname->name;
                } else {
                    $colorname = "";
                }

                $return_array[] = array("image" => $imagearray[0], 
                                        "brand" => $value->brand, 
                                        "name" => $value->name, 
                                        "actual_price" => (string)$get_price_sp, 
                                        "sp" => (string)$total_price, 
                                        "mrp" => (string)$get_price_mrp, 
                                        "percentage" => (string)round($percentagetemp), 
                                        "product_id" => (string)$value->id, 
                                        "num_of_addtocart" => $num_of_addtocart, 
                                        "sold_by" => $solder_name, 
                                        "size" => $size, 
                                        "color" => $colorname, 
                                        "quantity" => $quantity, 
                                        'total_items' => $value->quantity,
                                        'giftwrap_price'=>$total_giftwrap, 
                                        'shipping_charges'=>$value->national, 
                                        "size_label" => $value->size_label,
                                        "discount_coupon" => $discountCoupon,
                                        "coupon_code" => $couponCode,
                                );
                $q++;
            //}
        }

        $bankOfferArr = array();
        $getBankOffers = Offer::where('offer_type',1)->orderBy('id','DESC')->limit(2)->get();
        if($getBankOffers) {
            foreach($getBankOffers as $getBankOffer) {
                $bankOfferArr[] = $getBankOffer->description;
            }
        }
        
        //$deliveryChrge = ["delivery_charge"=>$total_delivery_charges];

        return $this->sendResponse($return_array, $bankOfferArr, 'Product list retrieved successfully', $request->path());
    }

    public function csv_uploade_for_product(Request $request, $id) {
        $input = $request->all();
        $counting = 0;
        $userid = $id;

        if(count($input['data']) > 0) {
            foreach ($input['data'] as $value) {
                if ($counting > 0) {
                    if (sizeof($value) > 13) {

                        $scom = Seller::select("commission")->where(["id"=>$userid])->first();
                        $calComm = ($value[3]  * $scom->commission) / 100;
                        $gstRate = ($calComm * 18) / 100;
                        $inputcommision = $calComm;
                        $inputtax = $gstRate;

                        $return_array = array(
                                        "sku" => $value[0], 
                                        "name" => $value[1],
                                        "is_variant" => 1,
                                        "mrp" => $value[2],
                                        "sp" => $value[3], 
                                        "commission" => $inputcommision, 
                                        "tax" => $inputtax, 
                                        "quantity" => $value[4], 
                                        "category_id" => $value[5], 
                                        "description" => $value[6], 
                                        "fit" => $value[7],
                                        "brand" => $value[8],
                                        "hsn_code" => $value[9], 
                                        "hsn_percent" => $value[10], 
                                        "national" => $value[11], 
                                        "weight" => $value[12], 
                                        "height" => $value[13],
                                        "length" => $value[14],
                                        "width" => $value[15],
                                        "ships_in" => $value[16],
                                        "status" => '',
                                        "user_id" => $userid
                                    );
                        $details = Product::create($return_array);
                    }
                }
                $counting++;
            }

            return $this->sendResponse1($return_array, 'All Product added successfully', $request->path());
        } else {

            return $this->sendResponse1([], 'Products not found', $request->path());
        }
        
    }

    public function updatesellerprofile(Request $request, $id) {
        $input = $request->all();
        $details = Seller::find($id);
        $details->name = $input['name'];
        if ($input['image'] != $details->image) {
            $details->image = $input['image'];
        }
        $findemailonissue = Seller::where('email', $input['email'])->WhereNotIn('id', [$id])->first();
        if (!empty($findemailonissue)) {
            return $this->sendError($request->path(), "Email Id already exist.");
        }
        $findemailonissue1 = Seller::where('phone', $input['phone'])->WhereNotIn('id', [$id])->first();
        if (!empty($findemailonissue1)) {
            return $this->sendError($request->path(), "Phone Number is already exist.");
        }
        $details->phone = $input['phone'];
        $details->email = $input['email'];
        $details->address = $input['address']??'';
        $details->latitude = $input['latitude']??'';
        $details->longitude = $input['longitude']??'';
        $details->state = $input['state']??'';
        $details->city = $input['city']??'';
        $details->pincode = $input['pincode']??'';
        $details->country = $input['country']??'';
        $details->save();


        // $jsonarray = [
        //     "phone" => $input['phone'],
        //     "name" => $input['name'],
        //     "pin" => $input['pincode'],
        //     "address" => $input['address'],
        //     "registered_name" => $input['name'],
        // ];

        // $headers = [
        //     'Content-Type' => 'application/json',
        //     'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
        //     'Accept' => 'application/json',
        // ];

        // $url = "https://staging-express.delhivery.com/api/backend/clientwarehouse/edit/";
        // $body = $jsonarray;

        // $client = new Client($headers);
        // $response = $client->post($url, [
        //     'http_errors' => false,
        //     'json' => $body,
        // ]);
        // $status = $response->getStatusCode();
        // $data = json_decode($response->getBody()->getContents());

        return $this->sendResponse1($details, 'Profile updated successfully', $request->path());
    }

    public function updateadminprofile(Request $request, $id) {
        $input = $request->all();
        $details = Admin::find($id);
        $details->name = $input['name'];
        if ($input['image'] != $details->image) {
            $details->image = $input['image'];
        }
        $details->phone = $input['phone'];
        $details->email = $input['email'];
        $details->save();
        return $this->sendResponse1($details, 'Profile updated successfully', $request->path());
    }
    public function changepasswordforseller(Request $request, $id) {
        $input = $request->all();
        $post = Seller::find($id);
        if ($post->password != $input['oldpassword']) {
            return $this->sendError($request->path(), "You enter incorrect old password");
        }
        $post->password = $input['password'];
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password changed successfully.', $request->path());
    }
    public function changepasswordforadmin(Request $request, $id) {
        $input = $request->all();
        $post = Admin::find($id);
        if ($post->password != $input['oldpassword']) {
            return $this->sendError($request->path(), "You enter incorrect old password");
        }
        $post->password = $input['password'];
        $post->save();
        return $this->sendResponse(['status' => 'success'], 'Password updated successfully.', $request->path());
    }
    
    public function allrequestlist1(Request $request) {
        // $input = $request->all();
        // switch ($input['type']) {
        //     case "Cancel":
        //         $detailsoforder = Order::find($input['id']);
        //         $detailsoforder->status = "canceled";
        //         $detailsoforder->save();
        //         $orderrequestdetails = DB::table('orderrequests')->where(['order_id' => $input['id']])->update(['request_type' => "canceled"]);
        //     break;
        //     case "return":
        //         $detailsoforder = Order::find($input['id']);
        //         $detailsoforder->status = "returned";
        //         $detailsoforder->save();
        //         $orderrequestdetails = DB::table('orderrequests')->where(['order_id' => $input['id']])->update(['request_type' => "returne"]);
        //     break;
        //     default:
        //         $detailsoforder = Order::find($input['id']);
        // }
        // return $this->sendResponse1([], 'Order list retrieved successfully.', $request->path());
    }

    public function orderreturnadmin(Request $request) {
        $details = Orderrequest::whereIn("request_type", ["exchange", "return"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {
        
            $findallorderdetails = Order::find($met->order_id);
            $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
            $finduserdetails = User::find($findallorderdetails->user_id);

            if($met->status == 0) {
                $orderstatus = "None";
            } else if($met->status == 1) {
                $orderstatus = "Reject";
            } else if($met->status == 2) {
                $orderstatus = "Accept";
            }
            
            $return_array[] = array($findallorderdetails->order_number, $finduserdetails->name, date('d-m-Y', strtotime($findallorderdetails->created_at)), $met->reason, $met->description, $orderdetails->final_price, $findallorderdetails->payment_type, $findallorderdetails->transcation_id, $met->request_type, $orderstatus, $met->order_id);
        }
        
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function ordercanceladmin(Request $request) {
        $details = Orderrequest::where(["request_type"=>"cancel"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
            
                $findallorderdetails = Order::find($met->order_id);
                $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
                $finduserdetails = User::find($met->user_id);

                $return_array[] = array($findallorderdetails->order_number, 
                    $finduserdetails->name, 
                    $met->refund_by, 
                    date('d-m-Y', strtotime($findallorderdetails->created_at)), 
                    $met->reason, 
                    $met->description, 
                    $orderdetails->final_price, 
                    $findallorderdetails->payment_type, 
                    $findallorderdetails->transcation_id);
            }
        }
        
        return $this->sendResponse1($return_array, 'Order Cancel list retrieved successfully.', $request->path());
    }

    public function orderreturnseller(Request $request, $id) {
        $details = Orderrequest::whereIn("request_type", ["exchange", "return"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
            
                $findallorderdetails = Order::find($met->order_id);

                if($findallorderdetails->seller_id == $id) {
                    $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
                    $finduserdetails = User::find($findallorderdetails->user_id);

                    $return_array[] = array($findallorderdetails->order_number, $finduserdetails->name, date('d-m-Y', strtotime($findallorderdetails->created_at)), $met->reason, $met->description, $orderdetails->final_price, $findallorderdetails->payment_type, $findallorderdetails->transcation_id, $met->request_type, $met->status);
                }
            }
        }
        
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function ordercancelseller(Request $request, $id) {
        $details = Orderrequest::where(["request_type"=>"cancel"])->orderBy('id','DESC')->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $met) {
            
                $findallorderdetails = Order::find($met->order_id);
                if($findallorderdetails) {
                    if($findallorderdetails->seller_id == $id) {
                        $orderdetails = OrderDetail::where(["order_id" => $met->order_id])->first();
                        $finduserdetails = User::find($findallorderdetails->user_id);

                        $return_array[] = array($findallorderdetails->order_number, $finduserdetails->name, $met->refund_by, date('d-m-Y', strtotime($findallorderdetails->created_at)), $met->reason, $met->description, $orderdetails->final_price, $findallorderdetails->payment_type, $findallorderdetails->transcation_id);
                    }
                }
            }
        }
        
        return $this->sendResponse1($return_array, 'Order Cancel list retrieved successfully.', $request->path());
    }

    public function orderlisting(Request $request, $id) {
        
        if ($id == "admin") {
            $getids = Order::select('id')->distinct('id')->get();

            $query = Order::query();
            $query->select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id');

            if (($request->has('start_date') && !empty($request->start_date)) && ($request->has('end_date') && !empty($request->end_date))) {
                $start = Carbon::parse($request->start_date);
                $end = Carbon::parse($request->end_date);
                $query->whereDate('orders.created_at','<=',$end->format('Y-m-d'))->whereDate('orders.created_at','>=',$start->format('Y-m-d'));
            }

            $details = $query->orderBy('orders.id','DESC')->get();

            $return_array = array();
            
            foreach ($details as $met) {

                $getSeller = Seller::select("name")->where(['id' => $met->seller_id])->first();
                $detail = OrderDetail::where(['order_id'=>$met->id])->first();

                $product = Product::select("name","ships_in","sku","sp","is_variant")->where(['id'=>$met->product_id])->first();
                if($product) {
                    if($product->is_variant ==2) {
                        if(!empty($detail->color) && !empty($detail->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$met->product_id, "size"=>$detail->size, "color"=>$detail->color])->first();
                        } else if(!empty($detail->color) && empty($detail->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$met->product_id, "color"=>$detail->color])->first();
                        } else if(empty($detail->color) && !empty($detail->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$met->product_id, "size"=>$detail->size])->first();
                        }

                        if($proPriceDetail) {
                            $get_price_sp = $proPriceDetail->sp; 
                            $get_price_mrp = $proPriceDetail->mrp;
                        } else {
                            $get_price_sp = $product->sp; 
                            $get_price_mrp = $product->mrp;    
                        }
                    } else {
                        $get_price_sp = $product->sp; 
                        $get_price_mrp = $product->mrp;
                    }

                    if($product) {
                        $productName = $product->name;
                    } else {
                        $productName = "";
                    }

                    if($met->gift_wrap_status == 1) {
                        $gift_wrap_status = "Yes";
                    } else {
                        $gift_wrap_status = "No";
                    }

                    $shipsINDAYS = "+". $product->ships_in ."days";
                    $oDate = date('d-m-y',strtotime($met->created_at));
                    $shipsIN = date('d-m-Y', strtotime($oDate . $shipsINDAYS));

                    $return_array[] = array($met->order_number, $getSeller->name, $productName, $product->sku, $get_price_sp, $met->buyer, $met->location, $met->payment_type, $detail->quantity ,$met->status, $gift_wrap_status, date('d-m-y H:i',strtotime($met->created_at)), $met->sla_time, $met->id, $met->id);
                }
            }
        } else {

        }

        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function buyerorderlisting(Request $request, $id) {
        
            $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id", "orders.order_number", "users.name as buyer", "orders.location", "orders.payment_type", "orders.status", "orders.created_at")->where(["orders.user_id"=>$id])->get();
            $return_array = array();
            
            foreach ($details as $met) {

                $return_array[] = array($met->order_number,$met->buyer,$met->location,$met->payment_type, 1, $met->status, date('d-m-y H:i',strtotime($met->created_at)),$met->id);   
            }

        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    
    public function sellerordercsv(Request $request, $id) {
        
        $input = $request->all();
        $inputOrder = $request->orderids;
        $inputOrderArr = explode(',', $inputOrder);

        $query = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.seller_id"=>$id])->whereIn("orders.id", $inputOrderArr);
        $details = $query->orderBy('id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();
            $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }
            $shipsINDAYS = "+". $product->ships_in ."days";
            $oDate = date('d-m-y',strtotime($met->created_at));
            $shipsIN = date('d-m-Y', strtotime($oDate . $shipsINDAYS));

            $return_array[] = array("order_number"=>$met->order_number, "product_name"=>$productName, "sku"=>$product->sku, "sale_price"=>$product->sp, "buyer_name"=>$met->buyer, "location"=>$met->location, "payment_type"=>$met->payment_type, "quantity"=>$detail->quantity, "status"=>$met->status, "order_date"=>date('d-m-y H:i',strtotime($met->created_at)), "Shipsin"=>$shipsIN);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }



    // Seller Order listings by status functions

    public function sellerorderlisting(Request $request, $id) {
        
        $query = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["seller_id"=>$id])->whereIn("orders.status", ['new order','reorder']);
        
        if (($request->has('start_date') && !empty($request->start_date)) && ($request->has('end_date') && !empty($request->end_date))) {
            $start = Carbon::parse($request->start_date);
            $end = Carbon::parse($request->end_date);
            $query->whereDate('orders.created_at','<=',$end->format('Y-m-d'))->whereDate('orders.created_at','>=',$start->format('Y-m-d'));
        }

        $details = $query->orderBy('orders.id','DESC')->get();

        //dd($details);
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name","ships_in","sku","sp","is_variant")->where(['id'=>$met->product_id])->first();
            if($product) {
                
                $get_price_sp = $detail->sale_price; 
                $get_price_mrp = $detail->price;
                

                if($product) {
                    $productName = $product->name;
                } else {
                    $productName = "";
                }

                if($met->gift_wrap_status == 1) {
                    $gift_wrap_status = "Yes";
                } else {
                    $gift_wrap_status = "No";
                }

                $get_current_date = strtotime(date('Y-m-d H:i:s'));
                $get_order_date = strtotime($met->created_at);
                $datediff = $get_current_date - $get_order_date;
                $getdayDiff = round($datediff / (60 * 60 * 24));
                
                //if($met->id == 38) {

                // echo date('Y-m-d H:i:s');
                // echo "<BR>";
                // echo $met->created_at;
                // echo "<BR>";
                // echo $getdayDiff;
                // die;
                
                

                if($met->sla_time == 1) {
                    if($getdayDiff == 0) {
                        $updatesla = "1 days";
                    } else if($getdayDiff == 1) {
                        $updatesla = "2 days";
                    } else {
                        $updatesla = "3 days";
                    }
                } else {
                    if($getdayDiff == 0) {
                        $updatesla = "0 days";
                    } else if($getdayDiff == 1) {
                        $updatesla = "1 days";
                    } else  if($getdayDiff == 2) {
                        $updatesla = "2 days";
                    } else  if($getdayDiff == 3) {
                        $updatesla = "3 days";
                    } else {
                        $updatesla = "3 days";
                    }
                }

                $return_array[] = array($met->order_number, $productName, $product->sku, $get_price_sp, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity , $gift_wrap_status, $met->status, date('d-m-Y H:i',strtotime($met->created_at)), $updatesla, $met->id, $met->id);
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting1(Request $request, $id) {  //  Pending RTD
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"accepted", "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name","ships_in","sku","sp")->where(['id'=>$met->product_id])->first();
            
            $get_price_sp = $detail->sale_price; 
                $get_price_mrp = $detail->price;

            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $get_current_date = strtotime(date('Y-m-d H:i:s'));
            $get_order_date = strtotime($met->created_at);
            $datediff = $get_current_date - $get_order_date;
            $getdayDiff = round($datediff / (60 * 60 * 24));
            
            //if($met->id == 38) {

            // echo date('Y-m-d H:i:s');
            // echo "<BR>";
            // echo $met->created_at;
            // echo "<BR>";
            // echo $getdayDiff;
            // die;
            
            

            if($met->sla_time == 1) {
                if($getdayDiff == 0) {
                    $updatesla = "1 days";
                } else if($getdayDiff == 1) {
                    $updatesla = "2 days";
                } else {
                    $updatesla = "3 days";
                }
            } else {
                if($getdayDiff == 0) {
                    $updatesla = "0 days";
                } else if($getdayDiff == 1) {
                    $updatesla = "1 days";
                } else  if($getdayDiff == 2) {
                    $updatesla = "2 days";
                } else  if($getdayDiff == 3) {
                    $updatesla = "3 days";
                } else {
                    $updatesla = "3 days";
                }
            }
            
            $mistakeData = SellerMistake::where(['order_id'=>$met->id, 'type'=>'breache'])->first();
            if($mistakeData) {
                $hasMistake = "SLA Breached";
            } else {
                $hasMistake = "";
            }

            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $product->sku, $get_price_sp, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity, $met->status , date('d-m-Y H:i',strtotime($met->created_at)), $updatesla, $hasMistake, $met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }


    public function sellerorderlisting2(Request $request, $id) {  //  Dispatched
        
        $manifest = Manifest::select('id','order_id', 'order_count', 'handover_date')->orderBy('id','DESC')->get();
        $return_array = array();
        
        foreach ($manifest as $met) {

            $checkOrder = Order::whereIn("status", ["packed", "manifested","not picked"])->whereIn("id", explode(',', $met->order_id))->where(["seller_id"=>$id])->get();
            if($checkOrder) {

                $mistakeData = SellerMistake::where(['order_id'=>$met->id, 'type'=>'reattempt'])->first();
                if($mistakeData) {
                    $hasMistake = "Pickup reattempt";
                } else {
                    $hasMistake = "";
                }

                $return_array[] = array('Delhivery Express', date('d-m-Y H:i:s',$met->handover_date), $met->order_count, 'http://mobuloustech.com/satvick/api/order-manifiest/'.$met->id, $hasMistake, $met->order_id);
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting3(Request $request, $id) {  //  In Transit list
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"transit", "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }


    public function sellerorderlisting4(Request $request, $id) {  //  Delivered
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"delivered", "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function sellerorderlisting5(Request $request, $id) {  //  Pending
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"pending", "seller_id"=>$id])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }
            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $met->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }


    public function changethestatusoforder(Request $request) {
        $oids = $request->order_id;
        $jsonarray = array();
        $jsonData = array();
        $returnData = array();

        $sellerDetail = Seller::where(['id' => $request->seller_id])->first();

        $x=1;
        $getorderids = '';
        foreach($oids as $oid) {

            $cancelData = array();

            $order = Order::where(['id'=>$oid])->first();
            $details = OrderDetail::where(['order_id'=>$order->id])->first();

            if($request->status == 'dispatched') {
                $msg = "Your order has been out for delivery";

            } else if($request->status == 'delivered') {
                $msg = "Your order has been delivered";

            } else if($request->status == 'transit') {
                $msg = "Your order has been in-transit";

            } else if($request->status == 'cancelled') {
                $msg = "Your order has been cancelled";

                $cancelData = ["order_id"=>$order->id, "product_id"=>$order->product_id, "user_id"=>$order->user_id, "seller_id"=>$order->seller_id, "request_type"=>"cancel", "refund_by"=>"Seller", "status"=>0, "price"=>$details->final_price];
                Orderrequest::create($cancelData);

                $jsonarray = [
                    "waybill"=>$details->wbn_num,
                    "cancellation"=>"true"
                ];
                $this->shippingapiordercancle($jsonarray);
            
            } else if($request->status == 'return') {

                $msg = "Your order request for return has been confirmed";
            
            } else if($request->status == 'accepted') {
                
                $prod = Product::where(['id'=>$order->product_id])->first();
                if($order->payment_type == "Cash On Delivery") {
                    $paymentType = "COD";
                    $codamt = $details->final_price;
                } else {
                    $paymentType = "Prepaid";
                    $codamt = 0;
                }

                if($order->status == "reorder") {

                    $msg = "";
                    $jsonData['shipments'][] = [
                            "add" => $order->location,
                            "address_type" => $order->address_type,
                            "phone" => $order->phone,
                            "payment_mode"=> "REPL",
                            "name" => $order->name,
                            "city" => $order->city,
                            "state" => $order->state,
                            "pin" => $order->pincode,
                            "country" => $order->country,

                            "order" => $order->order_number,
                            "hsn_code" => $prod->sku,
                            "client" => "THEBRANDSTANDSURFACE-B2C",
                            "shipment_width" =>$prod->width,

                            "seller_gst_tin" => $sellerDetail->gstnum,
                            "seller_name" =>$sellerDetail->name,
                            "seller_add" =>$sellerDetail->address,

                            "return_city"=> $sellerDetail->city,
                            "return_country"=> $sellerDetail->country,
                            "return_add"=> $sellerDetail->location,
                            "return_pin"=> $sellerDetail->pincode,
                            "return_name"=>$sellerDetail->name,

                            "weight" => $prod->weight,
                            "cod_amount" => $codamt,
                            "dangerous_good" => "False",
                            "waybill" => $details->wbn_num,
                            "order_date" =>$order->created_at,
                            "total_amount" => $details->final_price,
                            "quantity" =>$details->quantity,
                    ];

                } else {

                    $msg = "Your order has been confirmed";
                
                    $jsonData['shipments'][] = [
                            "add" => $order->location,
                            "address_type" => $order->address_type,
                            "phone" => $order->phone,
                            "payment_mode"=> $paymentType,
                            "name" => $order->name,
                            "city" => $order->city,
                            "state" => $order->state,
                            "pin" => $order->pincode,
                            "country" => $order->country,

                            "order" => $order->order_number,
                            "hsn_code" => $prod->sku,
                            "client" => "THEBRANDSTANDSURFACE-B2C",
                            "shipment_width" =>$prod->width,

                            "seller_gst_tin" => $sellerDetail->gstnum,
                            "seller_name" =>$sellerDetail->name,
                            "seller_add" =>$sellerDetail->address,

                            "weight" => $prod->weight,
                            "cod_amount" => $codamt,
                            "dangerous_good" => "False",
                            "waybill" => $details->wbn_num,
                            "order_date" =>$order->created_at,
                            "total_amount" => $details->final_price,
                            "quantity" =>$details->quantity,
                    ];
                }

                    $jsonData['pickup_location'] = [
                        "name" => $sellerDetail->name,
                        "city" => $sellerDetail->city,
                        "pin" => $sellerDetail->pincode,
                        "country" => $sellerDetail->country,
                        "phone" => $sellerDetail->phone,
                        "add" => $sellerDetail->address
                    ];

                    $jsonarray = 'format=json&data='. json_encode($jsonData);

                    $this->shippingapiordercreate($jsonarray);
                    $this->shippingapipackingslip($details->wbn_num);

            } else if($request->status == 'shipped') {

                $msg = "Your order has been shipped";
            
            } else if($request->status == 'packed') {

                $msg = "Your order has been packed by seller and ready to ship";
                if($x==1) {
                    $getorderids = $order->id;
                } else {
                    $getorderids = $getorderids .','.$order->id;
                }
            }

            $orderUpdate = Order::where(['id'=>$order->id])->first();
            $orderUpdate->status = $request->status;
            $orderUpdate->save();

            if(strlen($msg) > 1) {
                $notifyData = [
                    'user_id'=>$order->user_id,
                    'order_id'=>$order->id,
                    'order_number'=>$order->order_number,
                    'type'=>'Order',
                    'title'=>'Order - '.$order->order_number,
                    'content'=>$msg
                ];
                Notification::create($notifyData);
                $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$order->user_id])->first();
                $badge = Notification::where(["user_id"=>$order->user_id,'status'=>0])->count();
                if($userDevice) {
                    if(strlen($userDevice->deviceToken) > 0) {
                        $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
                    }
                }
            }
            $x++;
        }

        if($request->status == 'packed') {
            $return_array = array(
                            "order_id" => $getorderids, 
                            "order_count" => $x - 1, 
                            "handover_date" => strtotime('+12 hours'),
                        );
            Manifest::create($return_array);

            $jsonarray = [
                "pickup_time"=>date("H:i:s", strtotime('+12 hours')),
                "pickup_date"=>date("H:i:s", strtotime('+12 hours')),
                "pickup_location"=>$sellerDetail->name,
                "expected_package_count"=>$x,
            ];

            $this->shippingapiorderpickup($jsonarray);
        }
        
        return $this->sendResponse1([], 'Order Status Updated successfully.', $request->path());
    }


    public function shippingapiordercreate($jsonarray) {   //   order create

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];

        $url = "https://staging-express.delhivery.com/api/cmu/create.json";
        $myBody = $jsonarray;

        $client = new Client($headers);
        $response = $client->post($url, [
            'http_errors' => false,
            'body' => $myBody,
        ]);
        $status = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents());
        return $data;
    }

    public function shippingapipackingslip($jsonarray) {  //  Packing Slip creation

        $checkDetails = OrderDetail::where(['wbn_num'=>$jsonarray])->first();

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];
        $http = new Client($headers);
        $generateWBN = $http->get(url('https://staging-express.delhivery.com/api/p/packing_slip?wbns='.$jsonarray));
        $data_wbn = json_decode($generateWBN->getBody());

        if(strlen($checkDetails->barcode) <= 0 ) {

            $file_data = $data_wbn->packages[0]->barcode; 
            $file_name = 'image_'.time().'.png'; //generating unique file name; 
            $path = public_path() . "/barcode/" . $file_name;
            $img = $file_data;
            $img = substr($img, strpos($img, ",")+1);
            $data = base64_decode($img);
            file_put_contents($path, $data);
            $destinationPath = $this->baseurlslash . "barcode/" . $file_name;

            $details = OrderDetail::where(['wbn_num'=>$jsonarray])->first();
            $details->barcode = $destinationPath;
            $details->save();
        }

        if(strlen($checkDetails->barcode_more) <= 0 ) {

            $file_data1 = $data_wbn->packages[0]->oid_barcode; 
            $file_name1 = 'image_'.time().'.png'; //generating unique file name; 
            $path1 = public_path() . "/barcode/" . $file_name1;
            $img1 = $file_data1;
            $img1 = substr($img1, strpos($img1, ",")+1);
            $data1 = base64_decode($img1);
            file_put_contents($path1, $data1);
            $destinationPath1 = $this->baseurlslash . "barcode/" . $file_name1;

            $details = OrderDetail::where(['wbn_num'=>$jsonarray])->first();
            $details->barcode_more = $destinationPath1;
            $details->save();
        }
        return true;

    }

    public function shippingapiordercancle($jsonarray) {  //  cancle order

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];

        $url = "https://staging-express.delhivery.com/api/p/edit";
        $myBody = $jsonarray;

        $client = new Client($headers);
        $response = $client->post($url, [
            'http_errors' => false,
            'json' => $myBody,
        ]);
        $status = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents());
        return $data;
    }


    public function shippingapiorderpickup($jsonarray) {  //  Pickup request

        $headers['headers'] = [
            'Content-Type' => 'application/json',
            'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
            'Accept' => 'application/json',
        ];

        $url = "https://staging-express.delhivery.com/api/p/edit";
        $myBody = $jsonarray;

        $client = new Client($headers);
        $response = $client->post($url, [
            'http_errors' => false,
            'json' => $myBody,
        ]);
        $status = $response->getStatusCode();
        $data = json_decode($response->getBody()->getContents());
        return $data;
    }

    public function orderinvoice($oids) {

        $datas = Order::whereIn('id', [$oids])->get();
        $jsonarry = array();

        foreach($datas as $data) {
            $headers = [
                'Content-Type' => 'application/json',
                'Authorization' => 'Token dcafa28eb05642c39f31b14724e28cf45f91256a',
                'Accept' => 'application/json',
            ];

            $order = OrderDetail::where(['order_id'=>$data->id])->first();

            $http = new Client([
                'headers' => $headers
            ]);
            $generateWBN = $http->get(url('https://staging-express.delhivery.com/api/p/packing_slip?wbns='.$order->wbn_num));
            $data_wbn = json_decode($generateWBN->getBody());

            $user = User::where(['id'=>$data->user_id])->first();
            $seller = Seller::where(['id'=>$data->seller_id])->first();
            $product = Product::where(['id'=>$data->product_id])->first();

            $jsonarry['invoice']['user'] = [
                    "name"=>$data->name,
                    "location"=>$data->location,
            ];

            $jsonarry['invoice']['seller'] = [
                    "name"=>$seller->name,
                    "location"=>$seller->address,
                    "gst"=>$seller->gstnum,
                    "pan_num"=>$seller->pan_num,
            ];

            $jsonarry['invoice']['courier'] = [
                    "name"=>'Delivery Express',
            ];

            $addDay = '+ '.$product->ships_in.' days';

            $jsonarry['invoice']['products'] = [
                    "name"=>$product->name,
                    "quantity"=>$order->quantity,
                    "desc"=>$product->hsn_code,
                    "sku"=>$product->sku,
                    
                    "price"=>$order->sale_price,
                    "discount"=>$order->discount_price + $order->refer_discount,
                    "valuable_tax"=>$order->valuable_tax,
                    "total_tax"=>$order->gst_tax,
                    "total_price"=>$order->final_price,
                    "tracking_id"=>$order->wbn_num,
                    "order_number"=>$data->order_number,
                    "barcode"=>$order->barcode,
                    "payment_type"=>$data->payment_type,
                    "order_date"=>date('d-m', strtotime($data->created_at)),
                    "order_date_full"=>date('d-m-Y H:i:sa', strtotime($data->created_at)),
                    "dispatch_date"=>date('d-m', strtotime($data->created_at . $addDay)),
            ];

            $jsonarry['invoice']['shipping'] = [
                "quantity" =>1,
                "amount"=>$order->seller_shipping,
            ];
        }

        $items = [];
        view()->share('jsonarry',$jsonarry);
        //if($download == "pdf") {
        $pdf = PDF::loadView('pdfinvoice');
        //return $pdf->download('pdfview.pdf');
        //}
        return $pdf->download('pdfinvoice.pdf');
        //return $pdf->stream('pdfinvoice.pdf');
    }


    public function ordermanifest($oids) {
        $manifest = Manifest::where(['id'=>$oids])->first();
        if($manifest) {

            $datas = OrderDetail::whereIn('id', explode(',',$manifest->order_id))->get();
            $listArray = array();
            foreach ($datas as $value) {
                $listArray[] = ['wbn'=>$value->wbn_num, "order_number"=>$value->order_number, "date"=>$manifest->created_at];
            }

            $items = ['handover_date'=>$manifest->handover_date, 'ordercount'=>$manifest->order_count, 'lists'=>$listArray];
        } else {
            $items =[];
        }
        view()->share('items',$items);
        
        $pdf = PDF::loadView('manifest');
        //return view('manifest');
        return $pdf->download('manifest.pdf');
        //return $pdf->stream('manifest.pdf');
    }

    public function ordermanifest1($oids) {
        $manifest = Manifest::where(['id'=>$oids])->first();
        if($manifest) {
            $items = ['handover_date'=>$manifest->handover_date, 'ordercount'=>$manifest->order_count];
        } else {
            $items =[];
        }
        view()->share('items',$items);
        
        $pdf = PDF::loadView('manifest');
        return view('manifest');
        //return $pdf->download('manifest.pdf');
        //return $pdf->stream('manifest.pdf');

    }

    public function deleteorder(Request $request, $id) {
        Order::where('id', $id)->delete();
        return $this->sendResponse1([], "Order Id " . $id . ' deleted successfully', $request->path());
    }

    //  End of functions


    public function orderlistingseller(Request $request, $id) {
        $orId = array();
        $getOrderIds = Order::select('id')->where(['seller_id'=>$id])->get();
        
        foreach($getOrderIds as $getOrderId) {
            $orId[] = $getOrderId->order_id;
        }

        $getorId = array_unique($orId);

        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id", "orders.order_number", "users.name as buyer", "orders.location", "orders.payment_type", "orders.created_at")->whereIn("orders.id", $getorId)->get();
        $return_array = array();
        
        foreach ($details as $met) {
            $detailsaboutproduct = OrderDetail::where(['order_id'=>$met->id])->count();
            $checkDelivered = OrderDetail::where(['order_id'=>$met->id, 'status'=>'Delivered'])->count();
            
            if($detailsaboutproduct == $checkDelivered) {
                $status = "Delivered";
            } else {
                $status = "Pending";
            }

            $return_array[] = array($met->order_number,$met->buyer,$met->location,$met->payment_type, $detailsaboutproduct, $status, date('d-m-y H:i',strtotime($met->created_at)),$met->id);
        }

        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function orderrequest(Request $request, $id) {
        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.product_id", "orders.id as order_id", "users.name as buyer", "orders.location", "orders.dispatch_by", "orders.payment_type", "orders.order_date", "orders.amount", "orders.status")->get();
        // dd($details);
        $return_array = array();
        foreach ($details as $met) {
            $all_product = explode(",", $met->product_id);
            $check_request = Orderrequest::where('order_id', $met->order_id)->first();
            if (!empty($check_request)) {
                if ($check_request->request_type == "Exchange") {
                    $detailsaboutproduct = Product::whereIn('id', $all_product)->get();
                    if ($id == "admin") {
                        foreach ($detailsaboutproduct as $value) {
                            if ($value->user_type == $id) {
                                $return_array[] = array([$this->baseurl . explode(",", $value->images) [0], $value->sku, $value->brand, $value->id], $met->order_id, $met->buyer, $met->location, $met->order_date, $met->dispatch_by, $met->payment_type, $met->amount, [$met->status, $met->order_id], $met->order_id);
                            }
                        }
                    } else {
                        foreach ($detailsaboutproduct as $value) {
                            if ($value->user_id == $id) {
                                $return_array[] = array([$this->baseurl . explode(",", $value->images) [0], $value->sku, $value->brand, $value->id], $met->order_id, $met->buyer, $met->location, $met->order_date, $met->dispatch_by, $met->payment_type, $met->amount, [$met->status, $met->order_id], $met->order_id);
                            }
                        }
                    }
                }
            }
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function orderdetails(Request $request, $id) {

        $details = Order::join('users', 'users.id', '=', 'orders.user_id')->select("orders.id as id", "orders.*", "users.name", "users.email", "users.phone")->where(["orders.id"=>$id])->first();

        if($details) {
            $orderDetails = array();
            $getDetail = OrderDetail::where(['order_id'=>$details->id])->first();
            
            $seller = Seller::where(['id'=>$details->seller_id])->first();

            $pro = Product::where(['id'=>$details->product_id])->first();
            if(empty($pro->images)) {
                $img = url('/public/') . "/img/imagenotfound.jpg";
            } else {
                $imgArr = explode(',', $pro->images);
            }

            $notifyArr = array();
            $getNotify = Notification::where(['order_id'=>$details->id, 'type'=>'order'])->get();
            if($getNotify) {
                foreach($getNotify as $notify) {
                    $notifyArr[] = ["date"=> date("d M", strtotime($notify->updated_at)), "content"=>$notify->content];
                }
            }

            if($details->gift_wrap_status == 1) {
                $details->gift_wrap_status = "Yes";
            } else {
                $details->gift_wrap_status = "No";
            }

            if(!empty($getDetail->color)) {
                $colorName = Color::select('name')->where(["hexcode"=>$getDetail->color])->first();
                if($colorName) {
                    $defaultcolorname = $colorName->name;
                    if($colorName) {
                        $colorname = $colorName->name;
                    } else {
                        $colorname = "";  
                    }
                } else {
                    $defaultcolorname = "";
                    $colorname = "";  
                }
            } else {
                $defaultcolorname = "";
                $colorname = "";  
            }            

            $orderDetails[] = [
                    "id"=>$details->id,
                    "order_id"=>$details->id,
                    "product_id"=>$details->product_id,
                    "product_name"=>$pro->name,
                    "product_image"=>$imgArr[0],
                    "product_sku"=>$pro->sku,
                    "hsn_code"=>$pro->hsn_code,
                    "size"=>$getDetail->size,
                    "size_label"=>$pro->size_label,
                    "color"=>$colorname,
                    "quantity"=>$getDetail->quantity,
                    "sale_price"=>$getDetail->sale_price,
                    "discount_price"=>$getDetail->discount_price,
                    "shipping"=>$getDetail->seller_shipping,
                    "gst"=>$getDetail->gst_tax,
                    "total_price"=>$getDetail->final_price,
                    "gift_wrap_status"=>$details->gift_wrap_status,
                    "dispatch_by"=>$details->dispatch_at,
                    "payment_status"=>$details->payment_status,
                    "status"=>$details->status,
                    "seller"=>$seller->name,
                    "seller_email"=>$seller->email,
                    "seller_phone"=>$seller->phone,
                    "trackorder" => $notifyArr
            ];

            $orderfetch = [
                "id"=>$details->id,
                "order_number"=>$details->order_number,
                "location"=>$details->location,
                "payment_type"=>$details->payment_type,
                "transcation_id"=>$details->transcation_id,
                "coupen_code"=>$details->coupen_code,
                "created_at"=>$details->created_at,
                "user_name"=>$details->name,
                "user_email"=>$details->email,
                "phone"=>$details->phone,
                "details"=>$orderDetails,
            ];
        } else {
            $orderfetch = [];
        }

        return $this->sendResponse1($orderfetch, 'Order details retrieved successfully.', $request->path());
    }

    public function userlistforadmin(Request $request) {
        $details = User::select("image", "name", "email", "phone", "status", "id")->orderby('id', 'desc')->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->image)) {
                if (strpos($met->image, $this->baseurlslash) == false) {
                    $met->image = $this->baseurl . $met->image;
                }
            } else {
                $met->image = url('/public/') . "/img/imagenotfound.jpg";
            }

            $return_array[] = array($met->image, $met->name, $met->email, $met->phone, [$met->status, $met->id], $met->id);
            
        }
        return $this->sendResponse1($return_array, 'Buyer list retrieved successfully', $request->path());
    }

    public function sellerlistforadmin(Request $request) {
        $details = Seller::select("image", "name", "email", "phone", "status", "id","commission")->get();
        $return_array = array();
        foreach ($details as $met) {
            if (!empty($met->image)) {
            } else {
                $met->image = url('/public/') . "/img/imagenotfound.jpg";
            }

            $comm = $met->commission;

            if($met->status == 1) {
                $metstatus = 1;
            } else {
                $metstatus = '';
            }
            
            $return_array[] = array($met->image, $met->name, $met->email, $met->phone, [$metstatus, $met->id], $comm, [$met->id,$comm], $met->id);
        }
        return $this->sendResponse1($return_array, 'Buyer list retrieved successfully', $request->path());
    }

    public function viewprofileselleradmin(Request $request, $id) {

        if(empty($id)) {
            return $this->sendError($request->path(), 'Seller not found');
        }

        $check_token = Seller::where(['id' => $id])->first();
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Seller not found');
        }

        $userdetails = Seller::find($id);
        if (!empty($userdetails->image)) {
            $userdetails->image = url('/public/') . $userdetails->image;
        } else {
            $userdetails->image = url('/public/') . "/img/imagenotfound.jpg";
        }
        
        $return_array = array("user_id" => (string)$userdetails->id, 
                             "image" => $userdetails->image,
                             "name" => $userdetails->name,
                             "email" => $userdetails->email,
                             "phone" => $userdetails->phone,
                             "dob" => $userdetails->gstnum,
                             "gender" => $userdetails->pan_num,
                             "address"=>$userdetails->address,
                             "brand"=>$userdetails->brand,
                        );

        return $this->sendResponse($return_array, 'Seller details retrieved successfully', $request->path());
    }


    public function catlistforadmin(Request $request) {
        $details = DB::table('subcategories')->select("id", "name", "category_id")->orderBy('id','DESC')->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->category_id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Subcategory list retrieved successfully', $request->path());
    }

    public function catlistforadmin1(Request $request, $id) {
        $detailsofsub = Subcategory::find($id);
        $details = DB::table('subsubcategories')->select("id", "name")->where(['subcategory_id' => $id])->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1(["list" => $return_array, "list1" => $detailsofsub], 'Subcategory list retrieved successfully', $request->path());
    }

    public function subcategorydetailsupdate(Request $request, $id) {
        $input = $request->all();

            $findsubcatg = Subcategory::find($id);
            $findsubcatg->name = $input['name'];
            //$findsubcatg->category_id = $input['category_id'];
            $findsubcatg->image = $input['image'];
            $findsubcatg->save();
            return $this->sendResponse1(["list1" => $findsubcatg], 'Subcategory updated successfully', $request->path());
    }
    
    public function bannerdetailsbyid(Request $request, $id) {
        $bannerArray = array();
        $bannerdetails = Banner::find($id);

        $cartData = [$bannerdetails->category_id, $bannerdetails->category_id];

        $subcatlists = Subcategory::where('category_id', $bannerdetails->category_id)->get();
        $subcartData = array();
        foreach($subcatlists as $subcatlist) {
            $subcartData[] = [$subcatlist->id, $subcatlist->name];
        }

        $subsubcatlists = Subsubcategory::where('subcategory_id', $bannerdetails->subcategory_id)->get();
        $subsubcartData = array();
        foreach($subsubcatlists as $subsubcatlist) {
            if($subsubcatlist->status == 1) {
                $subsubcartData[] = [$subsubcatlist->id, $subsubcatlist->name];
            }
        }

        if(count($subsubcartData) > 0) {
            $bannerArray = [
                    "bannerdetails"=>$bannerdetails,
                    "catlist"=>$cartData,
                    "subcatlist"=>$subcartData,
                    "subsubcatlist"=>$subsubcartData
                ];
        }
        return $this->sendResponse1($bannerArray, 'Banner list successfully', $request->path());
    }

    public function flashproductlist(Request $request) {
        $detailsnew = Flashsale::find(1);

        if(strlen($detailsnew->subsubcategory)>=1 && $detailsnew->subsubcategory != 0) {
            $subname = Subsubcategory::where('id', $detailsnew->subsubcategory)->first();
            $return_array[] = array($subname->name, $subname->id);
            $approval = "Selected category";
        } else {
            $newarray = explode(",", $detailsnew->products);
            $details = DB::table('products')->select("id", "name")->whereIn('id', $newarray)->get();
            $return_array = array();
            foreach ($details as $met) {
                $return_array[] = array($met->name, $met->id);
            }
            $approval = "Selected products";
        }
        return $this->sendResponse1(array("data" => $return_array, "status" => $detailsnew->status, "time" => $detailsnew->time, "endtime" => $detailsnew->endtime, "label"=>$approval), 'Flash sale list retrieved successfully', $request->path());
    }

    public function updateflashtime(Request $request) {
        $input = $request->all();
        $detailsnew = Flashsale::find(1);
        if(!empty($input['time'])) {
            $detailsnew->time = date("Y-m-d H:i:s", strtotime($input['time']));
        }
        if(!empty($input['endtime'])) {
            $detailsnew->endtime = date("Y-m-d H:i:s", strtotime($input['endtime']));
        }
        $detailsnew->status = $input['status'];
        $detailsnew->save();
        return $this->sendResponse1([], 'Flash sale End time updated successfully', $request->path());
    }
    public function catlistforadmin1234(Request $request) {
        $details = DB::table('subcategories')->select("id", "name")->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1($return_array, 'Subcategory list retrieved successfully', $request->path());
    }
    public function productids(Request $request) {
        $details = DB::table('products')->select("id", "name")->get();
        $return_array = array();
        foreach ($details as $met) {
            $return_array[] = array($met->name, $met->id);
        }
        return $this->sendResponse1($return_array, 'Product list retrieved successfully', $request->path());
    }
    public function addptoflash(Request $request) {
        $input = $request->all();
        $details = Flashsale::find(1);
        $newarray = array();

        if(empty($input['product_id'])) {
            $details->subsubcategory = $input['subsubcatid'];
            $details->products = " ";
        
        } else {
            $details->subsubcategory = " ";
            $newarray = $details->products;
            $newarray = explode(",", $newarray);
            $newarray[] = $input['product_id'];
            $details->products = implode(",", array_unique($newarray));
        }
        
        $details->save();
        return $this->sendResponse1([], 'Product added to flash sale successfully', $request->path());
    }

    
    public function Categorylist(Request $request) {
        $details = DB::table('categories')->select("id as category_id", "name", "image")->get();
        foreach ($details as $value) {
            if (!empty($value->image)) {
                $value->image = url('/public/') . $value->image;
            } else {
                $value->image = url('/public/') . "/img/imagenotfound.jpg";
            }
        }
        return $this->sendResponse($details, 'Category list retrieved successfully', $request->path());
    }

    public function createbanner(Request $request) {
        $input = $request->all();
    
        if (isset($input['idm']) && !empty($input['idm'])) {
            $findbannerdetails = Banner::find($input['idm']);
    
            if (!empty($input['image'])) {
                $findbannerdetails->image = $input['image'];
                $findbannerdetails->save();
            }

            DB::table('banners')->where(['id'=> $findbannerdetails->id])->update(["type"=> $input['type'], "category_id"=>$input['category_id'], "subcategory_id"=>$input['subcategory_id'], "subsubcategory_id"=>$input['subsubcategory_id']]);

            return $this->sendResponse([], 'Banner created successfully', $request->path());
        } else {
            Banner::create($input);
            return $this->sendResponse([], 'Banner created successfully', $request->path());
        }
    }

    public function createtheme(Request $request) {
        $input = $request->all();
        Theme::create($input);
        return $this->sendResponse([], 'Theme created successfully', $request->path());
    }

    public function createBrand(Request $request) {
        $input = $request->all();
        $brandName = $input['name'];
        $brand = Brand::where(["name"=>$brandName])->first();

        if($brand) {
            return $this->sendError($request->path(), 'Brand Already Added');
        } else {
            $input['name'] = ucwords($brandName);
            Brand::create($input);
            return $this->sendResponse([], 'Brand created successfully', $request->path());
        }
    }

    public function editprofile(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['token' => 'required', 'user_id' => 'required', 'fullname' => 'required', 'email' => 'required', 'phone' => 'required', 'dob' => 'required', 'gender' => 'required']);
        
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $check_token = Token::where(['user_id' => $input['user_id'], 'token' => $input['token']])->first();
        
        if (empty($check_token)) {
            return $this->sendError($request->path(), 'Login Token Expire');
        }
        
        $userdetails = User::find($input['user_id']);
        
        if ($request->image) {
            $filename = substr(md5($userdetails->id . '-' . time()), 0, 15) . '.' . $request->file('image')->getClientOriginalExtension();
            $path = public_path('users-photos/' . $filename);
            Image::make($request->image)->save($path);
            $userdetails->image = '/users-photos/' . $filename;
        }
        
        $userdetails->name = $input['fullname'];
        $userdetails->phone = $input['phone'];
        $userdetails->dob = $input['dob'];
        $userdetails->gender = $input['gender'];
        $checkemail = User::whereNotIn('id', array($input['user_id']))->where('email', $input['email'])->first();
        
        if (!empty($checkemail)) {
            return $this->sendError($request->path(), 'Email id Already Exist');
        }
        
        $userdetails->email = $input['email'];
        
        if (!empty($userdetails->image)) {
            $datanameimage = url('/public/') . $userdetails->image;
        } else {
            $datanameimage = url('/public/') . "/img/imagenotfound.jpg";
        }
        $userdetails->save();
        $userdetails1 = User::find($input['user_id']);
        
        if (!empty($userdetails1->image)) {
            $userdetails1->image = url('/public/') . $userdetails1->image;
        } else {
            $userdetails1->image = url('/public/') . "/img/imagenotfound.jpg";
        }
        
        $return_array = array("user_id" => (string)$userdetails1->id, "image" => $userdetails1->image, "fullname" => $userdetails1->fullname, "address" => $userdetails1->address, "email" => $userdetails1->email, "phone" => $userdetails1->phone);

        // $otp = $userdetails1->identicard;
        //   $email = $userdetails1->email;
        //            $subject = "Asec Identity Card";
        //         $postData ="";
        //          try{
        //              Mail::send('emails.otpverify3', ['otp' =>$otp], function($message) use ($postData,$email)
        //                         {
        //                           $message->from('asecclass@asecenglish.awsapps.com', 'TMASEC');
        //                           $message->to($email, 'TMASEC')->subject('TMASEC Identity Card');
        //                         });
        //          }
        //           catch(Exception $e){
        //                         }
        return $this->sendResponse($return_array, 'User profile updated successfully', $request->path());
    }


    public function content(Request $request) {
        $input = $request->all();
        $details_of_content = Content::where(['type' => $input['type']])->first();
        if ($details_of_content) {
            $details_of_content->type = $input['type'];
            $details_of_content->value = $input['value'];
            $details_of_content->save();
        } else {
            Content::create($input);
        }
        return $this->sendResponse1($input['value'], 'Successfully updated data', $request->path());
    }
    public function getcontent(Request $request, $type) {
        $input = $request->all();
        $details_of_content = Content::where(['type' => $type])->first();
        return $this->sendResponse1($details_of_content->value, 'Successfully updated data', $request->path());
    }

    public function addsize(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['catid' => 'required', 'subcatid' => 'required', 'size' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $input = $request->all();
        $verifySize = Size::where(["subcategory_id"=>$input['subcatid'], "size"=>$input['size']])->first();

        if($verifySize) {
            return $this->sendError($request->path(), 'Size Already Added');
        } else {
            $arrayData = [
                "category_id"=>$input['catid'],
                "subcategory_id"=>$input['subcatid'],
                "size"=> strtoupper($input['size']),
            ];

            $obj = Size::create($arrayData);
            return $this->sendResponse($obj, "Size added successfully", $request->path());
        }
    }

    public function getsize(Request $request) {
        $input = $request->all();
        $sizeData = array();
        $validator = Validator::make($input, ['catid' => 'required', 'subcatid' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $input = $request->all();

        $objs = Size::select('size')->where(['category_id'=>$input['catid'],'subcategory_id'=>$input['subcatid']])->get();
        if($objs) {
            foreach ($objs as $value) {
                $sizeData[] = ["value"=>$value->size, "label"=>$value->size];
            }
        }

        return $this->sendResponse($sizeData, "Size listing", $request->path());
    }

    public function sizelist(Request $request) {
        $sizeData = array();

        $objs = Size::select('id','category_id','subcategory_id', 'size')->orderBy('id','DESC')->get();
        if($objs) {
            foreach ($objs as $value) {
                $subcat = Subcategory::select('name')->where('id',$value->subcategory_id)->first();

                if($subcat) {
                //    $subsubcat = Subsubcategory::select('name')->where('id',$value->subsubcategory_id)->first();

                //    if($subsubcat) {
                        $sizeData[] = [$value->category_id, $subcat->name, $value->size, $value->id];
                //    }
                }
            }
        }

        return $this->sendResponse($sizeData, "Size listing", $request->path());
    }

    public function sizedelete(Request $request, $id) {
        $getsize = Size::where(["id"=>$id])->first();

        if($getsize) {
            $verifySize = Product::where("size", 'like', '%'.$getsize->size.'%')->first();

            if($verifySize) {
                return $this->sendError($request->path(), 'Size not able to delete because related product available');
            } else {
                Size::where(["id"=>$id])->delete();
                $objs = "delete";
                return $this->sendResponse($objs, "Size deleted", $request->path());
            }
        } else {
            return $this->sendError($request->path(), 'Size not found');
        }
    }

    public function addcolor(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['color' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $input = $request->all();
        $hex = $input['hexcode'];
        $verifyColor = Color::where(["name"=>$input['color'], "hexcode"=>$hex])->first();

        if($verifyColor) {
            return $this->sendError($request->path(), 'Color Already Added');
        } else {
            $arrayData = [
                "name"=>$input['color'],
                "hexcode"=>$input['hexcode']
            ];

            $obj = Color::create($arrayData);
            return $this->sendResponse($obj, "Color added successfully", $request->path());
        }
    }

    public function getcolor(Request $request) {
        $objs = Color::select("name", "hexcode")->get();
        $colorData = array();

        foreach ($objs as $value) {
            $colorData[] = ["value"=>$value->hexcode, "label"=>"$value->name"];
        }
        return $this->sendResponse($colorData, "Color listing", $request->path());
    }

    public function colorlist(Request $request) {
        $objs = Color::select("id", "name", "hexcode")->get();
        $colorData = array();

        foreach ($objs as $value) {
            $colorData[] = [$value->name, $value->hexcode, $value->id];
        }
        return $this->sendResponse($colorData, "Color listing", $request->path());
    }

    public function colordelete(Request $request, $id) {
        $getcolor = Color::where(["id"=>$id])->first();

        if($getcolor) {
            $verifyColor = Product::where("color", 'like', '%'.$getcolor->hexcode.'%')->first();

            if($verifyColor) {
                return $this->sendError($request->path(), 'Color not able to delete because related product available');
            } else {
                Color::where(["id"=>$id])->delete();
                $objs = "delete";
                return $this->sendResponse($objs, "Color deleted", $request->path());
            }
        } else {
            return $this->sendError($request->path(), 'Color not found');
        }
    }

    public function addsubsubcat(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['name' => 'required','subcategory_id' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $verifySubCat = Subsubcategory::where(["name"=>$input['name'], "subcategory_id"=>$input['subcategory_id']])->first();

        if($verifySubCat) {
            return $this->sendError($request->path(), 'Sub-subcategory Already Added');
        } else {
            $arrayData = [
                "name"=>$input['name'],
                "subcategory_id"=>$input['subcategory_id'],
                "category_id"=>$input['category_id'],
                "occasion_status"=>$input['occasion_status'],
                "image"=>$input['image'],
            ];
        }

        $obj = Subsubcategory::create($arrayData);
        return $this->sendResponse($obj, "Sub sub-category successfully", $request->path());
    }

    public function subsubcategoryupdate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['name' => 'required','id' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $getSubSub = Subsubcategory::where(["id"=>$input['id']])->first();
        if($getSubSub) {
            
                $getSubSub->name = $input['name'];
                //$getSubSub->subcategory_id = $input['subcategory_id'];
                //$getSubSub->category_id = $input['category_id'];
                $getSubSub->occasion_status = $input['occasion_status'];
                if(strlen($input['image']) > 1) {
                    $getSubSub->image = $input['image'];
                }
                $getSubSub->save();

                return $this->sendResponse($getSubSub, "Sub sub-category updated successfully", $request->path());
        }
    }

    public function subsubcatlist(Request $request) {
        $objs = Subsubcategory::orderBy('id','DESC')->get();
        $catData = array();

        foreach ($objs as $value) {
            if(strlen($value->subcategory_id) > 0) {
                $subName = Subcategory::select("name")->where(["id"=>$value->subcategory_id])->first();
                
                if($subName) {
                    $catData[] = [
                                $value->category_id,
                                $subName->name, 
                                $value->name,
                                $value->occasion_status,
                                [$value->status, $value->id],
                                $value->id
                            ];
                }
            }
        }
        return $this->sendResponse($catData, "Subsubcategory listing", $request->path());
    }

    public function subsubcatsingle(Request $request, $id) {
        $objs = Subsubcategory::where('id',$id)->first();
        $catData = array();

        if(strlen($objs->subcategory_id) > 0) {
            $subName = Subcategory::select("name")->where(["id"=>$objs->subcategory_id])->first();
            
            if($subName) {
                $catData = [$subName->name, 
                              $objs->name, 
                              $objs->id
                            ];
            }
        }

        return $this->sendResponse($catData, "Subsubcategory data", $request->path());
    }


    public function addoffers(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['offer_type'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $startAt = date("d-m-Y H:i:s", strtotime($input['startdate']));
        $endAt = date("d-m-Y H:i:s", strtotime($input['enddate']));

        if($input['offer_type'] == 1) {
            $arrayData = [
                "offer_type"=>$input['offer_type'],
                "description"=>$input['description'],
                "startdate"=>$startAt,
                "enddate"=>$endAt,
            ];
        } else if($input['offer_type'] == 2) {

            $verifyOffer = Offer::where(["catid"=>$input['catid']])->first();

            if($verifyOffer) {
                return $this->sendError($request->path(), 'Already added this offer in this Sub-subcategory');                
            } else {
                $arrayData = [
                    "offer_type"=>$input['offer_type'],
                    "description"=>$input['description'],
                    "coupon_id"=>$input['coupon_id'],
                    "catid"=>$input['catid'],
                    "startdate"=>$startAt,
                    "enddate"=>$endAt,
                ];
            }
        } else {

            $verifyOffer = Offer::where(["catid"=>$input['catid']])->first();

            if($verifyOffer) {
                return $this->sendError($request->path(), 'Already added this offer in this Sub-subcategory');                
            } else {
                $arrayData = [
                    "offer_type"=>$input['offer_type'],
                    "description"=>$input['description'],
                    "coupon_id"=>$input['coupon_id'],
                    "catid"=>$input['catid'],
                    "startdate"=>$startAt,
                    "enddate"=>$endAt,
                ];
            }
        }

        $obj = Offer::create($arrayData);
        return $this->sendResponse($obj, "Offer added successfully", $request->path());
    }

    public function offerlists(Request $request) {
        $arrayData = array();
        $objs = Offer::orderBy('id','DESC')->get();
        
        foreach($objs as $obj) {

            if($obj->offer_type == 1) {
                $objType = "Bank offers";
            } else if($obj->offer_type == 2) {
                $objType = "Discounted offer";
            } else {
                $objType = "Announcment offer";
            }

            $getcat = Subsubcategory::where(["id"=>$obj->catid])->first();
            if($getcat) {
                $getcatname = $getcat->name;
            } else {
                $getcatname = "-";
            }

            $getcoupon = Cuponcode::where(["id"=>$obj->coupon_id])->first();
            if($getcoupon) {
                $getcouponname = $getcoupon->name;
            } else {
                $getcouponname = "";
            }

            $arrayData[] = [$objType, $obj->description, $getcatname, $getcouponname, $obj->id];
        }
        
        return $this->sendResponse($arrayData, "Offer added successfully", $request->path());
    }

    public function offerdelete(Request $request, $id) {
        $arrayData = array();
        $objs = Offer::where(["id"=>$id])->delete();
        $objs = Promotion::where(["offer_id"=>$id])->delete();
        return $this->sendResponse(["status" => "success"], "Offer deleted successfully", $request->path());
    }

    public function getrefer(Request $request) {
        $referArr = array();
        $getRefer = ReferEarn::where(["id"=>1])->first();
        $referArr = [$getRefer->price];

        return $this->sendResponse($referArr, "Refer and Earn successfully", $request->path());
    }

    public function updaterefer(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['price'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $getRefer = ReferEarn::where(["id"=>1])->first();
        $getRefer->price = $request->price;
        $getRefer->save();

        return $this->sendResponse($getRefer, "Refer and Earn updated successfully", $request->path());
    }

    public function sellersupportticketlist(Request $request, $id) {
        $supportArr = array();
        $getSupports = Support::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        foreach($getSupports as $getSupport) {

            $supportArr[] = [$getSupport->ticket_id, $getSupport->title, $getSupport->content,$getSupport->status, $getSupport->id];
        }

        return $this->sendResponse($supportArr, "Support Listing Retrive successfully", $request->path());
    }

    public function adminsupportticketlist(Request $request) {
        $supportArr = array();
        $getSupports = Support::orderBy("id","DESC")->get();
        foreach($getSupports as $getSupport) {
            $seller = Seller::where(["id"=>$getSupport->seller_id])->first();
            $supportArr[] = [$seller->name,$seller->email,$getSupport->ticket_id, $getSupport->title, $getSupport->content, $getSupport->status, $getSupport->id];
        }

        return $this->sendResponse($supportArr, "Support Listing Retrive successfully", $request->path());
    }

    public function supportticketadd(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, ['title'=>'required','content'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $ticketrandom = 'ticket-'.$this->random_strings(4);

        $arrayData = [
            "seller_id"=>$id,
            "ticket_id"=>$ticketrandom,
            "title"=>$input['title'],
            "content"=>$input['content'],
            "status"=>"Pending satvick action"
        ];

        $obj = Support::create($arrayData);
        return $this->sendResponse($obj, "Ticket added with ticket number:".$ticketrandom." successfully", $request->path());
    }

    public function supportticketupdate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['id'=>'required','status'=>'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $support = Support::where('id',$request->id)->first();
        $support->status = $request->status;
        $support->save();

        return $this->sendResponse($obj, "Ticket updated successfully", $request->path());
    }

    //Payment response

    public function response(Request $request) {
        $result = array();
        $response = Indipay::response($request);

        $result = [
            "order_id" => $response['order_id'],
            "tracking_id" => $response['tracking_id'],
            "order_status" => $response['order_status'],
            "status_message" => $response['status_message'],
        ];
        return $this->sendResponse($result, "Payment Response", $request->path());
    }

    public function dashoardforadmin(Request $request) {
        $result = array();

        $orderids = array();
        $getOrderids = Order::select('id')->get();
        foreach($getOrderids as $getOrderid) {
            $orderids[] = $getOrderid->order_id;
        }

        $orderid1s = array();
        $getOrderid1s = Order::select('id')->where(["status"=>"delivered"])->get();
        foreach($getOrderid1s as $getOrderid1) {
            $orderid1s[] = $getOrderid1->order_id;
        }

        if(isset($request->dateframe)) {
            $dateFrame = $request->dateframe;

            if($dateFrame == 6) {
                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(6))->whereIn('id', $orderids)->count();

                $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(6))->sum('final_price');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(6))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(6))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(6))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(6))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 12) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(12))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(12))->whereIn('id', $orderid1s)->sum('final_price');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(12))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(12))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(12))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(12))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 18) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(18))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderid1s)->sum('final_price');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(18))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(18))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(18))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 24) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(24))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderid1s)->sum('final_price');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(24))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(24))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(24))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 30) {
                $totalOrder = Order::where('created_at', '>=', Carbon::now()->subMonth(30))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderid1s)->sum('final_price');
                $livePro = Product::where(["status"=>1])->where("created_at",">", Carbon::now()->subMonths(30))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", Carbon::now()->subMonths(30))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(30))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }
            }
        } else {
            $totalOrder = Order::where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderids)->count();
            $revenue = OrderDetail::select("final_price")->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderid1s)->sum('final_price');
            $livePro = Product::where(["status"=>1])->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->count();
            $nonlivePro = Product::whereNotIn("status", [1])->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->count();

            $stateRecords = array();
            $getStates = Order::select('state')->distinct('state')->where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderids)->get();
            $getOrderCount = Order::where("created_at",">", $request->startdate)->where("created_at","<", $request->enddate)->whereIn('id', $orderids)->count();

            foreach($getStates as $getState) {
                $stateCount = Order::where(["state"=>$getState->state])->count();
                $orderper = round(($stateCount/$getOrderCount) * 100);
                $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
            }            
        }

        $orderRecords = OrderDetail::select(
            DB::raw('sum(final_price) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as months"))->whereYear('created_at', date('Y'))->whereIn('order_id', $orderids)->groupBy('months')->get();
        if(count($orderRecords) > 0) {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                if(isset($orderRecords[$i])) {
                    $graphArr[] = ["g$c" => (int)$orderRecords[$i]['sums']];
                } else {
                    $graphArr[] = ["g$c" => 0];
                }
            }
        } else {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                $graphArr[] = ["g$c" => 0];
            }
        }

        $result = [
            "orders" => $totalOrder,
            "revenue" => $revenue,
            "live_product" => $livePro,
            "nonlive_product" => $nonlivePro,
            "state_records"=>$stateRecords,
            "graph"=>$graphArr
        ];
        return $this->sendResponse($result, "Dashboard Response", $request->path());
    }

    public function dashoardforseller(Request $request, $id) {
        $result = array();

        $orderids = array();
        $getOrderids = Order::select('id')->where(["seller_id"=>$id])->get();
        foreach($getOrderids as $getOrderid) {
            $orderids[] = $getOrderid->order_id;
        }

        $orderid1s = array();
        $getOrderid1s = Order::select('id')->where(["seller_id"=>$id, "status"=>"delivered"])->get();
        foreach($getOrderid1s as $getOrderid1) {
            $orderid1s[] = $getOrderid1->order_id;
        }

        if(isset($request->dateframe)) {
            $dateFrame = $request->dateframe;

            if($dateFrame == 6) {

                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(6))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(6))->sum('final_price');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(6))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(6))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(6))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(6))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 12) {

                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(12))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(12))->sum('final_price');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(12))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(12))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(12))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(12))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 18) {

                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(18))->sum('final_price');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(18))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(18))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(18))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(18))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 24) {

                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(24))->sum('final_price');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(24))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(24))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(24))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(24))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }

            } else if($dateFrame == 30) {

                $totalOrder = Order::where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderids)->count();
                $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", Carbon::now()->subMonths(30))->sum('final_price');
                $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(30))->count();
                $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", Carbon::now()->subMonths(30))->count();

                $stateRecords = array();
                $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", Carbon::now()->subMonths(30))->get();
                $getOrderCount = Order::where("created_at",">", Carbon::now()->subMonths(30))->whereIn('id', $orderids)->count();

                foreach($getStates as $getState) {
                    $stateCount = Order::where(["state"=>$getState->state])->count();
                    $orderper = round(($stateCount/$getOrderCount) * 100);
                    $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
                }
            }

        } else {

            $startd = date('d-m-Y H:i:s', strtotime($request->startdate));
            $endd = date('d-m-Y H:i:s', strtotime($request->enddate));

            $totalOrder = Order::where("created_at",">", $startd)->where("created_at","<", $endd)->whereIn('id', $orderids)->count();
            $revenue = OrderDetail::select("final_price")->whereIn('order_id', $orderid1s)->where("created_at",">", $startd)->where("created_at","<", $endd)->sum('final_price');
            $livePro = Product::where(["status"=>1])->where(["user_id"=>$id])->where("created_at",">", $startd)->where("created_at","<", $endd)->count();
            $nonlivePro = Product::whereNotIn("status", [1])->where(["user_id"=>$id])->where("created_at",">", $startd)->where("created_at","<", $endd)->count();

            $stateRecords = array();
            $getStates = Order::select('state')->distinct('state')->whereIn('id', $orderids)->where("created_at",">", $startd)->where("created_at","<", $endd)->get();
            $getOrderCount = Order::where("created_at",">", $startd)->where("created_at","<", $endd)->whereIn('id', $orderids)->count();

            foreach($getStates as $getState) {
                $stateCount = Order::where(["state"=>$getState->state])->count();
                $orderper = round(($stateCount/$getOrderCount) * 100);
                $stateRecords[] = ["name"=>$getState->state, "percent"=>$orderper];
            }
        }

        $orderRecords = OrderDetail::select(
            DB::raw('sum(final_price) as sums'), 
            DB::raw("DATE_FORMAT(created_at,'%m') as months"))->whereYear('created_at', date('Y'))->whereIn('order_id', $orderids)->groupBy('months')->get();
        if(count($orderRecords) > 0) {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                if(isset($orderRecords[$i])) {
                    $graphArr[] = ["g$c" => (int)$orderRecords[$i]['sums']];
                } else {
                    $graphArr[] = ["g$c" => 0];
                }
            }
        } else {
            for($i=0;$i<12;$i++) {
                $c = $i+1;
                $graphArr[] = ["g$c" => 0];
            }
        }


        $offerCurrentDate = date('d-m-Y H:i:s');
        $strCurrentDate = strtotime($offerCurrentDate);
        $offerData = array();
        $objs = Offer::all();
        
        foreach($objs as $obj) {

            $startdate = date('d-m-Y H:i:s', strtotime($obj->startdate));
            $strStartdate = strtotime($startdate);
            $enddate = date('d-m-Y H:i:s', strtotime($obj->enddate));
            $strEnddate = strtotime($enddate);

            if($strCurrentDate > $strStartdate && $strCurrentDate < $strEnddate) {

                $offerData[] = ["id"=>$obj->id, "desc"=>$obj->description];
            }
        }

        $result = [
            "orders" => $totalOrder,
            "revenue" => $revenue,
            "live_product" => $livePro,
            "nonlive_product" => $nonlivePro,
            "state_records"=>$stateRecords,
            "graph"=>$graphArr,
            "offerData"=>$offerData
        ];
        return $this->sendResponse($result, "Dashboard Response", $request->path());
    }

    public function cartoffer(Request $request) {
        $offerArr = array();
        $catArrs = $request->catlist;

        foreach($catArrs as $catArr) {
            $objs = Offer::where(["catid"=>$catArr, "offer_type"=>2])->first();

            if($objs) {
                $offerArr[] =  [
                    "id"=>$objs->id,
                    "buy"=>$objs->buy,
                    "get"=>$objs->andget,
                    "catid"=>$objs->catid,
                ];
            }
        }
        return $this->sendResponse($offerArr, "Offer added successfully", $request->path());
    }

    public function updatestock(Request $request) {
        
        if($request->productprice_id == 0) {
            $check_pro = Product::where(['id' => $request->id])->first();
            if (empty($check_pro)) {
                    return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('products')->where(['id'=>$request->id])->update(['quantity' => $request->stock]);
        } else {
            $check_pro = ProductPrice::where(['id' => $request->productprice_id])->first();
            if (empty($check_pro)) {
                    return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('product_prices')->where(['id'=>$request->productprice_id])->update(['quantity' => $request->stock]);
        }
        return $this->sendResponse(['status' => "success"], 'Stock updated successfully', $request->path());
    }

    public function updateprice(Request $request) {

        if($request->productprice_id == 0) {
            $check_pro = Product::where(['id' => $request->id])->first();
            if (empty($check_pro)) {
                return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('products')->where(['id'=>$request->id])->update(['sp'=>$request->sp, "commision"=>$request->commission, "tax"=>$request->gst]);
        } else {

            $check_pro = ProductPrice::where(['id' => $request->productprice_id])->first();
            if (empty($check_pro)) {
                return $this->sendError($request->path(), 'Product not Found');
            }
            DB::table('product_prices')->where(['id'=>$request->productprice_id])->update(['sp'=>$request->sp, "commision"=>$request->commission, "tax"=>$request->gst]);

        }
        return $this->sendResponse(['status' => "success"], 'Selling price updated successfully', $request->path());
    }

    public function updatecommission(Request $request) {
        $check_pro = Seller::where(['id' => $request->id])->first();
        if (empty($check_pro)) {
            return $this->sendError($request->path(), 'Seller not Found');
        }
        DB::table('sellers')->where(['id'=>$request->id])->update(['commission' => $request->commission]);
        return $this->sendResponse(['status' => "success"], 'Commission updated successfully', $request->path());
    }

    public function payreport(Request $request, $id) {
        $input = $request->all();
        $orderArr = array();
        $orderDetailArr = array();
        $query = Order::query();

        if(isset($request->pay_type)) {
            if($request->pay_type == "COD") {       
                $query->where(["payment_type"=>"Cash On Delivery"]);
            } else {
                $query->where(["payment_type"=>"Online"]);
            }
        }
        if($request->has('pay_from_date')) {

            $start = Carbon::parse($request->pay_from_date);
            $end = Carbon::parse($request->pay_to_date);

            $query->whereDate('orders.created_at','<=',$end->format('Y-m-d'))->whereDate('orders.created_at','>=',$start->format('Y-m-d'));
        }

        $getOrders = $query->where(["seller_id"=>$id])->orderBy("id","DESC")->get();
        $y=1;
        $total_payoutAmt = 0;
        $total_priceAmt = 0;

        foreach($getOrders as $getOrder) {

            $oderDetail = OrderDetail::where(["order_id"=>$getOrder->id])->first();

            $orderDetailArr[] = [
                            "order_number"=>$getOrder->order_number,
                            "order_date"=>date("d-m-Y H:i:s",strtotime($getOrder->created_at)),
                            "invoice_amount"=>$oderDetail->final_price,
                            "discount"=>$oderDetail->discount_price + $oderDetail->refer_discount,
                            "commission"=>$oderDetail->admin_commision,
                            "shipping_cost"=>$oderDetail->seller_shipping,
                            "reverse_shipping_cost"=>0,
                            "gst"=>$oderDetail->gst_tax,
                            "tcs"=>$oderDetail->tcs_tax,
                            "protection_fund"=>$oderDetail->final_price,
                            "net_earning"=>$oderDetail->seller_amount,
                            "payout_type"=>"",

                        ];
            $y++;
        }

        $payoutRecords = PayoutRecord::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        if($payoutRecords) {

            foreach($payoutRecords as $payoutRecord) {
                $orderArr[] = ["seller_id"=>$payoutRecord->seller_id, "date"=>$payoutRecord->date, "orders_count"=>$payoutRecord->orders_count, "payout"=>$payoutRecord->payout, "transcation_id"=>$payoutRecord->transcation_id, "commission"=>$payoutRecord->commission, "adjustments"=>$payoutRecord->adjustments, "gst"=>$payoutRecord->gst, "tcs"=>$payoutRecord->tcs];
            }
        } else {

            $orderArr = [];    
        }
        

        return $this->sendResponse(["orderDetail"=>$orderDetailArr, "orders"=>$orderArr], "Payout report", $request->path());
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }


   function addsizechart(Request $request) {
 
     $input = $request->all();
     $validator = Validator::make($input, ['heading' => 'required','heading_values'=>'required','user_id'=>'required','subsub_cat_id'=>'required','type'=>'required']);
    
    if ($validator->fails()) {
         return $this->sendError($request->path(), $validator->errors()->first());
    }
    
    $headingArr = explode(',',$request->heading);
    if($request->type=='add') { 
     $checkSubcat =   SizeChart::where('subsub_cat_id',$request->subsub_cat_id)->first(); 
     if(!$checkSubcat) {
    foreach ($request->heading_values as $val) {
        $finalVal = implode(',',$val);
         
          $size = SizeChart::create([
                         'heading'=> $request->heading,
                         'heading_values' => $finalVal,
                         'user_id'=> $request->user_id,
                         'subsub_cat_id'=>$request->subsub_cat_id,
                         'status'=>1,
                ]);
    }
   
    return $this->sendResponse(['status' => "success"], 'Size chart added successfully', $request->path());
  } else {
    return $this->sendError($request->path(), "Size chart already added this sub sub category"); 
  }
   
   } else {
       SizeChart::where('subsub_cat_id',$request->subsub_cat_id)->delete();
       foreach ($request->heading_values as $val) {
        $finalVal = implode(',',$val);
         
          $size = SizeChart::create([
                         'heading'=> $request->heading,
                         'heading_values' => $finalVal,
                         'user_id'=> $request->user_id,
                         'subsub_cat_id'=>$request->subsub_cat_id,
                         'status'=>1,
                ]);

      }
      return $this->sendResponse(['status' => "success"], 'Size chart updated successfully', $request->path());

   }
   } 


  public function updateSizeChart(Request $request) {
    
    $subsubcatid  = $request->subsub_cat_id;
  
     $input = $request->all();
     $validator = Validator::make($input, ['subsub_cat_id'=>'required']);
    
    if ($validator->fails()) {
         return $this->sendError($request->path(), $validator->errors()->first());
    }  
    $subsubcatid  = $request->subsub_cat_id;
    $getdata = SizeChart::where('subsub_cat_id',$subsubcatid)->get();
    //echo "<pre>";
    //print_r($getdata->toArray());die;
    $j=0;
    $obj=[];

    foreach($getdata as $res) {
        $headingArr = explode(',',$res->heading); 
        $head=[];
        foreach($headingArr as $k=>$v) {
              $head[] = $v;
        }

        $valArr = explode(',',$res->heading_values); 

        for($i=0; $i<count($valArr); $i++) {
            $obj[$j][$i] = $valArr[$i];
        }

        $j++;
    }


    $final = array(
        'head'=>$head,
        'val'=> $obj,
    );
   
   return $this->sendResponse(["sizeChart"=>$final], "size chart details", $request->path());    

  } 


  public function showSizeChart(Request $request) {
     
        $subsubcatid  = $request->subsub_cat_id;
        $input = $request->all();
        $subsubcatid  = $request->subsub_cat_id;
   
        $getdata =   DB::table('size_charts as chart')
                ->select('sub.name','sub.id','chart.heading','chart.heading_values')
                ->join('subsubcategories as sub','sub.id','=','chart.subsub_cat_id')
                 ->groupBy('subsub_cat_id')
                ->get();


              foreach($getdata as $value) {
                 $head = explode(',',$value->heading) ; 
                    $res[] = [
                                //$value->category_id,
                                $value->name, 
                               // $value->name,
                                $head,
                               
                                $value->id
                            ];
                
              }  
        return $this->sendResponse(["sizeChart"=>$res], "size chart details", $request->path());   
  }

  public function deleteChart($id){
    $delete = SizeChart::where('subsub_cat_id',$id)->delete();
    return $this->sendResponse(['status' => "success"], 'Size chart deleted successfully', $request->path());
  }

  public function performanceseller(Request $request, $id) {

        $getPerformance = array();
        $performanceData = SellerPerformance::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        if($performanceData) {

            foreach($performanceData as $data) {
                $getPerformance[] = ["seller_id"=>$data->seller_id, "from_date"=>date('d-m-Y',strtotime($data->from_date)), "to_date"=>date('d-m-Y',strtotime($data->to_date)), "total_order"=>$data->total_order, "metric"=>$data->metric, "target"=>$data->target, "rating"=>$data->rating];
            }

        } else {
            $getPerformance = [];
        }


        $performanceDataGraph = SellerPerformance::where(["seller_id"=>$id])->whereMonth('created_at', Carbon::now()->month)->get();
        if($performanceDataGraph) {
            $returnGraph = array();
            $breachGraph = array();
            $reattemptGraph = array();

            foreach($performanceDataGraph as $performanceGraph) {

                $getReturnPercent = ($performanceGraph->total_return / $performanceGraph->total_order) * 100;
                $rating1 = 5;
                if($getReturnPercent == 6) {
                    $rating1 = $rating1 - 1;
                
                } else if($getReturnPercent == 7) {
                    $rating1 = $rating1 - 2;
                
                } else if($getReturnPercent == 8) {
                    $rating1 = $rating1 - 3;
                
                } else if($getReturnPercent == 9) {
                    $rating1 = $rating1 - 4;
                
                }  else if($getReturnPercent > 9) {
                    $rating1 = $rating1 - 5;
                }
            
                $getBreachesPercent = ($performanceGraph->total_breach / $performanceGraph->total_order) * 100;
                $rating2 = 5;
                if($getBreachesPercent == 6) {
                    $rating2 = $rating2 - 1;
                
                } else if($getBreachesPercent == 7) {
                    $rating2 = $rating2 - 2;
                
                } else if($getBreachesPercent == 8) {
                    $rating2 = $rating2 - 3;
                
                } else if($getBreachesPercent == 9) {
                    $rating2 = $rating2 - 4;
                
                }  else if($getBreachesPercent > 9) {
                    $rating2 = $rating2 - 5;
                }

                $getReattemptPercent = ($performanceGraph->total_reattempt / $performanceGraph->total_order) * 100;
                $rating3 = 5;
                if($getReattemptPercent == 6) {
                    $rating3 = $rating3 - 1;
                
                } else if($getReattemptPercent == 7) {
                    $rating3 = $rating3 - 2;
                
                } else if($getReattemptPercent == 8) {
                    $rating3 = $rating3 - 3;
                
                } else if($getReattemptPercent == 9) {
                    $rating3 = $rating3 - 4;
                
                }  else if($getReattemptPercent > 9) {
                    $rating3 = $rating3 - 5;
                }

                $returnGraph[] = ["total"=>$performanceGraph->total_order, "rating"=>$rating1, "date"=>date('d-m-Y',strtotime($performanceGraph->created_at))];

                $breachGraph[] = ["total"=>$performanceGraph->total_order, "rating"=>$rating2, "date"=>date('d-m-Y',strtotime($performanceGraph->created_at))];

                $reattemptGraph[] = ["total"=>$performanceGraph->total_order, "rating"=>$rating3, "date"=>date('d-m-Y',strtotime($performanceGraph->created_at))];

            }
        }

        return $this->sendResponse(['performance' => $getPerformance, "graphone"=>$returnGraph, "graphtwo"=>$breachGraph,"graphthree"=>$reattemptGraph], 'Performance Listing', $request->path());
  }

  public function pushNotification($notifyData, $userDevice, $badge) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAvtlLWfU:APA91bHkKDPg59guB8VXYN8zeJc5w8vfrYuCRYL6J4Hlmdz2SSo15JotADzXH-RBF9fdGxPgci7W_422oGlJz6I_KW1mZ99dy7fqJHxtiDy3_FT199aH8VkUgZmA5Zf8NaGeL6rnnKW8";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }

}
