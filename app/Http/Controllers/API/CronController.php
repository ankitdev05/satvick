<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Subsubcategory;
use App\Cuponcode;
use App\Content;
use App\Faq;
use App\Priority;
use App\Notification;
use App\Size;
use App\Color;
use App\Offer;
use App\Announcement;
use App\Refer;
use App\Promotion;
use App\Attribute;
use App\ReferEarn;
use App\Support;
use App\SizeChart;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use App\CurrencyRate;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;

class CronController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "https://mobuloustech.com/yodapi/public/";
        $this->baseurl = "https://mobuloustech.com/yodapi/public";
    }

    public function checkthestatusoforder() {
        $msg = '';
        $datas = Order::whereIn('status', ['packed','manifested',"not picked",'dispatched','transit','pending'])->get();
        foreach($datas as $data) {

            $order = OrderDetail::where(['order_id'=>$data->id])->first();

            $https = new Client;
            $generateWBN = $https->get(url('https://staging-express.delhivery.com/api/v1/packages/json/?waybill='.$order->wbn_num.'&verbose=2&token=dcafa28eb05642c39f31b14724e28cf45f91256a'));
            $data_wbn = json_decode($generateWBN->getBody());

            $getStatus = $data_wbn['ShipmentData'][0]->Shipment->Status->Status;

            if($getStatus == "Manifested") {
                $details = Order::where(['id'=>$data->id])->first();
                $details->status = "manifested";
                $details->save();

                $msg = "Your order has been ready for dispatched";
            }

            if($getStatus == "Not Picked") {
                $details = Order::where(['id'=>$data->id])->first();
                $details->status = "not picked";
                $details->save();
            }

            if($getStatus == "Dispatched") {
                $details = Order::where(['id'=>$data->id])->first();
                $details->status = "dispatched";
                $details->save();

                $msg = "Your order has been dispatched";
            }

            if($getStatus == "In Transit") {
                $details = Order::where(['id'=>$data->id])->first();
                $details->status = "transit";
                $details->save();

                $msg = "Your order has been out for delivery";
            }

            if($getStatus == "Delivered") {
                $details = Order::where(['id'=>$data->id])->first();
                $details->status = "delivered";
                $details->save();

                $msg = "Your order has been delivered";
            }

            if($getStatus == "Pending") {
                $details = Order::where(['id'=>$data->id])->first();
                $details->status = "pending";
                $details->save();

                $msg = "Your order is pending";
            }

            if(strlen($msg) > 0) {
                $notifyData = [
                    'user_id'=>$data->user_id,
                    'order_id'=>$data->id,
                    'order_number'=>$data->order_number,
                    'type'=>'Order',
                    'title'=>'Order - '.$data->order_number,
                    'content'=>$msg
                ];

                Notification::create($notifyData);
                $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>$data->user_id])->first();
                $badge = Notification::where(["user_id"=>$data->user_id,'status'=>0])->count();
                $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
            }
        }
    }


    public function checkthestatusofmanifestorder() {
        $msg = '';
        $datas = Order::whereIn('status', ['dispatched'])->get();
        foreach($datas as $data) {

            $order = Manifest::where('order_id', 'like', '%' .$data->id. '%')->first();

            if($order) {
                $order->delete();
            }
        }
    }

    public function performance(Request $request) {
        
        $sellerDatas = Seller::select("id")->where(["status"=>1])->get();

        if($sellerDatas) {
            
            foreach($sellerDatas as $sellerData) {

                $percentData = array();

                $currentDate = date('d-m-Y');
                $lessDAY = "-7 days";
                $getBeforeDay = date('d-m-Y', strtotime($currentDate . $lessDAY));

                $start = Carbon::parse($currentDate);
                $end = Carbon::parse($getBeforeDay);

                $orderDatas = Order::whereDate('orders.created_at','>=',$end->format('Y-m-d'))->whereDate('orders.created_at','<=',$start->format('Y-m-d'))->where(["seller_id"=>$sellerData->id])->orderBy('id','DESC')->get();

                if($orderDatas) {

                    $returnCount = 0;
                    $breachesCount = 0;
                    $reattemptCount = 0;
                    $totalOrder = 0;

                    foreach ($orderDatas as $data) {
                        
                        $totalOrder++;

                        $mistakes = SellerMistake::where(["order_id"=>$data->id])->first();
                        if($mistakes) {

                            if($mistakes->type == "return") {
                                $returnCount++;
                            } else if($mistakes->type == "breache") {
                                $breachesCount++;
                            } else if($mistakes->type == "reattempt") {
                                $reattemptCount++;
                            }
                        }
                    }

                    if($returnCount > 0) {
                        $getReturnPercent = ($totalOrder / $returnCount) * 100;
                    } else {
                        $getReturnPercent = 0;
                    }
                    if($breachesCount > 0) {
                        $getBreachesPercent = ($totalOrder / $breachesCount) * 100;
                    } else {
                        $getBreachesPercent = 0;
                    }
                    if($reattemptCount > 0) {
                        $getReattemptPercent = ($totalOrder / $reattemptCount) * 100;
                    } else {
                        $getReattemptPercent = 0;
                    }

                    $totalPercent = $getReturnPercent + $getBreachesPercent + $getReattemptPercent;

                    $rating = 5;
                    if($totalPercent == 6) {
                        $rating = $rating - 1;
                    
                    } else if($totalPercent == 7) {
                        $rating = $rating - 2;
                    
                    } else if($totalPercent == 8) {
                        $rating = $rating - 3;
                    
                    } else if($totalPercent == 9) {
                        $rating = $rating - 4;
                    
                    }  else if($totalPercent > 9) {
                        $rating = $rating - 5;
                    }

                    $percentData = ["seller_id"=>$sellerData->id, "from_date"=>$start, "to_date"=>$end, "total_order"=>$totalOrder, "total_return"=>$returnCount, "total_breach"=>$breachesCount, "total_reattempt"=>$reattemptCount, "metric"=>$totalPercent, "target"=>5, "rating"=> $rating];

                    SellerPerformance::create($percentData);
                }
            }
        }

        return $this->sendResponse(["performance"=>"success"], "Performance details", $request->path()); 
    }

    public function payout(Request $request) {

        $sellerDatas = Seller::select("id")->where(["status"=>1])->get();

        if($sellerDatas) {

            foreach($sellerDatas as $sellerData) {

                $currentDay = date('d');
                if($currentDay == 15) {

                    $currentDate = date('d-m-Y');
                    $lessDAY = "-15 days";
                    $getBeforeDay = date('d-m-Y', strtotime($currentDate . $lessDAY));

                    $start = Carbon::parse($currentDate);
                    $end = Carbon::parse($getBeforeDay);

                    $orderDatas = Order::whereDate('orders.created_at','>=',$end->format('Y-m-d'))->whereDate('orders.created_at','<=',$start->format('Y-m-d'))->where(["seller_id"=>$sellerData->id, "status"=>"delivered"])->orderBy('id','DESC')->get();

                    if($orderDatas) {
                        $orderCount = 0;
                        $totalPayout = 0;
                        $totalCommission = 0;
                        $totalGST = 0;
                        $totalTCS = 0;
                        $payoutArray = array();

                        foreach ($orderDatas as $odata) {
                            $orderCount++;

                            $details = OrderDetail::where(["order_id"=>$odata->id])->first();
                            $totalPayout = $totalPayout + $details->final_price;
                            $totalCommission = $totalCommission + $details->admin_commision;
                            $totalGST = $totalGST + $details->gst_tax;
                            $totalTCS = $totalGST + $details->tcs_tax;
                        }

                        if($orderCount > 0) {
                            $payoutArray = ["seller_id"=>$sellerData->id, "date"=>$currentDate, "orders_count"=>$orderCount, "payout"=>$totalPayout, "transcation_id"=>"", "commission"=>$totalCommission, "adjustments"=>0, "gst"=>$totalGST, "tcs"=>$totalTCS];

                            PayoutRecord::create($payoutArray);
                        }
                    }
                }

                if($currentDay == 30) {

                    $currentDate = date('d-m-Y');
                    $lessDAY = "-15 days";
                    $getBeforeDay = date('d-m-Y', strtotime($currentDate . $lessDAY));

                    $start = Carbon::parse($currentDate);
                    $end = Carbon::parse($getBeforeDay);

                    $orderDatas = Order::whereDate('orders.created_at','>=',$end->format('Y-m-d'))->whereDate('orders.created_at','<=',$start->format('Y-m-d'))->where(["seller_id"=>$sellerData->id, "status"=>"delivered"])->orderBy('id','DESC')->get();

                    if($orderDatas) {
                        $orderCount = 0;
                        $totalPayout = 0;
                        $totalCommission = 0;
                        $totalGST = 0;
                        $totalTCS = 0;
                        $payoutArray = array();

                        foreach ($orderDatas as $odata) {
                            $orderCount++;

                            $details = OrderDetail::where(["order_id"=>$odata->id])->first();
                            $totalPayout = $totalPayout + $details->final_price;
                            $totalCommission = $totalCommission + $details->admin_commision;
                            $totalGST = $totalGST + $details->gst_tax;
                            $totalTCS = $totalGST + $details->tcs_tax;
                        }

                        if($orderCount > 0) {
                            $payoutArray = ["seller_id"=>$sellerData->id, "date"=>$currentDate, "orders_count"=>$orderCount, "payout"=>$totalPayout, "transcation_id"=>"", "commission"=>$totalCommission, "adjustments"=>0, "gst"=>$totalGST, "tcs"=>$totalTCS];

                            PayoutRecord::create($payoutArray);
                        }
                    }
                }

                // if($currentDay == 23) {

                //     $currentDate = date('d-m-Y');
                //     $lessDAY = "-15 days";
                //     $getBeforeDay = date('d-m-Y', strtotime($currentDate . $lessDAY));

                //     $start = Carbon::parse($currentDate);
                //     $end = Carbon::parse($getBeforeDay);

                //     $orderDatas = Order::whereDate('orders.created_at','>=',$end->format('Y-m-d'))->whereDate('orders.created_at','<=',$start->format('Y-m-d'))->where(["seller_id"=>$sellerData->id, "status"=>"delivered"])->orderBy('id','DESC')->get();

                //     if($orderDatas) {
                //         $orderCount = 0;
                //         $totalPayout = 0;
                //         $totalCommission = 0;
                //         $totalGST = 0;
                //         $totalTCS = 0;
                //         $payoutArray = array();

                //         foreach ($orderDatas as $odata) {
                //             $orderCount++;

                //             $details = OrderDetail::where(["order_id"=>$odata->id])->first();
                //             $totalPayout = $totalPayout + $details->final_price;
                //             $totalCommission = $totalCommission + $details->admin_commision;
                //             $totalGST = $totalGST + $details->gst_tax;
                //             $totalTCS = $totalGST + $details->tcs_tax;
                //         }

                //         if($orderCount > 0) {
                //             $payoutArray = ["seller_id"=>$sellerData->id, "date"=>$currentDate, "orders_count"=>$orderCount, "payout"=>$totalPayout, "transcation_id"=>"", "commission"=>$totalCommission, "adjustments"=>0, "gst"=>$totalGST, "tcs"=>$totalTCS];

                //             PayoutRecord::create($payoutArray);
                //         }
                //     }
                // }
            }
        }
        return $this->sendResponse(["payout"=>"success"], "Payout details", $request->path());
    }


    public function orderpendingcheck(Request $request) {

        $orderDatas = Order::where(["status"=>'accepted'])->get();
        if($orderDatas) {

            foreach($orderDatas as $orderData) {

                $currentDate = date('d-m-Y');
                $orderDate = date('d-m-Y', strtotime($orderData->updated_at));
                $diff = strtotime($date2) - strtotime($date1);
                $diffDays = abs(round($diff / 86400));

                if($diffDays > 2) {

                    $mistakeData = ["order_id"=>$orderData->id, "seller_id"=>$orderData->seller_id, "type"=>'breache'];
                    SellerMistake::create($mistakeData);

                }
            }
        }

        $orderData1s = Manifested::select('id','order_id', 'order_count', 'handover_date', 'updated_at')->orderBy('id','DESC')->get();
        if($orderData1s) {

            foreach($orderData1s as $orderData1) {

                $getOrder = Order::where(["id"=>$orderData1->order_id])->first();

                $currentDate = date('d-m-Y');
                $orderDate = date('d-m-Y', strtotime($orderData1->updated_at));
                $diff = strtotime($date2) - strtotime($date1);
                $diffDays = abs(round($diff / 86400));

                if($diffDays > 2) {

                    $mistakeData = ["order_id"=>$orderData1->id, "seller_id"=>$getOrder->seller_id, "type"=>'reattempt'];
                    SellerMistake::create($mistakeData);
                }
            }
        }

        return $this->sendResponse(["orderpendingcheck"=>"success"], "Payout details", $request->path());
    }


    // public function exchangerate(Request $request) {
    //     $rate = CurrencyRate::all();
    //     if(count($rate) > 0) {

    //         $http = new Client();
    //         $generateWBN = $http->get(url('https://api.ratesexchange.eu/client/latest?apiKey=1ad9de46-fc6c-47f5-b147-432046d3501e&base_currency=INR&currencies=USD,GBP,EUR'));
    //         $dataResult = json_decode($generateWBN->getBody());
    //         $getRates = $dataResult->rates;
    //         foreach ($getRates as $key => $value) {
    //             $getCurrency = CurrencyRate::where(["currency" => $key])->first();
    //             $getCurrency->Amount = round($value, 4);
    //             $getCurrency->save();
    //         }
    //         return $this->sendResponse(["orderpendingcheck"=>$dataResult], "Payout details", $request->path());

    //     } else {

    //         $http = new Client();
    //         $generateWBN = $http->get(url('https://api.ratesexchange.eu/client/latest?apiKey=1ad9de46-fc6c-47f5-b147-432046d3501e&base_currency=INR&currencies=USD,GBP,EUR'));
    //         $dataResult = json_decode($generateWBN->getBody());
    //         $getRates = $dataResult->rates;
    //         foreach ($getRates as $key => $value) {
    //             CurrencyRate::create([
    //                 "currency" => $key,
    //                 "Amount" =>  round($value, 4)
    //             ]);
    //         }
    //         return $this->sendResponse(["orderpendingcheck"=>$dataResult], "Payout details", $request->path());
    //     }
    // }


}