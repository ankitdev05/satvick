<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Subsubcategory;
use App\Cuponcode;
use App\Content;
use App\Faq;
use App\Priority;
use App\Notification;
use App\Size;
use App\Color;
use App\Offer;
use App\Announcement;
use App\Refer;
use App\Promotion;
use App\PromotionList;
use App\Attribute;
use App\ReferEarn;
use App\Support;
use App\SizeChart;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use App\SellerApprove;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;

class AdminController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "https://mobuloustech.com/satvick/public/";
        $this->baseurl = "https://mobuloustech.com/satvick/public";
    }

    public function addSubAdmin(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['email' => 'required', 'name' => 'required', 'password' => 'required', 'phone' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $verifyAdmin = Admin::where(["email"=>$input['email']])->first();

        if($verifyAdmin) {
            return $this->sendError($request->path(), 'Sub-Admin Already Added');
        } else {
            $arrayData = [
                "type"=>"subadmin",
                "email"=>$input['email'],
                "name"=>$input['name'],
                "password"=>$input['password'],
                "phone"=>$input['phone'],
                "permit"=>$input['permit'],
            ];
        }

        $obj = Admin::create($arrayData);
        return $this->sendResponse($obj, "Subadmin added successfully", $request->path());
    }

    public function subAdminList(Request $request) {
        $getSubAdmins = Admin::where(['type'=> 'subadmin'])->get();
        $subadminArray = array();

        if($getSubAdmins) {

            foreach($getSubAdmins as $getSubAdmin) {
                
                $subadminArray[] = [$getSubAdmin->email, $getSubAdmin->name, $getSubAdmin->phone, $getSubAdmin->status, $getSubAdmin->created_at, $getSubAdmin->id];
            }
        }
        return $this->sendResponse($subadminArray, "Subadmin listing", $request->path());
    }

    public function subAdminEdit(Request $request) {
        $getSubAdmin = Admin::where(['type'=> 'subadmin', 'id'=>$request->id])->first();
        
        if($getSubAdmin) {

        	$subadminArray = ["email"=>$getSubAdmin->email, "name"=>$getSubAdmin->name, "phone"=>$getSubAdmin->phone, "status"=>$getSubAdmin->status, "permit"=>$getSubAdmin->permit, "created_at"=>$getSubAdmin->created_at];
        }
        return $this->sendResponse($subadminArray, "Subadmin Detail", $request->path());
    }

    public function subAdminUpdate(Request $request) {
        $getSubAdmin = Admin::where(['type'=> 'subadmin', 'id'=>$request->id])->first();
        
        if($getSubAdmin) {

        	$getSubAdmin->name = $request->name; 
        	$getSubAdmin->phone = $request->phone;
            $getSubAdmin->status = $request->status;
        	$getSubAdmin->permit = $request->permit;
            $getSubAdmin->save();
        }
        return $this->sendResponse($getSubAdmin, "Subadmin Detail Updated", $request->path());
    }

    public function sendpushnotification(Request $request) {

        $usersString = $request->user_ids;
        $userArray = explode(',', $usersString);

        if(count($userArray) > 0) {

            foreach($userArray as $userid) {
                $getUsers = User::where(["id"=>$userid])->first();
                if($getUsers) {

                    $notifyData = [
                        'user_id'=>$userid,
                        'order_id'=>0,
                        'order_number'=>0,
                        'type'=>'Promotional Notification',
                        'title'=> $request->title,
                        'content'=>$request->message
                    ];
                    Notification::create($notifyData);
                    $userDevice = Token::select('deviceType', 'deviceToken')->where(['user_id'=>$userid])->first();
                    if($userDevice) {
                        $badge = Notification::where(["user_id"=>$userid, 'status'=>0])->count();
                        $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
                    }
                }
            }
        }
        return $this->sendResponse1([], 'Notification Send Successfully.', $request->path());
  	}


  	public function allsellerpayment(Request $request) {
        $sellers = Seller::where(['status'=>1])->get();
        $sellerArray = array();

        if($sellers) {
            foreach($sellers as $seller) {
                $totalBuyerPayment = 0;
                $totalsellerPayment = 0;
                $totalShippingPayment = 0;
                $totalDiscountPayment = 0;
                $totalCommisionPayment = 0;
                $totalTcsPayment = 0;
                $totalOrders = 0;

                $orders = Order::where(['seller_id'=>$seller->id, 'status'=>'delivered'])->get();
                if($orders) {

                    foreach($orders as $order) {
                        $orderDetail = OrderDetail::where(['order_id'=>$order->id])->first();
                        $totalBuyerPayment = $totalBuyerPayment + $orderDetail->final_price;
                        $totalsellerPayment = $totalsellerPayment + $orderDetail->seller_amount;
                        $totalShippingPayment = $totalShippingPayment + $orderDetail->seller_shippping;
                        $totalDiscountPayment = $totalDiscountPayment + $orderDetail->discount_price + $orderDetail->refer_discount;
                        $totalCommisionPayment = $totalCommisionPayment + $orderDetail->admin_commision;
                        $totalTcsPayment = $totalTcsPayment + $orderDetail->tcs_tax;
                        $totalOrders++;
                    }
                }
                if($totalOrders > 0) {

                    $sellerArray[] = [$seller->name, $seller->email, $seller->state, $totalOrders, $totalsellerPayment, $totalBuyerPayment, $totalShippingPayment, $totalDiscountPayment, $totalCommisionPayment, $totalTcsPayment];
                }
            }
        }
        return $this->sendResponse1($sellerArray, 'Payment Lists', $request->path());
    }

    public function productSearchForFlashSale(Request $request) {
        $keyword = $request->keyword;
        $productArray = array();
        $productArrayOne = array();
        $productArrayTwo = array();

        $getProductNames = Product::select('id', 'name')->where('name', 'like', '%' . $keyword . '%')->get();
        if($getProductNames) {
            foreach($getProductNames as $getProductName) {
                $productArrayOne[] = ["id"=>$getProductName->id, "name"=>$getProductName->name];
            }
        }

        $getProductSKUs = Product::select('id', 'name')->where('sku', 'like', '%' . $keyword . '%')->get();
        if($getProductSKUs) {
            foreach($getProductSKUs as $getProductSKU) {
                $productArrayTwo[] = ["id"=>$getProductSKU->id, "name"=>$getProductSKU->name];
            }
        }

        $productArray = array_merge($productArrayOne, $productArrayTwo);

        return $this->sendResponse1($productArray, 'Product Lists', $request->path());
    }

    public function supportticketupdate(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, ['id'=>'required','status'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $checkSupport = Support::where(['id'=>$input['id']])->first();
        if(!$checkSupport) {
            return $this->sendError($request->path(), 'Ticket not Found');
        } else {
            $checkSupport->status = $input['status'];
            $checkSupport->save();

            return $this->sendResponse($checkSupport, "Ticket updated successfully", $request->path());
        }
    }

    public function categoryApprovalList(Request $request) {
        $getDatas = SellerApprove::orderBy('id','DESC')->get();
        $return_array = array();
        if($getDatas) {

            foreach($getDatas as $getData) {

                $seller = Seller::find($getData->seller_id);
                $subCat = Subcategory::find($getData->subcategory);

                if($getData->status == 1) {
                    $status = "Not Approved";
                } else {
                    $status = "Approved";
                }

                $return_array[] = [$getData->id, 
                            $seller->name, 
                            $seller->email, 
                            $getData->category, 
                            $subCat->name,
                            $status, 
                            $getData->id];
            }

            return $this->sendResponse($return_array, 'List retrieved successfully', $request->path());

        } else {
            return $this->sendError($request->path(), 'List Not Found!');
        }
    }

    public function changecategoryapproval(Request $request) {
        $getDatas = SellerApprove::where(["id"=>$request->id])->first();
        if($getDatas) {

            if($getDatas->status == 1) {
                $getDatas->status = 2;
            } else {
                $getDatas->status = 1;
            }
            $getDatas->save();

            return $this->sendResponse($getDatas, 'Status change successfully', $request->path());

        } else {
            return $this->sendError($request->path(), 'Not Found!');
        }
    }

    public function userlistfornotification(Request $request) {
        $details = User::select("name", "email", "id")->orderby('id', 'desc')->get();
        $return_array = array();
        foreach ($details as $met) {

            $return_array[] = [$met->name, $met->email, $met->id];
        }
        return $this->sendResponse1($return_array, 'Buyer list retrieved successfully', $request->path());
    }

    public function abounded(Request $request) {
        $datas = Cart::orderBy('id','DESC')->get();
        $result = array();
        foreach($datas as $data) {

            if(is_numeric($data->user_id)) {
                
                $user = User::where('id', $data->user_id)->first();
                if($user) {
                    $product = Product::where('id', $data->product_id)->first();
                    if($product) {

                        if(!empty($data->color)) {
                            $colorname = Color::where(["hexcode"=>$data->color])->first();
                            $colorname = $colorname->name;
                        } else {
                            $colorname = "";
                        }

                        $result[] = [$user->name, $user->email, $user->phone, $product->sku, $product->name, $data->size, $colorname, $data->quantity, date('d-m-Y H:i:s', strtotime($data->created_at))];
                    }
                }
            }
        }
        return $this->sendResponse1($result, 'List retrieved successfully', $request->path());
    }

    public function disablecod(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['products'=>'required', 'payment_mode'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $productids = explode(',', $input['products']);

        foreach($productids as $products) {

            DB::table('products')->where(['id'=>$products])->update(['payment_mode'=>$input['payment_mode']]);
        }

        return $this->sendResponse1([], 'Products updated Successfully.', $request->path());
    }


    public function shippingorderlisting(Request $request) {  //  In Transit list
        
        $details = Order::select("orders.id as id", "users.name as buyer", "orders.*")->join('users', 'users.id', '=', 'orders.user_id')->where(["orders.status"=>"transit"])->orderBy('orders.id','DESC')->get();
        $return_array = array();
        
        foreach ($details as $met) {

            $detail = OrderDetail::where(['order_id'=>$met->id])->first();

            $product = Product::select("name")->where(['id'=>$met->product_id])->first();
            if($product) {
                $productName = $product->name;
            } else {
                $productName = "";
            }

            $return_array[] = array($met->order_number, $detail->wbn_num, $productName, $met->buyer, $met->state, $met->city, $met->payment_type, $detail->quantity ,$met->status , date('d-m-y H:i',strtotime($met->created_at)),$met->id, $met->id);
        }
        return $this->sendResponse1($return_array, 'Order list retrieved successfully.', $request->path());
    }

    public function promotionlist(Request $request) {

        $getPros = Promotion::orderBy('id','DESC')->get();
        $arrData = array();

        if($getPros) {

            foreach($getPros as $getPro) {

                $getseller = Seller::where(["id"=>$getPro->seller_id])->first();
                
                $getOffer = Offer::select('catid','description','startdate','enddate','coupon_id')->where(["id"=>$getPro->offer_id])->first();
                $getPromotionCount = PromotionList::where(["promotion_id"=>$getPro->id])->count();

                $getcoupon = Cuponcode::where(["id"=>$getOffer->coupon_id])->first();
                $getcat = Subsubcategory::where(["id"=>$getOffer->catid])->first();

                if($getOffer) {

                    if($getcoupon) {
                        $getcouponname = $getcoupon->name;
                    } else {
                        $getcouponname = "";
                    }

                    if($getcat) {
                        $getcatname = $getcat->name;
                    } else {
                        $getcatname = "";
                    }

                    $date1 = strtotime($getOffer->enddate);
                    $date2 = strtotime(date("d-m-Y H:i:s"));
                    if($date2 < $date1) {
                        $arrData[] = [
                            $getseller->name,
                            $getseller->email,
                            $getcatname,
                            $getOffer->description,
                            $getOffer->startdate,
                            $getOffer->enddate,
                            $getcouponname,
                            $getPromotionCount,
                            $getPro->id
                        ];
                    }
                }
            }
        }

        return $this->sendResponse($arrData, "Promotion listing", $request->path());
    }


    public function promotionproductlist(Request $request, $id) {

        $getPros = PromotionList::where(["promotion_id"=>$id])->orderBy('id','DESC')->get();
        $arrData = array();

        if($getPros) {

            foreach($getPros as $getPro) {

                $getproduct = Product::where(["id"=>$getPro->product_id])->first();
                
                if($getproduct) {

                    $getcat = Subsubcategory::where(["id"=>$getproduct->subsubcategory_id])->first();
                    
                    if($getPro->status == 0) {
                        $proStatus = "Inactive";
                    } else {
                        $proStatus = "Active";
                    }

                    $arrData[] = [
                        $getproduct->sku,
                        $getproduct->name,
                        $getcat->name,
                        $proStatus,
                        $getPro->id,
                    ];
                }
            }
        }

        return $this->sendResponse($arrData, "Promotion Product listing", $request->path());
    }

    public function promotionproductstatus(Request $request) {
        $getDatas = PromotionList::where(["id"=>$request->id])->first();
        if($getDatas) {

            if($getDatas->status == 0) {
                $getDatas->status = 1;
            } else {
                $getDatas->status = 0;
            }
            $getDatas->save();

            return $this->sendResponse($getDatas, 'Status change successfully', $request->path());

        } else {
            return $this->sendError($request->path(), 'Not Found!');
        }
    }


    public function sublistforoffer(Request $request, $id) {
        $details = Subsubcategory::where(['subcategory_id' => $id])->get();
        $return_array = array();
        foreach ($details as $value) {

            if($value->status == 1) {
                $checkOffer = Offer::where('catid', $value->id)->first();
                if($checkOffer) {

                } else {
                    if($value->occasion_status == 1) {
                        $name = $value->name . ' - occasion';
                    } else {
                        $name = $value->name;
                    }
                    $return_array[] = [$value->id, $name];
                }
            }
        }
        return $this->sendResponse($return_array, 'Sub-subcategory retrieved successfully', $request->path());
    }


    public function pushNotification($notifyData, $userDevice, $badge) {
        $url = 'https://fcm.googleapis.com/fcm/send';  
        
        $message = array("id"=>$notifyData['order_id'],'title' => $notifyData['title'], "body"=>$notifyData['content'], "type"=>$notifyData['type'], 'sound' => 'default', 'badge'=>$badge);
        
        $registatoin_ids = array($userDevice->deviceToken);
        $fields = array('registration_ids' => $registatoin_ids, 'data' => $message, 'notification'=>$message);

        $GOOGLE_API_KEY = "AAAAvtlLWfU:APA91bHkKDPg59guB8VXYN8zeJc5w8vfrYuCRYL6J4Hlmdz2SSo15JotADzXH-RBF9fdGxPgci7W_422oGlJz6I_KW1mZ99dy7fqJHxtiDy3_FT199aH8VkUgZmA5Zf8NaGeL6rnnKW8";   
        $headers = array('Authorization: key=' . $GOOGLE_API_KEY , 'Content-Type: application/json',);
        
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);    
        
        if ($result === false) {
            return $abc = 'FCM Send Error: ' . curl_error($ch);
        } else {
            return $abc = "Notification Send";
        }
        curl_close($ch);
    }

}

?>