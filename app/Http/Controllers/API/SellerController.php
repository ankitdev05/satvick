<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\API\APIBaseController as APIBaseController;
use DB;
use App\Seller;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\Order;
use App\OrderDetail;
use App\OrderRequest;
use App\Admin;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Wishlist;
use App\Cart;
use App\Subsubcategory;
use App\Cuponcode;
use App\Content;
use App\Faq;
use App\Priority;
use App\Notification;
use App\Size;
use App\Color;
use App\Offer;
use App\Announcement;
use App\Refer;
use App\Promotion;
use App\PromotionList;
use App\Attribute;
use App\ReferEarn;
use App\Support;
use App\SizeChart;
use App\ProductImageColor;
use App\Manifest;
use App\SellerPerformance;
use App\SellerMistake;
use App\PayoutRecord;
use App\SellerApprove;
use App\AttributeGroup;
use App\AttributeOption;
use Image;
use Validator;
use Mail;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use PDF;

class SellerController extends APIBaseController {

    private $baseurlslash;
    private $baseurl;
    
    public function __construct() {
        $this->baseurlslash = "https://mobuloustech.com/satvick/public/";
        $this->baseurl = "https://mobuloustech.com/satvick/public";
    }

    public function checkcategoryapproval(Request $request, $id) {
        $getData = SellerApprove::where(["seller_id"=>$id, "status"=>2])->get();
        if(count($getData) > 0) {
            return $this->sendResponse(["status" => "success"], "Category Found", $request->path());
        } else {
            return $this->sendError($request->path(), 'Category not approved yet for products!');
        }
    }

    public function categoryapproval(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['seller_id'=>'required', 'category' => 'required','subcategory'=>'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $check = SellerApprove::where(["seller_id"=>$request->seller_id, "subcategory"=>$request->subcategory])->first();
        if($check) {

            if($check->status == 1) {
                return $this->sendError($request->path(), 'Category already in approval');
            } else {
                return $this->sendError($request->path(), 'You are already been Approved for this Category');
            }
        }
        SellerApprove::create([
            "seller_id"=>$request->seller_id,
            "category"=>$request->category,
            "subcategory"=>$request->subcategory,
        ]);

        return $this->sendResponse(["status" => "success"], "Approval for category  is saved successfully", $request->path());
    }

    public function sellerCategoryList(Request $request) {
        $details = SellerApprove::select("category")->where(["seller_id"=>$request->seller_id, "status"=>2])->groupBy('category')->get();
        $return_array = array();
        foreach ($details as $value) {
            $return_array[] = [$value->category];
        }
        return $this->sendResponse($details, 'Category list retrieved successfully', $request->path());
    }

    public function sellerSubCategoryList(Request $request) {
        $details = Subcategory::where(['category_id' => $request->id])->get();
        $return_array = array();
        foreach ($details as $value) {

            $check = SellerApprove::where(["category"=>$request->id, "subcategory"=>$value->id, "seller_id"=>$request->seller_id, "status"=>2])->first();
            if($check) {
                $return_array[] = [$value->id, $value->name];
            }
        }
        return $this->sendResponse($return_array, 'Subcategory list retrieved successfully', $request->path());
    }

    // public function sellerSubSubCategoryList(Request $request) {
    //     $details = Subsubcategory::where(['subcategory_id' => $request->id])->get();
    //     $return_array = array();
    //     foreach ($details as $value) {

    //         $check = SellerApprove::where(["subcategory"=>$request->id, "subsubcategory"=>$value->id, "seller_id"=>$request->seller_id, "status"=>2])->first();
    //         if($check) {
    //             $return_array[] = [$value->id, $value->name];
    //         }
    //     }
    //     return $this->sendResponse($return_array, 'Sub-subcategory list retrieved successfully', $request->path());
    // }

    public function attributegroupList(Request $request, $id) {
        $details = AttributeGroup::where(['seller_id'=>$id])->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $value) {
                $return_array[] = [$value->id, $value->name, $value->status];            
            }
        }
        return $this->sendResponse($return_array, 'Attribute Group list retrieved successfully', $request->path());
    }

    public function attributegroupadd(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['seller_id'=>'required', 'name' => 'required', 'status' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        AttributeGroup::create([
            "seller_id"=>$request->seller_id,
            "name"=>$request->name,
            "status"=>$request->status,
        ]);

        return $this->sendResponse(["status" => "success"], "Attribute Group is saved successfully", $request->path());
    }

    public function attributegroupedit(Request $request, $id) {
        $details = AttributeGroup::where(['id'=>$id])->first();
        $return_array = array();
        
        if($details) {
            $return_array = [$details->id, $details->name, $details->status];
        }
        return $this->sendResponse($return_array, 'Attribute Group retrieved successfully', $request->path());
    }

    public function attributegroupupdate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['id'=>'required', 'name' => 'required', 'status' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $details = AttributeGroup::where(['id'=>$id])->first();
        $details->name = $request->name;
        $details->statu = $request->status;
        $details->save();

        return $this->sendResponse(["status" => "success"], "Attribute Group is uppdated successfully", $request->path());
    }

    public function attributeoptionList(Request $request, $id) {
        $details = AttributeOption::where(['group_id'=>$id])->get();
        $return_array = array();
        
        if($details) {
            foreach ($details as $value) {
                $return_array[] = [$value->id, $value->name];
            }
        }
        return $this->sendResponse($return_array, 'Attribute Option list retrieved successfully', $request->path());
    }

    public function attributeoptionadd(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['seller_id'=>'required', 'group_id'=>'required', 'name' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        AttributeOption::create([
            "group_id"=>$request->group_id,
            "seller_id"=>$request->seller_id,
            "name"=>$request->name,
        ]);

        return $this->sendResponse(["status" => "success"], "Attribute Option is saved successfully", $request->path());
    }

    public function attributeoptionedit(Request $request, $id) {
        $details = AttributeOption::where(['id'=>$id])->first();
        $return_array = array();
        
        if($details) {
            $return_array = [$details->id, $details->name];
        }
        return $this->sendResponse($return_array, 'Attribute Option fetched successfully', $request->path());
    }

    public function attributeoptiondate(Request $request) {
        $input = $request->all();
        $validator = Validator::make($input, ['id'=>'required', 'name' => 'required']);
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }

        $details = AttributeOption::where(['id'=>$id])->first();
        $details->name = $request->name;
        $details->save();

        return $this->sendResponse(["status" => "success"], "Attribute Option is updated successfully", $request->path());
    }

    public function promotionoffer(Request $request) {
        $arrayData = array();
        $objs = Offer::where(["offer_type"=>2])->get();

        foreach($objs as $obj) {

            $date1 = strtotime($obj->enddate);
            $date2 = strtotime(date("d-m-Y H:i:s"));
            if($date2 <= $date1) {

                $arrayData[] = ["id"=>$obj->id, "select"=>$obj->description];
            }
        }
        return $this->sendResponse($arrayData, "Offer added successfully", $request->path());
    }

    public function promotion(Request $request, $id) {
        $input = $request->all();
        $validator = Validator::make($input, ['offer_id' => 'required', 'csv_data' => 'required']);
    
        if ($validator->fails()) {
            return $this->sendError($request->path(), $validator->errors()->first());
        }
        $input = $request->all();
        $arrayData = [
            "seller_id"=>$id,
            "offer_id"=>$input['offer_id'],
        ];
        $getOffer = Offer::where('id',$input['offer_id'])->first();

        $counting = 0;
        if(count($input['csv_data']) > 0) {

            $obj = Promotion::create($arrayData);
            $xx = 0;
            foreach ($input['csv_data'] as $value) {

                if($xx == 0) {} else {
                    if(strlen($value[0]) > 0) {
                        $getPro = Product::select('id','subsubcategory_id')->where(["sku"=>$value[0]])->first();
                        if($getPro) {
                            if($getOffer->catid == $getPro->subsubcategory_id) {
                                PromotionList::create([
                                    "seller_id" => $id,
                                    "promotion_id" => $obj->id,
                                    "sku" => $value[0],
                                    "product_id" => $getPro->id
                                ]);
                            }
                        }
                    }
                }
                $xx++;
            }
        }

        return $this->sendResponse($obj, "Promotion Added successfully", $request->path());
    }

    public function promotionlist(Request $request, $id) {

        $getPros = Promotion::where(["seller_id"=>$id])->orderBy('id','DESC')->get();
        $arrData = array();

        if($getPros) {
            foreach($getPros as $getPro) {
                $getOffer = Offer::select('catid','description','startdate','enddate','coupon_id')->where(["id"=>$getPro->offer_id])->first();
                $getcoupon = Cuponcode::where(["id"=>$getOffer->coupon_id])->first();
                $getcat = Subsubcategory::where(["id"=>$getOffer->catid])->first();

                if($getOffer) {

                    if($getcoupon) {
                        $getcouponname = $getcoupon->name;
                    } else {
                        $getcouponname = "";
                    }

                    if($getcat) {
                        $getcatname = $getcat->name;
                    } else {
                        $getcatname = "";
                    }

                        $date1 = date("d-m-Y", strtotime($getOffer->enddate));
                        $date2 = date("d-m-Y");
                        
                        $diff = abs(strtotime($date2) - strtotime($date1));
                        $years = floor($diff / (365*60*60*24));
                        $months = floor(($diff - $years * 365*60*60*24) / (30*60*60*24));
                        $diffdays = floor(($diff - $years * 365*60*60*24 - $months*30*60*60*24)/ (60*60*24));

                        $arrData[] = [
                            'title'=>$getcatname,
                            'content'=>"",
                            'ongoing'=>$diffdays,
                            'offertype'=>$getOffer->description,
                            'startdate'=>$getOffer->startdate,
                            'enddate'=>$getOffer->enddate,
                            'couponcode'=>$getcouponname,
                        ];
                    
                }
            }
        }

        return $this->sendResponse($arrData, "Promotion listing", $request->path());
    }

    public function promotiondelete(Request $request, $id) {
        $arrayData = array();
        $objs = Promotion::where(["id"=>$id])->delete();
        $objs = PromotionList::where(["promotion_id"=>$id])->delete();
        return $this->sendResponse(["status" => "success"], "Promotion deleted successfully", $request->path());
    }

    public function uniquesku(Request $request) {

        if(empty($request->product_id)) {
            $details = Product::where(['sku'=>$request->sku])->first();
            
            if($details) {
                return $this->sendError([], 'SKU already in use', $request->path());   
            } else {
                return $this->sendResponse([], 'SKU available', $request->path());    
            }
        } else {

            $details = Product::where(['sku'=>$request->sku])->first();
            if($details) {

                $detailsmore = Product::where(['id'=>$request->product_id, 'sku'=>$request->sku])->first();
                if($detailsmore) {
                    return $this->sendResponse([], 'SKU available', $request->path());
                } else {
                    return $this->sendError([], 'SKU already in use', $request->path());
                }
            } else {
                return $this->sendResponse([], 'SKU available', $request->path());    
            }
        }
    }

}

?>