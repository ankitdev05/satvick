<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\User;
use App\Token;
use App\Address;
use App\Product;
use App\ProductPrice;
use App\Subcategory;
use App\Banner;
use App\Theme;
use App\Brand;
use App\Flashsale;
use App\Color;
use App\Size;
use App\Wishlist;
use App\Subsubcategory;
use App\Priority;
use App\Cart;
use App\Seller;
use App\Order;
use App\OrderDetail;
use App\Notification;
use App\Cuponcode;
use App\Refer;
use App\Offer;
use App\Promotion;
use App\PromotionList;
use Image;
use Auth;
use Session;
use Validator;
use Intervention\Image\ImageServiceProvider;
use Softon\Indipay\Facades\Indipay;
use PDF;
use GuzzleHttp\Client;

class OrderController extends Controller {
    private $baseurl;

    public function __construct() {
        $this->baseurlslash = "https://mobuloustech.com/yodapi/public/";
        $this->baseurl = "https://mobuloustech.com/yodapi/public";
    }

    public function change_giftwrap(Request $request) {
        $input = $request->all();

        if($input['wrap'] == "yes") {
            $getCarts = Cart::where(["user_id"=>Auth::user()->id])->get();
            foreach ($getCarts as $getCart) {

                $updateCart = Cart::where(["id"=>$getCart->id])->first();
                $updateCart->giftwrap = 1;
                $updateCart->save();
            }
        } else {
            $getCarts = Cart::where(["user_id"=>Auth::user()->id])->get();
            foreach ($getCarts as $getCart) {

                $updateCart = Cart::where(["id"=>$getCart->id])->first();
                $updateCart->giftwrap = 0;
                $updateCart->save();
            }
        }
        echo json_encode(["status"=>1, "msg"=>"Gift wrap applied!"]);
        die;
    }

    public function apply_cupon(Request $request) {
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }

        $input = $request->all();
        $detailsofcupon = Cuponcode::where('name', $input['code'])->first();
        $data = array();

        if (empty($detailsofcupon)) {
            $data = [];
            echo json_encode(["status"=>0, "msg"=>"Coupon Code Not Found", "data"=>$data]);
            exit;
        
        } else {

            $check_code_reused = Order::where(['user_id' => Auth::user()->id, 'coupen_code' => $input['code']])->first();
            if ($check_code_reused) {
                $data = [];
                echo json_encode(["status"=>0, "msg"=>"You have already used this coupon!", "data"=>$data]);
                exit;
            }

            $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
            $totalAmount = 0;
            $getCarts = Cart::where(["user_id"=>Auth::user()->id])->get();

            if($checkoffer) {

                $date1 = strtotime($checkoffer->enddate);
                $date2 = strtotime(date("d-m-Y H:i:s"));
                if($date2 > $date1) {

                    $data = ["total"=>0, "proid"=>0];
                    echo json_encode(["status"=>0, "msg"=>"Coupon code expired!", "data"=>$data]);
                    exit;
                }

                foreach ($getCarts as $getCart) {

                    $pro = Product::select('user_id', 'sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();
                    $checkoffer = Offer::where(['catid'=>$pro->subsubcategory_id, 'coupon_id'=> $detailsofcupon->id])->first();
                    if($checkoffer) {

                        $checkava = "A";
                        $promotion = Promotion::where(['seller_id'=>$pro->user_id, 'offer_id'=> $checkoffer->id])->first();
                        if($promotion) {
                            $checkava = "B";
                            $productPromotion = PromotionList::where(['promotion_id'=>$promotion->id, 'product_id'=> $getCart->product_id,'status'=>1])->first();
                            if($productPromotion) {
                                $checkava = "C";
                                if($pro->is_variant == 2) {

                                    if(!empty($getCart->color) && !empty($getCart->size)) {
                                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                                    } else if(!empty($getCart->color) && empty($getCart->size)) {
                                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                                    } else if(empty($getCart->color) && !empty($getCart->size)) {
                                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                                    }

                                    $get_price_sp = $proPriceDetail->sp; 
                                    $get_price_mrp = $proPriceDetail->mrp;
                                } else {
                                    $get_price_sp = $pro->sp; 
                                    $get_price_mrp = $pro->mrp;
                                }

                                $cartAmount = $get_price_sp * $currencyValue * $getCart->quantity;
                                $totalAmount = $totalAmount + $cartAmount;
                            }
                        }
                    }
                }
            
            } else {

                foreach ($getCarts as $getCart) {
                    $pro = Product::select('sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();
                    if($pro->is_variant == 2) {

                        if(!empty($getCart->color) && !empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                        } else if(!empty($getCart->color) && empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                        } else if(empty($getCart->color) && !empty($getCart->size)) {
                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                        }

                        $get_price_sp = $proPriceDetail->sp; 
                        $get_price_mrp = $proPriceDetail->mrp;
                    } else {
                        $get_price_sp = $pro->sp; 
                        $get_price_mrp = $pro->mrp;
                    }

                    $cartAmount = $get_price_sp * $currencyValue * $getCart->quantity;
                    $totalAmount = $totalAmount + $cartAmount;
                }
            }

            if($totalAmount <= 0) {
                $data = ["total"=>$totalAmount, "proid"=>$getCart->product_id];
                echo json_encode(["status"=>0, "msg"=>"Coupon code not valid!", "data"=>$data]);
                exit;
            }

            if ($totalAmount < $detailsofcupon->min_price) {
                $data = ["total"=>$totalAmount, "proid"=>$getCart->product_id];
                echo json_encode(["status"=>0, "msg"=>"Shop more to unlock coupon code!", "data"=>$data]);
                exit;
            }

            $totalAmount = 0;
            $totalSave = 0;
            $total_price = 0;
            $final_discounted_price = 0;

            $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
            $getCarts = Cart::where(["user_id"=>Auth::user()->id])->get();
            
            if($checkoffer) {
                
                if(count($getCarts) > 0) {

                    foreach ($getCarts as $getCart) {
                    
                        $total_discounted_price = 0;
                        $pro = Product::select('user_id', 'sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();

                        $checkoffer = Offer::where(['catid'=>$pro->subsubcategory_id, 'coupon_id'=> $detailsofcupon->id])->first();
                        if($checkoffer) {

                            $promotion = Promotion::where(['seller_id'=>$pro->user_id, 'offer_id'=> $checkoffer->id])->first();
                            if($promotion) {
                        
                                $productPromotion = PromotionList::where(['promotion_id'=>$promotion->id, 'product_id'=> $getCart->product_id])->first();
                                if($productPromotion) {
                            
                                    if($pro->is_variant == 2) {

                                        if(!empty($getCart->color) && !empty($getCart->size)) {
                                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                                        } else if(!empty($getCart->color) && empty($getCart->size)) {
                                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                                        } else if(empty($getCart->color) && !empty($getCart->size)) {
                                            $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                                        }

                                        $get_price_sp = $proPriceDetail->sp; 
                                        $get_price_mrp = $proPriceDetail->mrp;
                                    } else {
                                        $get_price_sp = $pro->sp; 
                                        $get_price_mrp = $pro->mrp;
                                    }

                                    $cartAmount = $get_price_sp * $currencyValue * $getCart->quantity;
                                    $totalAmount = $totalAmount + $cartAmount;

                                    $totalMrp = $get_price_mrp * $currencyValue * $getCart->quantity;
                                    $cal = $totalMrp - $cartAmount;
                                    $totalsavediff = $totalSave + $cal;

                                    if($detailsofcupon->type == "percent") {
                                        $dicounted_price = ($totalAmount * $detailsofcupon->discount) / 100;
                                        $total_price = $total_price + round($totalAmount - $dicounted_price);
                                        $totalSave = $totalSave +  $totalsavediff + $dicounted_price;

                                        $final_discounted_price = $final_discounted_price + $total_discounted_price;
                                    } else {
                                        $dicounted_price = $detailsofcupon->discount;
                                        $total_price = $total_price + round($totalAmount - $dicounted_price);
                                        $totalSave = $totalSave + $totalsavediff + $dicounted_price;
                                        
                                        $final_discounted_price = $final_discounted_price + $dicounted_price;
                                    }

                                    $updateCart = Cart::where(["id"=>$getCart->id])->first();
                                    $updateCart->discount = $dicounted_price;
                                    $updateCart->coupon_code = $detailsofcupon->name;
                                    $updateCart->save();
                                }
                            }
                        }
                    }

                    $data = [$totalAmount, $final_discounted_price, $total_price, $totalSave];
                    echo json_encode(["status"=>1, "msg"=>"Coupon Applied", "data"=>$data]);
                    exit;
                
                } else {

                    $data = [];
                    echo json_encode(["status"=>0, "msg"=>"Coupon code not valid!", "data"=>$data]);
                    exit;
                }

            } else {

                if(count($getCarts) > 0) {

                    foreach ($getCarts as $getCart) {
                    
                        $total_discounted_price = 0;
                        $pro = Product::select('sp','mrp','is_variant','subsubcategory_id')->where(["id"=>$getCart->product_id])->first();
                
                        if($pro->is_variant == 2) {

                            if(!empty($getCart->color) && !empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                            } else if(!empty($getCart->color) && empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                            } else if(empty($getCart->color) && !empty($getCart->size)) {
                                $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                            }

                            $get_price_sp = $proPriceDetail->sp; 
                            $get_price_mrp = $proPriceDetail->mrp;
                        } else {
                            $get_price_sp = $pro->sp; 
                            $get_price_mrp = $pro->mrp;
                        }

                        $cartAmount = $get_price_sp * $currencyValue * $getCart->quantity;
                        $totalAmount = $totalAmount + $cartAmount;

                        $totalMrp = $get_price_mrp * $currencyValue * $getCart->quantity;
                        $cal = $totalMrp - $cartAmount;
                        $totalsavediff = $totalSave + $cal;

                        if($detailsofcupon->type == "percent") {
                            $dicounted_price = ($totalAmount * $detailsofcupon->discount) / 100;
                            $total_price = $total_price + round($totalAmount - $dicounted_price);
                            $totalSave = $totalSave +  $totalsavediff + $dicounted_price;

                            $final_discounted_price = $final_discounted_price + $total_discounted_price;
                        } else {
                            $dicounted_price = $detailsofcupon->discount;
                            $total_price = $total_price + round($totalAmount - $dicounted_price);
                            $totalSave = $totalSave + $totalsavediff + $dicounted_price;
                            
                            $final_discounted_price = $final_discounted_price + $dicounted_price;
                        }

                        $updateCart = Cart::where(["id"=>$getCart->id])->first();
                        $updateCart->discount = $dicounted_price;
                        $updateCart->coupon_code = $detailsofcupon->name;
                        $updateCart->save();
                    }

                    $data = [$totalAmount, $final_discounted_price, $total_price, $totalSave];
                    echo json_encode(["status"=>1, "msg"=>"Coupon Applied", "data"=>$data]);
                    exit;

                } else {
                    $data = [];
                    echo json_encode(["status"=>0, "msg"=>"Coupon code not valid!", "data"=>$data]);
                    exit;
                }
            }
        }
    }

    public function placeorder(Request $request) {
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }

        $cartData = array();
        $sp_total = 0;
        $sp_price = 0;
        $mrp_total = 0;
        $mrp_price = 0;

        $is_coupenApplied = "";
        $is_discount = 0;
        $coupon_flag = "false";

        $getCarts = Cart::where(["user_id"=>Auth::user()->id])->get();

        if($getCarts) {

            $totalShipping = 0;
            $totalGiftWrap = 0;

            foreach($getCarts as $getCart) {
                
                $proDetail = Product::where(["id"=>$getCart->product_id])->first();
                if($proDetail->is_variant == 2) {

                    if(!empty($getCart->color) && !empty($getCart->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size, "color"=>$getCart->color])->first();
                    } else if(!empty($getCart->color) && empty($getCart->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "color"=>$getCart->color])->first();
                    } else if(empty($getCart->color) && !empty($getCart->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$getCart->product_id, "size"=>$getCart->size])->first();
                    }

                    $get_price_sp = $proPriceDetail->sp; 
                    $get_price_mrp = $proPriceDetail->mrp;
                } else {
                    $get_price_sp = $proDetail->sp; 
                    $get_price_mrp = $proDetail->mrp;

                }
                
                $percentage = 100 - ($get_price_sp*$currencyValue*100)/$get_price_mrp*$currencyValue;
                
                $sellerdetails = Seller::find($proDetail->user_id);
                if (!empty($sellerdetails)) {
                    $seller_name = $sellerdetails->name;
                } else {
                    $seller_name = "";
                }

                if ($proDetail->images) {
                    $img = explode(',', $proDetail->images);
                    $proimage = $this->baseurl. $img[0];
                } else {
                    $proimage = url('/public/') . "/img/imagenotfound.jpg";
                }

                $sp_price = $get_price_sp * $currencyValue * $getCart->quantity;
                $sp_total = $sp_price + $sp_total;

                $mrp_price = $get_price_mrp * $currencyValue * $getCart->quantity;
                $mrp_total = $mrp_price + $mrp_total;

                if(!empty($getCart->color)) {
                    $colorname = Color::where(["hexcode"=>$getCart->color])->first();
                    $colorname = $colorname->name;
                } else {
                    $colorname = "";
                }

                if(strlen($getCart->coupon_code) > 0) {
                    $coupon_applied = "Coupon applied : ". $getCart->coupon_code;
                    $coupon_flag = $getCart->coupon_code;
                } else {
                    $coupon_applied = "";
                }

                $is_discount = $is_discount + $getCart->discount;

                $cartData[] = [
                    "cartid"=>$getCart->id,
                    "proid"=>$proDetail->id,
                    "productimage"=>$proimage,
                    "name"=>$proDetail->name,
                    "brand"=>$proDetail->brand,
                    "size"=>$getCart->size,
                    "color"=>$colorname,
                    "qty"=>$getCart->quantity,
                    "sp"=>$get_price_sp * $currencyValue,
                    "mrp"=>$get_price_mrp * $currencyValue,
                    "percentage"=>(int)$percentage,
                    "seller"=>$seller_name,
                    "size_label"=>$proDetail->size_label,
                    "coupon_applied" => $coupon_applied,
                ];

                $totalShipping = $totalShipping + $proDetail->national;
                if($getCart->giftwrap == 1) {
                    $totalGiftWrap = $totalGiftWrap + 25;
                }
            }   
        } else {
            $cartData = [];
        }
        
        $totalsaves = $mrp_total - $sp_total + $is_discount;

        if($coupon_flag == "false") {
            $is_coupenApplied = "";
        } else {
            $is_coupenApplied = $coupon_flag;
        }

        $paying = [
            "amount"=>$sp_total * $currencyValue,
            "shipping"=>$totalShipping * $currencyValue,
            "giftwrap"=>$totalGiftWrap * $currencyValue,
            
            "payable"=>$sp_total + $totalGiftWrap - $is_discount * $currencyValue,
            "payableWithShip"=>$sp_total + $totalGiftWrap + $totalShipping - $is_discount  * $currencyValue,
            "payableWithGift"=>$sp_total + $totalGiftWrap - $is_discount  * $currencyValue,
            "payableWithShipAndGift"=>$sp_total + $totalShipping + $totalGiftWrap - $is_discount  * $currencyValue,
            
            "discount"=>$totalsaves * $currencyValue,
            "coupon_discount"=>$is_discount * $currencyValue,
            "is_coupenApplied"=>$is_coupenApplied
        ];

        $detailsaddress = Address::where(["user_id"=>Auth::user()->id])->get();
        $userData = User::select('name','phone')->where(["id"=>Auth::user()->id])->first();

        $checkCodAvailable = $this->codavailable(Auth::user()->id);

        return view('placeorder', compact('cartData','paying','detailsaddress','userData', 'checkCodAvailable'));
    }

    public function codavailable($id) {

        $addtocarts = Cart::where(['user_id' => $id])->get();
        $flag = "true";
        $xx = 0;
        $yy = 0;
        foreach($addtocarts as $addtocart) {

            $value = Product::where('id', $addtocart->product_id)->first();
            
            if($value->payment_mode == 1) {
                $yy++;
            }

            if($value->payment_mode == 2) {
                $xx++;
            }
        }

        if($xx == $yy) {
            $flag = "true";
        
        } else if($xx >= $yy) {
            $flag = "false";
        
        } else if($xx <= $yy) {
            $flag = "true";
        }

        return $return_array['cod_available'] = $flag;
    }

    public function payingorder(Request $request) {
        $req = $request->all();
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }

        if($req['payment_type'] == "Cash On Delivery") {
            $this->cashondelivery($req);
            Session::flash('message_add', "Order placed successfully."); 
            return redirect()->route('orders');
        
        } else {

            $cartAmount = 0;
            $totalAmount = 0;
            $findallcartlist = Cart::where(["user_id" => Auth::user()->id])->get();
            
            foreach ($findallcartlist as $value) {
                $quantity = $value->quantity;
                $findp = Product::find($value->product_id);
                if($findp->is_variant == 2) {
                    
                    if(!empty($value->color) && !empty($value->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size, "color"=>$value->color])->first();
                    } else if(!empty($value->color) && empty($value->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "color"=>$value->color])->first();
                    } else if(empty($value->color) && !empty($value->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size])->first();
                    }

                    $get_price_sp = $proPriceDetail->sp; 
                    $get_price_mrp = $proPriceDetail->mrp;
                } else {
                    $get_price_sp = $findp->sp; 
                    $get_price_mrp = $findp->mrp;
                }

                $cartAmount = $value->quantity * $get_price_sp * $currencyValue;

                if(strlen($value->coupon_code) > 0) {
                    $getDiscount = $value->discount;
                    $discountAmt = $cartAmount - $value->discount;
                } else {
                    $getDiscount = 0;
                    $discountAmt = $cartAmount;
                }

                $totalAmount = $totalAmount + $discountAmt;
            }

            if($req['gift_wrap'] == 'yes') {
                $totalAmount = $totalAmount + 25;
            }

            $verifyRefer = Refer::where(["referto"=>Auth::user()->id, "status"=>1])->orderBy("id","ASC")->first();
            if($verifyRefer) {
                $getRefer = ReferEarn::where(["id"=>1])->first();
                $disAmt = $getRefer->price * $currencyValue;
                $totalAmount = $totalAmount - $disAmt;
            } else {
                $verifyRefer = Refer::where(["referby"=>Auth::user()->id, "status"=>1])->orderBy("id","ASC")->first();
                if($verifyRefer) {
                    $getRefer = ReferEarn::where(["id"=>1])->first();
                    $disAmt = $getRefer->price * $currencyValue;
                    $totalAmount = $totalAmount - $disAmt;
                }
            }

            $orderNum = 'YOD'. $this->random_strings(5);
            $parameters = [
                'tid' => rand(1,10000),
                'order_id' => $orderNum,
                'amount' => $totalAmount,
            ];
            
            $request->session()->put('location', $req['location']);
            $request->session()->put('giftwrap', $req['gift_wrap']);
            if(isset($req['coupan_code'])) {
                $request->session()->put('coupan_code', $req['coupan_code']);
            } else {
                $request->session()->put('coupan_code', '0');
            }

            $order = Indipay::prepare($parameters);
            return Indipay::process($order);
        }
    }

    public function cashondelivery($req) {
        $req = $req;
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }
        $findallcartlist = Cart::where(["user_id" => Auth::user()->id])->get();
        $couponCode = $req['coupan_code'];
        
        $userGet = User::where(['id' => Auth::user()->id])->first();
        $add = Address::where(["user_id"=>Auth::user()->id, 'id'=>$req['location']])->first();
        $userAdd = $add->address .','. $add->town .','. $add->city .','. $add->state.','. $add->pincode;

        foreach ($findallcartlist as $value) {

            $totalPrice = 0;
            $finalPrice = 0;
            $discountAmt = 0;
            $refer_discount = 0;
            $quantity = $value->quantity;
            $findp = Product::find($value->product_id);
            if($findp->is_variant == 2) {
                
                if(!empty($value->color) && !empty($value->size)) {
                    $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size, "color"=>$value->color])->first();
                } else if(!empty($value->color) && empty($value->size)) {
                    $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "color"=>$value->color])->first();
                } else if(empty($value->color) && !empty($value->size)) {
                    $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size])->first();
                }

                $pQty = $proPriceDetail->quantity;
                $updatepquantity = (int)$pQty - (int)$value->quantity;
                DB::table('product_prices')->where(['id' => $proPriceDetail->id])->update(['quantity' => $updatepquantity]);

                $get_price_sp = $proPriceDetail->sp; 
                $get_price_mrp = $proPriceDetail->mrp;
            } else {
                $get_price_sp = $findp->sp; 
                $get_price_mrp = $findp->mrp;

                $pQty = $findp->quantity;
                $updatepquantity = (int)$pQty - (int)$value->quantity;
                DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);
            }

            if($findp) {

                $sellerGet = Seller::where(['id' => $findp->user_id])->first();

                $totalPrice = $value->quantity * $get_price_sp;

                if($findp->hsn_percent > 0) {
                    $calulate_valuable_tax = $totalPrice / 100 * $findp->hsn_percent;
                    $calulate_valuable_tax = $totalPrice - $calulate_valuable_tax;
                } else {
                    $calulate_valuable_tax = $totalPrice;
                }

                $adminCommmisionPrice = $totalPrice / 100 * $sellerGet->commission;
                $getShippingPrice = $findp->national;  

                $priceTCS = $totalPrice / 100 * 1;
                $calculateTax = $adminCommmisionPrice + $getShippingPrice;
                $calculateTax = $calculateTax / 100 * 18;
                
                $sellerEarningAmount = $totalPrice - $adminCommmisionPrice - $getShippingPrice - $priceTCS - $calculateTax;
                $adminCommmisionPrice =  $adminCommmisionPrice + $getShippingPrice + $priceTCS + $calculateTax;

                if(strlen($value->coupon_code) > 0) {
                    $getDiscount = $value->discount;
                    $finalPrice = $totalPrice - $getDiscount;
                    $discountAmt = $getDiscount;
                } else {
                    $finalPrice = $totalPrice;
                    $discountAmt = 0;
                }

                if($discountAmt > 0 ) {
                    $detailsofcupon = Cuponcode::where('name', $value->coupon_code)->first();
                    $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
                    if($checkoffer) {

                        $breakDiscount = $discountAmt / 2;
                        $adminCommmisionPrice = $adminCommmisionPrice - $breakDiscount;
                        $sellerEarningAmount = $sellerEarningAmount - $breakDiscount;

                    } else {
                        $adminCommmisionPrice = $adminCommmisionPrice - $discountAmt;
                        $sellerEarningAmount = $sellerEarningAmount + $discountAmt;
                    }
                }

                $finalPrice = $finalPrice + $getShippingPrice;

                $verifyRefer = Refer::where(["referto"=>Auth::user()->id, "status"=>1])->orderBy("id","ASC")->first();
                if($verifyRefer) {
                    $getRefer = ReferEarn::where(["id"=>1])->first();
                    $disAmt = $getRefer->price;
                    $refer_discount =  $disAmt;
                    $finalPrice = $finalPrice - $disAmt;
                    $discountAmt = $discountAmt + $disAmt;
                    Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                    $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                } else {
                    $verifyRefer = Refer::where(["referby"=>Auth::user()->id, "status"=>1])->orderBy("id","ASC")->first();
                    if($verifyRefer) {
                        $getRefer = ReferEarn::where(["id"=>1])->first();
                        $disAmt = $getRefer->price;
                        $refer_discount = $disAmt;
                        $finalPrice = $finalPrice - $disAmt;
                        $discountAmt = $discountAmt + $disAmt;
                        Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                        $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                    }
                }

                if($value->giftwrap == 1) {
                    $finalPrice = $finalPrice + 25;
                    $gift_wrapup_status = 1;
                    $gift_wrapup_price = 25;
                    $sellerEarningAmount = $sellerEarningAmount + 25;
                } else {
                    $gift_wrapup_status = 0;
                    $gift_wrapup_price = 0;
                }
                
                $payStatus = 'pending';
                $shippingDate = $findp->ships_in;
                $shipDate = "+$shippingDate days";

                $http = new Client;
                $generateWBN = $http->get(url('https://staging-express.delhivery.com/waybill/api/bulk/json/?cl='.$userGet->name.'&token=dcafa28eb05642c39f31b14724e28cf45f91256a&count=1'));
                $data_wbn = json_decode($generateWBN->getBody());
                $orderNum = 'YOD'. $this->random_strings(5);

                $get_currentdate_time = strtotime(date('H:i:sa'));
                $allocateTime = "11:59:59am";
                $compare_time = strtotime($allocateTime);
                if($get_currentdate_time <= $compare_time){
                    $slatime = 1;
                } else {
                    $slatime = 0;
                }

                $orderData = array("order_number"=> $orderNum,
                                    "product_id" => $findp->id,
                                    "seller_id" => $findp->user_id,
                                    "user_id" => Auth::user()->id,
                                    "location" => $userAdd,
                                    "state" => $add->state,
                                    "city" => $add->city,
                                    "country" => $add->country,
                                    "pincode" => $add->pincode,
                                    "phone" => $add->phone,
                                    "name" => $add->name,
                                    "address_type" => $add->type,
                                    "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                    "payment_type" => $req['payment_type'],
                                    "transcation_id" => '',
                                    "coupen_code" => $couponCode,
                                    "gift_wrap_status" => $gift_wrapup_status,
                                    "sla_time"=>$slatime,
                                    "payment_status" => $payStatus,
                                    "status" => 'new order',
                                );
                $oid = Order::create($orderData);

                $orderProducts = array("order_id" => $oid->id,
                                        "size" => $value->size,
                                        "color" => $value->color,
                                        "quantity" => $value->quantity,
                                        "price" => $get_price_mrp,
                                        "sale_price" => $get_price_sp,
                                        "discount_price" => $discountAmt,
                                        "total_price" => $totalPrice - $discountAmt,
                                        "refer_discount"=> $refer_discount,
                                        "giftwrap_amount"=> $gift_wrapup_price,
                                        "seller_shipping"=> $getShippingPrice,
                                        "valuable_tax" => $calulate_valuable_tax,
                                        "gst_tax" => $calculateTax,
                                        "tcs_tax" => $priceTCS,
                                        "final_price" => $finalPrice,
                                        "seller_amount" => $sellerEarningAmount,
                                        "admin_commision" => $adminCommmisionPrice,
                                        "total_shipping"=> 0,
                                        "wbn_num" => $data_wbn
                                    );
                
                OrderDetail::create($orderProducts);

                Cart::where(['id'=>$value->id])->delete();

                $pQty = $findp->quantity;
                $updatepquantity = (int)$pQty - (int)$value->quantity;
                DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);

                $notifyData = [
                    'user_id'=>Auth::user()->id,
                    'order_id'=>$oid->id,
                    'order_number'=>$orderNum,
                    'type'=>'Order',
                    'title'=>'New Order - '.$orderNum,
                    'content'=>'Your new order has been placed'
                ];
                Notification::create($notifyData);
                // $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>Auth::user()->id])->first();
                // $badge = Notification::where(["user_id"=>Auth::user()->id,'status'=>0])->count();
                // $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
            }
        }

        return true;
    }

    public function response(Request $request) {
        $response = Indipay::response($request);
        $res = $this->online($response);

        if($res == 'true') {
            Session::flash('message_add', "Order placed successfully."); 
            return redirect()->route('orders');
        } else {
            $request->session()->put('failed', 'Payment failed');
            return redirect()->route('place-order');
        }
    }

    public function online($response) {
        $req = $request->all();
        $currencyValue = Session::get('currency_value');
        if(empty($currencyValue)) {
            $currencyValue = 1;
        }
        if($response['order_status'] == "Success") {

            $loc = request()->session()->get('location');
            $couponCode = request()->session()->get('coupan_code');
            $giftwrap = request()->session()->get('giftwrap');

            $sessoreder = Session::get('sessorder');
            $findallcartlist = Cart::where(["user_id" => Auth::user()->id])->get();
            $cartAmount = 0;
            
            $userGet = User::where(['id' => Auth::user()->id])->first();
            $add = Address::where(["user_id"=>Auth::user()->id, 'id'=>$loc])->first();
            $userAdd = $add->address .','. $add->town .','. $add->city .','. $add->state.','. $add->pincode;

            $orderNum = $response['order_id'];  // payment id
            
            foreach ($findallcartlist as $value) {

                $totalPrice = 0;
                $finalPrice = 0;
                $discountAmt = 0;
                $refer_discount = 0;

                $quantity = $value->quantity;
                $findp = Product::find($value->product_id);
                if($findp->is_variant == 2) {
                    if(!empty($value->color) && !empty($value->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size, "color"=>$value->color])->first();
                    } else if(!empty($value->color) && empty($value->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "color"=>$value->color])->first();
                    } else if(empty($value->color) && !empty($value->size)) {
                        $proPriceDetail = ProductPrice::where(["product_id"=>$value->product_id, "size"=>$value->size])->first();
                    }

                    $pQty = $proPriceDetail->quantity;
                    $updatepquantity = (int)$pQty - (int)$value->quantity;
                    DB::table('product_prices')->where(['id' => $proPriceDetail->id])->update(['quantity' => $updatepquantity]);

                    $get_price_sp = $proPriceDetail->sp; 
                    $get_price_mrp = $proPriceDetail->mrp;
                } else {
                    $get_price_sp = $findp->sp; 
                    $get_price_mrp = $findp->mrp;

                    $pQty = $findp->quantity;
                    $updatepquantity = (int)$pQty - (int)$value->quantity;
                    DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);
                }

                if($findp) {

                    $sellerGet = Seller::where(['id' => $findp->user_id])->first();

                    $totalPrice = $value->quantity * $get_price_sp;

                    if($findp->hsn_percent > 0) {
                        $calulate_valuable_tax = $totalPrice / 100 * $findp->hsn_percent;
                        $calulate_valuable_tax = $totalPrice - $calulate_valuable_tax;
                    } else {
                        $calulate_valuable_tax = $totalPrice;
                    }

                    $adminCommmisionPrice = $totalPrice / 100 * $sellerGet->commission;
                    $getShippingPrice = 0;
                    $priceTCS = $totalPrice / 100 * 1;
                    $calculateTax = $adminCommmisionPrice + $getShippingPrice;
                    $calculateTax = $calculateTax / 100 * 18;

                    $sellerEarningAmount = $totalPrice - $adminCommmisionPrice - $getShippingPrice - $priceTCS - $calculateTax;
                    $adminCommmisionPrice =  $adminCommmisionPrice + $getShippingPrice + $priceTCS + $calculateTax;
                    
                    if(strlen($value->coupon_code) > 0) {
                        $getDiscount = $value->discount;
                        $finalPrice = $totalPrice - $getDiscount;
                        $discountAmt = $getDiscount;
                    } else {
                        $finalPrice = $totalPrice;
                        $discountAmt = 0;
                    }

                    if($discountAmt > 0 ) {
                        $detailsofcupon = Cuponcode::where('name', $value->coupon_code)->first();
                        $checkoffer = Offer::where(['coupon_id'=> $detailsofcupon->id])->first();
                        if($checkoffer) {

                            $breakDiscount = $discountAmt / 2;
                            $adminCommmisionPrice = $adminCommmisionPrice - $breakDiscount;
                            $sellerEarningAmount = $sellerEarningAmount - $breakDiscount;

                        } else {
                            $adminCommmisionPrice = $adminCommmisionPrice - $discountAmt;
                            $sellerEarningAmount = $sellerEarningAmount + $discountAmt;
                        }
                    }

                    $verifyRefer = Refer::where(["referto"=>Auth::user()->id, "status"=>1])->orderBy("id","ASC")->first();
                    if($verifyRefer) {
                        $getRefer = ReferEarn::where(["id"=>1])->first();
                        $disAmt = $getRefer->price;
                        $refer_discount =  $disAmt;
                        $finalPrice = $finalPrice - $disAmt;
                        $discountAmt = $discountAmt + $disAmt;
                        Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                        $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                    } else {
                        $verifyRefer = Refer::where(["referby"=>Auth::user()->id, "status"=>1])->orderBy("id","ASC")->first();
                        if($verifyRefer) {
                            $getRefer = ReferEarn::where(["id"=>1])->first();
                            $disAmt = $getRefer->price;
                            $refer_discount = $disAmt;
                            $finalPrice = $finalPrice - $disAmt;
                            $discountAmt = $discountAmt + $disAmt;
                            Refer::where(["id"=>$verifyRefer->id])->update(["status"=>0]);
                            $adminCommmisionPrice = $adminCommmisionPrice - $disAmt;
                        }
                    }

                    if($value->giftwrap == 1) {
                        $finalPrice = $finalPrice + 25;
                        $gift_wrapup_status = 1;
                        $gift_wrapup_price = 25;
                        $sellerEarningAmount = $sellerEarningAmount + 25;
                    } else {
                        $gift_wrapup_status = 0;
                        $gift_wrapup_price = 0;
                    }
                    
                    $payStatus = 'success';
                    $shippingDate = $findp->ships_in;
                    $shipDate = "+$shippingDate days";

                    $http = new Client;
                    $generateWBN = $http->get(url('https://staging-express.delhivery.com/waybill/api/bulk/json/?cl='.$userGet->name.'&token=dcafa28eb05642c39f31b14724e28cf45f91256a&count=1'));
                    $data_wbn = json_decode($generateWBN->getBody());
                    $orderNum = 'YOD'. $this->random_strings(5);

                    $get_currentdate_time = strtotime(date('H:i:sa'));
                    $allocateTime = "11:59:59am";
                    $compare_time = strtotime($allocateTime);
                    if($get_currentdate_time <= $compare_time){
                        $slatime = 1;
                    } else {
                        $slatime = 0;
                    }

                    $orderData = array("order_number"=> $orderNum,
                                        "product_id" => $findp->id,
                                        "seller_id" => $findp->user_id,
                                        "user_id" => Auth::user()->id,
                                        "location" => $userAdd,
                                        "state" => $add->state,
                                        "city" => $add->city,
                                        "country" => $add->country,
                                        "pincode" => $add->pincode,
                                        "phone" => $add->phone,
                                        "name" => $add->name,
                                        "address_type" => $add->type,
                                        "dispatch_at" => date('Y-m-d', strtotime($shipDate)),
                                        "payment_type" => "Online",
                                        "transcation_id" => $response['tracking_id'],
                                        "coupen_code" => $couponCode,
                                        "gift_wrap_status" => $gift_wrapup_status,
                                        "sla_time"=>$slatime,
                                        "payment_status" => $payStatus,
                                        "status" => 'new order',
                                    );
                    $oid = Order::create($orderData);

                    $orderProducts = array("order_id" => $oid->id,
                                            "size" => $value->size,
                                            "color" => $value->color,
                                            "quantity" => $value->quantity,
                                            "price" => $get_price_mrp,
                                            "sale_price" => $get_price_sp,
                                            "discount_price" => $discountAmt,
                                            "total_price" => $totalPrice - $discountAmt,
                                            "refer_discount"=> $refer_discount,
                                            "giftwrap_amount"=> $gift_wrapup_price,
                                            "seller_shipping"=> $getShippingPrice,
                                            "valuable_tax" => $calulate_valuable_tax,
                                            "gst_tax" => $calculateTax,
                                            "tcs_tax" => $priceTCS,
                                            "final_price" => $finalPrice,
                                            "seller_amount" => $sellerEarningAmount,
                                            "admin_commision" => $adminCommmisionPrice,
                                            "total_shipping"=> 0,
                                            "wbn_num" => $data_wbn

                                        );
                    OrderDetail::create($orderProducts);
                    
                    Cart::where(['id'=>$value->id])->delete();

                    $pQty = $findp->quantity;
                    $updatepquantity = (int)$pQty - (int)$value->quantity;
                    DB::table('products')->where(['id' => $value->product_id])->update(['quantity' => $updatepquantity]);

                    $notifyData = [
                        'user_id'=>Auth::user()->id,
                        'order_id'=>$oid->id,
                        'order_number'=>$orderNum,
                        'type'=>'Order',
                        'title'=>'New Order - '.$orderNum,
                        'content'=>'Your new order has been placed'
                    ];
                    Notification::create($notifyData);
                    // $userDevice = Token::select('deviceType','deviceToken')->where(['user_id'=>Auth::user()->id])->first();
                    // $badge = Notification::where(["user_id"=>Auth::user()->id,'status'=>0])->count();
                    // $notifyResponse = $this->pushNotification($notifyData, $userDevice, $badge);
                }
            }

            request()->session()->forget('location');
            request()->session()->forget('coupan_code');
            return true;
        } else {
            return false;
        }
    }

    function random_strings($length_of_string) {
        $str_result = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return substr(str_shuffle($str_result), 0, $length_of_string);
    }


    public function orderinvoice($oid, $download) {

        //$items = DB::table("items")->get();
        $items = [];

        view()->share('items',$items);

        if($download == "pdf") {

            $pdf = PDF::loadView('pdfview');
            return $pdf->download('pdfview.pdf');
        }

        return view('pdfview');

    }

    public function ordermanifest($oid, $download) {

        //$items = DB::table("items")->get();
        $items = [];

        view()->share('items',$items);
        
        $pdf = PDF::loadView('manifest');
        
        if($download == "pdf") {
            return $pdf->download('manifest.pdf');
        }
    
        //return view('manifest');
        return $pdf->stream('manifest.pdf');

    }

}
