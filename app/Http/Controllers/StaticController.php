<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use DB;
use App\User;
use App\Token;
use App\Content;
use App\Faq;
use App\Priority;
use Image;
use Auth;
use Session;
use Validator;
use Intervention\Image\ImageServiceProvider;
use GuzzleHttp\Client;

class StaticController extends Controller {

  private $baseurlslash;
  private $baseurl;

  public function __construct() {
      $this->baseurlslash = "http://mobuloustech.com/yodapi/public/";
      $this->baseurl = "http://mobuloustech.com/yodapi/public";
  }

  public function aboutus(Request $request) {
      $detail = Content::where(['type'=>'about'])->first();
      return view('About',compact('detail'));
  }
   
  public function aboutus1(Request $request) {
      $detail = Content::where(['type'=>'about'])->first();
      return view('About1',compact('detail'));
  }

  public function tandc(Request $request) {
      $detail = Content::where(['type'=>'tandc'])->first();
      return view('tandc',compact('detail'));
  }
   
  public function tandc1(Request $request) {
      $detail = Content::where(['type'=>'tandc'])->first();
      return view('tandc1',compact('detail'));
  }

  public function faq(Request $request) {
      $detail = faq::get();
      return view('faq',compact('detail'));
  }
   
  public function faq1(Request $request) {
      $detail = faq::get();
      return view('faq1',compact('detail'));
  }

  public function privacypolicy(Request $request) {
      $detail = Content::where(['type'=>'privacypolicy'])->first();
      return view('privacypolicy',compact('detail'));
  }
   
  public function privacypolicy1(Request $request) {
      $detail = Content::where(['type'=>'privacypolicy'])->first();
      return view('privacypolicy1',compact('detail'));
  }

  public function changerate(Request $request) {
      // Session::forget('currency_code');
      // Session::forget('currency_value');
      $currencrCode = $request->currency;
      //$currencrCode = "USD";
      Session::put('currency_code',$currencrCode, 10);
      Session::save();
      $urlCreate = "https://api.exchangeratesapi.io/latest?base=INR&symbols=".$currencrCode;
      $http = new Client();
      $generateWBN = $http->get(url($urlCreate));
      $dataResult = json_decode($generateWBN->getBody());
      $getRates = $dataResult->rates;

      foreach ($getRates as $key => $value) {
          $amt = round($value, 4);
          Session::put('currency_value', $amt, 10);
          Session::save();
      }
      
      echo $amt;
      die;
  }


}
