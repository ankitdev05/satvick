<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    //

    protected $fillable = [
         'imagemain', 'himimage', 'herimage','title','description'
    ];
}
