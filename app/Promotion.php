<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model {
    //

    protected $fillable = [
         'seller_id', 'offer_id'
    ];
}
