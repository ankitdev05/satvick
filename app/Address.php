<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    //

    protected $fillable = [
        'user_id', 'name', 'phone', 'pincode', 'address', 'town', 'city', 'state', 'remark','type','country','latitude','longitude'
    ];
}
