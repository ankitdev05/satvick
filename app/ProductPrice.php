<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPrice extends Model
{
    
    protected $fillable = [ 'product_id', 'sp', 'commision', 'tax', 'quantity', 'mrp', 'discount', 'size', 'color'];
}
