<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsubcategory extends Model
{
    
    protected $fillable = [ 'subcategory_id','category_id','image','name','occasion_status','status','deleteStatus'];
}
