<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeGroup extends Model
{
    //

    protected $fillable = [
        'seller_id', 'name', 'status'
    ];
}
