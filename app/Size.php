<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Size extends Model
{
    //

    protected $fillable = [
         'category_id', 'subcategory_id', 'subsubcategory_id', 'size'
    ];
}
