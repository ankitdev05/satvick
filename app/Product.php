<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    
    protected $fillable = [ 'user_id','sku', 'name', 'sp', 'commision','tax','quantity', 'mrp', 'status','category_id','brand','hsn_code','description','fit','weight','height','length','width','ships_in','images','subcategory_id','subsubcategory_id','color','defaultcolor','size_label','size','sizechart','theme_id','delivery_time','discount','user_type','deleteStatus','hsn_percent','local','zonal','national','payment_mode','is_variant','variant_type'];
}
