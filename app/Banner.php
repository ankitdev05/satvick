<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model {
    //

    protected $fillable = [
         'image', 'type', 'category_id','subcategory_id','subsubcategory_id'
    ];
}
