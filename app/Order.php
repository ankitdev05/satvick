<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    protected $fillable = ['order_number','product_id','seller_id','user_id', 'location','state', 'name','phone','city','country','pincode','address_type','dispatch_at','coupen_code','gift_wrap_status','sla_time','payment_type', 'transcation_id', 'payment_status', 'status'];
}
