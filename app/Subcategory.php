<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    
    protected $fillable = [ 'category_id','name','occasion_status','image','deleteStatus'];
}
