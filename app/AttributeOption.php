<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttributeOption extends Model
{
    //

    protected $fillable = [
        'group_id', 'seller_id', 'name', 'status'
    ];
}
