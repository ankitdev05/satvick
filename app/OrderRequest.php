<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderRequest extends Model
{
    
    protected $fillable = [ 'order_id','product_id','user_id','seller_id','request_type','reason','description','size', 'color', 'refund_by', 'product_default','status','price'];
}
