<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerApprove extends Model
{
    //

    protected $fillable = [
        'seller_id', 'category','subcategory','subsubcategory','status'
    ];
}
