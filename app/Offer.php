<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model {
    //

    protected $fillable = [
         'offer_type', 'description', 'catid', 'coupon_id', 'startdate','enddate'
    ];
}
