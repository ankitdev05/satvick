<?php
use Session as Session;
	
	function getCount() {

		if(Auth::check()) {
	    	return $cartCount = \App\Cart::where(['user_id'=>Auth::user()->id])->count();
	    } else {
	    	if (Request()->session()->has('cartsession')) {
                $session_id = session::get('cartsession');
            } else {
                $session_id = Session::getId();
            }
	    	return $cartCount = \App\Cart::where(['user_id'=>$session_id])->count();
	    }
	}

	function getFooterContent() {
	    $getContent = \App\Content::where(['id'=>13])->first();
	    return $footerContent = $getContent->value;
	}

?>