<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','API\HomeController@index');

Route::get('home/','API\HomeController@index')->name('home');
Route::get('shop/{id}','InnerPageController@innerPage')->name('shop');

Route::post('change-rate','StaticController@changerate')->name('change-rate');

Route::get('products/{catid}/{subcatid}/{scid}/{color?}/{brand?}','ProductController@innerproductlist')->name('products');
Route::get('product/brand/{brand}','ProductController@brandproductlist')->name('product/brand');
Route::get('product/search/{catid}/{subcatid}/{brand}','ProductController@searchproductlist')->name('product/search');

Route::post('filters/{catid}/{subcatid}/{scid}','ProductController@filterproductlist')->name('filters');

// Static Pages
Route::get('about-us','StaticController@aboutus')->name('about-us');
Route::get('about-us1','StaticController@aboutus1')->name('about-us1');
Route::get('terms-condition','StaticController@tandc')->name('terms-condition');
Route::get('tandc','StaticController@tandc1')->name('tandc');
Route::get('faq','StaticController@faq')->name('faq');
Route::get('faq1','StaticController@faq1')->name('faq1');
Route::get('privacy-policy','StaticController@privacypolicy')->name('privacy-policy');
Route::get('privacy-policy1','StaticController@privacypolicy1')->name('privacy-policy1');

Route::any('login','LoginController@userlogin')->name('login');
Route::any('redirect','LoginController@redirectsocial')->name('redirect');
Route::any('callback','LoginController@callback')->name('callback');
Route::any('signup','LoginController@usersignup')->name('signup');
Route::get ('/redirect/{service}', 'LoginController@redirectsocial');
Route::get ('/callback/{service}', 'LoginController@callback');
route::any('logout','LoginController@logout')->name('logout');
Route::any('forgot','LoginController@forgot')->name('forgot');
Route::any('forgot-password','LoginController@forgotpassweb')->name('forgot-password');
Route::any('reset-password','LoginController@reset')->name('reset-password');
Route::any('reset','LoginController@resetpass')->name('reset');

Route::any('profile','LoginController@profile')->name('profile');
Route::any('profile-update','LoginController@profileupdate')->name('profile-update');
Route::any('address','LoginController@address')->name('address');
Route::any('change-password','LoginController@changepassword')->name('change-password');
Route::any('update-password','LoginController@passwordupdate')->name('update-password');
Route::any('add-address','LoginController@add_address')->name('add-address');
Route::any('update-address','LoginController@update_address')->name('update-address');
Route::get('removeaddress/{id}','LoginController@removeaddress')->name('removeaddress');
Route::any('refer','LoginController@refer')->name('refer');
Route::any('coupons','LoginController@coupon')->name('coupons');
Route::any('notifications','LoginController@notification')->name('notifications');
Route::any('wishlist','LoginController@wishlistproduct')->name('wishlist');
Route::get('wishremove/{cid}','LoginController@removefromwish')->name('wishremove');
Route::any('orders','LoginController@myorder')->name('orders');

Route::get('autocomplete', 'API\HomeController@autocomplete')->name('autocomplete');

Route::get('mybag','CartController@cartlist')->name('mybag');
Route::post('addtocart','CartController@addtocart')->name('addtocart');
Route::get('itemremove/{cid}','CartController@removefromcart')->name('itemremove');
Route::post('updatecart','CartController@cartupdate')->name('updatecart');
Route::get('add-wishlist/{id}','CartController@addtowishlist')->name('add-wishlist');

Route::post('apply_cupon','OrderController@apply_cupon')->name('apply_cupon');
Route::post('change_giftwrap','OrderController@change_giftwrap')->name('change_giftwrap');

Route::get('place-order','OrderController@placeorder')->name('place-order');
Route::get('order-invoice/{oid}/{download}','OrderController@orderinvoice')->name('order-invoice');
Route::get('order-manifiest/{oid}/{download}','OrderController@ordermanifest')->name('order-manifiest');

Route::post('paying','OrderController@payingorder')->name('paying');
Route::any('/indipay/response','OrderController@response');

Route::any('test','API\HomeController@test');


Route::any('product/description/{id}','ProductController@productdetails')->name('product/description/');
Route::post('getColorImage','ProductController@getColorImage')->name('getColorImage');
Route::post('getVariantPrice','ProductController@getVariantPrice')->name('getVariantPrice');