<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::post('allrequestlist1','API\ServiceController@allrequestlist1');

Route::post('filter_section','API\ServiceController@filter_section');

Route::post("sellersignup","API\ServiceController@test");


Route::post('changepassword','API\ServiceController@changepassword');

Route::post('addsizechart','API\ServiceController@addsizechart');
Route::post('updateSizeChart','API\ServiceController@updateSizeChart');
Route::post('showSizeChart','API\ServiceController@showSizeChart');
Route::post('getImageByColor','API\ServiceController@getImageByColor');
Route::get('deleteChart/{id}','API\ServiceController@deleteChart');

Route::post('notifystatus','API\ServiceController@notifystatus');

Route::post('viewprofile','API\ServiceController@viewprofile');

Route::post('set_as_default','API\ServiceController@set_as_default');

Route::post('addaddress','API\ServiceController@addaddress');

Route::post('editaddress','API\ServiceController@editaddress');

Route::post('removeaddress','API\ServiceController@removeaddress');

Route::get('viewaddress/{id}','API\ServiceController@viewaddress');

Route::get('productstatuschange1/{id}','API\ServiceController@productstatuschange1');

Route::get('userlistforadmin','API\ServiceController@userlistforadmin');

Route::get('userviewforadmin/{id}','API\ServiceController@viewprofileuseradmin');

Route::get('sellerlistforadmin','API\ServiceController@sellerlistforadmin');
Route::get('sellerviewforadmin/{id}','API\ServiceController@viewprofileselleradmin');

Route::get('productdelete/{id}','API\ServiceController@productdelete');

Route::get('catdeleteb/{id}','API\ServiceController@catdeleteb');

Route::get('catdelete11/{id}','API\ServiceController@catdelete11');

Route::get('catdelete113/{id}','API\ServiceController@catdelete113');

Route::get('catdelete11pp/{id}','API\ServiceController@catdelete11pp');

Route::get('getproductdetails/{id}','API\ServiceController@getproductdetails');

Route::post('csv_uploade_for_product/{id}','API\ServiceController@csv_uploade_for_product');

Route::post('editprofile','API\ServiceController@editprofile');

Route::post('updatesellerprofile/{id}','API\ServiceController@updatesellerprofile');

Route::post('changepasswordforseller/{id}','API\ServiceController@changepasswordforseller');

Route::post('orderlisting/{id}','API\ServiceController@orderlisting');

Route::get('order-invoice/{oid}','API\ServiceController@orderinvoice');
Route::get('order-manifiest/{oid}','API\ServiceController@ordermanifest');
Route::get('order-manifiest1/{oid}','API\ServiceController@ordermanifest1');

Route::get('orderlistingseller/{id}','API\ServiceController@orderlistingseller');

Route::get('buyerorderlisting/{id}','API\ServiceController@buyerorderlisting');

Route::get('orderdetails/{id}','API\ServiceController@orderdetails');

Route::get('Categorylist','API\ServiceController@Categorylist');

Route::any("adminlogin","API\ServiceController@adminlogin");

Route::post('updateadminprofile/{id}','API\ServiceController@updateadminprofile');

Route::post('changepasswordforadmin/{id}','API\ServiceController@changepasswordforadmin');

Route::get('bannerdetailsbyid/{id}','API\ServiceController@bannerdetailsbyid');

Route::get('catlistforadmin1234','API\ServiceController@catlistforadmin1234');

Route::get('brandapprovalstatuschange/{id}','API\ServiceController@brandapprovalstatuschange');

Route::get('brandfocusstatuschange/{id}','API\ServiceController@brandfocusstatuschange');

Route::get('productids','API\ServiceController@productids');

Route::post('addptoflash','API\ServiceController@addptoflash');

Route::get('flashproductlist','API\ServiceController@flashproductlist');

Route::post('updateflashtime','API\ServiceController@updateflashtime');

Route::post('homeapi','API\ServiceController@homeapi');

Route::get('sublistbycat/{id}','API\ServiceController@sublistbycat');

Route::get('subsubcatbyid/{id}','API\ServiceController@subsubcatbyid');

Route::get('sublistbycatremark/{id}','API\ServiceController@sublistbycatremark');

Route::get('brandlistforseller','API\ServiceController@brandlistforseller');

Route::get('themelistforseller','API\ServiceController@themelistforseller');

Route::post('productlist','API\ServiceController@productlist');

Route::post('productdetails','API\ServiceController@productdetails');

Route::post('addtowish','API\ServiceController@addtowish');

Route::post('wishlistproduct','API\ServiceController@wishlistproduct');

Route::post('addtocart','API\ServiceController@addtocart');

Route::post('removetocart','API\ServiceController@removetocart');

Route::post('updatecart','API\ServiceController@updatecart');

Route::post('checkphoneseller','API\ServiceController@checkphoneseller');

Route::post('checkemailseller','API\ServiceController@checkemailseller');

Route::post('changepasswordbyphone','API\ServiceController@changepasswordbyphone');

Route::post('checkphonedata','API\ServiceController@checkphonedata');

Route::post('searchlist','API\ServiceController@searchlist');

Route::post('createcode','API\ServiceController@createcode');

Route::get('getcontent/{type}','API\ServiceController@getcontent');

Route::get('faqlist','API\ServiceController@faqlist');

Route::post('addfaq','API\ServiceController@addfaq');

Route::get('deletefaq/{id}','API\ServiceController@deletefaq');

Route::post('content','API\ServiceController@content');

Route::post('setdefault','API\ServiceController@setdefault');

Route::get('themeorder','API\ServiceController@themeorder');

Route::post('setthemeorder','API\ServiceController@setthemeorder');

Route::get('orderrequest/{id}','API\ServiceController@orderrequest');

Route::any('indipay/response','API\ServiceController@response');


// Admin
Route::post('addSubAdmin','API\AdminController@addSubAdmin');
Route::get('subAdminList','API\AdminController@subAdminList');
Route::post('subAdminEdit','API\AdminController@subAdminEdit');
Route::post('subAdminUpdate','API\AdminController@subAdminUpdate');

Route::any('dashoardforadmin','API\ServiceController@dashoardforadmin');
Route::post('addsize','API\ServiceController@addsize');
Route::post('getsize','API\ServiceController@getsize');
Route::get('sizelist','API\ServiceController@sizelist');
Route::get('sizedelete/{id}','API\ServiceController@sizedelete');

Route::post('addcolor','API\ServiceController@addcolor');
Route::get('getcolor','API\ServiceController@getcolor');
Route::get('colorlist','API\ServiceController@colorlist');
Route::get('colordelete/{id}','API\ServiceController@colordelete');

Route::post('addsubcatg','API\ServiceController@addsubcatg');
Route::get('getsubcategorylist/{id}','API\ServiceController@getsubcategorylist');
Route::post('subcategorydetailsupdate/{id}','API\ServiceController@subcategorydetailsupdate');

Route::post('addsubsubcat','API\ServiceController@addsubsubcat');
Route::get('subsubcatlist','API\ServiceController@subsubcatlist');
Route::get('subsubcatsingle/{id}','API\ServiceController@subsubcatsingle');
Route::post('subsubcategoryupdate','API\ServiceController@subsubcategoryupdate');
Route::get('subsubcatdelete/{id}','API\ServiceController@subsubcatdelete');

Route::post('addoffer','API\ServiceController@addoffers');
Route::get('offerlist','API\ServiceController@offerlists');
Route::get('offerdelete/{id}','API\ServiceController@offerdelete');

Route::post('updaterefer','API\ServiceController@updaterefer');
Route::get('getrefer','API\ServiceController@getrefer');

Route::get('sellersupportticketlist/{id}','API\ServiceController@sellersupportticketlist');
Route::get('adminsupportticketlist','API\ServiceController@adminsupportticketlist');
Route::post('supportticketadd/{id}','API\ServiceController@supportticketadd'); //4093
Route::post('supportticketupdate','API\ServiceController@supportticketupdate');

Route::post('fileuploade','API\ServiceController@fileuploade');
Route::post('fileuploadecat','API\ServiceController@fileuploadecat');
Route::post('fileuploade1','API\ServiceController@fileuploade1');
Route::post('fileuploadebanner','API\ServiceController@fileuploadebanner');
Route::post('fileuploadebrand','API\ServiceController@fileuploadebrand');

Route::post('createBrand','API\ServiceController@createBrand');
Route::get('brandlistforadmin','API\ServiceController@brandlistforadmin');
Route::get('brandviewforadmin/{id}','API\ServiceController@brandviewforadmin');
Route::post('brandeditforadmin/{id}','API\ServiceController@brandeditforadmin');
Route::post('catdeletebrand/{id}','API\ServiceController@catdeletebrand');

Route::get('productlistforadmin/{id}','API\ServiceController@productlistforadmin');
Route::post('addproductbyseller/{id}','API\ServiceController@addproductbyseller');
Route::get('productdelete/{id}','API\ServiceController@productdelete');
Route::post('editproductbyseller/{id}','API\ServiceController@editproductbyseller');
Route::post('editproductbyadmin/{id}','API\ServiceController@editproductbyadmin');

Route::post('createtheme','API\ServiceController@createtheme');
Route::get('themelistforadmin','API\ServiceController@themelistforadmin');
Route::get('catdeletet/{id}','API\ServiceController@catdeletet');

Route::get('cuponlist','API\ServiceController@cuponlist');

Route::post('createbanner','API\ServiceController@createbanner');
Route::get('bannerlistforadmin','API\ServiceController@bannerlistforadmin');

Route::get('catdelete/{id}','API\ServiceController@catdelete');
Route::get('userdelete/{id}','API\ServiceController@userdelete');
Route::get('sellerdelete/{id}','API\ServiceController@sellerdelete');

Route::get('productstatuschange11/{id}','API\ServiceController@productstatuschange11'); //seller inactive
Route::get('productstatuschange/{id}','API\ServiceController@productstatuschange'); // product inactive
Route::get('productstatuschange2/{id}','API\ServiceController@productstatuschange2'); // subsubcategory inactive

Route::get('catlistforadmin','API\ServiceController@catlistforadmin');
Route::get('catlistforadmin1/{id}','API\ServiceController@catlistforadmin1');
Route::post('updatestock','API\ServiceController@updatestock');
Route::post('updateprice','API\ServiceController@updateprice');
Route::post('updatecommission','API\ServiceController@updatecommission');

Route::get('exchange-order/{id}/{status}',"API\ServiceController@exchangeorder");
Route::get('orderreturnadmin','API\ServiceController@orderreturnadmin');
Route::get('ordercanceladmin','API\ServiceController@ordercanceladmin');

Route::get('sellerloginbyadmin/{id}','API\ServiceController@sellerloginbyadmin');

Route::get('userlistfornotification','API\AdminController@userlistfornotification');
Route::post('sendnotification','API\AdminController@sendpushnotification');

Route::get('allsellerpayment','API\AdminController@allsellerpayment');

Route::post('productSearchForFlashSale','API\AdminController@productSearchForFlashSale');

Route::get('categoryapprovallist','API\AdminController@categoryApprovalList');

Route::post('changecategoryapproval','API\AdminController@changecategoryapproval');

Route::get('aboundedlist','API\AdminController@abounded');

Route::post('productdisablecod','API\AdminController@disablecod');

Route::get('shippingorderlisting','API\AdminController@shippingorderlisting');

Route::any('adminpromotionlist',"API\AdminController@promotionlist");
Route::any('adminpromotionproductlist/{id}',"API\AdminController@promotionproductlist");
Route::any('adminpromotionproductstatus/{id}',"API\AdminController@promotionproductstatus");

//Subcatlist for offer
Route::any('sublistforoffer/{id}',"API\AdminController@sublistforoffer");

//Seller

Route::any("sellersignup1","API\ServiceController@test1");
Route::any("sellerlogin","API\ServiceController@sellerlogin");
Route::any("sellerlogin1","API\ServiceController@sellerlogin1");

Route::any("forgotpasswordseller","API\ServiceController@forgotpasswordseller");
Route::any('dashoardforseller/{id}','API\ServiceController@dashoardforseller');
Route::get('productlistforseller/{id}','API\ServiceController@productlistforseller');

Route::any('promotionoffer',"API\SellerController@promotionoffer");
Route::any('promotion/{id}',"API\SellerController@promotion");
Route::any('promotionlist/{id}',"API\SellerController@promotionlist");
Route::any('promotiondelete/{id}',"API\SellerController@promotiondelete");

Route::any('payreport/{id}',"API\ServiceController@payreport");

Route::post('sellerordercsv/{id}','API\ServiceController@sellerordercsv');

Route::post('sellerorderlisting/{id}','API\ServiceController@sellerorderlisting');
Route::get('sellerorderlisting1/{id}','API\ServiceController@sellerorderlisting1'); // Aceepted
Route::get('sellerorderlisting2/{id}','API\ServiceController@sellerorderlisting2'); // dispatched - RTD
Route::get('sellerorderlisting3/{id}','API\ServiceController@sellerorderlisting3');  // transit
Route::get('sellerorderlisting4/{id}','API\ServiceController@sellerorderlisting4');  // Delivered
Route::get('sellerorderlisting5/{id}','API\ServiceController@sellerorderlisting5');  // Pending

Route::post('changethestatusoforder','API\ServiceController@changethestatusoforder');
Route::get('deleteorder/{id}','API\ServiceController@deleteorder');

Route::get('orderreturnseller/{id}','API\ServiceController@orderreturnseller');
Route::get('ordercancelseller/{id}','API\ServiceController@ordercancelseller');
Route::get('performanceseller/{id}','API\ServiceController@performanceseller');

Route::get('checkcategoryapproval/{id}','API\SellerController@checkcategoryapproval');
Route::post('categoryapproval','API\SellerController@categoryapproval');

Route::post('sellercategorylist','API\SellerController@sellerCategoryList');
Route::post('sellersubcategorylist','API\SellerController@sellerSubCategoryList');
Route::post('sellersubsubcategorylist','API\SellerController@sellerSubSubCategoryList');

Route::get('attribute-groups/{id}','API\SellerController@attributegroupList');
Route::post('attribute-group','API\SellerController@attributegroupadd');
Route::get('attribute-edit/{id}','API\SellerController@attributegroupedit');
Route::post('attribute-edit','API\SellerController@attributegroupupdate');

Route::get('attribute-options/{id}','API\SellerController@attributeoptionList');
Route::post('attribute-option','API\SellerController@attributeoptionadd');
Route::get('attribute-option-edit/{id}','API\SellerController@attributeoptionedit');
Route::post('attribute-option-edit','API\SellerController@attributeoptiondate');

Route::post('unique_sku','API\SellerController@uniquesku');


// app

Route::post('checkphone','API\ServiceController@checkphone');
Route::post('verifysignup','API\ServiceController@verifysignup');

Route::post('login','API\ServiceController@login');
Route::post('signup','API\ServiceController@signup');
Route::post('sociallogin','API\ServiceController@sociallogin');

Route::get('cupons','API\ServiceController@cupons');

Route::any('innerpageapi/{id}','API\ServiceController@innerpageapi');
Route::post('moreproductlist','API\ServiceController@moreproductlist');

Route::any('notficationlist','API\ServiceController@notficationlist');
Route::any('apply_cupon','API\ServiceController@apply_cupon');
Route::any('cancel_cupon','API\ServiceController@cancel_cupon');

Route::any('referal_list/{id}','API\ServiceController@referal_list');

Route::post('Cartlistproduct','API\ServiceController@Cartlistproduct');
Route::any('generateordercode',"API\ServiceController@generateOrderid");
Route::any('placeorder',"API\ServiceController@placeorder");

Route::any('myorderdetails',"API\ServiceController@myorderdetails");
Route::any('myorderhelp',"API\ServiceController@myorderhelp");

Route::any('request_for_order','API\ServiceController@request_for_order');

Route::any('cartoffer',"API\ServiceController@cartoffer");

Route::get('codavailable/{id}',"API\ServiceController@codavailable");


// CRON JOB

Route::get('checkthestatusoforder','API\CronController@checkthestatusoforder');
Route::get('checkthestatusofmanifestorder','API\CronController@checkthestatusofmanifestorder');
Route::get('performance','API\CronController@performance');
Route::get('payout','API\CronController@payout');
Route::get('orderpendingcheck','API\CronController@orderpendingcheck');
// Currency Rate
Route::get('exchangerate','API\CronController@exchangerate');